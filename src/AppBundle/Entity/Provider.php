<?php

namespace AppBundle\Entity;

/**
 * Provider
 */
class Provider
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $npi;

    /**
     * @var string
     */
    private $dea;

    /**
     * @var string
     */
    private $license;

    /**
     * @var string
     */
    private $lastname;

    /**
     * @var string
     */
    private $firstname;

    /**
     * @var string
     */
    private $middlename;

    /**
     * @var \AppBundle\Entity\ClinicianDegree
     */
    private $degree;

    /**
     * @var \AppBundle\Entity\ClinicianSpecialty
     */
    private $speciality;

    /**
     * @var integer
     */
    private $degree_id;

    /**
     * @var integer
     */
    private $speciality_id;


    /**
     * @var string
     */
    private $gender;

    /**
     * @var \DateTime
     */
    private $dateBirth;

    /**
     * @var integer
     */
    private $user_id;

    /**
     * @var \AppBundle\Entity\User
     */
    private $user;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $submission;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->submission = new \Doctrine\Common\Collections\ArrayCollection();
        $this->modifiedAt = new \DateTime('now');
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set npi
     *
     * @param string $npi
     *
     * @return Provider
     */
    public function setNpi($npi)
    {
        $this->npi = $npi;

        return $this;
    }

    /**
     * Get npi
     *
     * @return string
     */
    public function getNpi()
    {
        return $this->npi;
    }

    /**
     * Set dea
     *
     * @param string $dea
     *
     * @return Provider
     */
    public function setDea($dea)
    {
        $this->dea = $dea;

        return $this;
    }

    /**
     * Get dea
     *
     * @return string
     */
    public function getDea()
    {
        return $this->dea;
    }

    /**
     * Set license
     *
     * @param string $license
     *
     * @return Provider
     */
    public function setLicense($license)
    {
        $this->license = $license;

        return $this;
    }

    /**
     * Get license
     *
     * @return string
     */
    public function getLicense()
    {
        return $this->license;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return Provider
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return Provider
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set middlename
     *
     * @param string $middlename
     *
     * @return Provider
     */
    public function setMiddlename($middlename)
    {
        $this->middlename = $middlename;

        return $this;
    }

    /**
     * Get middlename
     *
     * @return string
     */
    public function getMiddlename()
    {
        return $this->middlename;
    }

    /**
     * Set degree
     *
     * @param string $degree
     *
     * @return Provider
     */
    public function setDegree($degree)
    {
        $this->degree = $degree;

        return $this;
    }

    /**
     * Get degree
     *
     * @return string
     */
    public function getDegree()
    {
        return $this->degree;
    }

    /**
     * Set speciality
     *
     * @param string $speciality
     *
     * @return Provider
     */
    public function setSpeciality($speciality)
    {
        $this->speciality = $speciality;

        return $this;
    }

    /**
     * Get speciality
     *
     * @return string
     */
    public function getSpeciality()
    {
        return $this->speciality;
    }

    /**
     * Set gender
     *
     * @param string $gender
     *
     * @return Provider
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set dateBirth
     *
     * @param \DateTime $dateBirth
     *
     * @return Provider
     */
    public function setDateBirth($dateBirth)
    {
        $this->dateBirth = $dateBirth;

        return $this;
    }

    /**
     * Get dateBirth
     *
     * @return \DateTime
     */
    public function getDateBirth()
    {
        return $this->dateBirth;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return Provider
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Provider
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add submission
     *
     * @param \AppBundle\Entity\Submission $submission
     *
     * @return Provider
     */
    public function addSubmission(\AppBundle\Entity\Submission $submission)
    {
        $this->submission[] = $submission;

        return $this;
    }

    /**
     * Remove submission
     *
     * @param \AppBundle\Entity\Submission $submission
     */
    public function removeSubmission(\AppBundle\Entity\Submission $submission)
    {
        $this->submission->removeElement($submission);
    }

    /**
     * Get submission
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubmission()
    {
        return $this->submission;
    }
    /**
     * @var boolean
     */
    private $active = true;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $modifiedAt;


    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Provider
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Provider
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set modifiedAt
     * @return Provider
     */
    public function setModifiedAt()
    {
        $this->modifiedAt = new \DateTime('now');;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }
    /**
     * @var integer
     */
    private $clinicianId;


    /**
     * Set clinicianId
     *
     * @param integer $clinicianId
     *
     * @return Provider
     */
    public function setClinicianId($clinicianId)
    {
        $this->clinicianId = $clinicianId;

        return $this;
    }

    /**
     * Get clinicianId
     *
     * @return integer
     */
    public function getClinicianId()
    {
        return $this->clinicianId;
    }



    /**
     * Set degreeId
     *
     * @param integer $degreeId
     *
     * @return Provider
     */
    public function setDegreeId($degreeId)
    {
        $this->degree_id = $degreeId;

        return $this;
    }

    /**
     * Get degreeId
     *
     * @return integer
     */
    public function getDegreeId()
    {
        return $this->degree_id;
    }

    /**
     * Set specialityId
     *
     * @param integer $specialityId
     *
     * @return Provider
     */
    public function setSpecialityId($specialityId)
    {
        $this->speciality_id = $specialityId;

        return $this;
    }

    /**
     * Get specialityId
     *
     * @return integer
     */
    public function getSpecialityId()
    {
        return $this->speciality_id;
    }

    public function __toString(){
        return (string)$this->getId();
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $recognitions;


    /**
     * Add recognition
     *
     * @param \AppBundle\Entity\Recognition $recognition
     *
     * @return Provider
     */
    public function addRecognition(\AppBundle\Entity\Recognition $recognition)
    {
        $this->recognitions[] = $recognition;

        return $this;
    }

    /**
     * Remove recognition
     *
     * @param \AppBundle\Entity\Recognition $recognition
     */
    public function removeRecognition(\AppBundle\Entity\Recognition $recognition)
    {
        $this->recognitions->removeElement($recognition);
    }

    /**
     * Get recognitions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRecognitions()
    {
        return $this->recognitions;
    }
    /**
     * @var string|null
     */
    private $responsibleProviderID;

    /**
     * @var string|null
     */
    private $practiceAddress1;

    /**
     * @var string|null
     */
    private $practiceAddress2;

    /**
     * @var string|null
     */
    private $practiceCity;

    /**
     * @var string|null
     */
    private $practiceState;

    /**
     * @var string|null
     */
    private $practiceZipCode;

    /**
     * @var string|null
     */
    private $emailaddress;

    /**
     * @var string|null
     */
    private $practicePhone;

    /**
     * @var string|null
     */
    private $practiceID;

    /**
     * @var string|null
     */
    private $practiceName;

    /**
     * @var string|null
     */
    private $individualGroup;

    /**
     * @var string|null
     */
    private $CCHITCertified;

    /**
     * @var string|null
     */
    private $fullPatientPanel;


    /**
     * Set responsibleProviderID.
     *
     * @param string|null $responsibleProviderID
     *
     * @return Provider
     */
    public function setResponsibleProviderID($responsibleProviderID = null)
    {
        $this->responsibleProviderID = $responsibleProviderID;

        return $this;
    }

    /**
     * Get responsibleProviderID.
     *
     * @return string|null
     */
    public function getResponsibleProviderID()
    {
        return $this->responsibleProviderID;
    }

    /**
     * Set practiceAddress1.
     *
     * @param string|null $practiceAddress1
     *
     * @return Provider
     */
    public function setPracticeAddress1($practiceAddress1 = null)
    {
        $this->practiceAddress1 = $practiceAddress1;

        return $this;
    }

    /**
     * Get practiceAddress1.
     *
     * @return string|null
     */
    public function getPracticeAddress1()
    {
        return $this->practiceAddress1;
    }

    /**
     * Set practiceAddress2.
     *
     * @param string|null $practiceAddress2
     *
     * @return Provider
     */
    public function setPracticeAddress2($practiceAddress2 = null)
    {
        $this->practiceAddress2 = $practiceAddress2;

        return $this;
    }

    /**
     * Get practiceAddress2.
     *
     * @return string|null
     */
    public function getPracticeAddress2()
    {
        return $this->practiceAddress2;
    }

    /**
     * Set practiceCity.
     *
     * @param string|null $practiceCity
     *
     * @return Provider
     */
    public function setPracticeCity($practiceCity = null)
    {
        $this->practiceCity = $practiceCity;

        return $this;
    }

    /**
     * Get practiceCity.
     *
     * @return string|null
     */
    public function getPracticeCity()
    {
        return $this->practiceCity;
    }

    /**
     * Set practiceState.
     *
     * @param string|null $practiceState
     *
     * @return Provider
     */
    public function setPracticeState($practiceState = null)
    {
        $this->practiceState = $practiceState;

        return $this;
    }

    /**
     * Get practiceState.
     *
     * @return string|null
     */
    public function getPracticeState()
    {
        return $this->practiceState;
    }

    /**
     * Set practiceZipCode.
     *
     * @param string|null $practiceZipCode
     *
     * @return Provider
     */
    public function setPracticeZipCode($practiceZipCode = null)
    {
        $this->practiceZipCode = $practiceZipCode;

        return $this;
    }

    /**
     * Get practiceZipCode.
     *
     * @return string|null
     */
    public function getPracticeZipCode()
    {
        return $this->practiceZipCode;
    }

    /**
     * Set emailaddress.
     *
     * @param string|null $emailaddress
     *
     * @return Provider
     */
    public function setEmailaddress($emailaddress = null)
    {
        $this->emailaddress = $emailaddress;

        return $this;
    }

    /**
     * Get emailaddress.
     *
     * @return string|null
     */
    public function getEmailaddress()
    {
        return $this->emailaddress;
    }

    /**
     * Set practicePhone.
     *
     * @param string|null $practicePhone
     *
     * @return Provider
     */
    public function setPracticePhone($practicePhone = null)
    {
        $this->practicePhone = $practicePhone;

        return $this;
    }

    /**
     * Get practicePhone.
     *
     * @return string|null
     */
    public function getPracticePhone()
    {
        return $this->practicePhone;
    }

    /**
     * Set practiceID.
     *
     * @param string|null $practiceID
     *
     * @return Provider
     */
    public function setPracticeID($practiceID = null)
    {
        $this->practiceID = $practiceID;

        return $this;
    }

    /**
     * Get practiceID.
     *
     * @return string|null
     */
    public function getPracticeID()
    {
        return $this->practiceID;
    }

    /**
     * Set practiceName.
     *
     * @param string|null $practiceName
     *
     * @return Provider
     */
    public function setPracticeName($practiceName = null)
    {
        $this->practiceName = $practiceName;

        return $this;
    }

    /**
     * Get practiceName.
     *
     * @return string|null
     */
    public function getPracticeName()
    {
        return $this->practiceName;
    }

    /**
     * Set individualGroup.
     *
     * @param string|null $individualGroup
     *
     * @return Provider
     */
    public function setIndividualGroup($individualGroup = null)
    {
        $this->individualGroup = $individualGroup;

        return $this;
    }

    /**
     * Get individualGroup.
     *
     * @return string|null
     */
    public function getIndividualGroup()
    {
        return $this->individualGroup;
    }

    /**
     * Set cCHITCertified.
     *
     * @param string|null $cCHITCertified
     *
     * @return Provider
     */
    public function setCCHITCertified($cCHITCertified = null)
    {
        $this->CCHITCertified = $cCHITCertified;

        return $this;
    }

    /**
     * Get cCHITCertified.
     *
     * @return string|null
     */
    public function getCCHITCertified()
    {
        return $this->CCHITCertified;
    }

    /**
     * Set fullPatientPanel.
     *
     * @param string|null $fullPatientPanel
     *
     * @return Provider
     */
    public function setFullPatientPanel($fullPatientPanel = null)
    {
        $this->fullPatientPanel = $fullPatientPanel;

        return $this;
    }

    /**
     * Get fullPatientPanel.
     *
     * @return string|null
     */
    public function getFullPatientPanel()
    {
        return $this->fullPatientPanel;
    }
}
