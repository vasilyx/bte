<?php

namespace AppBundle\Entity;

/**
 * Submission
 */
class Submission
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var boolean
     */
    private $from_ehr;

    /**
     * @var string
     */
    private $ehr;

    /**
     * @var string
     */
    private $ehr_id;

    /**
     * @var \DateTime
     */
    private $reportingStart;

    /**
     * @var \DateTime
     */
    private $reportingEnd;

    /**
     * @var boolean
     */
    private $reportingMethod;

    /**
     * @var integer
     */
    private $user_id;

    /**
     * @var integer
     */
    private $program_id;

    /**
     * @var \AppBundle\Entity\User
     */
    private $user;

    /**
     * @var \AppBundle\Entity\Program
     */
    private $program;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $provider;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->provider = new \Doctrine\Common\Collections\ArrayCollection();
        $this->modifiedAt = new \DateTime('now');
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fromEhr
     *
     * @param boolean $fromEhr
     *
     * @return Submission
     */
    public function setFromEhr($fromEhr)
    {
        $this->from_ehr = $fromEhr;

        return $this;
    }

    /**
     * Get fromEhr
     *
     * @return boolean
     */
    public function getFromEhr()
    {
        return $this->from_ehr;
    }

    /**
     * Set ehr
     *
     * @param string $ehr
     *
     * @return Submission
     */
    public function setEhr($ehr)
    {
        $this->ehr = $ehr;

        return $this;
    }

    /**
     * Get ehr
     *
     * @return string
     */
    public function getEhr()
    {
        return $this->ehr;
    }

    /**
     * Set ehrId
     *
     * @param string $ehrId
     *
     * @return Submission
     */
    public function setEhrId($ehrId)
    {
        $this->ehr_id = $ehrId;

        return $this;
    }

    /**
     * Get ehrId
     *
     * @return string
     */
    public function getEhrId()
    {
        return $this->ehr_id;
    }

    /**
     * Set reportingStart
     *
     * @param \DateTime $reportingStart
     *
     * @return Submission
     */
    public function setReportingStart($reportingStart)
    {
        $this->reportingStart = $reportingStart;

        return $this;
    }

    /**
     * Get reportingStart
     *
     * @return \DateTime
     */
    public function getReportingStart()
    {
        return $this->reportingStart;
    }

    /**
     * Set reportingEnd
     *
     * @param \DateTime $reportingEnd
     *
     * @return Submission
     */
    public function setReportingEnd($reportingEnd)
    {
        $this->reportingEnd = $reportingEnd;

        return $this;
    }

    /**
     * Get reportingEnd
     *
     * @return \DateTime
     */
    public function getReportingEnd()
    {
        return $this->reportingEnd;
    }

    /**
     * Set reportingMethod
     *
     * @param boolean $reportingMethod
     *
     * @return Submission
     */
    public function setReportingMethod($reportingMethod)
    {
        $this->reportingMethod = $reportingMethod;

        return $this;
    }

    /**
     * Get reportingMethod
     *
     * @return boolean
     */
    public function getReportingMethod()
    {
        return $this->reportingMethod;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return Submission
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set programId
     *
     * @param integer $programId
     *
     * @return Submission
     */
    public function setProgramId($programId)
    {
        $this->program_id = $programId;

        return $this;
    }

    /**
     * Get programId
     *
     * @return integer
     */
    public function getProgramId()
    {
        return $this->program_id;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Submission
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set program
     *
     * @param \AppBundle\Entity\Program $program
     *
     * @return Submission
     */
    public function setProgram(\AppBundle\Entity\Program $program = null)
    {
        $this->program = $program;

        return $this;
    }

    /**
     * Get program
     *
     * @return \AppBundle\Entity\Program
     */
    public function getProgram()
    {
        return $this->program;
    }

    /**
     * Add provider
     *
     * @param \AppBundle\Entity\Provider $provider
     *
     * @return Submission
     */
    public function addProvider(\AppBundle\Entity\Provider $provider)
    {
        $provider->addSubmission($this);
        $this->provider[] = $provider;

        return $this;
    }

    /**
     * Remove provider
     *
     * @param \AppBundle\Entity\Provider $provider
     */
    public function removeProvider(\AppBundle\Entity\Provider $provider)
    {
        $this->provider->removeElement($provider);
    }

    /**
     * Get provider
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProvider()
    {
        return $this->provider;
    }
    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $modifiedAt;


    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Submission
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set modifiedAt
     *
     * @return Submission
     */
    public function setModifiedAt()
    {
        $this->modifiedAt = new \DateTime('now');;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }
    /**
     * @var boolean
     */
    private $reportingFor;


    /**
     * Set reportingFor
     *
     * @param boolean $reportingFor
     *
     * @return Submission
     */
    public function setReportingFor($reportingFor)
    {
        $this->reportingFor = $reportingFor;

        return $this;
    }

    /**
     * Get reportingFor
     *
     * @return boolean
     */
    public function getReportingFor()
    {
        return $this->reportingFor;
    }
    /**
     * @var \AppBundle\Entity\Recognition
     */
    private $recognition;


    /**
     * Set recognition
     *
     * @param \AppBundle\Entity\Recognition $recognition
     *
     * @return Submission
     */
    public function setRecognition(\AppBundle\Entity\Recognition $recognition = null)
    {
        $this->recognition = $recognition;

        return $this;
    }

    /**
     * Get recognition
     *
     * @return \AppBundle\Entity\Recognition
     */
    public function getRecognition()
    {
        return $this->recognition;
    }

    public function __toString(){
        return (string)$this->getId();
    }

    /**
     * Add recognition
     *
     * @param \AppBundle\Entity\Recognition $recognition
     *
     * @return Submission
     */
    public function addRecognition(\AppBundle\Entity\Recognition $recognition)
    {
        $this->recognition[] = $recognition;

        return $this;
    }

    /**
     * Remove recognition
     *
     * @param \AppBundle\Entity\Recognition $recognition
     */
    public function removeRecognition(\AppBundle\Entity\Recognition $recognition)
    {
        $this->recognition->removeElement($recognition);
    }
    /**
     * @var float
     */
    private $sumScore;


    /**
     * Set sumScore
     *
     * @param float $sumScore
     *
     * @return Submission
     */
    public function setSumScore($sumScore)
    {
        $this->sumScore = $sumScore;

        return $this;
    }

    /**
     * Get sumScore
     *
     * @return float
     */
    public function getSumScore()
    {
        return $this->sumScore;
    }
}
