<?php

namespace AppBundle\Entity;

/**
 * Log
 */
class Log
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;


    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $userId;



    /**
     * @var int
     */
    private $typeId;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string|null
     */
    private $description;

    /**
     * @var int
     */
    private $statusId;

    /**
     * @var string|null
     */
    private $status;

    /**
     * @var \DateTime
     */
    private $createdAt = 'CURRENT_TIMESTAMP';


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId.
     *
     * @param int $userId
     *
     * @return Log
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId.
     *
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }



    /**
     * Set typeId.
     *
     * @param int $typeId
     *
     * @return Log
     */
    public function setTypeId($typeId)
    {
        $this->typeId = $typeId;

        return $this;
    }

    /**
     * Get typeId.
     *
     * @return int
     */
    public function getTypeId()
    {
        return $this->typeId;
    }

    /**
     * Set type.
     *
     * @param string $type
     *
     * @return Log
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return Log
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set statusId.
     *
     * @param int $statusId
     *
     * @return Log
     */
    public function setStatusId($statusId)
    {
        $this->statusId = $statusId;

        return $this;
    }

    /**
     * Get statusId.
     *
     * @return int
     */
    public function getStatusId()
    {
        return $this->statusId;
    }

    /**
     * Set status.
     *
     * @param string|null $status
     *
     * @return Log
     */
    public function setStatus($status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return string|null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createdAt.
     *
     * @return Log
     */
    public function setCreatedAt()
    {
        $this->createdAt = new \DateTime('now');

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }


    /**
     * @var int|null
     */
    private $temp_id;


    /**
     * Set tempId.
     *
     * @param int|null $tempId
     *
     * @return Log
     */
    public function setTempId($tempId = null)
    {
        $this->temp_id = $tempId;

        return $this;
    }

    /**
     * Get tempId.
     *
     * @return int|null
     */
    public function getTempId()
    {
        return $this->temp_id;
    }
    /**
     * @var int|null
     */
    private $submission_id;


    /**
     * Set submissionId.
     *
     * @param int|null $submissionId
     *
     * @return Log
     */
    public function setSubmissionId($submissionId = null)
    {
        $this->submission_id = $submissionId;

        return $this;
    }

    /**
     * Get submissionId.
     *
     * @return int|null
     */
    public function getSubmissionId()
    {
        return $this->submission_id;
    }
}
