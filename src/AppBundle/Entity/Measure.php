<?php

namespace AppBundle\Entity;

/**
 * Measure
 */
class Measure
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $program_id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $funcName;

    /**
     * @var integer
     */
    private $total;

    /**
     * @var boolean
     */
    private $published;

    /**
     * @var \AppBundle\Entity\Program
     */
    private $program;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set programId
     *
     * @param integer $programId
     *
     * @return Measure
     */
    public function setProgramId($programId)
    {
        $this->program_id = $programId;

        return $this;
    }

    /**
     * Get programId
     *
     * @return integer
     */
    public function getProgramId()
    {
        return $this->program_id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Measure
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set funcName
     *
     * @param string $funcName
     *
     * @return Measure
     */
    public function setFuncName($funcName)
    {
        $this->funcName = $funcName;

        return $this;
    }

    /**
     * Get funcName
     *
     * @return string
     */
    public function getFuncName()
    {
        return $this->funcName;
    }

    /**
     * Set total
     *
     * @param integer $total
     *
     * @return Measure
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return integer
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set published
     *
     * @param boolean $published
     *
     * @return Measure
     */
    public function setPublished($published)
    {
        $this->published = $published;

        return $this;
    }

    /**
     * Get published
     *
     * @return boolean
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * Set program
     *
     * @param \AppBundle\Entity\Program $program
     *
     * @return Measure
     */
    public function setProgram(\AppBundle\Entity\Program $program = null)
    {
        $this->program = $program;

        return $this;
    }

    /**
     * Get program
     *
     * @return \AppBundle\Entity\Program
     */
    public function getProgram()
    {
        return $this->program;
    }
}
