<?php

namespace AppBundle\Entity;

/**
 * SubmissionHeartFailure
 */
class SubmissionHeartFailure
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $responsibleProviderID;

    /**
     * @var string
     */
    private $npi;

    /**
     * @var string
     */
    private $groupID;

    /**
     * @var string
     */
    private $individualGroup;

    /**
     * @var string
     */
    private $chartID;

    /**
     * @var \DateTime
     */
    private $lastVisitDate;

    /**
     * @var \DateTime
     */
    private $patientDOB;

    /**
     * @var integer
     */
    private $patientGender;

    /**
     * @var integer
     */
    private $patientRace;

    /**
     * @var boolean
     */
    private $medicarePartB;

    /**
     * @var boolean
     */
    private $HFDiagnosis;

    /**
     * @var boolean
     */
    private $LVSDDiagnosis;

    /**
     * @var \DateTime
     */
    private $lvfAssessDate;

    /**
     * @var boolean
     */
    private $betablockerTherapy;

    /**
     * @var boolean
     */
    private $aceiArbTherapy;

    /**
     * @var \DateTime
     */
    private $bloodPressureDate1;

    /**
     * @var integer
     */
    private $systolic1;

    /**
     * @var integer
     */
    private $diastolic1;

    /**
     * @var \DateTime
     */
    private $bloodPressureDate2;

    /**
     * @var integer
     */
    private $systolic2;

    /**
     * @var integer
     */
    private $diastolic2;

    /**
     * @var \DateTime
     */
    private $weightMeasureDate;

    /**
     * @var boolean
     */
    private $peripheraledemaAssessment;

    /**
     * @var boolean
     */
    private $orthopneaAssessment;

    /**
     * @var boolean
     */
    private $JVPAssessment;

    /**
     * @var boolean
     */
    private $tobaccoStatus;

    /**
     * @var \DateTime
     */
    private $tobaccoStatusAssessmentDate;

    /**
     * @var boolean
     */
    private $tobaccoCessationAdviceOrTreatment;

    /**
     * @var \DateTime
     */
    private $tobaccoCessationAdviceOrTreatmentDate;

    /**
     * @var float
     */
    private $bmiValue;

    /**
     * @var \DateTime
     */
    private $bmiValueDate;

    /**
     * @var float
     */
    private $nutritionCounseling;

    /**
     * @var \DateTime
     */
    private $nutritionCounselingDate;

    /**
     * @var boolean
     */
    private $activityStatus;

    /**
     * @var \DateTime
     */
    private $activityStatusDate;

    /**
     * @var boolean
     */
    private $activityCounseling;

    /**
     * @var \DateTime
     */
    private $activityCounselingDate;

    /**
     * @var string
     */
    private $heartfailureType;

    /**
     * @var boolean
     */
    private $heartfailureSeverity;

    /**
     * @var boolean
     */
    private $esrdPatient;

    /**
     * @var boolean
     */
    private $dialysisPatient;

    /**
     * @var integer
     */
    private $submission_id;

    /**
     * @var integer
     */
    private $temp_id;

    /**
     * @var \AppBundle\Entity\Submission
     */
    private $submision;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set responsibleProviderID
     *
     * @param integer $responsibleProviderID
     *
     * @return SubmissionHeartFailure
     */
    public function setResponsibleProviderID($responsibleProviderID)
    {
        $this->responsibleProviderID = $responsibleProviderID;

        return $this;
    }

    /**
     * Get responsibleProviderID
     *
     * @return integer
     */
    public function getResponsibleProviderID()
    {
        return $this->responsibleProviderID;
    }

    /**
     * Set npi
     *
     * @param string $npi
     *
     * @return SubmissionHeartFailure
     */
    public function setNpi($npi)
    {
        $this->npi = $npi;

        return $this;
    }

    /**
     * Get npi
     *
     * @return string
     */
    public function getNpi()
    {
        return $this->npi;
    }

    /**
     * Set groupID
     *
     * @param string $groupID
     *
     * @return SubmissionHeartFailure
     */
    public function setGroupID($groupID)
    {
        $this->groupID = $groupID;

        return $this;
    }

    /**
     * Get groupID
     *
     * @return string
     */
    public function getGroupID()
    {
        return $this->groupID;
    }

    /**
     * Set individualGroup
     *
     * @param string $individualGroup
     *
     * @return SubmissionHeartFailure
     */
    public function setIndividualGroup($individualGroup)
    {
        $this->individualGroup = $individualGroup;

        return $this;
    }

    /**
     * Get individualGroup
     *
     * @return string
     */
    public function getIndividualGroup()
    {
        return $this->individualGroup;
    }

    /**
     * Set chartID
     *
     * @param string $chartID
     *
     * @return SubmissionHeartFailure
     */
    public function setChartID($chartID)
    {
        $this->chartID = $chartID;

        return $this;
    }

    /**
     * Get chartID
     *
     * @return string
     */
    public function getChartID()
    {
        return $this->chartID;
    }

    /**
     * Set lastVisitDate
     *
     * @param \DateTime $lastVisitDate
     *
     * @return SubmissionHeartFailure
     */
    public function setLastVisitDate($lastVisitDate)
    {
        $this->lastVisitDate = $lastVisitDate;

        return $this;
    }

    /**
     * Get lastVisitDate
     *
     * @return \DateTime
     */
    public function getLastVisitDate()
    {
        return $this->lastVisitDate;
    }

    /**
     * Set patientDOB
     *
     * @param \DateTime $patientDOB
     *
     * @return SubmissionHeartFailure
     */
    public function setPatientDOB($patientDOB)
    {
        $this->patientDOB = $patientDOB;

        return $this;
    }

    /**
     * Get patientDOB
     *
     * @return \DateTime
     */
    public function getPatientDOB()
    {
        return $this->patientDOB;
    }

    /**
     * Set patientGender
     *
     * @param integer $patientGender
     *
     * @return SubmissionHeartFailure
     */
    public function setPatientGender($patientGender)
    {
        $this->patientGender = $patientGender;

        return $this;
    }

    /**
     * Get patientGender
     *
     * @return integer
     */
    public function getPatientGender()
    {
        return $this->patientGender;
    }

    /**
     * Set patientRace
     *
     * @param integer $patientRace
     *
     * @return SubmissionHeartFailure
     */
    public function setPatientRace($patientRace)
    {
        $this->patientRace = $patientRace;

        return $this;
    }

    /**
     * Get patientRace
     *
     * @return integer
     */
    public function getPatientRace()
    {
        return $this->patientRace;
    }

    /**
     * Set medicarePartB
     *
     * @param boolean $medicarePartB
     *
     * @return SubmissionHeartFailure
     */
    public function setMedicarePartB($medicarePartB)
    {
        $this->medicarePartB = $medicarePartB;

        return $this;
    }

    /**
     * Get medicarePartB
     *
     * @return boolean
     */
    public function getMedicarePartB()
    {
        return $this->medicarePartB;
    }

    /**
     * Set hFDiagnosis
     *
     * @param boolean $hFDiagnosis
     *
     * @return SubmissionHeartFailure
     */
    public function setHFDiagnosis($hFDiagnosis)
    {
        $this->HFDiagnosis = $hFDiagnosis;

        return $this;
    }

    /**
     * Get hFDiagnosis
     *
     * @return boolean
     */
    public function getHFDiagnosis()
    {
        return $this->HFDiagnosis;
    }

    /**
     * Set lVSDDiagnosis
     *
     * @param boolean $lVSDDiagnosis
     *
     * @return SubmissionHeartFailure
     */
    public function setLVSDDiagnosis($lVSDDiagnosis)
    {
        $this->LVSDDiagnosis = $lVSDDiagnosis;

        return $this;
    }

    /**
     * Get lVSDDiagnosis
     *
     * @return boolean
     */
    public function getLVSDDiagnosis()
    {
        return $this->LVSDDiagnosis;
    }

    /**
     * Set lvfAssessDate
     *
     * @param \DateTime $lvfAssessDate
     *
     * @return SubmissionHeartFailure
     */
    public function setLvfAssessDate($lvfAssessDate)
    {
        $this->lvfAssessDate = $lvfAssessDate;

        return $this;
    }

    /**
     * Get lvfAssessDate
     *
     * @return \DateTime
     */
    public function getLvfAssessDate()
    {
        return $this->lvfAssessDate;
    }

    /**
     * Set betablockerTherapy
     *
     * @param boolean $betablockerTherapy
     *
     * @return SubmissionHeartFailure
     */
    public function setBetablockerTherapy($betablockerTherapy)
    {
        $this->betablockerTherapy = $betablockerTherapy;

        return $this;
    }

    /**
     * Get betablockerTherapy
     *
     * @return boolean
     */
    public function getBetablockerTherapy()
    {
        return $this->betablockerTherapy;
    }

    /**
     * Set aceiArbTherapy
     *
     * @param boolean $aceiArbTherapy
     *
     * @return SubmissionHeartFailure
     */
    public function setAceiArbTherapy($aceiArbTherapy)
    {
        $this->aceiArbTherapy = $aceiArbTherapy;

        return $this;
    }

    /**
     * Get aceiArbTherapy
     *
     * @return boolean
     */
    public function getAceiArbTherapy()
    {
        return $this->aceiArbTherapy;
    }

    /**
     * Set bloodPressureDate1
     *
     * @param \DateTime $bloodPressureDate1
     *
     * @return SubmissionHeartFailure
     */
    public function setBloodPressureDate1($bloodPressureDate1)
    {
        $this->bloodPressureDate1 = $bloodPressureDate1;

        return $this;
    }

    /**
     * Get bloodPressureDate1
     *
     * @return \DateTime
     */
    public function getBloodPressureDate1()
    {
        return $this->bloodPressureDate1;
    }

    /**
     * Set systolic1
     *
     * @param integer $systolic1
     *
     * @return SubmissionHeartFailure
     */
    public function setSystolic1($systolic1)
    {
        $this->systolic1 = $systolic1;

        return $this;
    }

    /**
     * Get systolic1
     *
     * @return integer
     */
    public function getSystolic1()
    {
        return $this->systolic1;
    }

    /**
     * Set diastolic1
     *
     * @param integer $diastolic1
     *
     * @return SubmissionHeartFailure
     */
    public function setDiastolic1($diastolic1)
    {
        $this->diastolic1 = $diastolic1;

        return $this;
    }

    /**
     * Get diastolic1
     *
     * @return integer
     */
    public function getDiastolic1()
    {
        return $this->diastolic1;
    }

    /**
     * Set bloodPressureDate2
     *
     * @param \DateTime $bloodPressureDate2
     *
     * @return SubmissionHeartFailure
     */
    public function setBloodPressureDate2($bloodPressureDate2)
    {
        $this->bloodPressureDate2 = $bloodPressureDate2;

        return $this;
    }

    /**
     * Get bloodPressureDate2
     *
     * @return \DateTime
     */
    public function getBloodPressureDate2()
    {
        return $this->bloodPressureDate2;
    }

    /**
     * Set systolic2
     *
     * @param integer $systolic2
     *
     * @return SubmissionHeartFailure
     */
    public function setSystolic2($systolic2)
    {
        $this->systolic2 = $systolic2;

        return $this;
    }

    /**
     * Get systolic2
     *
     * @return integer
     */
    public function getSystolic2()
    {
        return $this->systolic2;
    }

    /**
     * Set diastolic2
     *
     * @param integer $diastolic2
     *
     * @return SubmissionHeartFailure
     */
    public function setDiastolic2($diastolic2)
    {
        $this->diastolic2 = $diastolic2;

        return $this;
    }

    /**
     * Get diastolic2
     *
     * @return integer
     */
    public function getDiastolic2()
    {
        return $this->diastolic2;
    }

    /**
     * Set weightMeasureDate
     *
     * @param \DateTime $weightMeasureDate
     *
     * @return SubmissionHeartFailure
     */
    public function setWeightMeasureDate($weightMeasureDate)
    {
        $this->weightMeasureDate = $weightMeasureDate;

        return $this;
    }

    /**
     * Get weightMeasureDate
     *
     * @return \DateTime
     */
    public function getWeightMeasureDate()
    {
        return $this->weightMeasureDate;
    }

    /**
     * Set peripheraledemaAssessment
     *
     * @param boolean $peripheraledemaAssessment
     *
     * @return SubmissionHeartFailure
     */
    public function setPeripheraledemaAssessment($peripheraledemaAssessment)
    {
        $this->peripheraledemaAssessment = $peripheraledemaAssessment;

        return $this;
    }

    /**
     * Get peripheraledemaAssessment
     *
     * @return boolean
     */
    public function getPeripheraledemaAssessment()
    {
        return $this->peripheraledemaAssessment;
    }

    /**
     * Set orthopneaAssessment
     *
     * @param boolean $orthopneaAssessment
     *
     * @return SubmissionHeartFailure
     */
    public function setOrthopneaAssessment($orthopneaAssessment)
    {
        $this->orthopneaAssessment = $orthopneaAssessment;

        return $this;
    }

    /**
     * Get orthopneaAssessment
     *
     * @return boolean
     */
    public function getOrthopneaAssessment()
    {
        return $this->orthopneaAssessment;
    }

    /**
     * Set jVPAssessment
     *
     * @param boolean $jVPAssessment
     *
     * @return SubmissionHeartFailure
     */
    public function setJVPAssessment($jVPAssessment)
    {
        $this->JVPAssessment = $jVPAssessment;

        return $this;
    }

    /**
     * Get jVPAssessment
     *
     * @return boolean
     */
    public function getJVPAssessment()
    {
        return $this->JVPAssessment;
    }

    /**
     * Set tobaccoStatus
     *
     * @param boolean $tobaccoStatus
     *
     * @return SubmissionHeartFailure
     */
    public function setTobaccoStatus($tobaccoStatus)
    {
        $this->tobaccoStatus = $tobaccoStatus;

        return $this;
    }

    /**
     * Get tobaccoStatus
     *
     * @return boolean
     */
    public function getTobaccoStatus()
    {
        return $this->tobaccoStatus;
    }

    /**
     * Set tobaccoStatusAssessmentDate
     *
     * @param \DateTime $tobaccoStatusAssessmentDate
     *
     * @return SubmissionHeartFailure
     */
    public function setTobaccoStatusAssessmentDate($tobaccoStatusAssessmentDate)
    {
        $this->tobaccoStatusAssessmentDate = $tobaccoStatusAssessmentDate;

        return $this;
    }

    /**
     * Get tobaccoStatusAssessmentDate
     *
     * @return \DateTime
     */
    public function getTobaccoStatusAssessmentDate()
    {
        return $this->tobaccoStatusAssessmentDate;
    }

    /**
     * Set tobaccoCessationAdviceOrTreatment
     *
     * @param boolean $tobaccoCessationAdviceOrTreatment
     *
     * @return SubmissionHeartFailure
     */
    public function setTobaccoCessationAdviceOrTreatment($tobaccoCessationAdviceOrTreatment)
    {
        $this->tobaccoCessationAdviceOrTreatment = $tobaccoCessationAdviceOrTreatment;

        return $this;
    }

    /**
     * Get tobaccoCessationAdviceOrTreatment
     *
     * @return boolean
     */
    public function getTobaccoCessationAdviceOrTreatment()
    {
        return $this->tobaccoCessationAdviceOrTreatment;
    }

    /**
     * Set tobaccoCessationAdviceOrTreatmentDate
     *
     * @param \DateTime $tobaccoCessationAdviceOrTreatmentDate
     *
     * @return SubmissionHeartFailure
     */
    public function setTobaccoCessationAdviceOrTreatmentDate($tobaccoCessationAdviceOrTreatmentDate)
    {
        $this->tobaccoCessationAdviceOrTreatmentDate = $tobaccoCessationAdviceOrTreatmentDate;

        return $this;
    }

    /**
     * Get tobaccoCessationAdviceOrTreatmentDate
     *
     * @return \DateTime
     */
    public function getTobaccoCessationAdviceOrTreatmentDate()
    {
        return $this->tobaccoCessationAdviceOrTreatmentDate;
    }

    /**
     * Set bmiValue
     *
     * @param float $bmiValue
     *
     * @return SubmissionHeartFailure
     */
    public function setBmiValue($bmiValue)
    {
        $this->bmiValue = $bmiValue;

        return $this;
    }

    /**
     * Get bmiValue
     *
     * @return float
     */
    public function getBmiValue()
    {
        return $this->bmiValue;
    }

    /**
     * Set bmiValueDate
     *
     * @param \DateTime $bmiValueDate
     *
     * @return SubmissionHeartFailure
     */
    public function setBmiValueDate($bmiValueDate)
    {
        $this->bmiValueDate = $bmiValueDate;

        return $this;
    }

    /**
     * Get bmiValueDate
     *
     * @return \DateTime
     */
    public function getBmiValueDate()
    {
        return $this->bmiValueDate;
    }

    /**
     * Set nutritionCounseling
     *
     * @param float $nutritionCounseling
     *
     * @return SubmissionHeartFailure
     */
    public function setNutritionCounseling($nutritionCounseling)
    {
        $this->nutritionCounseling = $nutritionCounseling;

        return $this;
    }

    /**
     * Get nutritionCounseling
     *
     * @return float
     */
    public function getNutritionCounseling()
    {
        return $this->nutritionCounseling;
    }

    /**
     * Set nutritionCounselingDate
     *
     * @param \DateTime $nutritionCounselingDate
     *
     * @return SubmissionHeartFailure
     */
    public function setNutritionCounselingDate($nutritionCounselingDate)
    {
        $this->nutritionCounselingDate = $nutritionCounselingDate;

        return $this;
    }

    /**
     * Get nutritionCounselingDate
     *
     * @return \DateTime
     */
    public function getNutritionCounselingDate()
    {
        return $this->nutritionCounselingDate;
    }

    /**
     * Set activityStatus
     *
     * @param boolean $activityStatus
     *
     * @return SubmissionHeartFailure
     */
    public function setActivityStatus($activityStatus)
    {
        $this->activityStatus = $activityStatus;

        return $this;
    }

    /**
     * Get activityStatus
     *
     * @return boolean
     */
    public function getActivityStatus()
    {
        return $this->activityStatus;
    }

    /**
     * Set activityStatusDate
     *
     * @param \DateTime $activityStatusDate
     *
     * @return SubmissionHeartFailure
     */
    public function setActivityStatusDate($activityStatusDate)
    {
        $this->activityStatusDate = $activityStatusDate;

        return $this;
    }

    /**
     * Get activityStatusDate
     *
     * @return \DateTime
     */
    public function getActivityStatusDate()
    {
        return $this->activityStatusDate;
    }

    /**
     * Set activityCounseling
     *
     * @param boolean $activityCounseling
     *
     * @return SubmissionHeartFailure
     */
    public function setActivityCounseling($activityCounseling)
    {
        $this->activityCounseling = $activityCounseling;

        return $this;
    }

    /**
     * Get activityCounseling
     *
     * @return boolean
     */
    public function getActivityCounseling()
    {
        return $this->activityCounseling;
    }

    /**
     * Set activityCounselingDate
     *
     * @param \DateTime $activityCounselingDate
     *
     * @return SubmissionHeartFailure
     */
    public function setActivityCounselingDate($activityCounselingDate)
    {
        $this->activityCounselingDate = $activityCounselingDate;

        return $this;
    }

    /**
     * Get activityCounselingDate
     *
     * @return \DateTime
     */
    public function getActivityCounselingDate()
    {
        return $this->activityCounselingDate;
    }

    /**
     * Set heartfailureType
     *
     * @param string $heartfailureType
     *
     * @return SubmissionHeartFailure
     */
    public function setHeartfailureType($heartfailureType)
    {
        $this->heartfailureType = $heartfailureType;

        return $this;
    }

    /**
     * Get heartfailureType
     *
     * @return string
     */
    public function getHeartfailureType()
    {
        return $this->heartfailureType;
    }

    /**
     * Set heartfailureSeverity
     *
     * @param boolean $heartfailureSeverity
     *
     * @return SubmissionHeartFailure
     */
    public function setHeartfailureSeverity($heartfailureSeverity)
    {
        $this->heartfailureSeverity = $heartfailureSeverity;

        return $this;
    }

    /**
     * Get heartfailureSeverity
     *
     * @return boolean
     */
    public function getHeartfailureSeverity()
    {
        return $this->heartfailureSeverity;
    }

    /**
     * Set esrdPatient
     *
     * @param boolean $esrdPatient
     *
     * @return SubmissionHeartFailure
     */
    public function setEsrdPatient($esrdPatient)
    {
        $this->esrdPatient = $esrdPatient;

        return $this;
    }

    /**
     * Get esrdPatient
     *
     * @return boolean
     */
    public function getEsrdPatient()
    {
        return $this->esrdPatient;
    }

    /**
     * Set dialysisPatient
     *
     * @param boolean $dialysisPatient
     *
     * @return SubmissionHeartFailure
     */
    public function setDialysisPatient($dialysisPatient)
    {
        $this->dialysisPatient = $dialysisPatient;

        return $this;
    }

    /**
     * Get dialysisPatient
     *
     * @return boolean
     */
    public function getDialysisPatient()
    {
        return $this->dialysisPatient;
    }

    /**
     * Set submissionId
     *
     * @param integer $submissionId
     *
     * @return SubmissionHeartFailure
     */
    public function setSubmissionId($submissionId)
    {
        $this->submission_id = $submissionId;

        return $this;
    }

    /**
     * Get submissionId
     *
     * @return integer
     */
    public function getSubmissionId()
    {
        return $this->submission_id;
    }

    /**
     * Set tempId
     *
     * @param integer $tempId
     *
     * @return SubmissionHeartFailure
     */
    public function setTempId($tempId)
    {
        $this->temp_id = $tempId;

        return $this;
    }

    /**
     * Get tempId
     *
     * @return integer
     */
    public function getTempId()
    {
        return $this->temp_id;
    }

    /**
     * Set submision
     *
     * @param \AppBundle\Entity\Submission $submision
     *
     * @return SubmissionHeartFailure
     */
    public function setSubmision(\AppBundle\Entity\Submission $submision = null)
    {
        $this->submision = $submision;

        return $this;
    }

    /**
     * Get submision
     *
     * @return \AppBundle\Entity\Submission
     */
    public function getSubmision()
    {
        return $this->submision;
    }
    /**
     * @var \DateTime
     */
    private $heartfailureAssessedDate;


    /**
     * Set heartfailureAssessedDate
     *
     * @param \DateTime $heartfailureAssessedDate
     *
     * @return SubmissionHeartFailure
     */
    public function setHeartfailureAssessedDate($heartfailureAssessedDate)
    {
        $this->heartfailureAssessedDate = $heartfailureAssessedDate;

        return $this;
    }

    /**
     * Get heartfailureAssessedDate
     *
     * @return \DateTime
     */
    public function getHeartfailureAssessedDate()
    {
        return $this->heartfailureAssessedDate;
    }
}
