<?php

namespace AppBundle\Entity;

/**
 * SubmissionAsthma
 */
class SubmissionAsthma
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $responsibleProviderID;

    /**
     * @var string
     */
    private $npi;

    /**
     * @var string
     */
    private $groupID;

    /**
     * @var string
     */
    private $individualGroup;

    /**
     * @var string
     */
    private $chartID;

    /**
     * @var \DateTime
     */
    private $lastVisitDate;

    /**
     * @var \DateTime
     */
    private $patientDOB;

    /**
     * @var integer
     */
    private $patientGender;

    /**
     * @var string
     */
    private $patientRace;

    /**
     * @var boolean
     */
    private $medicarePartB;

    /**
     * @var boolean
     */
    private $asthmaDiagnosis;

    /**
     * @var string
     */
    private $asthmaDiseaseSeverity;

    /**
     * @var \DateTime
     */
    private $asthmaDiseaseSeverityDate;

    /**
     * @var integer
     */
    private $spirometryEvalResults;

    /**
     * @var \DateTime
     */
    private $spirometryEvalDate;

    /**
     * @var \DateTime
     */
    private $SABAPrescribedDate;

    /**
     * @var boolean
     */
    private $inhaledCorticosteroidDispensed;

    /**
     * @var \DateTime
     */
    private $inhaledCorticosteroidDispensedDate;

    /**
     * @var boolean
     */
    private $inhaledCorticosteroidPrescribed;

    /**
     * @var \DateTime
     */
    private $inhaledCorticosteroidPrescribedDate;

    /**
     * @var \DateTime
     */
    private $oralCorticosteroidPrescribed;

    /**
     * @var \DateTime
     */
    private $oralCorticosteroidPrescribedDate;

    /**
     * @var \DateTime
     */
    private $inhaledBronchodilatorPrescribedDate;

    /**
     * @var \DateTime
     */
    private $oralBeta3AgonistsDispensedDate;

    /**
     * @var \DateTime
     */
    private $oralBeta3AgonistsPrescribedDate;

    /**
     * @var \DateTime
     */
    private $asthmaMedicationPrescribedDate;

    /**
     * @var boolean
     */
    private $asthmaExacerbationEpisode;

    /**
     * @var string
     */
    private $asthmaExacerbationSeverity;

    /**
     * @var \DateTime
     */
    private $asthmaExacerbationDate;

    /**
     * @var \DateTime
     */
    private $asthmaExacerbationReferralDate;

    /**
     * @var \DateTime
     */
    private $faceToFaceVisit1;

    /**
     * @var \DateTime
     */
    private $faceToFaceVisit2;

    /**
     * @var \DateTime
     */
    private $asthmaActionPlanDate;

    /**
     * @var \DateTime
     */
    private $asthmaSelfManagementPlanDate;

    /**
     * @var \DateTime
     */
    private $inhalerEducationDate;

    /**
     * @var boolean
     */
    private $tobaccoExposureStatus;

    /**
     * @var \DateTime
     */
    private $tobaccoExposureStatusDate;

    /**
     * @var boolean
     */
    private $tobaccoStatus;

    /**
     * @var \DateTime
     */
    private $tobaccoStatusAssessmentDate;

    /**
     * @var \DateTime
     */
    private $tobaccoCessationAdviceOrTreatmentDate;

    /**
     * @var integer
     */
    private $influenzaImmunization;

    /**
     * @var \DateTime
     */
    private $influenzaImmunizationDate;

    /**
     * @var integer
     */
    private $submission_id;

    /**
     * @var integer
     */
    private $temp_id;

    /**
     * @var \AppBundle\Entity\Submission
     */
    private $submision;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set responsibleProviderID
     *
     * @param string $responsibleProviderID
     *
     * @return SubmissionAsthma
     */
    public function setResponsibleProviderID($responsibleProviderID)
    {
        $this->responsibleProviderID = $responsibleProviderID;

        return $this;
    }

    /**
     * Get responsibleProviderID
     *
     * @return string
     */
    public function getResponsibleProviderID()
    {
        return $this->responsibleProviderID;
    }

    /**
     * Set npi
     *
     * @param string $npi
     *
     * @return SubmissionAsthma
     */
    public function setNpi($npi)
    {
        $this->npi = $npi;

        return $this;
    }

    /**
     * Get npi
     *
     * @return string
     */
    public function getNpi()
    {
        return $this->npi;
    }

    /**
     * Set groupID
     *
     * @param string $groupID
     *
     * @return SubmissionAsthma
     */
    public function setGroupID($groupID)
    {
        $this->groupID = $groupID;

        return $this;
    }

    /**
     * Get groupID
     *
     * @return string
     */
    public function getGroupID()
    {
        return $this->groupID;
    }

    /**
     * Set individualGroup
     *
     * @param string $individualGroup
     *
     * @return SubmissionAsthma
     */
    public function setIndividualGroup($individualGroup)
    {
        $this->individualGroup = $individualGroup;

        return $this;
    }

    /**
     * Get individualGroup
     *
     * @return string
     */
    public function getIndividualGroup()
    {
        return $this->individualGroup;
    }

    /**
     * Set chartID
     *
     * @param string $chartID
     *
     * @return SubmissionAsthma
     */
    public function setChartID($chartID)
    {
        $this->chartID = $chartID;

        return $this;
    }

    /**
     * Get chartID
     *
     * @return string
     */
    public function getChartID()
    {
        return $this->chartID;
    }

    /**
     * Set lastVisitDate
     *
     * @param \DateTime $lastVisitDate
     *
     * @return SubmissionAsthma
     */
    public function setLastVisitDate($lastVisitDate)
    {
        $this->lastVisitDate = $lastVisitDate;

        return $this;
    }

    /**
     * Get lastVisitDate
     *
     * @return \DateTime
     */
    public function getLastVisitDate()
    {
        return $this->lastVisitDate;
    }

    /**
     * Set patientDOB
     *
     * @param \DateTime $patientDOB
     *
     * @return SubmissionAsthma
     */
    public function setPatientDOB($patientDOB)
    {
        $this->patientDOB = $patientDOB;

        return $this;
    }

    /**
     * Get patientDOB
     *
     * @return \DateTime
     */
    public function getPatientDOB()
    {
        return $this->patientDOB;
    }

    /**
     * Set patientGender
     *
     * @param integer $patientGender
     *
     * @return SubmissionAsthma
     */
    public function setPatientGender($patientGender)
    {
        $this->patientGender = $patientGender;

        return $this;
    }

    /**
     * Get patientGender
     *
     * @return integer
     */
    public function getPatientGender()
    {
        return $this->patientGender;
    }

    /**
     * Set patientRace
     *
     * @param string $patientRace
     *
     * @return SubmissionAsthma
     */
    public function setPatientRace($patientRace)
    {
        $this->patientRace = $patientRace;

        return $this;
    }

    /**
     * Get patientRace
     *
     * @return string
     */
    public function getPatientRace()
    {
        return $this->patientRace;
    }

    /**
     * Set medicarePartB
     *
     * @param boolean $medicarePartB
     *
     * @return SubmissionAsthma
     */
    public function setMedicarePartB($medicarePartB)
    {
        $this->medicarePartB = $medicarePartB;

        return $this;
    }

    /**
     * Get medicarePartB
     *
     * @return boolean
     */
    public function getMedicarePartB()
    {
        return $this->medicarePartB;
    }

    /**
     * Set asthmaDiagnosis
     *
     * @param boolean $asthmaDiagnosis
     *
     * @return SubmissionAsthma
     */
    public function setAsthmaDiagnosis($asthmaDiagnosis)
    {
        $this->asthmaDiagnosis = $asthmaDiagnosis;

        return $this;
    }

    /**
     * Get asthmaDiagnosis
     *
     * @return boolean
     */
    public function getAsthmaDiagnosis()
    {
        return $this->asthmaDiagnosis;
    }

    /**
     * Set asthmaDiseaseSeverity
     *
     * @param string $asthmaDiseaseSeverity
     *
     * @return SubmissionAsthma
     */
    public function setAsthmaDiseaseSeverity($asthmaDiseaseSeverity)
    {
        $this->asthmaDiseaseSeverity = $asthmaDiseaseSeverity;

        return $this;
    }

    /**
     * Get asthmaDiseaseSeverity
     *
     * @return string
     */
    public function getAsthmaDiseaseSeverity()
    {
        return $this->asthmaDiseaseSeverity;
    }

    /**
     * Set asthmaDiseaseSeverityDate
     *
     * @param \DateTime $asthmaDiseaseSeverityDate
     *
     * @return SubmissionAsthma
     */
    public function setAsthmaDiseaseSeverityDate($asthmaDiseaseSeverityDate)
    {
        $this->asthmaDiseaseSeverityDate = $asthmaDiseaseSeverityDate;

        return $this;
    }

    /**
     * Get asthmaDiseaseSeverityDate
     *
     * @return \DateTime
     */
    public function getAsthmaDiseaseSeverityDate()
    {
        return $this->asthmaDiseaseSeverityDate;
    }

    /**
     * Set spirometryEvalResults
     *
     * @param integer $spirometryEvalResults
     *
     * @return SubmissionAsthma
     */
    public function setSpirometryEvalResults($spirometryEvalResults)
    {
        $this->spirometryEvalResults = $spirometryEvalResults;

        return $this;
    }

    /**
     * Get spirometryEvalResults
     *
     * @return integer
     */
    public function getSpirometryEvalResults()
    {
        return $this->spirometryEvalResults;
    }

    /**
     * Set spirometryEvalDate
     *
     * @param \DateTime $spirometryEvalDate
     *
     * @return SubmissionAsthma
     */
    public function setSpirometryEvalDate($spirometryEvalDate)
    {
        $this->spirometryEvalDate = $spirometryEvalDate;

        return $this;
    }

    /**
     * Get spirometryEvalDate
     *
     * @return \DateTime
     */
    public function getSpirometryEvalDate()
    {
        return $this->spirometryEvalDate;
    }

    /**
     * Set sABAPrescribedDate
     *
     * @param \DateTime $sABAPrescribedDate
     *
     * @return SubmissionAsthma
     */
    public function setSABAPrescribedDate($sABAPrescribedDate)
    {
        $this->SABAPrescribedDate = $sABAPrescribedDate;

        return $this;
    }

    /**
     * Get sABAPrescribedDate
     *
     * @return \DateTime
     */
    public function getSABAPrescribedDate()
    {
        return $this->SABAPrescribedDate;
    }

    /**
     * Set inhaledCorticosteroidDispensed
     *
     * @param boolean $inhaledCorticosteroidDispensed
     *
     * @return SubmissionAsthma
     */
    public function setInhaledCorticosteroidDispensed($inhaledCorticosteroidDispensed)
    {
        $this->inhaledCorticosteroidDispensed = $inhaledCorticosteroidDispensed;

        return $this;
    }

    /**
     * Get inhaledCorticosteroidDispensed
     *
     * @return boolean
     */
    public function getInhaledCorticosteroidDispensed()
    {
        return $this->inhaledCorticosteroidDispensed;
    }

    /**
     * Set inhaledCorticosteroidDispensedDate
     *
     * @param \DateTime $inhaledCorticosteroidDispensedDate
     *
     * @return SubmissionAsthma
     */
    public function setInhaledCorticosteroidDispensedDate($inhaledCorticosteroidDispensedDate)
    {
        $this->inhaledCorticosteroidDispensedDate = $inhaledCorticosteroidDispensedDate;

        return $this;
    }

    /**
     * Get inhaledCorticosteroidDispensedDate
     *
     * @return \DateTime
     */
    public function getInhaledCorticosteroidDispensedDate()
    {
        return $this->inhaledCorticosteroidDispensedDate;
    }

    /**
     * Set inhaledCorticosteroidPrescribed
     *
     * @param boolean $inhaledCorticosteroidPrescribed
     *
     * @return SubmissionAsthma
     */
    public function setInhaledCorticosteroidPrescribed($inhaledCorticosteroidPrescribed)
    {
        $this->inhaledCorticosteroidPrescribed = $inhaledCorticosteroidPrescribed;

        return $this;
    }

    /**
     * Get inhaledCorticosteroidPrescribed
     *
     * @return boolean
     */
    public function getInhaledCorticosteroidPrescribed()
    {
        return $this->inhaledCorticosteroidPrescribed;
    }

    /**
     * Set inhaledCorticosteroidPrescribedDate
     *
     * @param \DateTime $inhaledCorticosteroidPrescribedDate
     *
     * @return SubmissionAsthma
     */
    public function setInhaledCorticosteroidPrescribedDate($inhaledCorticosteroidPrescribedDate)
    {
        $this->inhaledCorticosteroidPrescribedDate = $inhaledCorticosteroidPrescribedDate;

        return $this;
    }

    /**
     * Get inhaledCorticosteroidPrescribedDate
     *
     * @return \DateTime
     */
    public function getInhaledCorticosteroidPrescribedDate()
    {
        return $this->inhaledCorticosteroidPrescribedDate;
    }

    /**
     * Set oralCorticosteroidPrescribed
     *
     * @param \DateTime $oralCorticosteroidPrescribed
     *
     * @return SubmissionAsthma
     */
    public function setOralCorticosteroidPrescribed($oralCorticosteroidPrescribed)
    {
        $this->oralCorticosteroidPrescribed = $oralCorticosteroidPrescribed;

        return $this;
    }

    /**
     * Get oralCorticosteroidPrescribed
     *
     * @return \DateTime
     */
    public function getOralCorticosteroidPrescribed()
    {
        return $this->oralCorticosteroidPrescribed;
    }

    /**
     * Set oralCorticosteroidPrescribedDate
     *
     * @param \DateTime $oralCorticosteroidPrescribedDate
     *
     * @return SubmissionAsthma
     */
    public function setOralCorticosteroidPrescribedDate($oralCorticosteroidPrescribedDate)
    {
        $this->oralCorticosteroidPrescribedDate = $oralCorticosteroidPrescribedDate;

        return $this;
    }

    /**
     * Get oralCorticosteroidPrescribedDate
     *
     * @return \DateTime
     */
    public function getOralCorticosteroidPrescribedDate()
    {
        return $this->oralCorticosteroidPrescribedDate;
    }

    /**
     * Set inhaledBronchodilatorPrescribedDate
     *
     * @param \DateTime $inhaledBronchodilatorPrescribedDate
     *
     * @return SubmissionAsthma
     */
    public function setInhaledBronchodilatorPrescribedDate($inhaledBronchodilatorPrescribedDate)
    {
        $this->inhaledBronchodilatorPrescribedDate = $inhaledBronchodilatorPrescribedDate;

        return $this;
    }

    /**
     * Get inhaledBronchodilatorPrescribedDate
     *
     * @return \DateTime
     */
    public function getInhaledBronchodilatorPrescribedDate()
    {
        return $this->inhaledBronchodilatorPrescribedDate;
    }

    /**
     * Set oralBeta3AgonistsDispensedDate
     *
     * @param \DateTime $oralBeta3AgonistsDispensedDate
     *
     * @return SubmissionAsthma
     */
    public function setOralBeta3AgonistsDispensedDate($oralBeta3AgonistsDispensedDate)
    {
        $this->oralBeta3AgonistsDispensedDate = $oralBeta3AgonistsDispensedDate;

        return $this;
    }

    /**
     * Get oralBeta3AgonistsDispensedDate
     *
     * @return \DateTime
     */
    public function getOralBeta3AgonistsDispensedDate()
    {
        return $this->oralBeta3AgonistsDispensedDate;
    }

    /**
     * Set oralBeta3AgonistsPrescribedDate
     *
     * @param \DateTime $oralBeta3AgonistsPrescribedDate
     *
     * @return SubmissionAsthma
     */
    public function setOralBeta3AgonistsPrescribedDate($oralBeta3AgonistsPrescribedDate)
    {
        $this->oralBeta3AgonistsPrescribedDate = $oralBeta3AgonistsPrescribedDate;

        return $this;
    }

    /**
     * Get oralBeta3AgonistsPrescribedDate
     *
     * @return \DateTime
     */
    public function getOralBeta3AgonistsPrescribedDate()
    {
        return $this->oralBeta3AgonistsPrescribedDate;
    }

    /**
     * Set asthmaMedicationPrescribedDate
     *
     * @param \DateTime $asthmaMedicationPrescribedDate
     *
     * @return SubmissionAsthma
     */
    public function setAsthmaMedicationPrescribedDate($asthmaMedicationPrescribedDate)
    {
        $this->asthmaMedicationPrescribedDate = $asthmaMedicationPrescribedDate;

        return $this;
    }

    /**
     * Get asthmaMedicationPrescribedDate
     *
     * @return \DateTime
     */
    public function getAsthmaMedicationPrescribedDate()
    {
        return $this->asthmaMedicationPrescribedDate;
    }

    /**
     * Set asthmaExacerbationEpisode
     *
     * @param boolean $asthmaExacerbationEpisode
     *
     * @return SubmissionAsthma
     */
    public function setAsthmaExacerbationEpisode($asthmaExacerbationEpisode)
    {
        $this->asthmaExacerbationEpisode = $asthmaExacerbationEpisode;

        return $this;
    }

    /**
     * Get asthmaExacerbationEpisode
     *
     * @return boolean
     */
    public function getAsthmaExacerbationEpisode()
    {
        return $this->asthmaExacerbationEpisode;
    }

    /**
     * Set asthmaExacerbationSeverity
     *
     * @param string $asthmaExacerbationSeverity
     *
     * @return SubmissionAsthma
     */
    public function setAsthmaExacerbationSeverity($asthmaExacerbationSeverity)
    {
        $this->asthmaExacerbationSeverity = $asthmaExacerbationSeverity;

        return $this;
    }

    /**
     * Get asthmaExacerbationSeverity
     *
     * @return string
     */
    public function getAsthmaExacerbationSeverity()
    {
        return $this->asthmaExacerbationSeverity;
    }

    /**
     * Set asthmaExacerbationDate
     *
     * @param \DateTime $asthmaExacerbationDate
     *
     * @return SubmissionAsthma
     */
    public function setAsthmaExacerbationDate($asthmaExacerbationDate)
    {
        $this->asthmaExacerbationDate = $asthmaExacerbationDate;

        return $this;
    }

    /**
     * Get asthmaExacerbationDate
     *
     * @return \DateTime
     */
    public function getAsthmaExacerbationDate()
    {
        return $this->asthmaExacerbationDate;
    }

    /**
     * Set asthmaExacerbationReferralDate
     *
     * @param \DateTime $asthmaExacerbationReferralDate
     *
     * @return SubmissionAsthma
     */
    public function setAsthmaExacerbationReferralDate($asthmaExacerbationReferralDate)
    {
        $this->asthmaExacerbationReferralDate = $asthmaExacerbationReferralDate;

        return $this;
    }

    /**
     * Get asthmaExacerbationReferralDate
     *
     * @return \DateTime
     */
    public function getAsthmaExacerbationReferralDate()
    {
        return $this->asthmaExacerbationReferralDate;
    }

    /**
     * Set faceToFaceVisit1
     *
     * @param \DateTime $faceToFaceVisit1
     *
     * @return SubmissionAsthma
     */
    public function setFaceToFaceVisit1($faceToFaceVisit1)
    {
        $this->faceToFaceVisit1 = $faceToFaceVisit1;

        return $this;
    }

    /**
     * Get faceToFaceVisit1
     *
     * @return \DateTime
     */
    public function getFaceToFaceVisit1()
    {
        return $this->faceToFaceVisit1;
    }

    /**
     * Set faceToFaceVisit2
     *
     * @param \DateTime $faceToFaceVisit2
     *
     * @return SubmissionAsthma
     */
    public function setFaceToFaceVisit2($faceToFaceVisit2)
    {
        $this->faceToFaceVisit2 = $faceToFaceVisit2;

        return $this;
    }

    /**
     * Get faceToFaceVisit2
     *
     * @return \DateTime
     */
    public function getFaceToFaceVisit2()
    {
        return $this->faceToFaceVisit2;
    }

    /**
     * Set asthmaActionPlanDate
     *
     * @param \DateTime $asthmaActionPlanDate
     *
     * @return SubmissionAsthma
     */
    public function setAsthmaActionPlanDate($asthmaActionPlanDate)
    {
        $this->asthmaActionPlanDate = $asthmaActionPlanDate;

        return $this;
    }

    /**
     * Get asthmaActionPlanDate
     *
     * @return \DateTime
     */
    public function getAsthmaActionPlanDate()
    {
        return $this->asthmaActionPlanDate;
    }

    /**
     * Set asthmaSelfManagementPlanDate
     *
     * @param \DateTime $asthmaSelfManagementPlanDate
     *
     * @return SubmissionAsthma
     */
    public function setAsthmaSelfManagementPlanDate($asthmaSelfManagementPlanDate)
    {
        $this->asthmaSelfManagementPlanDate = $asthmaSelfManagementPlanDate;

        return $this;
    }

    /**
     * Get asthmaSelfManagementPlanDate
     *
     * @return \DateTime
     */
    public function getAsthmaSelfManagementPlanDate()
    {
        return $this->asthmaSelfManagementPlanDate;
    }

    /**
     * Set inhalerEducationDate
     *
     * @param \DateTime $inhalerEducationDate
     *
     * @return SubmissionAsthma
     */
    public function setInhalerEducationDate($inhalerEducationDate)
    {
        $this->inhalerEducationDate = $inhalerEducationDate;

        return $this;
    }

    /**
     * Get inhalerEducationDate
     *
     * @return \DateTime
     */
    public function getInhalerEducationDate()
    {
        return $this->inhalerEducationDate;
    }

    /**
     * Set tobaccoExposureStatus
     *
     * @param boolean $tobaccoExposureStatus
     *
     * @return SubmissionAsthma
     */
    public function setTobaccoExposureStatus($tobaccoExposureStatus)
    {
        $this->tobaccoExposureStatus = $tobaccoExposureStatus;

        return $this;
    }

    /**
     * Get tobaccoExposureStatus
     *
     * @return boolean
     */
    public function getTobaccoExposureStatus()
    {
        return $this->tobaccoExposureStatus;
    }

    /**
     * Set tobaccoExposureStatusDate
     *
     * @param \DateTime $tobaccoExposureStatusDate
     *
     * @return SubmissionAsthma
     */
    public function setTobaccoExposureStatusDate($tobaccoExposureStatusDate)
    {
        $this->tobaccoExposureStatusDate = $tobaccoExposureStatusDate;

        return $this;
    }

    /**
     * Get tobaccoExposureStatusDate
     *
     * @return \DateTime
     */
    public function getTobaccoExposureStatusDate()
    {
        return $this->tobaccoExposureStatusDate;
    }

    /**
     * Set tobaccoStatus
     *
     * @param boolean $tobaccoStatus
     *
     * @return SubmissionAsthma
     */
    public function setTobaccoStatus($tobaccoStatus)
    {
        $this->tobaccoStatus = $tobaccoStatus;

        return $this;
    }

    /**
     * Get tobaccoStatus
     *
     * @return boolean
     */
    public function getTobaccoStatus()
    {
        return $this->tobaccoStatus;
    }

    /**
     * Set tobaccoStatusAssessmentDate
     *
     * @param \DateTime $tobaccoStatusAssessmentDate
     *
     * @return SubmissionAsthma
     */
    public function setTobaccoStatusAssessmentDate($tobaccoStatusAssessmentDate)
    {
        $this->tobaccoStatusAssessmentDate = $tobaccoStatusAssessmentDate;

        return $this;
    }

    /**
     * Get tobaccoStatusAssessmentDate
     *
     * @return \DateTime
     */
    public function getTobaccoStatusAssessmentDate()
    {
        return $this->tobaccoStatusAssessmentDate;
    }

    /**
     * Set tobaccoCessationAdviceOrTreatmentDate
     *
     * @param \DateTime $tobaccoCessationAdviceOrTreatmentDate
     *
     * @return SubmissionAsthma
     */
    public function setTobaccoCessationAdviceOrTreatmentDate($tobaccoCessationAdviceOrTreatmentDate)
    {
        $this->tobaccoCessationAdviceOrTreatmentDate = $tobaccoCessationAdviceOrTreatmentDate;

        return $this;
    }

    /**
     * Get tobaccoCessationAdviceOrTreatmentDate
     *
     * @return \DateTime
     */
    public function getTobaccoCessationAdviceOrTreatmentDate()
    {
        return $this->tobaccoCessationAdviceOrTreatmentDate;
    }

    /**
     * Set influenzaImmunization
     *
     * @param integer $influenzaImmunization
     *
     * @return SubmissionAsthma
     */
    public function setInfluenzaImmunization($influenzaImmunization)
    {
        $this->influenzaImmunization = $influenzaImmunization;

        return $this;
    }

    /**
     * Get influenzaImmunization
     *
     * @return integer
     */
    public function getInfluenzaImmunization()
    {
        return $this->influenzaImmunization;
    }

    /**
     * Set influenzaImmunizationDate
     *
     * @param \DateTime $influenzaImmunizationDate
     *
     * @return SubmissionAsthma
     */
    public function setInfluenzaImmunizationDate($influenzaImmunizationDate)
    {
        $this->influenzaImmunizationDate = $influenzaImmunizationDate;

        return $this;
    }

    /**
     * Get influenzaImmunizationDate
     *
     * @return \DateTime
     */
    public function getInfluenzaImmunizationDate()
    {
        return $this->influenzaImmunizationDate;
    }

    /**
     * Set submissionId
     *
     * @param integer $submissionId
     *
     * @return SubmissionAsthma
     */
    public function setSubmissionId($submissionId)
    {
        $this->submission_id = $submissionId;

        return $this;
    }

    /**
     * Get submissionId
     *
     * @return integer
     */
    public function getSubmissionId()
    {
        return $this->submission_id;
    }

    /**
     * Set tempId
     *
     * @param integer $tempId
     *
     * @return SubmissionAsthma
     */
    public function setTempId($tempId)
    {
        $this->temp_id = $tempId;

        return $this;
    }

    /**
     * Get tempId
     *
     * @return integer
     */
    public function getTempId()
    {
        return $this->temp_id;
    }

    /**
     * Set submision
     *
     * @param \AppBundle\Entity\Submission $submision
     *
     * @return SubmissionAsthma
     */
    public function setSubmision(\AppBundle\Entity\Submission $submision = null)
    {
        $this->submision = $submision;

        return $this;
    }

    /**
     * Get submision
     *
     * @return \AppBundle\Entity\Submission
     */
    public function getSubmision()
    {
        return $this->submision;
    }
}
