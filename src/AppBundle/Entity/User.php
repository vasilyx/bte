<?php

namespace AppBundle\Entity;

use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * User
 */
class User implements UserInterface
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_WRONG_PASSWORD = 2;
    const STATUS_LONG_TIME_INACTIVE = 3;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var integer
     */
    private $status;

    /**
     * @var \DateTime
     */
    private $datetimeRegistration;

    /**
     * @var \DateTime
     */
    private $datetimeLastLogin;

    /**
     * @var string
     */
    private $forgottenPasswordToken;

    /**
     * @var string
     */
    private $confirmEmailToken;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $providers;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->providers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->modifiedAt = new \DateTime('now');
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return User
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set datetimeRegistration
     *
     * @param \DateTime $datetimeRegistration
     *
     * @return User
     */
    public function setDatetimeRegistration($datetimeRegistration)
    {
        $this->datetimeRegistration = $datetimeRegistration;

        return $this;
    }

    /**
     * Get datetimeRegistration
     *
     * @return \DateTime
     */
    public function getDatetimeRegistration()
    {
        return $this->datetimeRegistration;
    }

    /**
     * Set datetimeLastLogin
     *
     * @param \DateTime $datetimeLastLogin
     *
     * @return User
     */
    public function setDatetimeLastLogin($datetimeLastLogin)
    {
        $this->datetimeLastLogin = $datetimeLastLogin;

        return $this;
    }

    /**
     * Get datetimeLastLogin
     *
     * @return \DateTime
     */
    public function getDatetimeLastLogin()
    {
        return $this->datetimeLastLogin;
    }

    /**
     * Set forgottenPasswordToken
     *
     * @param string $forgottenPasswordToken
     *
     * @return User
     */
    public function setForgottenPasswordToken($forgottenPasswordToken)
    {
        $this->forgottenPasswordToken = $forgottenPasswordToken;

        return $this;
    }

    /**
     * Get forgottenPasswordToken
     *
     * @return string
     */
    public function getForgottenPasswordToken()
    {
        return $this->forgottenPasswordToken;
    }

    /**
     * Set confirmEmailToken
     *
     * @param string $confirmEmailToken
     *
     * @return User
     */
    public function setConfirmEmailToken($confirmEmailToken)
    {
        $this->confirmEmailToken = $confirmEmailToken;

        return $this;
    }

    /**
     * Get confirmEmailToken
     *
     * @return string
     */
    public function getConfirmEmailToken()
    {
        return $this->confirmEmailToken;
    }

    /**
     * Add provider
     *
     * @param \AppBundle\Entity\Provider $provider
     *
     * @return User
     */
    public function addProvider(\AppBundle\Entity\Provider $provider)
    {
        $this->providers[] = $provider;

        return $this;
    }

    /**
     * Remove provider
     *
     * @param \AppBundle\Entity\Provider $provider
     */
    public function removeProvider(\AppBundle\Entity\Provider $provider)
    {
        $this->providers->removeElement($provider);
    }

    /**
     * Get providers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProviders()
    {
        return $this->providers;
    }

    public function getRoles()
    {
        // return empty($this->roles)? array('ROLE_USER') : explode(',', $this->roles);
        return ['ROLE_EMAIL_NOT_CONFIRMED'];
    }

    public function getSalt()
    {
        return null;
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * @return mixed
     */
    public function getActiveApiToken()
    {
        return $this->activeApiToken;
    }

    /**
     * @param mixed $activeApiToken
     * @return $this
     */
    public function setActiveApiToken($activeApiToken)
    {
        $this->activeApiToken = $activeApiToken;
        return $this;
    }

    /**
     * @return $this
     */
    public function generateConfirmationToken()
    {
        $this->setConfirmEmailToken(
            base_convert(sha1(uniqid(mt_rand(), true)), 16, 36)
        );
        return $this;
    }

    /**
     * @return $this
     */
    public function generateForgotPasswordToken()
    {
        $this->setForgottenPasswordToken(
            base_convert(sha1(uniqid(mt_rand(), true)), 16, 36)
        );
        return $this;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $submissions;


    /**
     * Add submission
     *
     * @param \AppBundle\Entity\Submission $submission
     *
     * @return User
     */
    public function addSubmission(\AppBundle\Entity\Submission $submission)
    {
        $this->submissions[] = $submission;

        return $this;
    }

    /**
     * Remove submission
     *
     * @param \AppBundle\Entity\Submission $submission
     */
    public function removeSubmission(\AppBundle\Entity\Submission $submission)
    {
        $this->submissions->removeElement($submission);
    }

    /**
     * Get submissions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubmissions()
    {
        return $this->submissions;
    }
    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $modifiedAt;


    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set modifiedAt
     *
     * @return User
     */
    public function setModifiedAt()
    {
        $this->modifiedAt = new \DateTime('now');;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }
    /**
     * @var string
     */
    private $firstname;

    /**
     * @var string
     */
    private $lastname;


    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    public function getFullName()
    {
        return $this->getFirstname() . ' ' . $this->getLastname();
    }

    public function __toString(){
        return (string)$this->getUsername();
    }
    /**
     * @var boolean
     */
    private $hasAgreement;


    /**
     * Set hasAgreement
     *
     * @param boolean $hasAgreement
     *
     * @return User
     */
    public function setHasAgreement($hasAgreement)
    {
        $this->hasAgreement = $hasAgreement;

        return $this;
    }

    /**
     * Get hasAgreement
     *
     * @return boolean
     */
    public function getHasAgreement()
    {
        return $this->hasAgreement;
    }


    /**
     * @var boolean
     */
    private $wrongLoginQuantity;

    /**
     * Set wrongLoginQuantity
     *
     * @param integer $wrongLoginQuantity
     *
     * @return User
     */
    public function setWrongLoginQuantity($wrongLoginQuantity)
    {
        $this->wrongLoginQuantity = $wrongLoginQuantity;

        return $this;
    }

    /**
     * Get wrongLoginQuantity
     *
     * @return integer
     */
    public function getWrongLoginQuantity()
    {
        return $this->wrongLoginQuantity;
    }


    /**
     * @var string
     */
    private $twoFactorAuthenticationToken;

    /**
     * Set twoFactorAuthenticationToken
     *
     * @param string $twoFactorAuthenticationToken
     *
     * @return User
     */
    public function setTwoFactorAuthenticationToken($twoFactorAuthenticationToken)
    {
        $this->twoFactorAuthenticationToken = $twoFactorAuthenticationToken;

        return $this;
    }

    /**
     * Get twoFactorAuthenticationToken
     *
     * @return string
     */
    public function getTwoFactorAuthenticationToken()
    {
        return $this->twoFactorAuthenticationToken;
    }



    /**
     * @var boolean
     */
    private $isTest;

    /**
     * Set isTest
     *
     * @param boolean $isTest
     *
     * @return User
     */
    public function setIsTest($isTest)
    {
        $this->isTest = $isTest;

        return $this;
    }

    /**
     * Get isTest
     *
     * @return boolean
     */
    public function getIsTest()
    {
        return $this->isTest;
    }


    /**
     * @var \AppBundle\Entity\UserPractice
     */
    private $userPractice;


    /**
     * Set userPractice
     *
     * @param \AppBundle\Entity\UserPractice userPractice
     *
     * @return User
     */
    public function setUserPractice($userPractice)
    {
        $this->userPractice = $userPractice;

        return $this;
    }
    /**
     * Get userPractice
     *
     * @return \AppBundle\Entity\UserPractice
     */
    public function getUserPractice()
    {
        return $this->userPractice;
    }
    /**
     * @var string|null
     */
    private $notes;


    /**
     * Set notes.
     *
     * @param string|null $notes
     *
     * @return User
     */
    public function setNotes($notes = null)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes.
     *
     * @return string|null
     */
    public function getNotes()
    {
        return $this->notes;
    }
}
