<?php

namespace AppBundle\Entity;

/**
 * SubmissionHypertension
 */
class SubmissionHypertension
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $responsibleProviderID;

    /**
     * @var string
     */
    private $npi;

    /**
     * @var string
     */
    private $groupID;

    /**
     * @var string
     */
    private $individualGroup;

    /**
     * @var string
     */
    private $chartID;

    /**
     * @var \DateTime
     */
    private $lastVisitDate;

    /**
     * @var \DateTime
     */
    private $patientDOB;

    /**
     * @var integer
     */
    private $patientGender;

    /**
     * @var boolean
     */
    private $medicarePartB;

    /**
     * @var \DateTime
     */
    private $bloodPressureDate1;

    /**
     * @var integer
     */
    private $systolic1;

    /**
     * @var integer
     */
    private $diastolic1;

    /**
     * @var integer
     */
    private $systolic2;

    /**
     * @var integer
     */
    private $diastolic2;

    /**
     * @var \DateTime
     */
    private $bloodPressureDate2;

    /**
     * @var integer
     */
    private $hypertensionDiagnosis;

    /**
     * @var boolean
     */
    private $chronicKidneyDiseaseDiagnosis;

    /**
     * @var boolean
     */
    private $diabetesDiagnosis;

    /**
     * @var boolean
     */
    private $aceiArbTherapy;

    /**
     * @var boolean
     */
    private $calciumChannelBlocker;

    /**
     * @var boolean
     */
    private $thiazide;

    /**
     * @var boolean
     */
    private $nephropathyDiagnosis;

    /**
     * @var boolean
     */
    private $nephropathyScreening;

    /**
     * @var \DateTime
     */
    private $nephropathyScreeningDate;

    /**
     * @var boolean
     */
    private $esrdPatient;

    /**
     * @var boolean
     */
    private $dialysisPatient;

    /**
     * @var boolean
     */
    private $creatinine;

    /**
     * @var \DateTime
     */
    private $creatinineDate;

    /**
     * @var integer
     */
    private $tobaccoStatus;

    /**
     * @var \DateTime
     */
    private $tobaccoStatusAssessmentDate;

    /**
     * @var \DateTime
     */
    private $tobaccoCessationAdviceOrTreatmentDate;

    /**
     * @var float
     */
    private $bmiValue;

    /**
     * @var \DateTime
     */
    private $bmiValueDate;

    /**
     * @var boolean
     */
    private $DASHDietCounseling;

    /**
     * @var \DateTime
     */
    private $DASHDietDate;

    /**
     * @var boolean
     */
    private $DASHSodiumCounseling;

    /**
     * @var \DateTime
     */
    private $DASHSodiumDate;

    /**
     * @var boolean
     */
    private $activityStatus;

    /**
     * @var \DateTime
     */
    private $activityStatusDate;

    /**
     * @var boolean
     */
    private $activityCounseling;

    /**
     * @var \DateTime
     */
    private $activityCounselingDate;

    /**
     * @var integer
     */
    private $submission_id;

    /**
     * @var integer
     */
    private $temp_id;

    /**
     * @var \AppBundle\Entity\Submission
     */
    private $submision;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set responsibleProviderID
     *
     * @param integer $responsibleProviderID
     *
     * @return SubmissionHypertension
     */
    public function setResponsibleProviderID($responsibleProviderID)
    {
        $this->responsibleProviderID = $responsibleProviderID;

        return $this;
    }

    /**
     * Get responsibleProviderID
     *
     * @return integer
     */
    public function getResponsibleProviderID()
    {
        return $this->responsibleProviderID;
    }

    /**
     * Set npi
     *
     * @param string $npi
     *
     * @return SubmissionHypertension
     */
    public function setNpi($npi)
    {
        $this->npi = $npi;

        return $this;
    }

    /**
     * Get npi
     *
     * @return string
     */
    public function getNpi()
    {
        return $this->npi;
    }

    /**
     * Set groupID
     *
     * @param string $groupID
     *
     * @return SubmissionHypertension
     */
    public function setGroupID($groupID)
    {
        $this->groupID = $groupID;

        return $this;
    }

    /**
     * Get groupID
     *
     * @return string
     */
    public function getGroupID()
    {
        return $this->groupID;
    }

    /**
     * Set individualGroup
     *
     * @param string $individualGroup
     *
     * @return SubmissionHypertension
     */
    public function setIndividualGroup($individualGroup)
    {
        $this->individualGroup = $individualGroup;

        return $this;
    }

    /**
     * Get individualGroup
     *
     * @return string
     */
    public function getIndividualGroup()
    {
        return $this->individualGroup;
    }

    /**
     * Set chartID
     *
     * @param string $chartID
     *
     * @return SubmissionHypertension
     */
    public function setChartID($chartID)
    {
        $this->chartID = $chartID;

        return $this;
    }

    /**
     * Get chartID
     *
     * @return string
     */
    public function getChartID()
    {
        return $this->chartID;
    }

    /**
     * Set lastVisitDate
     *
     * @param \DateTime $lastVisitDate
     *
     * @return SubmissionHypertension
     */
    public function setLastVisitDate($lastVisitDate)
    {
        $this->lastVisitDate = $lastVisitDate;

        return $this;
    }

    /**
     * Get lastVisitDate
     *
     * @return \DateTime
     */
    public function getLastVisitDate()
    {
        return $this->lastVisitDate;
    }

    /**
     * Set patientDOB
     *
     * @param \DateTime $patientDOB
     *
     * @return SubmissionHypertension
     */
    public function setPatientDOB($patientDOB)
    {
        $this->patientDOB = $patientDOB;

        return $this;
    }

    /**
     * Get patientDOB
     *
     * @return \DateTime
     */
    public function getPatientDOB()
    {
        return $this->patientDOB;
    }

    /**
     * Set patientGender
     *
     * @param integer $patientGender
     *
     * @return SubmissionHypertension
     */
    public function setPatientGender($patientGender)
    {
        $this->patientGender = $patientGender;

        return $this;
    }

    /**
     * Get patientGender
     *
     * @return integer
     */
    public function getPatientGender()
    {
        return $this->patientGender;
    }

    /**
     * Set medicarePartB
     *
     * @param boolean $medicarePartB
     *
     * @return SubmissionHypertension
     */
    public function setMedicarePartB($medicarePartB)
    {
        $this->medicarePartB = $medicarePartB;

        return $this;
    }

    /**
     * Get medicarePartB
     *
     * @return boolean
     */
    public function getMedicarePartB()
    {
        return $this->medicarePartB;
    }

    /**
     * Set bloodPressureDate1
     *
     * @param \DateTime $bloodPressureDate1
     *
     * @return SubmissionHypertension
     */
    public function setBloodPressureDate1($bloodPressureDate1)
    {
        $this->bloodPressureDate1 = $bloodPressureDate1;

        return $this;
    }

    /**
     * Get bloodPressureDate1
     *
     * @return \DateTime
     */
    public function getBloodPressureDate1()
    {
        return $this->bloodPressureDate1;
    }

    /**
     * Set systolic1
     *
     * @param integer $systolic1
     *
     * @return SubmissionHypertension
     */
    public function setSystolic1($systolic1)
    {
        $this->systolic1 = $systolic1;

        return $this;
    }

    /**
     * Get systolic1
     *
     * @return integer
     */
    public function getSystolic1()
    {
        return $this->systolic1;
    }

    /**
     * Set diastolic1
     *
     * @param integer $diastolic1
     *
     * @return SubmissionHypertension
     */
    public function setDiastolic1($diastolic1)
    {
        $this->diastolic1 = $diastolic1;

        return $this;
    }

    /**
     * Get diastolic1
     *
     * @return integer
     */
    public function getDiastolic1()
    {
        return $this->diastolic1;
    }

    /**
     * Set systolic2
     *
     * @param integer $systolic2
     *
     * @return SubmissionHypertension
     */
    public function setSystolic2($systolic2)
    {
        $this->systolic2 = $systolic2;

        return $this;
    }

    /**
     * Get systolic2
     *
     * @return integer
     */
    public function getSystolic2()
    {
        return $this->systolic2;
    }

    /**
     * Set diastolic2
     *
     * @param integer $diastolic2
     *
     * @return SubmissionHypertension
     */
    public function setDiastolic2($diastolic2)
    {
        $this->diastolic2 = $diastolic2;

        return $this;
    }

    /**
     * Get diastolic2
     *
     * @return integer
     */
    public function getDiastolic2()
    {
        return $this->diastolic2;
    }

    /**
     * Set bloodPressureDate2
     *
     * @param \DateTime $bloodPressureDate2
     *
     * @return SubmissionHypertension
     */
    public function setBloodPressureDate2($bloodPressureDate2)
    {
        $this->bloodPressureDate2 = $bloodPressureDate2;

        return $this;
    }

    /**
     * Get bloodPressureDate2
     *
     * @return \DateTime
     */
    public function getBloodPressureDate2()
    {
        return $this->bloodPressureDate2;
    }

    /**
     * Set hypertensionDiagnosis
     *
     * @param integer $hypertensionDiagnosis
     *
     * @return SubmissionHypertension
     */
    public function setHypertensionDiagnosis($hypertensionDiagnosis)
    {
        $this->hypertensionDiagnosis = $hypertensionDiagnosis;

        return $this;
    }

    /**
     * Get hypertensionDiagnosis
     *
     * @return integer
     */
    public function getHypertensionDiagnosis()
    {
        return $this->hypertensionDiagnosis;
    }

    /**
     * Set chronicKidneyDiseaseDiagnosis
     *
     * @param boolean $chronicKidneyDiseaseDiagnosis
     *
     * @return SubmissionHypertension
     */
    public function setChronicKidneyDiseaseDiagnosis($chronicKidneyDiseaseDiagnosis)
    {
        $this->chronicKidneyDiseaseDiagnosis = $chronicKidneyDiseaseDiagnosis;

        return $this;
    }

    /**
     * Get chronicKidneyDiseaseDiagnosis
     *
     * @return boolean
     */
    public function getChronicKidneyDiseaseDiagnosis()
    {
        return $this->chronicKidneyDiseaseDiagnosis;
    }

    /**
     * Set diabetesDiagnosis
     *
     * @param boolean $diabetesDiagnosis
     *
     * @return SubmissionHypertension
     */
    public function setDiabetesDiagnosis($diabetesDiagnosis)
    {
        $this->diabetesDiagnosis = $diabetesDiagnosis;

        return $this;
    }

    /**
     * Get diabetesDiagnosis
     *
     * @return boolean
     */
    public function getDiabetesDiagnosis()
    {
        return $this->diabetesDiagnosis;
    }

    /**
     * Set aceiArbTherapy
     *
     * @param boolean $aceiArbTherapy
     *
     * @return SubmissionHypertension
     */
    public function setAceiArbTherapy($aceiArbTherapy)
    {
        $this->aceiArbTherapy = $aceiArbTherapy;

        return $this;
    }

    /**
     * Get aceiArbTherapy
     *
     * @return boolean
     */
    public function getAceiArbTherapy()
    {
        return $this->aceiArbTherapy;
    }

    /**
     * Set calciumChannelBlocker
     *
     * @param boolean $calciumChannelBlocker
     *
     * @return SubmissionHypertension
     */
    public function setCalciumChannelBlocker($calciumChannelBlocker)
    {
        $this->calciumChannelBlocker = $calciumChannelBlocker;

        return $this;
    }

    /**
     * Get calciumChannelBlocker
     *
     * @return boolean
     */
    public function getCalciumChannelBlocker()
    {
        return $this->calciumChannelBlocker;
    }

    /**
     * Set thiazide
     *
     * @param boolean $thiazide
     *
     * @return SubmissionHypertension
     */
    public function setThiazide($thiazide)
    {
        $this->thiazide = $thiazide;

        return $this;
    }

    /**
     * Get thiazide
     *
     * @return boolean
     */
    public function getThiazide()
    {
        return $this->thiazide;
    }

    /**
     * Set nephropathyDiagnosis
     *
     * @param boolean $nephropathyDiagnosis
     *
     * @return SubmissionHypertension
     */
    public function setNephropathyDiagnosis($nephropathyDiagnosis)
    {
        $this->nephropathyDiagnosis = $nephropathyDiagnosis;

        return $this;
    }

    /**
     * Get nephropathyDiagnosis
     *
     * @return boolean
     */
    public function getNephropathyDiagnosis()
    {
        return $this->nephropathyDiagnosis;
    }

    /**
     * Set nephropathyScreening
     *
     * @param boolean $nephropathyScreening
     *
     * @return SubmissionHypertension
     */
    public function setNephropathyScreening($nephropathyScreening)
    {
        $this->nephropathyScreening = $nephropathyScreening;

        return $this;
    }

    /**
     * Get nephropathyScreening
     *
     * @return boolean
     */
    public function getNephropathyScreening()
    {
        return $this->nephropathyScreening;
    }

    /**
     * Set nephropathyScreeningDate
     *
     * @param \DateTime $nephropathyScreeningDate
     *
     * @return SubmissionHypertension
     */
    public function setNephropathyScreeningDate($nephropathyScreeningDate)
    {
        $this->nephropathyScreeningDate = $nephropathyScreeningDate;

        return $this;
    }

    /**
     * Get nephropathyScreeningDate
     *
     * @return \DateTime
     */
    public function getNephropathyScreeningDate()
    {
        return $this->nephropathyScreeningDate;
    }

    /**
     * Set esrdPatient
     *
     * @param boolean $esrdPatient
     *
     * @return SubmissionHypertension
     */
    public function setEsrdPatient($esrdPatient)
    {
        $this->esrdPatient = $esrdPatient;

        return $this;
    }

    /**
     * Get esrdPatient
     *
     * @return boolean
     */
    public function getEsrdPatient()
    {
        return $this->esrdPatient;
    }

    /**
     * Set dialysisPatient
     *
     * @param boolean $dialysisPatient
     *
     * @return SubmissionHypertension
     */
    public function setDialysisPatient($dialysisPatient)
    {
        $this->dialysisPatient = $dialysisPatient;

        return $this;
    }

    /**
     * Get dialysisPatient
     *
     * @return boolean
     */
    public function getDialysisPatient()
    {
        return $this->dialysisPatient;
    }

    /**
     * Set creatinine
     *
     * @param boolean $creatinine
     *
     * @return SubmissionHypertension
     */
    public function setCreatinine($creatinine)
    {
        $this->creatinine = $creatinine;

        return $this;
    }

    /**
     * Get creatinine
     *
     * @return boolean
     */
    public function getCreatinine()
    {
        return $this->creatinine;
    }

    /**
     * Set creatinineDate
     *
     * @param \DateTime $creatinineDate
     *
     * @return SubmissionHypertension
     */
    public function setCreatinineDate($creatinineDate)
    {
        $this->creatinineDate = $creatinineDate;

        return $this;
    }

    /**
     * Get creatinineDate
     *
     * @return \DateTime
     */
    public function getCreatinineDate()
    {
        return $this->creatinineDate;
    }

    /**
     * Set tobaccoStatus
     *
     * @param integer $tobaccoStatus
     *
     * @return SubmissionHypertension
     */
    public function setTobaccoStatus($tobaccoStatus)
    {
        $this->tobaccoStatus = $tobaccoStatus;

        return $this;
    }

    /**
     * Get tobaccoStatus
     *
     * @return integer
     */
    public function getTobaccoStatus()
    {
        return $this->tobaccoStatus;
    }

    /**
     * Set tobaccoStatusAssessmentDate
     *
     * @param \DateTime $tobaccoStatusAssessmentDate
     *
     * @return SubmissionHypertension
     */
    public function setTobaccoStatusAssessmentDate($tobaccoStatusAssessmentDate)
    {
        $this->tobaccoStatusAssessmentDate = $tobaccoStatusAssessmentDate;

        return $this;
    }

    /**
     * Get tobaccoStatusAssessmentDate
     *
     * @return \DateTime
     */
    public function getTobaccoStatusAssessmentDate()
    {
        return $this->tobaccoStatusAssessmentDate;
    }

    /**
     * Set tobaccoCessationAdviceOrTreatmentDate
     *
     * @param \DateTime $tobaccoCessationAdviceOrTreatmentDate
     *
     * @return SubmissionHypertension
     */
    public function setTobaccoCessationAdviceOrTreatmentDate($tobaccoCessationAdviceOrTreatmentDate)
    {
        $this->tobaccoCessationAdviceOrTreatmentDate = $tobaccoCessationAdviceOrTreatmentDate;

        return $this;
    }

    /**
     * Get tobaccoCessationAdviceOrTreatmentDate
     *
     * @return \DateTime
     */
    public function getTobaccoCessationAdviceOrTreatmentDate()
    {
        return $this->tobaccoCessationAdviceOrTreatmentDate;
    }

    /**
     * Set bmiValue
     *
     * @param float $bmiValue
     *
     * @return SubmissionHypertension
     */
    public function setBmiValue($bmiValue)
    {
        $this->bmiValue = $bmiValue;

        return $this;
    }

    /**
     * Get bmiValue
     *
     * @return float
     */
    public function getBmiValue()
    {
        return $this->bmiValue;
    }

    /**
     * Set bmiValueDate
     *
     * @param \DateTime $bmiValueDate
     *
     * @return SubmissionHypertension
     */
    public function setBmiValueDate($bmiValueDate)
    {
        $this->bmiValueDate = $bmiValueDate;

        return $this;
    }

    /**
     * Get bmiValueDate
     *
     * @return \DateTime
     */
    public function getBmiValueDate()
    {
        return $this->bmiValueDate;
    }

    /**
     * Set dASHDietCounseling
     *
     * @param boolean $dASHDietCounseling
     *
     * @return SubmissionHypertension
     */
    public function setDASHDietCounseling($dASHDietCounseling)
    {
        $this->DASHDietCounseling = $dASHDietCounseling;

        return $this;
    }

    /**
     * Get dASHDietCounseling
     *
     * @return boolean
     */
    public function getDASHDietCounseling()
    {
        return $this->DASHDietCounseling;
    }

    /**
     * Set dASHDietDate
     *
     * @param \DateTime $dASHDietDate
     *
     * @return SubmissionHypertension
     */
    public function setDASHDietDate($dASHDietDate)
    {
        $this->DASHDietDate = $dASHDietDate;

        return $this;
    }

    /**
     * Get dASHDietDate
     *
     * @return \DateTime
     */
    public function getDASHDietDate()
    {
        return $this->DASHDietDate;
    }

    /**
     * Set dASHSodiumCounseling
     *
     * @param boolean $dASHSodiumCounseling
     *
     * @return SubmissionHypertension
     */
    public function setDASHSodiumCounseling($dASHSodiumCounseling)
    {
        $this->DASHSodiumCounseling = $dASHSodiumCounseling;

        return $this;
    }

    /**
     * Get dASHSodiumCounseling
     *
     * @return boolean
     */
    public function getDASHSodiumCounseling()
    {
        return $this->DASHSodiumCounseling;
    }

    /**
     * Set dASHSodiumDate
     *
     * @param \DateTime $dASHSodiumDate
     *
     * @return SubmissionHypertension
     */
    public function setDASHSodiumDate($dASHSodiumDate)
    {
        $this->DASHSodiumDate = $dASHSodiumDate;

        return $this;
    }

    /**
     * Get dASHSodiumDate
     *
     * @return \DateTime
     */
    public function getDASHSodiumDate()
    {
        return $this->DASHSodiumDate;
    }

    /**
     * Set activityStatus
     *
     * @param boolean $activityStatus
     *
     * @return SubmissionHypertension
     */
    public function setActivityStatus($activityStatus)
    {
        $this->activityStatus = $activityStatus;

        return $this;
    }

    /**
     * Get activityStatus
     *
     * @return boolean
     */
    public function getActivityStatus()
    {
        return $this->activityStatus;
    }

    /**
     * Set activityStatusDate
     *
     * @param \DateTime $activityStatusDate
     *
     * @return SubmissionHypertension
     */
    public function setActivityStatusDate($activityStatusDate)
    {
        $this->activityStatusDate = $activityStatusDate;

        return $this;
    }

    /**
     * Get activityStatusDate
     *
     * @return \DateTime
     */
    public function getActivityStatusDate()
    {
        return $this->activityStatusDate;
    }

    /**
     * Set activityCounseling
     *
     * @param boolean $activityCounseling
     *
     * @return SubmissionHypertension
     */
    public function setActivityCounseling($activityCounseling)
    {
        $this->activityCounseling = $activityCounseling;

        return $this;
    }

    /**
     * Get activityCounseling
     *
     * @return boolean
     */
    public function getActivityCounseling()
    {
        return $this->activityCounseling;
    }

    /**
     * Set activityCounselingDate
     *
     * @param \DateTime $activityCounselingDate
     *
     * @return SubmissionHypertension
     */
    public function setActivityCounselingDate($activityCounselingDate)
    {
        $this->activityCounselingDate = $activityCounselingDate;

        return $this;
    }

    /**
     * Get activityCounselingDate
     *
     * @return \DateTime
     */
    public function getActivityCounselingDate()
    {
        return $this->activityCounselingDate;
    }

    /**
     * Set submissionId
     *
     * @param integer $submissionId
     *
     * @return SubmissionHypertension
     */
    public function setSubmissionId($submissionId)
    {
        $this->submission_id = $submissionId;

        return $this;
    }

    /**
     * Get submissionId
     *
     * @return integer
     */
    public function getSubmissionId()
    {
        return $this->submission_id;
    }

    /**
     * Set tempId
     *
     * @param integer $tempId
     *
     * @return SubmissionHypertension
     */
    public function setTempId($tempId)
    {
        $this->temp_id = $tempId;

        return $this;
    }

    /**
     * Get tempId
     *
     * @return integer
     */
    public function getTempId()
    {
        return $this->temp_id;
    }

    /**
     * Set submision
     *
     * @param \AppBundle\Entity\Submission $submision
     *
     * @return SubmissionHypertension
     */
    public function setSubmision(\AppBundle\Entity\Submission $submision = null)
    {
        $this->submision = $submision;

        return $this;
    }

    /**
     * Get submision
     *
     * @return \AppBundle\Entity\Submission
     */
    public function getSubmision()
    {
        return $this->submision;
    }
}
