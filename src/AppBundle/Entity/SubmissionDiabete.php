<?php

namespace AppBundle\Entity;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * SubmissionDiabete
 */
class SubmissionDiabete
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $ResponsibleProviderID;

    /**
     * @var integer
     */
    private $npi;

    /**
     * @var integer
     */
    private $groupID;

    /**
     * @var string
     */
    private $individualGroup;

    /**
     * @var integer
     */
    private $ChartID;

    /**
     * @var \DateTime
     */
    private $lastVisitDate;

    /**
     * @var \DateTime
     */
    private $patientDOB;


    /**
     * @var string
     */
    private $patientGender;

    /**
     * @var string
     */
    private $patientRace;

    /**
     * @var boolean
     */
    private $medicarePartB;

    /**
     * @var boolean
     */
    private $diabetesDiagnosis;

    /**
     * @var float
     */
    private $HbA1c1;

    /**
     * @var \DateTime
     */
    private $HbA1cDate1;

    /**
     * @var float
     */
    private $HbA1c2;

    /**
     * @var \DateTime
     */
    private $HbA1cDate2;

    /**
     * @var integer
     */
    private $LDLLevel;

    /**
     * @var integer
     */
    private $LDLDate;

    /**
     * @var boolean
     */
    private $statinStatus;

    /**
     * @var integer
     */
    private $systolic1;

    /**
     * @var integer
     */
    private $diastolic1;

    /**
     * @var \DateTime
     */
    private $bloodPressureDate1;

    /**
     * @var integer
     */
    private $systolic2;

    /**
     * @var integer
     */
    private $diastolic2;

    /**
     * @var \DateTime
     */
    private $bloodPressureDate2;

    /**
     * @var integer
     */
    private $tobaccoStatus;

    /**
     * @var \DateTime
     */
    private $tobaccoStatusAssessmentDate;

    /**
     * @var \DateTime
     */
    private $tobaccoCessationAdviceOrTreatmentDate;

    /**
     * @var boolean
     */
    private $footExam;

    /**
     * @var \DateTime
     */
    private $footExamDate;

    /**
     * @var boolean
     */
    private $bilateralAmputation;

    /**
     * @var boolean
     */
    private $retinopathy;

    /**
     * @var \DateTime
     */
    private $retinalExamDate;

    /**
     * @var boolean
     */
    private $blindness;

    /**
     * @var boolean
     */
    private $nephropathyDiagnosis;

    /**
     * @var boolean
     */
    private $nephropathyEvidence;

    /**
     * @var integer
     */
    private $microalbuminuriaLab;

    /**
     * @var \DateTime
     */
    private $microalbuminuriaDate;

    /**
     * @var integer
     */
    private $macroalbuminuriaLab;

    /**
     * @var \DateTime
     */
    private $macroalbuminuriaDate;

    /**
     * @var boolean
     */
    private $hypertensionDiagnosis;

    /**
     * @var boolean
     */
    private $aceiArbTherapy;

    /**
     * @var float
     */
    private $bmiCalculation;

    /**
     * @var \DateTime
     */
    private $bmiDate;

    /**
     * @var boolean
     */
    private $bmiCounseling;

    /**
     * @var \DateTime
     */
    private $bmiCounselingDate;

    /**
     * @var boolean
     */
    private $ascvdAssessment;

    /**
     * @var integer
     */
    private $ascvdResults;

    /**
     * @var \DateTime
     */
    private $ascvdDate;

    /**
     * @var integer
     */
    private $aspirinTherapy;

    /**
     * @var boolean
     */
    private $aspirinUseConsidered;

    /**
     * @var boolean
     */
    private $PHQ2screening;

    /**
     * @var \DateTime
     */
    private $PHQ2screeningDate;

    /**
     * @var boolean
     */
    private $PHQ9screening;

    /**
     * @var \DateTime
     */
    private $PHQ9screeningDate;

    /**
     * @var integer
     */
    private $submission_id;

    /**
     * @var \AppBundle\Entity\Submission
     */
    private $submision;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set responsibleProviderID
     *
     * @param integer $responsibleProviderID
     *
     * @return SubmissionDiabete
     */
    public function setResponsibleProviderID($responsibleProviderID)
    {
        $this->ResponsibleProviderID = $responsibleProviderID;

        return $this;
    }

    /**
     * Get responsibleProviderID
     *
     * @return integer
     */
    public function getResponsibleProviderID()
    {
        return $this->ResponsibleProviderID;
    }

    /**
     * Set npi
     *
     * @param integer $npi
     *
     * @return SubmissionDiabete
     */
    public function setNpi($npi)
    {
        $this->npi = $npi;

        return $this;
    }

    /**
     * Get npi
     *
     * @return integer
     */
    public function getNpi()
    {
        return $this->npi;
    }

    /**
     * Set groupID
     *
     * @param integer $groupID
     *
     * @return SubmissionDiabete
     */
    public function setGroupID($groupID)
    {
        $this->groupID = $groupID;

        return $this;
    }

    /**
     * Get groupID
     *
     * @return integer
     */
    public function getGroupID()
    {
        return $this->groupID;
    }

    /**
     * Set individualGroup
     *
     * @param string $individualGroup
     *
     * @return SubmissionDiabete
     */
    public function setIndividualGroup($individualGroup)
    {
        $this->individualGroup = $individualGroup;

        return $this;
    }

    /**
     * Get individualGroup
     *
     * @return string
     */
    public function getIndividualGroup()
    {
        return $this->individualGroup;
    }

    /**
     * Set chartID
     *
     * @param integer $chartID
     *
     * @return SubmissionDiabete
     */
    public function setChartID($chartID)
    {
        $this->ChartID = $chartID;

        return $this;
    }

    /**
     * Get chartID
     *
     * @return integer
     */
    public function getChartID()
    {
        return $this->ChartID;
    }

    /**
     * Set lastVisitDate
     *
     * @param \DateTime $lastVisitDate
     *
     * @return SubmissionDiabete
     */
    public function setLastVisitDate($lastVisitDate)
    {
        $this->lastVisitDate = $lastVisitDate;

        return $this;
    }

    /**
     * Get lastVisitDate
     *
     * @return \DateTime
     */
    public function getLastVisitDate()
    {
        return $this->lastVisitDate;
    }

    /**
     * Set patientDOB
     *
     * @param \DateTime $patientDOB
     *
     * @return SubmissionHeartFailure
     */
    public function setPatientDOB($patientDOB)
    {
        $this->patientDOB = $patientDOB;

        return $this;
    }

    /**
     * Get patientDOB
     *
     * @return \DateTime
     */
    public function getPatientDOB()
    {
        return $this->patientDOB;
    }

    /**
     * Set patientGender
     *
     * @param string $patientGender
     *
     * @return SubmissionDiabete
     */
    public function setPatientGender($patientGender)
    {
        $this->patientGender = $patientGender;
    }

    /**
     * Get patientGender
     * 1 - Female, 0 - Male
     * @return string
     */
    public function getPatientGender()
    {
        return $this->patientGender;
    }

    /**
     * Set patientRace
     *
     * @param string $patientRace
     *
     * @return SubmissionDiabete
     */
    public function setPatientRace($patientRace)
    {
        $this->patientRace = $patientRace;

        return $this;
    }

    /**
     * Get patientRace
     *
     * @return string
     */
    public function getPatientRace()
    {
        return $this->patientRace;
    }

    /**
     * Set medicarePartB
     *
     * @param boolean $medicarePartB
     *
     * @return SubmissionDiabete
     */
    public function setMedicarePartB($medicarePartB)
    {
        $this->medicarePartB = $medicarePartB;

        return $this;
    }

    /**
     * Get medicarePartB
     *
     * @return boolean
     */
    public function getMedicarePartB()
    {
        return $this->medicarePartB;
    }

    /**
     * Set diabetesDiagnosis
     *
     * @param boolean $diabetesDiagnosis
     *
     * @return SubmissionDiabete
     */
    public function setDiabetesDiagnosis($diabetesDiagnosis)
    {
        $this->diabetesDiagnosis = $diabetesDiagnosis;

        return $this;
    }

    /**
     * Get diabetesDiagnosis
     *
     * @return boolean
     */
    public function getDiabetesDiagnosis()
    {
        return $this->diabetesDiagnosis;
    }

    /**
     * Set hbA1c1
     *
     * @param float $hbA1c1
     *
     * @return SubmissionDiabete
     */
    public function setHbA1c1($hbA1c1)
    {
        $this->HbA1c1 = $hbA1c1;

        return $this;
    }

    /**
     * Get hbA1c1
     *
     * @return float
     */
    public function getHbA1c1()
    {
        return $this->HbA1c1;
    }

    /**
     * Set hbA1cDate1
     *
     * @param \DateTime $hbA1cDate1
     *
     * @return SubmissionDiabete
     */
    public function setHbA1cDate1($hbA1cDate1)
    {
        $this->HbA1cDate1 = $hbA1cDate1;

        return $this;
    }

    /**
     * Get hbA1cDate1
     *
     * @return \DateTime
     */
    public function getHbA1cDate1()
    {
        return $this->HbA1cDate1;
    }

    /**
     * Set hbA1c2
     *
     * @param float $hbA1c2
     *
     * @return SubmissionDiabete
     */
    public function setHbA1c2($hbA1c2)
    {
        $this->HbA1c2 = $hbA1c2;

        return $this;
    }

    /**
     * Get hbA1c2
     *
     * @return float
     */
    public function getHbA1c2()
    {
        return $this->HbA1c2;
    }

    /**
     * Set hbA1cDate2
     *
     * @param \DateTime $hbA1cDate2
     *
     * @return SubmissionDiabete
     */
    public function setHbA1cDate2($hbA1cDate2)
    {
        $this->HbA1cDate2 = $hbA1cDate2;

        return $this;
    }

    /**
     * Get hbA1cDate2
     *
     * @return \DateTime
     */
    public function getHbA1cDate2()
    {
        return $this->HbA1cDate2;
    }

    /**
     * Set lDLLevel
     *
     * @param integer $lDLLevel
     *
     * @return SubmissionDiabete
     */
    public function setLDLLevel( $lDLLevel)
    {
        $this->LDLLevel = $lDLLevel;

        return $this;
    }

    /**
     * Get lDLLevel
     *
     * @return integer
     */
    public function getLDLLevel()
    {
        return $this->LDLLevel;
    }

    /**
     * Set lDLDate
     *
     * @param \DateTime $lDLDate
     *
     * @return SubmissionDiabete
     */
    public function setLDLDate($lDLDate)
    {
        $this->LDLDate = $lDLDate;

        return $this;
    }

    /**
     * Get lDLDate
     *
     * @return \DateTime
     */
    public function getLDLDate()
    {
        return $this->LDLDate;
    }

    /**
     * Set statinStatus
     *
     * @param boolean $statinStatus
     *
     * @return SubmissionDiabete
     */
    public function setStatinStatus($statinStatus)
    {
        $this->statinStatus = $statinStatus;

        return $this;
    }

    /**
     * Get statinStatus
     *
     * @return boolean
     */
    public function getStatinStatus()
    {
        return $this->statinStatus;
    }

    /**
     * Set systolic1
     *
     * @param integer $systolic1
     *
     * @return SubmissionDiabete
     */
    public function setSystolic1($systolic1)
    {
        $this->systolic1 = $systolic1;

        return $this;
    }

    /**
     * Get systolic1
     *
     * @return integer
     */
    public function getSystolic1()
    {
        return $this->systolic1;
    }

    /**
     * Set diastolic1
     *
     * @param integer $diastolic1
     *
     * @return SubmissionDiabete
     */
    public function setDiastolic1($diastolic1)
    {
        $this->diastolic1 = $diastolic1;

        return $this;
    }

    /**
     * Get diastolic1
     *
     * @return integer
     */
    public function getDiastolic1()
    {
        return $this->diastolic1;
    }

    /**
     * Set bloodPressureDate1
     *
     * @param \DateTime $bloodPressureDate1
     *
     * @return SubmissionDiabete
     */
    public function setBloodPressureDate1($bloodPressureDate1)
    {
        $this->bloodPressureDate1 = $bloodPressureDate1;

        return $this;
    }

    /**
     * Get bloodPressureDate1
     *
     * @return \DateTime
     */
    public function getBloodPressureDate1()
    {
        return $this->bloodPressureDate1;
    }

    /**
     * Set systolic2
     *
     * @param integer $systolic2
     *
     * @return SubmissionDiabete
     */
    public function setSystolic2($systolic2)
    {
        $this->systolic2 = $systolic2;

        return $this;
    }

    /**
     * Get systolic2
     *
     * @return integer
     */
    public function getSystolic2()
    {
        return $this->systolic2;
    }

    /**
     * Set diastolic2
     *
     * @param integer $diastolic2
     *
     * @return SubmissionDiabete
     */
    public function setDiastolic2($diastolic2)
    {
        $this->diastolic2 = $diastolic2;

        return $this;
    }

    /**
     * Get diastolic2
     *
     * @return integer
     */
    public function getDiastolic2()
    {
        return $this->diastolic2;
    }

    /**
     * Set bloodPressureDate2
     *
     * @param \DateTime $bloodPressureDate2
     *
     * @return SubmissionDiabete
     */
    public function setBloodPressureDate2($bloodPressureDate2)
    {
        $this->bloodPressureDate2 = $bloodPressureDate2;

        return $this;
    }

    /**
     * Get bloodPressureDate2
     *
     * @return \DateTime
     */
    public function getBloodPressureDate2()
    {
        return $this->bloodPressureDate2;
    }

    /**
     * Set tobaccoStatus
     *
     * @param integer $tobaccoStatus
     *
     * @return SubmissionDiabete
     */
    public function setTobaccoStatus($tobaccoStatus)
    {
        $this->tobaccoStatus = $tobaccoStatus;

        return $this;
    }

    /**
     * Get tobaccoStatus
     *
     * @return integer
     */
    public function getTobaccoStatus()
    {
        return $this->tobaccoStatus;
    }

    /**
     * Set tobaccoStatusAssessmentDate
     *
     * @param \DateTime $tobaccoStatusAssessmentDate
     *
     * @return SubmissionDiabete
     */
    public function setTobaccoStatusAssessmentDate($tobaccoStatusAssessmentDate)
    {
        $this->tobaccoStatusAssessmentDate = $tobaccoStatusAssessmentDate;

        return $this;
    }

    /**
     * Get tobaccoStatusAssessmentDate
     *
     * @return \DateTime
     */
    public function getTobaccoStatusAssessmentDate()
    {
        return $this->tobaccoStatusAssessmentDate;
    }

    /**
     * Set tobaccoCessationAdviceOrTreatmentDate
     *
     * @param \DateTime $tobaccoCessationAdviceOrTreatmentDate
     *
     * @return SubmissionDiabete
     */
    public function setTobaccoCessationAdviceOrTreatmentDate($tobaccoCessationAdviceOrTreatmentDate)
    {
        $this->tobaccoCessationAdviceOrTreatmentDate = $tobaccoCessationAdviceOrTreatmentDate;

        return $this;
    }

    /**
     * Get tobaccoCessationAdviceOrTreatmentDate
     *
     * @return \DateTime
     */
    public function getTobaccoCessationAdviceOrTreatmentDate()
    {
        return $this->tobaccoCessationAdviceOrTreatmentDate;
    }

    /**
     * Set footExam
     *
     * @param boolean $footExam
     *
     * @return SubmissionDiabete
     */
    public function setFootExam($footExam)
    {
        $this->footExam = $footExam;

        return $this;
    }

    /**
     * Get footExam
     *
     * @return boolean
     */
    public function getFootExam()
    {
        return $this->footExam;
    }

    /**
     * Set footExamDate
     *
     * @param \DateTime $footExamDate
     *
     * @return SubmissionDiabete
     */
    public function setFootExamDate($footExamDate)
    {
        $this->footExamDate = $footExamDate;

        return $this;
    }

    /**
     * Get footExamDate
     *
     * @return \DateTime
     */
    public function getFootExamDate()
    {
        return $this->footExamDate;
    }

    /**
     * Set bilateralAmputation
     *
     * @return SubmissionDiabete
     */
    public function setBilateralAmputation($bilateralamputation)
    {
        $this->bilateralAmputation = $bilateralamputation;

        return $this;
    }

    /**
     * Get bilateralAmputation
     *
     * @return boolean
     */
    public function getBilateralAmputation()
    {
        return $this->bilateralAmputation;
    }

    /**
     * Set retinopathy
     *
     * @param boolean $retinopathy
     *
     * @return SubmissionDiabete
     */
    public function setRetinopathy($retinopathy)
    {
        $this->retinopathy = $retinopathy;

        return $this;
    }

    /**
     * Get retinopathy
     *
     * @return boolean
     */
    public function getRetinopathy()
    {
        return $this->retinopathy;
    }

    /**
     * Set retinalExamDate
     *
     * @param \DateTime $retinalExamDate
     *
     * @return SubmissionDiabete
     */
    public function setRetinalExamDate($retinalExamDate)
    {
        $this->retinalExamDate = $retinalExamDate;

        return $this;
    }

    /**
     * Get retinalExamDate
     *
     * @return \DateTime
     */
    public function getRetinalExamDate()
    {
        return $this->retinalExamDate;
    }

    /**
     * Set blindness
     *
     * @param boolean $blindness
     *
     * @return SubmissionDiabete
     */
    public function setBlindness($blindness)
    {
        $this->blindness = $blindness;

        return $this;
    }

    /**
     * Get blindness
     *
     * @return boolean
     */
    public function getBlindness()
    {
        return $this->blindness;
    }

    /**
     * Set nephropathyDiagnosis
     *
     * @param boolean $nephropathyDiagnosis
     *
     * @return SubmissionDiabete
     */
    public function setNephropathyDiagnosis($nephropathyDiagnosis)
    {
        $this->nephropathyDiagnosis = $nephropathyDiagnosis;

        return $this;
    }

    /**
     * Get nephropathyDiagnosis
     *
     * @return boolean
     */
    public function getNephropathyDiagnosis()
    {
        return $this->nephropathyDiagnosis;
    }

    /**
     * Set nephropathyEvidence
     *
     * @param boolean $nephropathyEvidence
     *
     * @return SubmissionDiabete
     */
    public function setNephropathyEvidence($nephropathyEvidence)
    {
        $this->nephropathyEvidence = $nephropathyEvidence;

        return $this;
    }

    /**
     * Get nephropathyEvidence
     *
     * @return boolean
     */
    public function getNephropathyEvidence()
    {
        return $this->nephropathyEvidence;
    }

    /**
     * Set microalbuminuriaLab
     *
     * @param integer $microalbuminuriaLab
     *
     * @return SubmissionDiabete
     */
    public function setMicroalbuminuriaLab($microalbuminuriaLab)
    {
        $this->microalbuminuriaLab = $microalbuminuriaLab;

        return $this;
    }

    /**
     * Get microalbuminuriaLab
     *
     * @return integer
     */
    public function getMicroalbuminuriaLab()
    {
        return $this->microalbuminuriaLab;
    }

    /**
     * Set microalbuminuriaDate
     *
     * @param \DateTime $microalbuminuriaDate
     *
     * @return SubmissionDiabete
     */
    public function setMicroalbuminuriaDate($microalbuminuriaDate)
    {
        $this->microalbuminuriaDate = $microalbuminuriaDate;

        return $this;
    }

    /**
     * Get microalbuminuriaDate
     *
     * @return \DateTime
     */
    public function getMicroalbuminuriaDate()
    {
        return $this->microalbuminuriaDate;
    }

    /**
     * Set macroalbuminuriaLab
     *
     * @param integer $macroalbuminuriaLab
     *
     * @return SubmissionDiabete
     */
    public function setMacroalbuminuriaLab($macroalbuminuriaLab)
    {
        $this->macroalbuminuriaLab = $macroalbuminuriaLab;


        return $this;
    }

    /**
     * Get macroalbuminuriaLab
     *
     * @return integer
     */
    public function getMacroalbuminuriaLab()
    {
        return $this->macroalbuminuriaLab;
    }

    /**
     * Set macroalbuminuriaDate
     *
     * @param \DateTime $macroalbuminuriaDate
     *
     * @return SubmissionDiabete
     */
    public function setMacroalbuminuriaDate($macroalbuminuriaDate)
    {
        $this->macroalbuminuriaDate = $macroalbuminuriaDate;

        return $this;
    }

    /**
     * Get macroalbuminuriaDate
     *
     * @return \DateTime
     */
    public function getMacroalbuminuriaDate()
    {
        return $this->macroalbuminuriaDate;
    }

    /**
     * Set hypertensionDiagnosis
     *
     * @param boolean $hypertensionDiagnosis
     *
     * @return SubmissionDiabete
     */
    public function setHypertensionDiagnosis($hypertensionDiagnosis)
    {
        $this->hypertensionDiagnosis = $hypertensionDiagnosis;

        return $this;
    }

    /**
     * Get hypertensionDiagnosis
     *
     * @return boolean
     */
    public function getHypertensionDiagnosis()
    {
        return $this->hypertensionDiagnosis;
    }

    /**
     * Set aceiArbTherapy
     *
     * @param boolean $aceiArbTherapy
     *
     * @return SubmissionDiabete
     */
    public function setAceiArbTherapy($aceiArbTherapy)
    {
        $this->aceiArbTherapy = $aceiArbTherapy;


        return $this;
    }

    /**
     * Get aceiArbTherapy
     *
     * @return boolean
     */
    public function getAceiArbTherapy()
    {
        return $this->aceiArbTherapy;
    }

    /**
     * Set bmiCalculation
     *
     * @param float $bmiCalculation
     *
     * @return SubmissionDiabete
     */
    public function setBmiCalculation($bmiCalculation)
    {
        $this->bmiCalculation = $bmiCalculation;

        return $this;
    }

    /**
     * Get bmiCalculation
     *
     * @return float
     */
    public function getBmiCalculation()
    {
        return $this->bmiCalculation;
    }

    /**
     * Set bmiDate
     *
     * @param \DateTime $bmiDate
     *
     * @return SubmissionDiabete
     */
    public function setBmiDate($bmiDate)
    {
        $this->bmiDate = $bmiDate;

        return $this;
    }

    /**
     * Get bmiDate
     *
     * @return \DateTime
     */
    public function getBmiDate()
    {
        return $this->bmiDate;
    }

    /**
     * Set bmiCounseling
     *
     * @param boolean $bmiCounseling
     *
     * @return SubmissionDiabete
     */
    public function setBmiCounseling($bmiCounseling)
    {
        $this->bmiCounseling = $bmiCounseling;

        return $this;
    }

    /**
     * Get bmiCounseling
     *
     * @return boolean
     */
    public function getBmiCounseling()
    {
        return $this->bmiCounseling;
    }

    /**
     * Set bmiCounselingDate
     *
     * @param \DateTime $bmiCounselingDate
     *
     * @return SubmissionDiabete
     */
    public function setBmiCounselingDate($bmiCounselingDate)
    {
        $this->bmiCounselingDate = $bmiCounselingDate;

        return $this;
    }

    /**
     * Get bmiCounselingDate
     *
     * @return \DateTime
     */
    public function getBmiCounselingDate()
    {
        return $this->bmiCounselingDate;
    }

    /**
     * Set ascvdAssessment
     *
     * @param boolean $ascvdAssessment
     *
     * @return SubmissionDiabete
     */
    public function setAscvdAssessment($ascvdAssessment)
    {
        $this->ascvdAssessment = $ascvdAssessment;

        return $this;
    }

    /**
     * Get ascvdAssessment
     *
     * @return boolean
     */
    public function getAscvdAssessment()
    {
        return $this->ascvdAssessment;
    }

    /**
     * Set ascvdDate
     *
     * @param \DateTime $ascvdDate
     *
     * @return SubmissionDiabete
     */
    public function setAscvdDate($ascvdDate)
    {
        $this->ascvdDate = $ascvdDate;

        return $this;
    }

    /**
     * Get ascvdDate
     *
     * @return \DateTime
     */
    public function getAscvdDate()
    {
        return $this->ascvdDate;
    }

    /**
     * Set aspirinTherapy
     *
     * @param integer $aspirinTherapy
     *
     * @return SubmissionDiabete
     */
    public function setAspirinTherapy($aspirinTherapy)
    {
        $this->aspirinTherapy = $aspirinTherapy;

        return $this;
    }

    /**
     * Get aspirinTherapy
     *
     * @return integer
     */
    public function getAspirinTherapy()
    {
        return $this->aspirinTherapy;
    }

    /**
     * Set aspirinUseConsidered
     *
     * @param boolean $aspirinUseConsidered
     *
     * @return SubmissionDiabete
     */
    public function setAspirinUseConsidered($aspirinUseConsidered)
    {
        $this->aspirinUseConsidered = $aspirinUseConsidered;

        return $this;
    }

    /**
     * Get aspirinUseConsidered
     *
     * @return boolean
     */
    public function getAspirinUseConsidered()
    {
        return $this->aspirinUseConsidered;
    }

    /**
     * Set pHQ2screening
     *
     * @param boolean $pHQ2screening
     *
     * @return SubmissionDiabete
     */
    public function setPHQ2screening($pHQ2screening)
    {
        $this->PHQ2screening = $pHQ2screening;

        return $this;
    }

    /**
     * Get pHQ2screening
     *
     * @return boolean
     */
    public function getPHQ2screening()
    {
        return $this->PHQ2screening;
    }

    /**
     * Set pHQ2screeningDate
     *
     * @param \DateTime $pHQ2screeningDate
     *
     * @return SubmissionDiabete
     */
    public function setPHQ2screeningDate($pHQ2screeningDate)
    {
        $this->PHQ2screeningDate = $pHQ2screeningDate;

        return $this;
    }

    /**
     * Get pHQ2screeningDate
     *
     * @return \DateTime
     */
    public function getPHQ2screeningDate()
    {
        return $this->PHQ2screeningDate;
    }

    /**
     * Set pHQ9screening
     *
     * @param boolean $pHQ9screening
     *
     * @return SubmissionDiabete
     */
    public function setPHQ9screening($pHQ9screening)
    {
        $this->PHQ9screening = $pHQ9screening;

        return $this;
    }

    /**
     * Get pHQ9screening
     *
     * @return boolean
     */
    public function getPHQ9screening()
    {
        return $this->PHQ9screening;
    }

    /**
     * Set pHQ9screeningDate
     *
     * @param \DateTime $pHQ9screeningDate
     *
     * @return SubmissionDiabete
     */
    public function setPHQ9screeningDate($pHQ9screeningDate)
    {
        $this->PHQ9screeningDate = $pHQ9screeningDate;

        return $this;
    }

    /**
     * Get pHQ9screeningDate
     *
     * @return \DateTime
     */
    public function getPHQ9screeningDate()
    {
        return $this->PHQ9screeningDate;
    }

    /**
     * Set submissionId
     *
     * @param integer $submissionId
     *
     * @return SubmissionDiabete
     */
    public function setSubmissionId($submissionId)
    {
        $this->submission_id = $submissionId;

        return $this;
    }

    /**
     * Get submissionId
     *
     * @return integer
     */
    public function getSubmissionId()
    {
        return $this->submission_id;
    }

    /**
     * Set submision
     *
     * @param \AppBundle\Entity\Submission $submision
     *
     * @return SubmissionDiabete
     */
    public function setSubmision(\AppBundle\Entity\Submission $submision = null)
    {
        $this->submision = $submision;

        return $this;
    }

    /**
     * Get submision
     *
     * @return \AppBundle\Entity\Submission
     */
    public function getSubmision()
    {
        return $this->submision;
    }

    /**
     * @var integer
     */
    private $temp_id;


    /**
     * Set tempId
     *
     * @param integer $tempId
     *
     * @return SubmissionDiabete
     */
    public function setTempId($tempId)
    {
        $this->temp_id = $tempId;

        return $this;
    }

    /**
     * Get tempId
     *
     * @return integer
     */
    public function getTempId()
    {
        return $this->temp_id;
    }

    public function __toString(){
        return (string)$this->getId();
    }


    /**
     * Set ascvdResults
     *
     * @param integer $ascvdResults
     *
     * @return SubmissionDiabete
     */
    public function setAscvdResults($ascvdResults)
    {
        $this->ascvdResults = $ascvdResults;

        return $this;
    }

    /**
     * Get ascvdResults
     *
     * @return integer
     */
    public function getAscvdResults()
    {
        return $this->ascvdResults;
    }
}
