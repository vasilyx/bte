<?php

namespace AppBundle\Entity;

/**
 * SubmissionMaternity
 */
class SubmissionMaternity
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string|null
     */
    private $responsibleProviderID;

    /**
     * @var string|null
     */
    private $npi;

    /**
     * @var string|null
     */
    private $groupID;

    /**
     * @var string|null
     */
    private $individualGroup;

    /**
     * @var string|null
     */
    private $chartID;

    /**
     * @var \DateTime|null
     */
    private $deliveryDate;

    /**
     * @var \DateTime|null
     */
    private $patientDOB;

    /**
     * @var int|null
     */
    private $patientGender;

    /**
     * @var string|null
     */
    private $patientRace;

    /**
     * @var int|null
     */
    private $medicarePartB;

    /**
     * @var \DateTime|null
     */
    private $lmpDate;

    /**
     * @var \DateTime|null
     */
    private $dueDate;

    /**
     * @var \DateTime|null
     */
    private $initialVisitDate;

    /**
     * @var \DateTime|null
     */
    private $tri2VisitDate1;

    /**
     * @var \DateTime|null
     */
    private $tri2VisitDate2;

    /**
     * @var \DateTime|null
     */
    private $tri2VisitDate3;

    /**
     * @var \DateTime|null
     */
    private $tri2VisitDate4;

    /**
     * @var \DateTime|null
     */
    private $tri2VisitDate5;

    /**
     * @var \DateTime|null
     */
    private $tri2VisitDate6;

    /**
     * @var \DateTime|null
     */
    private $tri3VisitDate1;

    /**
     * @var \DateTime|null
     */
    private $tri3VisitDate2;

    /**
     * @var \DateTime|null
     */
    private $tri3VisitDate3;

    /**
     * @var \DateTime|null
     */
    private $tri3VisitDate4;

    /**
     * @var \DateTime|null
     */
    private $tri3VisitDate5;

    /**
     * @var \DateTime|null
     */
    private $tri3VisitDate6;

    /**
     * @var \DateTime|null
     */
    private $tri3VisitDate7;

    /**
     * @var \DateTime|null
     */
    private $postpartumVisitDate;

    /**
     * @var \DateTime|null
     */
    private $aboRhScreenDate;

    /**
     * @var \DateTime|null
     */
    private $cbcDate1;

    /**
     * @var \DateTime|null
     */
    private $vdrlScreenDate;

    /**
     * @var \DateTime|null
     */
    private $chlamydiaScreenDate;

    /**
     * @var \DateTime|null
     */
    private $gonorrheaScreenDate;

    /**
     * @var \DateTime|null
     */
    private $hivScreenDate;

    /**
     * @var \DateTime|null
     */
    private $urineCultureSensitivityDate;

    /**
     * @var \DateTime|null
     */
    private $HbA1Cdate;

    /**
     * @var \DateTime|null
     */
    private $rubellaScreenDate;

    /**
     * @var \DateTime|null
     */
    private $fetalAneuploidyScreenDate;

    /**
     * @var \DateTime|null
     */
    private $afpScreenDate;

    /**
     * @var \DateTime|null
     */
    private $quadScreenDate;

    /**
     * @var \DateTime|null
     */
    private $cbcDate2;

    /**
     * @var \DateTime|null
     */
    private $glucoseScreenDate;

    /**
     * @var \DateTime|null
     */
    private $antibodiesScreenDate;

    /**
     * @var \DateTime|null
     */
    private $rhoGAMScreenDate;

    /**
     * @var \DateTime|null
     */
    private $groupBStrepScreenDate;

    /**
     * @var \DateTime|null
     */
    private $cysticFibrosisScreeningDate;

    /**
     * @var \DateTime|null
     */
    private $spinalMuscularAtrophyDate;

    /**
     * @var \DateTime|null
     */
    private $hemoglobinopathiesDate;

    /**
     * @var \DateTime|null
     */
    private $panEthicExpandedCarrierScreenDate;

    /**
     * @var \DateTime|null
     */
    private $genticCarrierScreen;

    /**
     * @var \DateTime|null
     */
    private $influenzaDate;

    /**
     * @var \DateTime|null
     */
    private $tetanusDate;

    /**
     * @var \DateTime|null
     */
    private $diphtheriaDate;

    /**
     * @var \DateTime|null
     */
    private $pertussisTdapDate;

    /**
     * @var int|null
     */
    private $hxPreeclampsia;

    /**
     * @var int|null
     */
    private $hxPretermdelivery;

    /**
     * @var \DateTime|null
     */
    private $lowDoseAspirinDate;

    /**
     * @var \DateTime|null
     */
    private $ultrasoundDate;

    /**
     * @var int|null
     */
    private $groupBStrep;

    /**
     * @var \DateTime|null
     */
    private $antibioticProphylaxisDate;

    /**
     * @var \DateTime|null
     */
    private $corticoSteroidDate;

    /**
     * @var int|null
     */
    private $hxCSection;

    /**
     * @var \DateTime|null
     */
    private $birthPlanDate;

    /**
     * @var \DateTime|null
     */
    private $decisionMakingConsultDate;

    /**
     * @var int|null
     */
    private $ntsv;

    /**
     * @var \DateTime|null
     */
    private $electivecsectionDate;

    /**
     * @var int|null
     */
    private $infantWeight;

    /**
     * @var int|null
     */
    private $NICUlevel;

    /**
     * @var \DateTime|null
     */
    private $postPartumDepressionScreenDate;

    /**
     * @var \DateTime|null
     */
    private $drugAlcoholScreenDate;

    /**
     * @var \DateTime|null
     */
    private $interpersonalViolenceScreenDate;

    /**
     * @var int|null
     */
    private $submission_id;

    /**
     * @var int|null
     */
    private $temp_id;

    /**
     * @var \AppBundle\Entity\Submission
     */
    private $submision;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set responsibleProviderID.
     *
     * @param string|null $responsibleProviderID
     *
     * @return SubmissionMaternity
     */
    public function setResponsibleProviderID($responsibleProviderID = null)
    {
        $this->responsibleProviderID = $responsibleProviderID;

        return $this;
    }

    /**
     * Get responsibleProviderID.
     *
     * @return string|null
     */
    public function getResponsibleProviderID()
    {
        return $this->responsibleProviderID;
    }

    /**
     * Set npi.
     *
     * @param string|null $npi
     *
     * @return SubmissionMaternity
     */
    public function setNpi($npi = null)
    {
        $this->npi = $npi;

        return $this;
    }

    /**
     * Get npi.
     *
     * @return string|null
     */
    public function getNpi()
    {
        return $this->npi;
    }

    /**
     * Set groupID.
     *
     * @param string|null $groupID
     *
     * @return SubmissionMaternity
     */
    public function setGroupID($groupID = null)
    {
        $this->groupID = $groupID;

        return $this;
    }

    /**
     * Get groupID.
     *
     * @return string|null
     */
    public function getGroupID()
    {
        return $this->groupID;
    }

    /**
     * Set individualGroup.
     *
     * @param string|null $individualGroup
     *
     * @return SubmissionMaternity
     */
    public function setIndividualGroup($individualGroup = null)
    {
        $this->individualGroup = $individualGroup;

        return $this;
    }

    /**
     * Get individualGroup.
     *
     * @return string|null
     */
    public function getIndividualGroup()
    {
        return $this->individualGroup;
    }

    /**
     * Set chartID.
     *
     * @param string|null $chartID
     *
     * @return SubmissionMaternity
     */
    public function setChartID($chartID = null)
    {
        $this->chartID = $chartID;

        return $this;
    }

    /**
     * Get chartID.
     *
     * @return string|null
     */
    public function getChartID()
    {
        return $this->chartID;
    }

    /**
     * Set deliveryDate.
     *
     * @param \DateTime|null $deliveryDate
     *
     * @return SubmissionMaternity
     */
    public function setDeliveryDate($deliveryDate = null)
    {
        $this->deliveryDate = $deliveryDate;

        return $this;
    }

    /**
     * Get deliveryDate.
     *
     * @return \DateTime|null
     */
    public function getDeliveryDate()
    {
        return $this->deliveryDate;
    }

    /**
     * Set patientDOB.
     *
     * @param \DateTime|null $patientDOB
     *
     * @return SubmissionMaternity
     */
    public function setPatientDOB($patientDOB = null)
    {
        $this->patientDOB = $patientDOB;

        return $this;
    }

    /**
     * Get patientDOB.
     *
     * @return \DateTime|null
     */
    public function getPatientDOB()
    {
        return $this->patientDOB;
    }

    /**
     * Set patientGender.
     *
     * @param int|null $patientGender
     *
     * @return SubmissionMaternity
     */
    public function setPatientGender($patientGender = null)
    {
        $this->patientGender = $patientGender;

        return $this;
    }

    /**
     * Get patientGender.
     *
     * @return int|null
     */
    public function getPatientGender()
    {
        return $this->patientGender;
    }

    /**
     * Set patientRace.
     *
     * @param string|null $patientRace
     *
     * @return SubmissionMaternity
     */
    public function setPatientRace($patientRace = null)
    {
        $this->patientRace = $patientRace;

        return $this;
    }

    /**
     * Get patientRace.
     *
     * @return string|null
     */
    public function getPatientRace()
    {
        return $this->patientRace;
    }

    /**
     * Set medicarePartB.
     *
     * @param int|null $medicarePartB
     *
     * @return SubmissionMaternity
     */
    public function setMedicarePartB($medicarePartB = null)
    {
        $this->medicarePartB = $medicarePartB;

        return $this;
    }

    /**
     * Get medicarePartB.
     *
     * @return int|null
     */
    public function getMedicarePartB()
    {
        return $this->medicarePartB;
    }

    /**
     * Set lmpDate.
     *
     * @param \DateTime|null $lmpDate
     *
     * @return SubmissionMaternity
     */
    public function setLmpDate($lmpDate = null)
    {
        $this->lmpDate = $lmpDate;

        return $this;
    }

    /**
     * Get lmpDate.
     *
     * @return \DateTime|null
     */
    public function getLmpDate()
    {
        return $this->lmpDate;
    }

    /**
     * Set dueDate.
     *
     * @param \DateTime|null $dueDate
     *
     * @return SubmissionMaternity
     */
    public function setDueDate($dueDate = null)
    {
        $this->dueDate = $dueDate;

        return $this;
    }

    /**
     * Get dueDate.
     *
     * @return \DateTime|null
     */
    public function getDueDate()
    {
        return $this->dueDate;
    }

    /**
     * Set initialVisitDate.
     *
     * @param \DateTime|null $initialVisitDate
     *
     * @return SubmissionMaternity
     */
    public function setInitialVisitDate($initialVisitDate = null)
    {
        $this->initialVisitDate = $initialVisitDate;

        return $this;
    }

    /**
     * Get initialVisitDate.
     *
     * @return \DateTime|null
     */
    public function getInitialVisitDate()
    {
        return $this->initialVisitDate;
    }

    /**
     * Set tri2VisitDate1.
     *
     * @param \DateTime|null $tri2VisitDate1
     *
     * @return SubmissionMaternity
     */
    public function setTri2VisitDate1($tri2VisitDate1 = null)
    {
        $this->tri2VisitDate1 = $tri2VisitDate1;

        return $this;
    }

    /**
     * Get tri2VisitDate1.
     *
     * @return \DateTime|null
     */
    public function getTri2VisitDate1()
    {
        return $this->tri2VisitDate1;
    }

    /**
     * Set tri2VisitDate2.
     *
     * @param \DateTime|null $tri2VisitDate2
     *
     * @return SubmissionMaternity
     */
    public function setTri2VisitDate2($tri2VisitDate2 = null)
    {
        $this->tri2VisitDate2 = $tri2VisitDate2;

        return $this;
    }

    /**
     * Get tri2VisitDate2.
     *
     * @return \DateTime|null
     */
    public function getTri2VisitDate2()
    {
        return $this->tri2VisitDate2;
    }

    /**
     * Set tri2VisitDate3.
     *
     * @param \DateTime|null $tri2VisitDate3
     *
     * @return SubmissionMaternity
     */
    public function setTri2VisitDate3($tri2VisitDate3 = null)
    {
        $this->tri2VisitDate3 = $tri2VisitDate3;

        return $this;
    }

    /**
     * Get tri2VisitDate3.
     *
     * @return \DateTime|null
     */
    public function getTri2VisitDate3()
    {
        return $this->tri2VisitDate3;
    }

    /**
     * Set tri2VisitDate4.
     *
     * @param \DateTime|null $tri2VisitDate4
     *
     * @return SubmissionMaternity
     */
    public function setTri2VisitDate4($tri2VisitDate4 = null)
    {
        $this->tri2VisitDate4 = $tri2VisitDate4;

        return $this;
    }

    /**
     * Get tri2VisitDate4.
     *
     * @return \DateTime|null
     */
    public function getTri2VisitDate4()
    {
        return $this->tri2VisitDate4;
    }

    /**
     * Set tri2VisitDate5.
     *
     * @param \DateTime|null $tri2VisitDate5
     *
     * @return SubmissionMaternity
     */
    public function setTri2VisitDate5($tri2VisitDate5 = null)
    {
        $this->tri2VisitDate5 = $tri2VisitDate5;

        return $this;
    }

    /**
     * Get tri2VisitDate5.
     *
     * @return \DateTime|null
     */
    public function getTri2VisitDate5()
    {
        return $this->tri2VisitDate5;
    }

    /**
     * Set tri2VisitDate6.
     *
     * @param \DateTime|null $tri2VisitDate6
     *
     * @return SubmissionMaternity
     */
    public function setTri2VisitDate6($tri2VisitDate6 = null)
    {
        $this->tri2VisitDate6 = $tri2VisitDate6;

        return $this;
    }

    /**
     * Get tri2VisitDate6.
     *
     * @return \DateTime|null
     */
    public function getTri2VisitDate6()
    {
        return $this->tri2VisitDate6;
    }

    /**
     * Set tri3VisitDate1.
     *
     * @param \DateTime|null $tri3VisitDate1
     *
     * @return SubmissionMaternity
     */
    public function setTri3VisitDate1($tri3VisitDate1 = null)
    {
        $this->tri3VisitDate1 = $tri3VisitDate1;

        return $this;
    }

    /**
     * Get tri3VisitDate1.
     *
     * @return \DateTime|null
     */
    public function getTri3VisitDate1()
    {
        return $this->tri3VisitDate1;
    }

    /**
     * Set tri3VisitDate2.
     *
     * @param \DateTime|null $tri3VisitDate2
     *
     * @return SubmissionMaternity
     */
    public function setTri3VisitDate2($tri3VisitDate2 = null)
    {
        $this->tri3VisitDate2 = $tri3VisitDate2;

        return $this;
    }

    /**
     * Get tri3VisitDate2.
     *
     * @return \DateTime|null
     */
    public function getTri3VisitDate2()
    {
        return $this->tri3VisitDate2;
    }

    /**
     * Set tri3VisitDate3.
     *
     * @param \DateTime|null $tri3VisitDate3
     *
     * @return SubmissionMaternity
     */
    public function setTri3VisitDate3($tri3VisitDate3 = null)
    {
        $this->tri3VisitDate3 = $tri3VisitDate3;

        return $this;
    }

    /**
     * Get tri3VisitDate3.
     *
     * @return \DateTime|null
     */
    public function getTri3VisitDate3()
    {
        return $this->tri3VisitDate3;
    }

    /**
     * Set tri3VisitDate4.
     *
     * @param \DateTime|null $tri3VisitDate4
     *
     * @return SubmissionMaternity
     */
    public function setTri3VisitDate4($tri3VisitDate4 = null)
    {
        $this->tri3VisitDate4 = $tri3VisitDate4;

        return $this;
    }

    /**
     * Get tri3VisitDate4.
     *
     * @return \DateTime|null
     */
    public function getTri3VisitDate4()
    {
        return $this->tri3VisitDate4;
    }

    /**
     * Set tri3VisitDate5.
     *
     * @param \DateTime|null $tri3VisitDate5
     *
     * @return SubmissionMaternity
     */
    public function setTri3VisitDate5($tri3VisitDate5 = null)
    {
        $this->tri3VisitDate5 = $tri3VisitDate5;

        return $this;
    }

    /**
     * Get tri3VisitDate5.
     *
     * @return \DateTime|null
     */
    public function getTri3VisitDate5()
    {
        return $this->tri3VisitDate5;
    }

    /**
     * Set tri3VisitDate6.
     *
     * @param \DateTime|null $tri3VisitDate6
     *
     * @return SubmissionMaternity
     */
    public function setTri3VisitDate6($tri3VisitDate6 = null)
    {
        $this->tri3VisitDate6 = $tri3VisitDate6;

        return $this;
    }

    /**
     * Get tri3VisitDate6.
     *
     * @return \DateTime|null
     */
    public function getTri3VisitDate6()
    {
        return $this->tri3VisitDate6;
    }

    /**
     * Set tri3VisitDate7.
     *
     * @param \DateTime|null $tri3VisitDate7
     *
     * @return SubmissionMaternity
     */
    public function setTri3VisitDate7($tri3VisitDate7 = null)
    {
        $this->tri3VisitDate7 = $tri3VisitDate7;

        return $this;
    }

    /**
     * Get tri3VisitDate7.
     *
     * @return \DateTime|null
     */
    public function getTri3VisitDate7()
    {
        return $this->tri3VisitDate7;
    }

    /**
     * Set postpartumVisitDate.
     *
     * @param \DateTime|null $postpartumVisitDate
     *
     * @return SubmissionMaternity
     */
    public function setPostpartumVisitDate($postpartumVisitDate = null)
    {
        $this->postpartumVisitDate = $postpartumVisitDate;

        return $this;
    }

    /**
     * Get postpartumVisitDate.
     *
     * @return \DateTime|null
     */
    public function getPostpartumVisitDate()
    {
        return $this->postpartumVisitDate;
    }

    /**
     * Set aboRhScreenDate.
     *
     * @param \DateTime|null $aboRhScreenDate
     *
     * @return SubmissionMaternity
     */
    public function setAboRhScreenDate($aboRhScreenDate = null)
    {
        $this->aboRhScreenDate = $aboRhScreenDate;

        return $this;
    }

    /**
     * Get aboRhScreenDate.
     *
     * @return \DateTime|null
     */
    public function getAboRhScreenDate()
    {
        return $this->aboRhScreenDate;
    }

    /**
     * Set cbcDate1.
     *
     * @param \DateTime|null $cbcDate1
     *
     * @return SubmissionMaternity
     */
    public function setCbcDate1($cbcDate1 = null)
    {
        $this->cbcDate1 = $cbcDate1;

        return $this;
    }

    /**
     * Get cbcDate1.
     *
     * @return \DateTime|null
     */
    public function getCbcDate1()
    {
        return $this->cbcDate1;
    }

    /**
     * Set vdrlScreenDate.
     *
     * @param \DateTime|null $vdrlScreenDate
     *
     * @return SubmissionMaternity
     */
    public function setVdrlScreenDate($vdrlScreenDate = null)
    {
        $this->vdrlScreenDate = $vdrlScreenDate;

        return $this;
    }

    /**
     * Get vdrlScreenDate.
     *
     * @return \DateTime|null
     */
    public function getVdrlScreenDate()
    {
        return $this->vdrlScreenDate;
    }

    /**
     * Set chlamydiaScreenDate.
     *
     * @param \DateTime|null $chlamydiaScreenDate
     *
     * @return SubmissionMaternity
     */
    public function setChlamydiaScreenDate($chlamydiaScreenDate = null)
    {
        $this->chlamydiaScreenDate = $chlamydiaScreenDate;

        return $this;
    }

    /**
     * Get chlamydiaScreenDate.
     *
     * @return \DateTime|null
     */
    public function getChlamydiaScreenDate()
    {
        return $this->chlamydiaScreenDate;
    }

    /**
     * Set gonorrheaScreenDate.
     *
     * @param \DateTime|null $gonorrheaScreenDate
     *
     * @return SubmissionMaternity
     */
    public function setGonorrheaScreenDate($gonorrheaScreenDate = null)
    {
        $this->gonorrheaScreenDate = $gonorrheaScreenDate;

        return $this;
    }

    /**
     * Get gonorrheaScreenDate.
     *
     * @return \DateTime|null
     */
    public function getGonorrheaScreenDate()
    {
        return $this->gonorrheaScreenDate;
    }

    /**
     * Set hivScreenDate.
     *
     * @param \DateTime|null $hivScreenDate
     *
     * @return SubmissionMaternity
     */
    public function setHivScreenDate($hivScreenDate = null)
    {
        $this->hivScreenDate = $hivScreenDate;

        return $this;
    }

    /**
     * Get hivScreenDate.
     *
     * @return \DateTime|null
     */
    public function getHivScreenDate()
    {
        return $this->hivScreenDate;
    }

    /**
     * Set urineCultureSensitivityDate.
     *
     * @param \DateTime|null $urineCultureSensitivityDate
     *
     * @return SubmissionMaternity
     */
    public function setUrineCultureSensitivityDate($urineCultureSensitivityDate = null)
    {
        $this->urineCultureSensitivityDate = $urineCultureSensitivityDate;

        return $this;
    }

    /**
     * Get urineCultureSensitivityDate.
     *
     * @return \DateTime|null
     */
    public function getUrineCultureSensitivityDate()
    {
        return $this->urineCultureSensitivityDate;
    }

    /**
     * Set hbA1Cdate.
     *
     * @param \DateTime|null $hbA1Cdate
     *
     * @return SubmissionMaternity
     */
    public function setHbA1Cdate($hbA1Cdate = null)
    {
        $this->HbA1Cdate = $hbA1Cdate;

        return $this;
    }

    /**
     * Get hbA1Cdate.
     *
     * @return \DateTime|null
     */
    public function getHbA1Cdate()
    {
        return $this->HbA1Cdate;
    }

    /**
     * Set rubellaScreenDate.
     *
     * @param \DateTime|null $rubellaScreenDate
     *
     * @return SubmissionMaternity
     */
    public function setRubellaScreenDate($rubellaScreenDate = null)
    {
        $this->rubellaScreenDate = $rubellaScreenDate;

        return $this;
    }

    /**
     * Get rubellaScreenDate.
     *
     * @return \DateTime|null
     */
    public function getRubellaScreenDate()
    {
        return $this->rubellaScreenDate;
    }

    /**
     * Set fetalAneuploidyScreenDate.
     *
     * @param \DateTime|null $fetalAneuploidyScreenDate
     *
     * @return SubmissionMaternity
     */
    public function setFetalAneuploidyScreenDate($fetalAneuploidyScreenDate = null)
    {
        $this->fetalAneuploidyScreenDate = $fetalAneuploidyScreenDate;

        return $this;
    }

    /**
     * Get fetalAneuploidyScreenDate.
     *
     * @return \DateTime|null
     */
    public function getFetalAneuploidyScreenDate()
    {
        return $this->fetalAneuploidyScreenDate;
    }

    /**
     * Set afpScreenDate.
     *
     * @param \DateTime|null $afpScreenDate
     *
     * @return SubmissionMaternity
     */
    public function setAfpScreenDate($afpScreenDate = null)
    {
        $this->afpScreenDate = $afpScreenDate;

        return $this;
    }

    /**
     * Get afpScreenDate.
     *
     * @return \DateTime|null
     */
    public function getAfpScreenDate()
    {
        return $this->afpScreenDate;
    }

    /**
     * Set quadScreenDate.
     *
     * @param \DateTime|null $quadScreenDate
     *
     * @return SubmissionMaternity
     */
    public function setQuadScreenDate($quadScreenDate = null)
    {
        $this->quadScreenDate = $quadScreenDate;

        return $this;
    }

    /**
     * Get quadScreenDate.
     *
     * @return \DateTime|null
     */
    public function getQuadScreenDate()
    {
        return $this->quadScreenDate;
    }

    /**
     * Set cbcDate2.
     *
     * @param \DateTime|null $cbcDate2
     *
     * @return SubmissionMaternity
     */
    public function setCbcDate2($cbcDate2 = null)
    {
        $this->cbcDate2 = $cbcDate2;

        return $this;
    }

    /**
     * Get cbcDate2.
     *
     * @return \DateTime|null
     */
    public function getCbcDate2()
    {
        return $this->cbcDate2;
    }

    /**
     * Set glucoseScreenDate.
     *
     * @param \DateTime|null $glucoseScreenDate
     *
     * @return SubmissionMaternity
     */
    public function setGlucoseScreenDate($glucoseScreenDate = null)
    {
        $this->glucoseScreenDate = $glucoseScreenDate;

        return $this;
    }

    /**
     * Get glucoseScreenDate.
     *
     * @return \DateTime|null
     */
    public function getGlucoseScreenDate()
    {
        return $this->glucoseScreenDate;
    }

    /**
     * Set antibodiesScreenDate.
     *
     * @param \DateTime|null $antibodiesScreenDate
     *
     * @return SubmissionMaternity
     */
    public function setAntibodiesScreenDate($antibodiesScreenDate = null)
    {
        $this->antibodiesScreenDate = $antibodiesScreenDate;

        return $this;
    }

    /**
     * Get antibodiesScreenDate.
     *
     * @return \DateTime|null
     */
    public function getAntibodiesScreenDate()
    {
        return $this->antibodiesScreenDate;
    }

    /**
     * Set rhoGAMScreenDate.
     *
     * @param \DateTime|null $rhoGAMScreenDate
     *
     * @return SubmissionMaternity
     */
    public function setRhoGAMScreenDate($rhoGAMScreenDate = null)
    {
        $this->rhoGAMScreenDate = $rhoGAMScreenDate;

        return $this;
    }

    /**
     * Get rhoGAMScreenDate.
     *
     * @return \DateTime|null
     */
    public function getRhoGAMScreenDate()
    {
        return $this->rhoGAMScreenDate;
    }

    /**
     * Set groupBStrepScreenDate.
     *
     * @param \DateTime|null $groupBStrepScreenDate
     *
     * @return SubmissionMaternity
     */
    public function setGroupBStrepScreenDate($groupBStrepScreenDate = null)
    {
        $this->groupBStrepScreenDate = $groupBStrepScreenDate;

        return $this;
    }

    /**
     * Get groupBStrepScreenDate.
     *
     * @return \DateTime|null
     */
    public function getGroupBStrepScreenDate()
    {
        return $this->groupBStrepScreenDate;
    }

    /**
     * Set cysticFibrosisScreeningDate.
     *
     * @param \DateTime|null $cysticFibrosisScreeningDate
     *
     * @return SubmissionMaternity
     */
    public function setCysticFibrosisScreeningDate($cysticFibrosisScreeningDate = null)
    {
        $this->cysticFibrosisScreeningDate = $cysticFibrosisScreeningDate;

        return $this;
    }

    /**
     * Get cysticFibrosisScreeningDate.
     *
     * @return \DateTime|null
     */
    public function getCysticFibrosisScreeningDate()
    {
        return $this->cysticFibrosisScreeningDate;
    }

    /**
     * Set spinalMuscularAtrophyDate.
     *
     * @param \DateTime|null $spinalMuscularAtrophyDate
     *
     * @return SubmissionMaternity
     */
    public function setSpinalMuscularAtrophyDate($spinalMuscularAtrophyDate = null)
    {
        $this->spinalMuscularAtrophyDate = $spinalMuscularAtrophyDate;

        return $this;
    }

    /**
     * Get spinalMuscularAtrophyDate.
     *
     * @return \DateTime|null
     */
    public function getSpinalMuscularAtrophyDate()
    {
        return $this->spinalMuscularAtrophyDate;
    }

    /**
     * Set hemoglobinopathiesDate.
     *
     * @param \DateTime|null $hemoglobinopathiesDate
     *
     * @return SubmissionMaternity
     */
    public function setHemoglobinopathiesDate($hemoglobinopathiesDate = null)
    {
        $this->hemoglobinopathiesDate = $hemoglobinopathiesDate;

        return $this;
    }

    /**
     * Get hemoglobinopathiesDate.
     *
     * @return \DateTime|null
     */
    public function getHemoglobinopathiesDate()
    {
        return $this->hemoglobinopathiesDate;
    }

    /**
     * Set panEthicExpandedCarrierScreenDate.
     *
     * @param \DateTime|null $panEthicExpandedCarrierScreenDate
     *
     * @return SubmissionMaternity
     */
    public function setPanEthicExpandedCarrierScreenDate($panEthicExpandedCarrierScreenDate = null)
    {
        $this->panEthicExpandedCarrierScreenDate = $panEthicExpandedCarrierScreenDate;

        return $this;
    }

    /**
     * Get panEthicExpandedCarrierScreenDate.
     *
     * @return \DateTime|null
     */
    public function getPanEthicExpandedCarrierScreenDate()
    {
        return $this->panEthicExpandedCarrierScreenDate;
    }

    /**
     * Set genticCarrierScreen.
     *
     * @param \DateTime|null $genticCarrierScreen
     *
     * @return SubmissionMaternity
     */
    public function setGenticCarrierScreen($genticCarrierScreen = null)
    {
        $this->genticCarrierScreen = $genticCarrierScreen;

        return $this;
    }

    /**
     * Get genticCarrierScreen.
     *
     * @return \DateTime|null
     */
    public function getGenticCarrierScreen()
    {
        return $this->genticCarrierScreen;
    }

    /**
     * Set influenzaDate.
     *
     * @param \DateTime|null $influenzaDate
     *
     * @return SubmissionMaternity
     */
    public function setInfluenzaDate($influenzaDate = null)
    {
        $this->influenzaDate = $influenzaDate;

        return $this;
    }

    /**
     * Get influenzaDate.
     *
     * @return \DateTime|null
     */
    public function getInfluenzaDate()
    {
        return $this->influenzaDate;
    }

    /**
     * Set tetanusDate.
     *
     * @param \DateTime|null $tetanusDate
     *
     * @return SubmissionMaternity
     */
    public function setTetanusDate($tetanusDate = null)
    {
        $this->tetanusDate = $tetanusDate;

        return $this;
    }

    /**
     * Get tetanusDate.
     *
     * @return \DateTime|null
     */
    public function getTetanusDate()
    {
        return $this->tetanusDate;
    }

    /**
     * Set diphtheriaDate.
     *
     * @param \DateTime|null $diphtheriaDate
     *
     * @return SubmissionMaternity
     */
    public function setDiphtheriaDate($diphtheriaDate = null)
    {
        $this->diphtheriaDate = $diphtheriaDate;

        return $this;
    }

    /**
     * Get diphtheriaDate.
     *
     * @return \DateTime|null
     */
    public function getDiphtheriaDate()
    {
        return $this->diphtheriaDate;
    }

    /**
     * Set pertussisTdapDate.
     *
     * @param \DateTime|null $pertussisTdapDate
     *
     * @return SubmissionMaternity
     */
    public function setPertussisTdapDate($pertussisTdapDate = null)
    {
        $this->pertussisTdapDate = $pertussisTdapDate;

        return $this;
    }

    /**
     * Get pertussisTdapDate.
     *
     * @return \DateTime|null
     */
    public function getPertussisTdapDate()
    {
        return $this->pertussisTdapDate;
    }

    /**
     * Set hxPreeclampsia.
     *
     * @param int|null $hxPreeclampsia
     *
     * @return SubmissionMaternity
     */
    public function setHxPreeclampsia($hxPreeclampsia = null)
    {
        $this->hxPreeclampsia = $hxPreeclampsia;

        return $this;
    }

    /**
     * Get hxPreeclampsia.
     *
     * @return int|null
     */
    public function getHxPreeclampsia()
    {
        return $this->hxPreeclampsia;
    }

    /**
     * Set hxPretermdelivery.
     *
     * @param int|null $hxPretermdelivery
     *
     * @return SubmissionMaternity
     */
    public function setHxPretermdelivery($hxPretermdelivery = null)
    {
        $this->hxPretermdelivery = $hxPretermdelivery;

        return $this;
    }

    /**
     * Get hxPretermdelivery.
     *
     * @return int|null
     */
    public function getHxPretermdelivery()
    {
        return $this->hxPretermdelivery;
    }

    /**
     * Set lowDoseAspirinDate.
     *
     * @param \DateTime|null $lowDoseAspirinDate
     *
     * @return SubmissionMaternity
     */
    public function setLowDoseAspirinDate($lowDoseAspirinDate = null)
    {
        $this->lowDoseAspirinDate = $lowDoseAspirinDate;

        return $this;
    }

    /**
     * Get lowDoseAspirinDate.
     *
     * @return \DateTime|null
     */
    public function getLowDoseAspirinDate()
    {
        return $this->lowDoseAspirinDate;
    }

    /**
     * Set ultrasoundDate.
     *
     * @param \DateTime|null $ultrasoundDate
     *
     * @return SubmissionMaternity
     */
    public function setUltrasoundDate($ultrasoundDate = null)
    {
        $this->ultrasoundDate = $ultrasoundDate;

        return $this;
    }

    /**
     * Get ultrasoundDate.
     *
     * @return \DateTime|null
     */
    public function getUltrasoundDate()
    {
        return $this->ultrasoundDate;
    }

    /**
     * Set groupBStrep.
     *
     * @param int|null $groupBStrep
     *
     * @return SubmissionMaternity
     */
    public function setGroupBStrep($groupBStrep = null)
    {
        $this->groupBStrep = $groupBStrep;

        return $this;
    }

    /**
     * Get groupBStrep.
     *
     * @return int|null
     */
    public function getGroupBStrep()
    {
        return $this->groupBStrep;
    }

    /**
     * Set antibioticProphylaxisDate.
     *
     * @param \DateTime|null $antibioticProphylaxisDate
     *
     * @return SubmissionMaternity
     */
    public function setAntibioticProphylaxisDate($antibioticProphylaxisDate = null)
    {
        $this->antibioticProphylaxisDate = $antibioticProphylaxisDate;

        return $this;
    }

    /**
     * Get antibioticProphylaxisDate.
     *
     * @return \DateTime|null
     */
    public function getAntibioticProphylaxisDate()
    {
        return $this->antibioticProphylaxisDate;
    }

    /**
     * Set corticoSteroidDate.
     *
     * @param \DateTime|null $corticoSteroidDate
     *
     * @return SubmissionMaternity
     */
    public function setCorticoSteroidDate($corticoSteroidDate = null)
    {
        $this->corticoSteroidDate = $corticoSteroidDate;

        return $this;
    }

    /**
     * Get corticoSteroidDate.
     *
     * @return \DateTime|null
     */
    public function getCorticoSteroidDate()
    {
        return $this->corticoSteroidDate;
    }

    /**
     * Set hxCSection.
     *
     * @param int|null $hxCSection
     *
     * @return SubmissionMaternity
     */
    public function setHxCSection($hxCSection = null)
    {
        $this->hxCSection = $hxCSection;

        return $this;
    }

    /**
     * Get hxCSection.
     *
     * @return int|null
     */
    public function getHxCSection()
    {
        return $this->hxCSection;
    }

    /**
     * Set birthPlanDate.
     *
     * @param \DateTime|null $birthPlanDate
     *
     * @return SubmissionMaternity
     */
    public function setBirthPlanDate($birthPlanDate = null)
    {
        $this->birthPlanDate = $birthPlanDate;

        return $this;
    }

    /**
     * Get birthPlanDate.
     *
     * @return \DateTime|null
     */
    public function getBirthPlanDate()
    {
        return $this->birthPlanDate;
    }

    /**
     * Set decisionMakingConsultDate.
     *
     * @param \DateTime|null $decisionMakingConsultDate
     *
     * @return SubmissionMaternity
     */
    public function setDecisionMakingConsultDate($decisionMakingConsultDate = null)
    {
        $this->decisionMakingConsultDate = $decisionMakingConsultDate;

        return $this;
    }

    /**
     * Get decisionMakingConsultDate.
     *
     * @return \DateTime|null
     */
    public function getDecisionMakingConsultDate()
    {
        return $this->decisionMakingConsultDate;
    }

    /**
     * Set ntsv.
     *
     * @param int|null $ntsv
     *
     * @return SubmissionMaternity
     */
    public function setNtsv($ntsv = null)
    {
        $this->ntsv = $ntsv;

        return $this;
    }

    /**
     * Get ntsv.
     *
     * @return int|null
     */
    public function getNtsv()
    {
        return $this->ntsv;
    }

    /**
     * Set electivecsectionDate.
     *
     * @param \DateTime|null $electivecsectionDate
     *
     * @return SubmissionMaternity
     */
    public function setElectivecsectionDate($electivecsectionDate = null)
    {
        $this->electivecsectionDate = $electivecsectionDate;

        return $this;
    }

    /**
     * Get electivecsectionDate.
     *
     * @return \DateTime|null
     */
    public function getElectivecsectionDate()
    {
        return $this->electivecsectionDate;
    }

    /**
     * Set infantWeight.
     *
     * @param int|null $infantWeight
     *
     * @return SubmissionMaternity
     */
    public function setInfantWeight($infantWeight = null)
    {
        $this->infantWeight = $infantWeight;

        return $this;
    }

    /**
     * Get infantWeight.
     *
     * @return int|null
     */
    public function getInfantWeight()
    {
        return $this->infantWeight;
    }

    /**
     * Set nICUlevel.
     *
     * @param int|null $nICUlevel
     *
     * @return SubmissionMaternity
     */
    public function setNICUlevel($nICUlevel = null)
    {
        $this->NICUlevel = $nICUlevel;

        return $this;
    }

    /**
     * Get nICUlevel.
     *
     * @return int|null
     */
    public function getNICUlevel()
    {
        return $this->NICUlevel;
    }

    /**
     * Set postPartumDepressionScreenDate.
     *
     * @param \DateTime|null $postPartumDepressionScreenDate
     *
     * @return SubmissionMaternity
     */
    public function setPostPartumDepressionScreenDate($postPartumDepressionScreenDate = null)
    {
        $this->postPartumDepressionScreenDate = $postPartumDepressionScreenDate;

        return $this;
    }

    /**
     * Get postPartumDepressionScreenDate.
     *
     * @return \DateTime|null
     */
    public function getPostPartumDepressionScreenDate()
    {
        return $this->postPartumDepressionScreenDate;
    }

    /**
     * Set drugAlcoholScreenDate.
     *
     * @param \DateTime|null $drugAlcoholScreenDate
     *
     * @return SubmissionMaternity
     */
    public function setDrugAlcoholScreenDate($drugAlcoholScreenDate = null)
    {
        $this->drugAlcoholScreenDate = $drugAlcoholScreenDate;

        return $this;
    }

    /**
     * Get drugAlcoholScreenDate.
     *
     * @return \DateTime|null
     */
    public function getDrugAlcoholScreenDate()
    {
        return $this->drugAlcoholScreenDate;
    }

    /**
     * Set interpersonalViolenceScreenDate.
     *
     * @param \DateTime|null $interpersonalViolenceScreenDate
     *
     * @return SubmissionMaternity
     */
    public function setInterpersonalViolenceScreenDate($interpersonalViolenceScreenDate = null)
    {
        $this->interpersonalViolenceScreenDate = $interpersonalViolenceScreenDate;

        return $this;
    }

    /**
     * Get interpersonalViolenceScreenDate.
     *
     * @return \DateTime|null
     */
    public function getInterpersonalViolenceScreenDate()
    {
        return $this->interpersonalViolenceScreenDate;
    }

    /**
     * Set submissionId.
     *
     * @param int|null $submissionId
     *
     * @return SubmissionMaternity
     */
    public function setSubmissionId($submissionId = null)
    {
        $this->submission_id = $submissionId;

        return $this;
    }

    /**
     * Get submissionId.
     *
     * @return int|null
     */
    public function getSubmissionId()
    {
        return $this->submission_id;
    }

    /**
     * Set tempId.
     *
     * @param int|null $tempId
     *
     * @return SubmissionMaternity
     */
    public function setTempId($tempId = null)
    {
        $this->temp_id = $tempId;

        return $this;
    }

    /**
     * Get tempId.
     *
     * @return int|null
     */
    public function getTempId()
    {
        return $this->temp_id;
    }

    /**
     * Set submision.
     *
     * @param \AppBundle\Entity\Submission|null $submision
     *
     * @return SubmissionMaternity
     */
    public function setSubmision(\AppBundle\Entity\Submission $submision = null)
    {
        $this->submision = $submision;

        return $this;
    }

    /**
     * Get submision.
     *
     * @return \AppBundle\Entity\Submission|null
     */
    public function getSubmision()
    {
        return $this->submision;
    }
}
