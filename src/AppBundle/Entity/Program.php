<?php

namespace AppBundle\Entity;

/**
 * Program
 */
class Program
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $table_name;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $submissions;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $measures;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->submissions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->measures = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Program
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set tableName
     *
     * @param string $tableName
     *
     * @return Program
     */
    public function setTableName($tableName)
    {
        $this->table_name = $tableName;

        return $this;
    }

    /**
     * Get tableName
     *
     * @return string
     */
    public function getTableName()
    {
        return $this->table_name;
    }

    /**
     * Add submission
     *
     * @param \AppBundle\Entity\Submission $submission
     *
     * @return Program
     */
    public function addSubmission(\AppBundle\Entity\Submission $submission)
    {
        $this->submissions[] = $submission;

        return $this;
    }

    /**
     * Remove submission
     *
     * @param \AppBundle\Entity\Submission $submission
     */
    public function removeSubmission(\AppBundle\Entity\Submission $submission)
    {
        $this->submissions->removeElement($submission);
    }

    /**
     * Get submissions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubmissions()
    {
        return $this->submissions;
    }

    /**
     * Add measure
     *
     * @param \AppBundle\Entity\Measure $measure
     * @return Program
     */
    public function addMeasure(\AppBundle\Entity\Measure $measure)
    {
        $this->measures[] = $measure;

        return $this;
    }

    /**
     * Remove measure
     *
     * @param \AppBundle\Entity\Measure $measure
     */
    public function removeMeasure(\AppBundle\Entity\Measure $measure)
    {
        $this->measures->removeElement($measure);
    }


    /**
     * Get measures
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMeasures()
    {
        return $this->measures;
    }

    public function __toString(){
        return $this->getName();
    }
}
