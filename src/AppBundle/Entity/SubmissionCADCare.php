<?php

namespace AppBundle\Entity;

/**
 * SubmissionCADCare
 */
class SubmissionCADCare
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $responsibleProviderID;

    /**
     * @var string
     */
    private $npi;

    /**
     * @var string
     */
    private $groupID;

    /**
     * @var string
     */
    private $individualGroup;

    /**
     * @var string
     */
    private $chartID;

    /**
     * @var \DateTime
     */
    private $lastVisitDate;

    /**
     * @var \DateTime
     */
    private $patientDOB;

    /**
     * @var integer
     */
    private $patientGender;

    /**
     * @var boolean
     */
    private $medicarePartB;

    /**
     * @var boolean
     */
    private $cadDiagnosis;

    /**
     * @var \DateTime
     */
    private $bloodPressureDate1;

    /**
     * @var boolean
     */
    private $statinStatus;

    /**
     * @var integer
     */
    private $systolic1;

    /**
     * @var integer
     */
    private $diastolic1;

    /**
     * @var integer
     */
    private $systolic2;

    /**
     * @var integer
     */
    private $diastolic2;

    /**
     * @var \DateTime
     */
    private $bloodPressureDate2;

    /**
     * @var boolean
     */
    private $antiPlatelets;

    /**
     * @var boolean
     */
    private $betaBlockers;

    /**
     * @var boolean
     */
    private $diabetesDiagnosis;

    /**
     * @var boolean
     */
    private $hypertensionDiagnosis;

    /**
     * @var boolean
     */
    private $chronicKidneyDiseaseDiagnosis;

    /**
     * @var boolean
     */
    private $chfDiagnosis;

    /**
     * @var boolean
     */
    private $aceiArbTherapy;

    /**
     * @var boolean
     */
    private $esrdPatient;

    /**
     * @var boolean
     */
    private $dialysisPatient;

    /**
     * @var integer
     */
    private $tobaccoStatus;

    /**
     * @var \DateTime
     */
    private $tobaccoStatusAssessmentDate;

    /**
     * @var boolean
     */
    private $tobaccoCessationAdviceOrTreatment;

    /**
     * @var \DateTime
     */
    private $tobaccoCessationAdviceOrTreatmentDate;

    /**
     * @var float
     */
    private $bmiValue;

    /**
     * @var \DateTime
     */
    private $bmiValueDate;

    /**
     * @var boolean
     */
    private $nutritionCounseling;

    /**
     * @var \DateTime
     */
    private $nutritionCounselingDate;


    /**
     * @var boolean
     */
    private $activityCounseling;

    /**
     * @var \DateTime
     */
    private $activityCounselingDate;

    /**
     * @var boolean
     */
    private $PHQ2screening;

    /**
     * @var \DateTime
     */
    private $PHQ2screeningDate;

    /**
     * @var boolean
     */
    private $PHQ9screening;

    /**
     * @var \DateTime
     */
    private $PHQ9screeningDate;

    /**
     * @var integer
     */
    private $submission_id;

    /**
     * @var integer
     */
    private $temp_id;

    /**
     * @var \AppBundle\Entity\Submission
     */
    private $submision;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set responsibleProviderID
     *
     * @param integer $responsibleProviderID
     *
     * @return SubmissionCADCare
     */
    public function setResponsibleProviderID($responsibleProviderID)
    {
        $this->responsibleProviderID = $responsibleProviderID;

        return $this;
    }

    /**
     * Get responsibleProviderID
     *
     * @return integer
     */
    public function getResponsibleProviderID()
    {
        return $this->responsibleProviderID;
    }

    /**
     * Set npi
     *
     * @param string $npi
     *
     * @return SubmissionCADCare
     */
    public function setNpi($npi)
    {
        $this->npi = $npi;

        return $this;
    }

    /**
     * Get npi
     *
     * @return string
     */
    public function getNpi()
    {
        return $this->npi;
    }

    /**
     * Set groupID
     *
     * @param string $groupID
     *
     * @return SubmissionCADCare
     */
    public function setGroupID($groupID)
    {
        $this->groupID = $groupID;

        return $this;
    }

    /**
     * Get groupID
     *
     * @return string
     */
    public function getGroupID()
    {
        return $this->groupID;
    }

    /**
     * Set individualGroup
     *
     * @param string $individualGroup
     *
     * @return SubmissionCADCare
     */
    public function setIndividualGroup($individualGroup)
    {
        $this->individualGroup = $individualGroup;

        return $this;
    }

    /**
     * Get individualGroup
     *
     * @return string
     */
    public function getIndividualGroup()
    {
        return $this->individualGroup;
    }

    /**
     * Set chartID
     *
     * @param string $chartID
     *
     * @return SubmissionCADCare
     */
    public function setChartID($chartID)
    {
        $this->chartID = $chartID;

        return $this;
    }

    /**
     * Get chartID
     *
     * @return string
     */
    public function getChartID()
    {
        return $this->chartID;
    }

    /**
     * Set lastVisitDate
     *
     * @param \DateTime $lastVisitDate
     *
     * @return SubmissionCADCare
     */
    public function setLastVisitDate($lastVisitDate)
    {
        $this->lastVisitDate = $lastVisitDate;

        return $this;
    }

    /**
     * Get lastVisitDate
     *
     * @return \DateTime
     */
    public function getLastVisitDate()
    {
        return $this->lastVisitDate;
    }

    /**
     * Set patientDOB
     *
     * @param \DateTime $patientDOB
     *
     * @return SubmissionCADCare
     */
    public function setPatientDOB($patientDOB)
    {
        $this->patientDOB = $patientDOB;

        return $this;
    }

    /**
     * Get patientDOB
     *
     * @return \DateTime
     */
    public function getPatientDOB()
    {
        return $this->patientDOB;
    }

    /**
     * Set patientGender
     *
     * @param integer $patientGender
     *
     * @return SubmissionCADCare
     */
    public function setPatientGender($patientGender)
    {
        $this->patientGender = $patientGender;
    }

    /**
     * Get patientGender
     *
     * @return integer
     */
    public function getPatientGender()
    {
        return $this->patientGender;
    }

    /**
     * Set medicarePartB
     *
     * @param boolean $medicarePartB
     *
     * @return SubmissionCADCare
     */
    public function setMedicarePartB($medicarePartB)
    {
        $this->medicarePartB = $medicarePartB;

        return $this;
    }

    /**
     * Get medicarePartB
     *
     * @return boolean
     */
    public function getMedicarePartB()
    {
        return $this->medicarePartB;
    }

    /**
     * Set cadDiagnosis
     *
     * @param boolean $cadDiagnosis
     *
     * @return SubmissionCADCare
     */
    public function setCadDiagnosis($cadDiagnosis)
    {
        $this->cadDiagnosis = $cadDiagnosis;

        return $this;
    }

    /**
     * Get cadDiagnosis
     *
     * @return boolean
     */
    public function getCadDiagnosis()
    {
        return $this->cadDiagnosis;
    }

    /**
     * Set bloodPressureDate1
     *
     * @param \DateTime $bloodPressureDate1
     *
     * @return SubmissionCADCare
     */
    public function setBloodPressureDate1($bloodPressureDate1)
    {
        $this->bloodPressureDate1 = $bloodPressureDate1;

        return $this;
    }

    /**
     * Get bloodPressureDate1
     *
     * @return \DateTime
     */
    public function getBloodPressureDate1()
    {
        return $this->bloodPressureDate1;
    }

    /**
     * Set statinStatus
     *
     * @param boolean $statinStatus
     *
     * @return SubmissionCADCare
     */
    public function setStatinStatus($statinStatus)
    {
        $this->statinStatus = $statinStatus;

        return $this;
    }

    /**
     * Get statinStatus
     *
     * @return boolean
     */
    public function getStatinStatus()
    {
        return $this->statinStatus;
    }

    /**
     * Set systolic1
     *
     * @param integer $systolic1
     *
     * @return SubmissionCADCare
     */
    public function setSystolic1($systolic1)
    {
        $this->systolic1 = $systolic1;

        return $this;
    }

    /**
     * Get systolic1
     *
     * @return integer
     */
    public function getSystolic1()
    {
        return $this->systolic1;
    }

    /**
     * Set diastolic1
     *
     * @param integer $diastolic1
     *
     * @return SubmissionCADCare
     */
    public function setDiastolic1($diastolic1)
    {
        $this->diastolic1 = $diastolic1;

        return $this;
    }

    /**
     * Get diastolic1
     *
     * @return integer
     */
    public function getDiastolic1()
    {
        return $this->diastolic1;
    }

    /**
     * Set systolic2
     *
     * @param integer $systolic2
     *
     * @return SubmissionCADCare
     */
    public function setSystolic2($systolic2)
    {
        $this->systolic2 = $systolic2;

        return $this;
    }

    /**
     * Get systolic2
     *
     * @return integer
     */
    public function getSystolic2()
    {
        return $this->systolic2;
    }

    /**
     * Set diastolic2
     *
     * @param integer $diastolic2
     *
     * @return SubmissionCADCare
     */
    public function setDiastolic2($diastolic2)
    {
        $this->diastolic2 = $diastolic2;

        return $this;
    }

    /**
     * Get diastolic2
     *
     * @return integer
     */
    public function getDiastolic2()
    {
        return $this->diastolic2;
    }

    /**
     * Set bloodPressureDate2
     *
     * @param \DateTime $bloodPressureDate2
     *
     * @return SubmissionCADCare
     */
    public function setBloodPressureDate2($bloodPressureDate2)
    {
        $this->bloodPressureDate2 = $bloodPressureDate2;

        return $this;
    }

    /**
     * Get bloodPressureDate2
     *
     * @return \DateTime
     */
    public function getBloodPressureDate2()
    {
        return $this->bloodPressureDate2;
    }

    /**
     * Set antiPlatelets
     *
     * @param boolean $antiPlatelets
     *
     * @return SubmissionCADCare
     */
    public function setAntiPlatelets($antiPlatelets)
    {
        $this->antiPlatelets = $antiPlatelets;

        return $this;
    }

    /**
     * Get antiPlatelets
     *
     * @return boolean
     */
    public function getAntiPlatelets()
    {
        return $this->antiPlatelets;
    }

    /**
     * Set betaBlockers
     *
     * @param boolean $betaBlockers
     *
     * @return SubmissionCADCare
     */
    public function setBetaBlockers($betaBlockers)
    {
        $this->betaBlockers = $betaBlockers;

        return $this;
    }

    /**
     * Get betaBlockers
     *
     * @return boolean
     */
    public function getBetaBlockers()
    {
        return $this->betaBlockers;
    }

    /**
     * Set diabetesDiagnosis
     *
     * @param boolean $diabetesDiagnosis
     *
     * @return SubmissionCADCare
     */
    public function setDiabetesDiagnosis($diabetesDiagnosis)
    {
        $this->diabetesDiagnosis = $diabetesDiagnosis;

        return $this;
    }

    /**
     * Get diabetesDiagnosis
     *
     * @return boolean
     */
    public function getDiabetesDiagnosis()
    {
        return $this->diabetesDiagnosis;
    }

    /**
     * Set hypertensionDiagnosis
     *
     * @param boolean $hypertensionDiagnosis
     *
     * @return SubmissionCADCare
     */
    public function setHypertensionDiagnosis($hypertensionDiagnosis)
    {
        $this->hypertensionDiagnosis = $hypertensionDiagnosis;

        return $this;
    }

    /**
     * Get hypertensionDiagnosis
     *
     * @return boolean
     */
    public function getHypertensionDiagnosis()
    {
        return $this->hypertensionDiagnosis;
    }

    /**
     * Set chronicKidneyDiseaseDiagnosis
     *
     * @param boolean $chronicKidneyDiseaseDiagnosis
     *
     * @return SubmissionCADCare
     */
    public function setChronicKidneyDiseaseDiagnosis($chronicKidneyDiseaseDiagnosis)
    {
        $this->chronicKidneyDiseaseDiagnosis = $chronicKidneyDiseaseDiagnosis;

        return $this;
    }

    /**
     * Get chronicKidneyDiseaseDiagnosis
     *
     * @return boolean
     */
    public function getChronicKidneyDiseaseDiagnosis()
    {
        return $this->chronicKidneyDiseaseDiagnosis;
    }

    /**
     * Set chfDiagnosis
     *
     * @param boolean $chfDiagnosis
     *
     * @return SubmissionCADCare
     */
    public function setChfDiagnosis($chfDiagnosis)
    {
        $this->chfDiagnosis = $chfDiagnosis;

        return $this;
    }

    /**
     * Get chfDiagnosis
     *
     * @return boolean
     */
    public function getChfDiagnosis()
    {
        return $this->chfDiagnosis;
    }

    /**
     * Set aceiArbTherapy
     *
     * @param boolean $aceiArbTherapy
     *
     * @return SubmissionCADCare
     */
    public function setAceiArbTherapy($aceiArbTherapy)
    {
        $this->aceiArbTherapy = $aceiArbTherapy;

        return $this;
    }

    /**
     * Get aceiArbTherapy
     *
     * @return boolean
     */
    public function getAceiArbTherapy()
    {
        return $this->aceiArbTherapy;
    }

    /**
     * Set esrdPatient
     *
     * @param boolean $esrdPatient
     *
     * @return SubmissionCADCare
     */
    public function setEsrdPatient($esrdPatient)
    {
        $this->esrdPatient = $esrdPatient;

        return $this;
    }

    /**
     * Get esrdPatient
     *
     * @return boolean
     */
    public function getEsrdPatient()
    {
        return $this->esrdPatient;
    }

    /**
     * Set dialysisPatient
     *
     * @param boolean $dialysisPatient
     *
     * @return SubmissionCADCare
     */
    public function setDialysisPatient($dialysisPatient)
    {
        $this->dialysisPatient = $dialysisPatient;

        return $this;
    }

    /**
     * Get dialysisPatient
     *
     * @return boolean
     */
    public function getDialysisPatient()
    {
        return $this->dialysisPatient;
    }

    /**
     * Set tobaccoStatus
     *
     * @param integer $tobaccoStatus
     *
     * @return SubmissionCADCare
     */
    public function setTobaccoStatus($tobaccoStatus)
    {
        $this->tobaccoStatus = $tobaccoStatus;

        return $this;
    }

    /**
     * Get tobaccoStatus
     *
     * @return integer
     */
    public function getTobaccoStatus()
    {
        return $this->tobaccoStatus;
    }

    /**
     * Set tobaccoStatusAssessmentDate
     *
     * @param \DateTime $tobaccoStatusAssessmentDate
     *
     * @return SubmissionCADCare
     */
    public function setTobaccoStatusAssessmentDate($tobaccoStatusAssessmentDate)
    {
        $this->tobaccoStatusAssessmentDate = $tobaccoStatusAssessmentDate;

        return $this;
    }

    /**
     * Get tobaccoStatusAssessmentDate
     *
     * @return \DateTime
     */
    public function getTobaccoStatusAssessmentDate()
    {
        return $this->tobaccoStatusAssessmentDate;
    }

    /**
     * Set tobaccoCessationAdviceOrTreatment
     *
     * @param boolean $tobaccoCessationAdviceOrTreatment
     *
     * @return SubmissionCADCare
     */
    public function setTobaccoCessationAdviceOrTreatment($tobaccoCessationAdviceOrTreatment)
    {
        $this->tobaccoCessationAdviceOrTreatment = $tobaccoCessationAdviceOrTreatment;
        return $this;
    }

    /**
     * Get tobaccoCessationAdviceOrTreatment
     *
     * @return boolean
     */
    public function getTobaccoCessationAdviceOrTreatment()
    {
        return $this->tobaccoCessationAdviceOrTreatment;
    }

    /**
     * Set tobaccoCessationAdviceOrTreatmentDate
     *
     * @param \DateTime $tobaccoCessationAdviceOrTreatmentDate
     *
     * @return SubmissionCADCare
     */
    public function setTobaccoCessationAdviceOrTreatmentDate($tobaccoCessationAdviceOrTreatmentDate)
    {
        $this->tobaccoCessationAdviceOrTreatmentDate = $tobaccoCessationAdviceOrTreatmentDate;

        return $this;
    }

    /**
     * Get tobaccoCessationAdviceOrTreatmentDate
     *
     * @return \DateTime
     */
    public function getTobaccoCessationAdviceOrTreatmentDate()
    {
        return $this->tobaccoCessationAdviceOrTreatmentDate;
    }

    /**
     * Set bmiValue
     *
     * @param float $bmiValue
     *
     * @return SubmissionCADCare
     */
    public function setBmiValue($bmiValue)
    {
        $this->bmiValue = $bmiValue;

        return $this;
    }

    /**
     * Get bmiValue
     *
     * @return float
     */
    public function getBmiValue()
    {
        return $this->bmiValue;
    }

    /**
     * Set bmiValueDate
     *
     * @param \DateTime $bmiValueDate
     *
     * @return SubmissionCADCare
     */
    public function setBmiValueDate($bmiValueDate)
    {
        $this->bmiValueDate = $bmiValueDate;

        return $this;
    }

    /**
     * Get bmiValueDate
     *
     * @return \DateTime
     */
    public function getBmiValueDate()
    {
        return $this->bmiValueDate;
    }

    /**
     * Set nutritionCounseling
     *
     * @param boolean $nutritionCounseling
     *
     * @return SubmissionCADCare
     */
    public function setNutritionCounseling($nutritionCounseling)
    {
        $this->nutritionCounseling = $nutritionCounseling;

        return $this;
    }

    /**
     * Get nutritionCounseling
     *
     * @return boolean
     */
    public function getNutritionCounseling()
    {
        return $this->nutritionCounseling;
    }

    /**
     * Set nutritionCounselingDate
     *
     * @param \DateTime $nutritionCounselingDate
     *
     * @return SubmissionCADCare
     */
    public function setNutritionCounselingDate($nutritionCounselingDate)
    {
        $this->nutritionCounselingDate = $nutritionCounselingDate;

        return $this;
    }

    /**
     * Get nutritionCounselingDate
     *
     * @return \DateTime
     */
    public function getNutritionCounselingDate()
    {
        return $this->nutritionCounselingDate;
    }


    /**
     * Set activityCounseling
     *
     * @param boolean $activityCounseling
     *
     * @return SubmissionCADCare
     */
    public function setActivityCounseling($activityCounseling)
    {
        $this->activityCounseling = $activityCounseling;

        return $this;
    }

    /**
     * Get activityCounseling
     *
     * @return boolean
     */
    public function getActivityCounseling()
    {
        return $this->activityCounseling;
    }

    /**
     * Set activityCounselingDate
     *
     * @param \DateTime $activityCounselingDate
     *
     * @return SubmissionCADCare
     */
    public function setActivityCounselingDate($activityCounselingDate)
    {
        $this->activityCounselingDate = $activityCounselingDate;

        return $this;
    }

    /**
     * Get activityCounselingDate
     *
     * @return \DateTime
     */
    public function getActivityCounselingDate()
    {
        return $this->activityCounselingDate;
    }

    /**
     * Set pHQ2screening
     *
     * @param boolean $pHQ2screening
     *
     * @return SubmissionCADCare
     */
    public function setPHQ2screening($pHQ2screening)
    {
        $this->PHQ2screening = $pHQ2screening;

        return $this;
    }

    /**
     * Get pHQ2screening
     *
     * @return boolean
     */
    public function getPHQ2screening()
    {
        return $this->PHQ2screening;
    }

    /**
     * Set pHQ2screeningDate
     *
     * @param \DateTime $pHQ2screeningDate
     *
     * @return SubmissionCADCare
     */
    public function setPHQ2screeningDate($pHQ2screeningDate)
    {
        $this->PHQ2screeningDate = $pHQ2screeningDate;

        return $this;
    }

    /**
     * Get pHQ2screeningDate
     *
     * @return \DateTime
     */
    public function getPHQ2screeningDate()
    {
        return $this->PHQ2screeningDate;
    }

    /**
     * Set pHQ9screening
     *
     * @param boolean $pHQ9screening
     *
     * @return SubmissionCADCare
     */
    public function setPHQ9screening($pHQ9screening)
    {
        $this->PHQ9screening = $pHQ9screening;

        return $this;
    }

    /**
     * Get pHQ9screening
     *
     * @return boolean
     */
    public function getPHQ9screening()
    {
        return $this->PHQ9screening;
    }

    /**
     * Set pHQ9screeningDate
     *
     * @param \DateTime $pHQ9screeningDate
     *
     * @return SubmissionCADCare
     */
    public function setPHQ9screeningDate($pHQ9screeningDate)
    {
        $this->PHQ9screeningDate = $pHQ9screeningDate;

        return $this;
    }

    /**
     * Get pHQ9screeningDate
     *
     * @return \DateTime
     */
    public function getPHQ9screeningDate()
    {
        return $this->PHQ9screeningDate;
    }

    /**
     * Set submissionId
     *
     * @param integer $submissionId
     *
     * @return SubmissionCADCare
     */
    public function setSubmissionId($submissionId)
    {
        $this->submission_id = $submissionId;

        return $this;
    }

    /**
     * Get submissionId
     *
     * @return integer
     */
    public function getSubmissionId()
    {
        return $this->submission_id;
    }

    /**
     * Set tempId
     *
     * @param integer $tempId
     *
     * @return SubmissionCADCare
     */
    public function setTempId($tempId)
    {
        $this->temp_id = $tempId;

        return $this;
    }

    /**
     * Get tempId
     *
     * @return integer
     */
    public function getTempId()
    {
        return $this->temp_id;
    }

    /**
     * Set submision
     *
     * @param \AppBundle\Entity\Submission $submision
     *
     * @return SubmissionCADCare
     */
    public function setSubmision(\AppBundle\Entity\Submission $submision = null)
    {
        $this->submision = $submision;

        return $this;
    }

    /**
     * Get submision
     *
     * @return \AppBundle\Entity\Submission
     */
    public function getSubmision()
    {
        return $this->submision;
    }
    /**
     * @var integer
     */
    private $influenzaImmunization;

    /**
     * @var \DateTime
     */
    private $influenzaImmunizationDate;


    /**
     * Set influenzaImmunization
     *
     * @param integer $influenzaImmunization
     *
     * @return SubmissionCADCare
     */
    public function setInfluenzaImmunization($influenzaImmunization)
    {
        $this->influenzaImmunization = $influenzaImmunization;

        return $this;
    }

    /**
     * Get influenzaImmunization
     *
     * @return integer
     */
    public function getInfluenzaImmunization()
    {
        return $this->influenzaImmunization;
    }

    /**
     * Set influenzaImmunizationDate
     *
     * @param \DateTime $influenzaImmunizationDate
     *
     * @return SubmissionCADCare
     */
    public function setInfluenzaImmunizationDate($influenzaImmunizationDate)
    {
        $this->influenzaImmunizationDate = $influenzaImmunizationDate;

        return $this;
    }

    /**
     * Get influenzaImmunizationDate
     *
     * @return \DateTime
     */
    public function getInfluenzaImmunizationDate()
    {
        return $this->influenzaImmunizationDate;
    }
}
