<?php

namespace AppBundle\Entity;

/**
 * SubmissionDepression
 */
class SubmissionDepression
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $responsibleProviderID;

    /**
     * @var string
     */
    private $npi;

    /**
     * @var string
     */
    private $groupID;

    /**
     * @var string
     */
    private $individualGroup;

    /**
     * @var string
     */
    private $chartID;

    /**
     * @var \DateTime
     */
    private $lastVisitDate;

    /**
     * @var \DateTime
     */
    private $patientDOB;

    /**
     * @var integer
     */
    private $patientGender;

    /**
     * @var string
     */
    private $patientRace;

    /**
     * @var boolean
     */
    private $medicarePartB;

    /**
     * @var integer
     */
    private $PHQ2ScreeningScore;

    /**
     * @var \DateTime
     */
    private $PHQ2ScreeningDate;

    /**
     * @var integer
     */
    private $PHQ9ScreeningScore;

    /**
     * @var \DateTime
     */
    private $PHQ9ScreeningDate;

    /**
     * @var integer
     */
    private $PHQ9Screening2Score;

    /**
     * @var \DateTime
     */
    private $PHQ9Screening2Date;

    /**
     * @var integer
     */
    private $PHQAdolescentScreeningScore;

    /**
     * @var \DateTime
     */
    private $PHQAdolescentScreeningDate;

    /**
     * @var \DateTime
     */
    private $cognitiveBehaviorTherapyDate;

    /**
     * @var \DateTime
     */
    private $specialistReferralDate;

    /**
     * @var \DateTime
     */
    private $psychotherapyDate;

    /**
     * @var \DateTime
     */
    private $antidepressantInitialDispensedDate;

    /**
     * @var \DateTime
     */
    private $antidepressantActiveDate;

    /**
     * @var \DateTime
     */
    private $antidepressantInitialPrescribedDate;

    /**
     * @var boolean
     */
    private $depressionDiagnosis;

    /**
     * @var boolean
     */
    private $dysthymiaDiagnosis;

    /**
     * @var boolean
     */
    private $substanceUseScreening;

    /**
     * @var \DateTime
     */
    private $substanceUseScreeningDate;

    /**
     * @var \DateTime
     */
    private $substanceUseCounselingDate;

    /**
     * @var boolean
     */
    private $activityCounseling;

    /**
     * @var \DateTime
     */
    private $activityCounselingDate;

    /**
     * @var boolean
     */
    private $careCoordination;

    /**
     * @var boolean
     */
    private $monitoringandTreatment;

    /**
     * @var boolean
     */
    private $caseLoadReviews;

    /**
     * @var integer
     */
    private $submission_id;

    /**
     * @var integer
     */
    private $temp_id;

    /**
     * @var \AppBundle\Entity\Submission
     */
    private $submision;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set responsibleProviderID
     *
     * @param string $responsibleProviderID
     *
     * @return SubmissionDepression
     */
    public function setResponsibleProviderID($responsibleProviderID)
    {
        $this->responsibleProviderID = $responsibleProviderID;

        return $this;
    }

    /**
     * Get responsibleProviderID
     *
     * @return string
     */
    public function getResponsibleProviderID()
    {
        return $this->responsibleProviderID;
    }

    /**
     * Set npi
     *
     * @param string $npi
     *
     * @return SubmissionDepression
     */
    public function setNpi($npi)
    {
        $this->npi = $npi;

        return $this;
    }

    /**
     * Get npi
     *
     * @return string
     */
    public function getNpi()
    {
        return $this->npi;
    }

    /**
     * Set groupID
     *
     * @param string $groupID
     *
     * @return SubmissionDepression
     */
    public function setGroupID($groupID)
    {
        $this->groupID = $groupID;

        return $this;
    }

    /**
     * Get groupID
     *
     * @return string
     */
    public function getGroupID()
    {
        return $this->groupID;
    }

    /**
     * Set individualGroup
     *
     * @param string $individualGroup
     *
     * @return SubmissionDepression
     */
    public function setIndividualGroup($individualGroup)
    {
        $this->individualGroup = $individualGroup;

        return $this;
    }

    /**
     * Get individualGroup
     *
     * @return string
     */
    public function getIndividualGroup()
    {
        return $this->individualGroup;
    }

    /**
     * Set chartID
     *
     * @param string $chartID
     *
     * @return SubmissionDepression
     */
    public function setChartID($chartID)
    {
        $this->chartID = $chartID;

        return $this;
    }

    /**
     * Get chartID
     *
     * @return string
     */
    public function getChartID()
    {
        return $this->chartID;
    }

    /**
     * Set lastVisitDate
     *
     * @param \DateTime $lastVisitDate
     *
     * @return SubmissionDepression
     */
    public function setLastVisitDate($lastVisitDate)
    {
        $this->lastVisitDate = $lastVisitDate;

        return $this;
    }

    /**
     * Get lastVisitDate
     *
     * @return \DateTime
     */
    public function getLastVisitDate()
    {
        return $this->lastVisitDate;
    }

    /**
     * Set patientDOB
     *
     * @param \DateTime $patientDOB
     *
     * @return SubmissionDepression
     */
    public function setPatientDOB($patientDOB)
    {
        $this->patientDOB = $patientDOB;

        return $this;
    }

    /**
     * Get patientDOB
     *
     * @return \DateTime
     */
    public function getPatientDOB()
    {
        return $this->patientDOB;
    }

    /**
     * Set patientGender
     *
     * @param integer $patientGender
     *
     * @return SubmissionDepression
     */
    public function setPatientGender($patientGender)
    {
        $this->patientGender = $patientGender;

        return $this;
    }

    /**
     * Get patientGender
     *
     * @return integer
     */
    public function getPatientGender()
    {
        return $this->patientGender;
    }

    /**
     * Set patientRace
     *
     * @param string $patientRace
     *
     * @return SubmissionDepression
     */
    public function setPatientRace($patientRace)
    {
        $this->patientRace = $patientRace;

        return $this;
    }

    /**
     * Get patientRace
     *
     * @return string
     */
    public function getPatientRace()
    {
        return $this->patientRace;
    }

    /**
     * Set medicarePartB
     *
     * @param boolean $medicarePartB
     *
     * @return SubmissionDepression
     */
    public function setMedicarePartB($medicarePartB)
    {
        $this->medicarePartB = $medicarePartB;

        return $this;
    }

    /**
     * Get medicarePartB
     *
     * @return boolean
     */
    public function getMedicarePartB()
    {
        return $this->medicarePartB;
    }

    /**
     * Set pHQ2ScreeningScore
     *
     * @param integer $pHQ2ScreeningScore
     *
     * @return SubmissionDepression
     */
    public function setPHQ2ScreeningScore($pHQ2ScreeningScore)
    {
        $this->PHQ2ScreeningScore = $pHQ2ScreeningScore;

        return $this;
    }

    /**
     * Get pHQ2ScreeningScore
     *
     * @return integer
     */
    public function getPHQ2ScreeningScore()
    {
        return $this->PHQ2ScreeningScore;
    }

    /**
     * Set pHQ2ScreeningDate
     *
     * @param \DateTime $pHQ2ScreeningDate
     *
     * @return SubmissionDepression
     */
    public function setPHQ2ScreeningDate($pHQ2ScreeningDate)
    {
        $this->PHQ2ScreeningDate = $pHQ2ScreeningDate;

        return $this;
    }

    /**
     * Get pHQ2ScreeningDate
     *
     * @return \DateTime
     */
    public function getPHQ2ScreeningDate()
    {
        return $this->PHQ2ScreeningDate;
    }

    /**
     * Set pHQ9ScreeningScore
     *
     * @param integer $pHQ9ScreeningScore
     *
     * @return SubmissionDepression
     */
    public function setPHQ9ScreeningScore($pHQ9ScreeningScore)
    {
        $this->PHQ9ScreeningScore = $pHQ9ScreeningScore;

        return $this;
    }

    /**
     * Get pHQ9ScreeningScore
     *
     * @return integer
     */
    public function getPHQ9ScreeningScore()
    {
        return $this->PHQ9ScreeningScore;
    }

    /**
     * Set pHQ9ScreeningDate
     *
     * @param \DateTime $pHQ9ScreeningDate
     *
     * @return SubmissionDepression
     */
    public function setPHQ9ScreeningDate($pHQ9ScreeningDate)
    {
        $this->PHQ9ScreeningDate = $pHQ9ScreeningDate;

        return $this;
    }

    /**
     * Get pHQ9ScreeningDate
     *
     * @return \DateTime
     */
    public function getPHQ9ScreeningDate()
    {
        return $this->PHQ9ScreeningDate;
    }

    /**
     * Set pHQ9Screening2Score
     *
     * @param integer $pHQ9Screening2Score
     *
     * @return SubmissionDepression
     */
    public function setPHQ9Screening2Score($pHQ9Screening2Score)
    {
        $this->PHQ9Screening2Score = $pHQ9Screening2Score;

        return $this;
    }

    /**
     * Get pHQ9Screening2Score
     *
     * @return integer
     */
    public function getPHQ9Screening2Score()
    {
        return $this->PHQ9Screening2Score;
    }

    /**
     * Set pHQ9Screening2Date
     *
     * @param \DateTime $pHQ9Screening2Date
     *
     * @return SubmissionDepression
     */
    public function setPHQ9Screening2Date($pHQ9Screening2Date)
    {
        $this->PHQ9Screening2Date = $pHQ9Screening2Date;

        return $this;
    }

    /**
     * Get pHQ9Screening2Date
     *
     * @return \DateTime
     */
    public function getPHQ9Screening2Date()
    {
        return $this->PHQ9Screening2Date;
    }

    /**
     * Set pHQAdolescentScreeningScore
     *
     * @param integer $pHQAdolescentScreeningScore
     *
     * @return SubmissionDepression
     */
    public function setPHQAdolescentScreeningScore($pHQAdolescentScreeningScore)
    {
        $this->PHQAdolescentScreeningScore = $pHQAdolescentScreeningScore;

        return $this;
    }

    /**
     * Get pHQAdolescentScreeningScore
     *
     * @return integer
     */
    public function getPHQAdolescentScreeningScore()
    {
        return $this->PHQAdolescentScreeningScore;
    }

    /**
     * Set pHQAdolescentScreeningDate
     *
     * @param \DateTime $pHQAdolescentScreeningDate
     *
     * @return SubmissionDepression
     */
    public function setPHQAdolescentScreeningDate($pHQAdolescentScreeningDate)
    {
        $this->PHQAdolescentScreeningDate = $pHQAdolescentScreeningDate;

        return $this;
    }

    /**
     * Get pHQAdolescentScreeningDate
     *
     * @return \DateTime
     */
    public function getPHQAdolescentScreeningDate()
    {
        return $this->PHQAdolescentScreeningDate;
    }

    /**
     * Set cognitiveBehaviorTherapyDate
     *
     * @param \DateTime $cognitiveBehaviorTherapyDate
     *
     * @return SubmissionDepression
     */
    public function setCognitiveBehaviorTherapyDate($cognitiveBehaviorTherapyDate)
    {
        $this->cognitiveBehaviorTherapyDate = $cognitiveBehaviorTherapyDate;

        return $this;
    }

    /**
     * Get cognitiveBehaviorTherapyDate
     *
     * @return \DateTime
     */
    public function getCognitiveBehaviorTherapyDate()
    {
        return $this->cognitiveBehaviorTherapyDate;
    }

    /**
     * Set specialistReferralDate
     *
     * @param \DateTime $specialistReferralDate
     *
     * @return SubmissionDepression
     */
    public function setSpecialistReferralDate($specialistReferralDate)
    {
        $this->specialistReferralDate = $specialistReferralDate;

        return $this;
    }

    /**
     * Get specialistReferralDate
     *
     * @return \DateTime
     */
    public function getSpecialistReferralDate()
    {
        return $this->specialistReferralDate;
    }

    /**
     * Set psychotherapyDate
     *
     * @param \DateTime $psychotherapyDate
     *
     * @return SubmissionDepression
     */
    public function setPsychotherapyDate($psychotherapyDate)
    {
        $this->psychotherapyDate = $psychotherapyDate;

        return $this;
    }

    /**
     * Get psychotherapyDate
     *
     * @return \DateTime
     */
    public function getPsychotherapyDate()
    {
        return $this->psychotherapyDate;
    }

    /**
     * Set antidepressantInitialDispensedDate
     *
     * @param \DateTime $antidepressantInitialDispensedDate
     *
     * @return SubmissionDepression
     */
    public function setAntidepressantInitialDispensedDate($antidepressantInitialDispensedDate)
    {
        $this->antidepressantInitialDispensedDate = $antidepressantInitialDispensedDate;

        return $this;
    }

    /**
     * Get antidepressantInitialDispensedDate
     *
     * @return \DateTime
     */
    public function getAntidepressantInitialDispensedDate()
    {
        return $this->antidepressantInitialDispensedDate;
    }

    /**
     * Set antidepressantActiveDate
     *
     * @param \DateTime $antidepressantActiveDate
     *
     * @return SubmissionDepression
     */
    public function setAntidepressantActiveDate($antidepressantActiveDate)
    {
        $this->antidepressantActiveDate = $antidepressantActiveDate;

        return $this;
    }

    /**
     * Get antidepressantActiveDate
     *
     * @return \DateTime
     */
    public function getAntidepressantActiveDate()
    {
        return $this->antidepressantActiveDate;
    }

    /**
     * Set antidepressantInitialPrescribedDate
     *
     * @param \DateTime $antidepressantInitialPrescribedDate
     *
     * @return SubmissionDepression
     */
    public function setAntidepressantInitialPrescribedDate($antidepressantInitialPrescribedDate)
    {
        $this->antidepressantInitialPrescribedDate = $antidepressantInitialPrescribedDate;

        return $this;
    }

    /**
     * Get antidepressantInitialPrescribedDate
     *
     * @return \DateTime
     */
    public function getAntidepressantInitialPrescribedDate()
    {
        return $this->antidepressantInitialPrescribedDate;
    }

    /**
     * Set depressionDiagnosis
     *
     * @param boolean $depressionDiagnosis
     *
     * @return SubmissionDepression
     */
    public function setDepressionDiagnosis($depressionDiagnosis)
    {
        $this->depressionDiagnosis = $depressionDiagnosis;

        return $this;
    }

    /**
     * Get depressionDiagnosis
     *
     * @return boolean
     */
    public function getDepressionDiagnosis()
    {
        return $this->depressionDiagnosis;
    }

    /**
     * Set dysthymiaDiagnosis
     *
     * @param boolean $dysthymiaDiagnosis
     *
     * @return SubmissionDepression
     */
    public function setDysthymiaDiagnosis($dysthymiaDiagnosis)
    {
        $this->dysthymiaDiagnosis = $dysthymiaDiagnosis;

        return $this;
    }

    /**
     * Get dysthymiaDiagnosis
     *
     * @return boolean
     */
    public function getDysthymiaDiagnosis()
    {
        return $this->dysthymiaDiagnosis;
    }

    /**
     * Set substanceUseScreening
     *
     * @param boolean $substanceUseScreening
     *
     * @return SubmissionDepression
     */
    public function setSubstanceUseScreening($substanceUseScreening)
    {
        $this->substanceUseScreening = $substanceUseScreening;

        return $this;
    }

    /**
     * Get substanceUseScreening
     *
     * @return boolean
     */
    public function getSubstanceUseScreening()
    {
        return $this->substanceUseScreening;
    }

    /**
     * Set substanceUseScreeningDate
     *
     * @param \DateTime $substanceUseScreeningDate
     *
     * @return SubmissionDepression
     */
    public function setSubstanceUseScreeningDate($substanceUseScreeningDate)
    {
        $this->substanceUseScreeningDate = $substanceUseScreeningDate;

        return $this;
    }

    /**
     * Get substanceUseScreeningDate
     *
     * @return \DateTime
     */
    public function getSubstanceUseScreeningDate()
    {
        return $this->substanceUseScreeningDate;
    }

    /**
     * Set substanceUseCounselingDate
     *
     * @param \DateTime $substanceUseCounselingDate
     *
     * @return SubmissionDepression
     */
    public function setSubstanceUseCounselingDate($substanceUseCounselingDate)
    {
        $this->substanceUseCounselingDate = $substanceUseCounselingDate;

        return $this;
    }

    /**
     * Get substanceUseCounselingDate
     *
     * @return \DateTime
     */
    public function getSubstanceUseCounselingDate()
    {
        return $this->substanceUseCounselingDate;
    }

    /**
     * Set activityCounseling
     *
     * @param boolean $activityCounseling
     *
     * @return SubmissionDepression
     */
    public function setActivityCounseling($activityCounseling)
    {
        $this->activityCounseling = $activityCounseling;

        return $this;
    }

    /**
     * Get activityCounseling
     *
     * @return boolean
     */
    public function getActivityCounseling()
    {
        return $this->activityCounseling;
    }

    /**
     * Set activityCounselingDate
     *
     * @param \DateTime $activityCounselingDate
     *
     * @return SubmissionDepression
     */
    public function setActivityCounselingDate($activityCounselingDate)
    {
        $this->activityCounselingDate = $activityCounselingDate;

        return $this;
    }

    /**
     * Get activityCounselingDate
     *
     * @return \DateTime
     */
    public function getActivityCounselingDate()
    {
        return $this->activityCounselingDate;
    }

    /**
     * Set careCoordination
     *
     * @param boolean $careCoordination
     *
     * @return SubmissionDepression
     */
    public function setCareCoordination($careCoordination)
    {
        $this->careCoordination = $careCoordination;

        return $this;
    }

    /**
     * Get careCoordination
     *
     * @return boolean
     */
    public function getCareCoordination()
    {
        return $this->careCoordination;
    }

    /**
     * Set monitoringandTreatment
     *
     * @param boolean $monitoringandTreatment
     *
     * @return SubmissionDepression
     */
    public function setMonitoringandTreatment($monitoringandTreatment)
    {
        $this->monitoringandTreatment = $monitoringandTreatment;

        return $this;
    }

    /**
     * Get monitoringandTreatment
     *
     * @return boolean
     */
    public function getMonitoringandTreatment()
    {
        return $this->monitoringandTreatment;
    }

    /**
     * Set caseLoadReviews
     *
     * @param boolean $caseLoadReviews
     *
     * @return SubmissionDepression
     */
    public function setCaseLoadReviews($caseLoadReviews)
    {
        $this->caseLoadReviews = $caseLoadReviews;

        return $this;
    }

    /**
     * Get caseLoadReviews
     *
     * @return boolean
     */
    public function getCaseLoadReviews()
    {
        return $this->caseLoadReviews;
    }

    /**
     * Set submissionId
     *
     * @param integer $submissionId
     *
     * @return SubmissionDepression
     */
    public function setSubmissionId($submissionId)
    {
        $this->submission_id = $submissionId;

        return $this;
    }

    /**
     * Get submissionId
     *
     * @return integer
     */
    public function getSubmissionId()
    {
        return $this->submission_id;
    }

    /**
     * Set tempId
     *
     * @param integer $tempId
     *
     * @return SubmissionDepression
     */
    public function setTempId($tempId)
    {
        $this->temp_id = $tempId;

        return $this;
    }

    /**
     * Get tempId
     *
     * @return integer
     */
    public function getTempId()
    {
        return $this->temp_id;
    }

    /**
     * Set submision
     *
     * @param \AppBundle\Entity\Submission $submision
     *
     * @return SubmissionDepression
     */
    public function setSubmision(\AppBundle\Entity\Submission $submision = null)
    {
        $this->submision = $submision;

        return $this;
    }

    /**
     * Get submision
     *
     * @return \AppBundle\Entity\Submission
     */
    public function getSubmision()
    {
        return $this->submision;
    }
}
