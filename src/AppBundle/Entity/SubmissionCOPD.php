<?php

namespace AppBundle\Entity;

/**
 * SubmissionCOPD
 */
class SubmissionCOPD
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $responsibleProviderID;

    /**
     * @var string
     */
    private $npi;

    /**
     * @var string
     */
    private $groupID;

    /**
     * @var string
     */
    private $individualGroup;

    /**
     * @var string
     */
    private $chartID;

    /**
     * @var \DateTime
     */
    private $lastVisitDate;

    /**
     * @var \DateTime
     */
    private $patientDOB;

    /**
     * @var integer
     */
    private $patientGender;

    /**
     * @var string
     */
    private $patientRace;

    /**
     * @var boolean
     */
    private $medicarePartB;

    /**
     * @var boolean
     */
    private $COPDDiagnosis;

    /**
     * @var boolean
     */
    private $COPDSymptoms;

    /**
     * @var integer
     */
    private $spirometryEvalResults;

    /**
     * @var \DateTime
     */
    private $spirometryEvalDate;

    /**
     * @var \DateTime
     */
    private $inhaledBronchodilatorPrescribedDate;

    /**
     * @var \DateTime
     */
    private $inhaledCorticosteroidDispensedDate;

    /**
     * @var \DateTime
     */
    private $inhaledCorticosteroidPrescribedDate;

    /**
     * @var integer
     */
    private $tobaccoStatus;

    /**
     * @var \DateTime
     */
    private $tobaccoStatusAssessmentDate;

    /**
     * @var \DateTime
     */
    private $tobaccoCessationAdviceOrTreatmentDate;

    /**
     * @var boolean
     */
    private $respiratoryFailureDiagnosis;

    /**
     * @var boolean
     */
    private $rightHeartFailureDiagnosis;

    /**
     * @var integer
     */
    private $oxygenSaturationLevel;

    /**
     * @var boolean
     */
    private $oxygenSaturationLevelDate;

    /**
     * @var integer
     */
    private $paO2Level;

    /**
     * @var \DateTime
     */
    private $paO2LevelDate;

    /**
     * @var \DateTime
     */
    private $longTermOxygenTherapyDate;

    /**
     * @var integer
     */
    private $influenzaImmunization;

    /**
     * @var \DateTime
     */
    private $influenzaImmunizationDate;

    /**
     * @var integer
     */
    private $pneumococcalVaccinePCV13;

    /**
     * @var \DateTime
     */
    private $pneumococcalVaccinePCV13Date;

    /**
     * @var integer
     */
    private $pneumococcalVaccinePCV23;

    /**
     * @var \DateTime
     */
    private $pneumococcalVaccinePCV23Date;

    /**
     * @var integer
     */
    private $COPDExacerbationEpisode;

    /**
     * @var \DateTime
     */
    private $COPDExacerbationEpisodeDate;

    /**
     * @var \DateTime
     */
    private $COPDExacerbationEpisodeStateDate;

    /**
     * @var \DateTime
     */
    private $faceToFaceVisit;

    /**
     * @var \DateTime
     */
    private $lowDoseCTDate;

    /**
     * @var boolean
     */
    private $lowDoseCTReferral;

    /**
     * @var boolean
     */
    private $lowDoseCTDocumented;

    /**
     * @var integer
     */
    private $advancedDirective;

    /**
     * @var integer
     */
    private $submission_id;

    /**
     * @var integer
     */
    private $temp_id;

    /**
     * @var \AppBundle\Entity\Submission
     */
    private $submision;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set responsibleProviderID
     *
     * @param string $responsibleProviderID
     *
     * @return SubmissionCOPD
     */
    public function setResponsibleProviderID($responsibleProviderID)
    {
        $this->responsibleProviderID = $responsibleProviderID;

        return $this;
    }

    /**
     * Get responsibleProviderID
     *
     * @return string
     */
    public function getResponsibleProviderID()
    {
        return $this->responsibleProviderID;
    }

    /**
     * Set npi
     *
     * @param string $npi
     *
     * @return SubmissionCOPD
     */
    public function setNpi($npi)
    {
        $this->npi = $npi;

        return $this;
    }

    /**
     * Get npi
     *
     * @return string
     */
    public function getNpi()
    {
        return $this->npi;
    }

    /**
     * Set groupID
     *
     * @param string $groupID
     *
     * @return SubmissionCOPD
     */
    public function setGroupID($groupID)
    {
        $this->groupID = $groupID;

        return $this;
    }

    /**
     * Get groupID
     *
     * @return string
     */
    public function getGroupID()
    {
        return $this->groupID;
    }

    /**
     * Set individualGroup
     *
     * @param string $individualGroup
     *
     * @return SubmissionCOPD
     */
    public function setIndividualGroup($individualGroup)
    {
        $this->individualGroup = $individualGroup;

        return $this;
    }

    /**
     * Get individualGroup
     *
     * @return string
     */
    public function getIndividualGroup()
    {
        return $this->individualGroup;
    }

    /**
     * Set chartID
     *
     * @param string $chartID
     *
     * @return SubmissionCOPD
     */
    public function setChartID($chartID)
    {
        $this->chartID = $chartID;

        return $this;
    }

    /**
     * Get chartID
     *
     * @return string
     */
    public function getChartID()
    {
        return $this->chartID;
    }

    /**
     * Set lastVisitDate
     *
     * @param \DateTime $lastVisitDate
     *
     * @return SubmissionCOPD
     */
    public function setLastVisitDate($lastVisitDate)
    {
        $this->lastVisitDate = $lastVisitDate;

        return $this;
    }

    /**
     * Get lastVisitDate
     *
     * @return \DateTime
     */
    public function getLastVisitDate()
    {
        return $this->lastVisitDate;
    }

    /**
     * Set patientDOB
     *
     * @param \DateTime $patientDOB
     *
     * @return SubmissionCOPD
     */
    public function setPatientDOB($patientDOB)
    {
        $this->patientDOB = $patientDOB;

        return $this;
    }

    /**
     * Get patientDOB
     *
     * @return \DateTime
     */
    public function getPatientDOB()
    {
        return $this->patientDOB;
    }

    /**
     * Set patientGender
     *
     * @param integer $patientGender
     *
     * @return SubmissionCOPD
     */
    public function setPatientGender($patientGender)
    {
        $this->patientGender = $patientGender;

        return $this;
    }

    /**
     * Get patientGender
     *
     * @return integer
     */
    public function getPatientGender()
    {
        return $this->patientGender;
    }

    /**
     * Set patientRace
     *
     * @param string $patientRace
     *
     * @return SubmissionCOPD
     */
    public function setPatientRace($patientRace)
    {
        $this->patientRace = $patientRace;

        return $this;
    }

    /**
     * Get patientRace
     *
     * @return string
     */
    public function getPatientRace()
    {
        return $this->patientRace;
    }

    /**
     * Set medicarePartB
     *
     * @param boolean $medicarePartB
     *
     * @return SubmissionCOPD
     */
    public function setMedicarePartB($medicarePartB)
    {
        $this->medicarePartB = $medicarePartB;

        return $this;
    }

    /**
     * Get medicarePartB
     *
     * @return boolean
     */
    public function getMedicarePartB()
    {
        return $this->medicarePartB;
    }

    /**
     * Set cOPDDiagnosis
     *
     * @param boolean $cOPDDiagnosis
     *
     * @return SubmissionCOPD
     */
    public function setCOPDDiagnosis($cOPDDiagnosis)
    {
        $this->COPDDiagnosis = $cOPDDiagnosis;

        return $this;
    }

    /**
     * Get cOPDDiagnosis
     *
     * @return boolean
     */
    public function getCOPDDiagnosis()
    {
        return $this->COPDDiagnosis;
    }

    /**
     * Set cOPDSymptoms
     *
     * @param boolean $cOPDSymptoms
     *
     * @return SubmissionCOPD
     */
    public function setCOPDSymptoms($cOPDSymptoms)
    {
        $this->COPDSymptoms = $cOPDSymptoms;

        return $this;
    }

    /**
     * Get cOPDSymptoms
     *
     * @return boolean
     */
    public function getCOPDSymptoms()
    {
        return $this->COPDSymptoms;
    }

    /**
     * Set spirometryEvalResults
     *
     * @param integer $spirometryEvalResults
     *
     * @return SubmissionCOPD
     */
    public function setSpirometryEvalResults($spirometryEvalResults)
    {
        $this->spirometryEvalResults = $spirometryEvalResults;

        return $this;
    }

    /**
     * Get spirometryEvalResults
     *
     * @return integer
     */
    public function getSpirometryEvalResults()
    {
        return $this->spirometryEvalResults;
    }

    /**
     * Set spirometryEvalDate
     *
     * @param \DateTime $spirometryEvalDate
     *
     * @return SubmissionCOPD
     */
    public function setSpirometryEvalDate($spirometryEvalDate)
    {
        $this->spirometryEvalDate = $spirometryEvalDate;

        return $this;
    }

    /**
     * Get spirometryEvalDate
     *
     * @return \DateTime
     */
    public function getSpirometryEvalDate()
    {
        return $this->spirometryEvalDate;
    }

    /**
     * Set inhaledBronchodilatorPrescribedDate
     *
     * @param \DateTime $inhaledBronchodilatorPrescribedDate
     *
     * @return SubmissionCOPD
     */
    public function setInhaledBronchodilatorPrescribedDate($inhaledBronchodilatorPrescribedDate)
    {
        $this->inhaledBronchodilatorPrescribedDate = $inhaledBronchodilatorPrescribedDate;

        return $this;
    }

    /**
     * Get inhaledBronchodilatorPrescribedDate
     *
     * @return \DateTime
     */
    public function getInhaledBronchodilatorPrescribedDate()
    {
        return $this->inhaledBronchodilatorPrescribedDate;
    }

    /**
     * Set inhaledCorticosteroidDispensedDate
     *
     * @param \DateTime $inhaledCorticosteroidDispensedDate
     *
     * @return SubmissionCOPD
     */
    public function setInhaledCorticosteroidDispensedDate($inhaledCorticosteroidDispensedDate)
    {
        $this->inhaledCorticosteroidDispensedDate = $inhaledCorticosteroidDispensedDate;

        return $this;
    }

    /**
     * Get inhaledCorticosteroidDispensedDate
     *
     * @return \DateTime
     */
    public function getInhaledCorticosteroidDispensedDate()
    {
        return $this->inhaledCorticosteroidDispensedDate;
    }

    /**
     * Set inhaledCorticosteroidPrescribedDate
     *
     * @param \DateTime $inhaledCorticosteroidPrescribedDate
     *
     * @return SubmissionCOPD
     */
    public function setInhaledCorticosteroidPrescribedDate($inhaledCorticosteroidPrescribedDate)
    {
        $this->inhaledCorticosteroidPrescribedDate = $inhaledCorticosteroidPrescribedDate;

        return $this;
    }

    /**
     * Get inhaledCorticosteroidPrescribedDate
     *
     * @return \DateTime
     */
    public function getInhaledCorticosteroidPrescribedDate()
    {
        return $this->inhaledCorticosteroidPrescribedDate;
    }

    /**
     * Set tobaccoStatus
     *
     * @param integer $tobaccoStatus
     *
     * @return SubmissionCOPD
     */
    public function setTobaccoStatus($tobaccoStatus)
    {
        $this->tobaccoStatus = $tobaccoStatus;

        return $this;
    }

    /**
     * Get tobaccoStatus
     *
     * @return integer
     */
    public function getTobaccoStatus()
    {
        return $this->tobaccoStatus;
    }

    /**
     * Set tobaccoStatusAssessmentDate
     *
     * @param \DateTime $tobaccoStatusAssessmentDate
     *
     * @return SubmissionCOPD
     */
    public function setTobaccoStatusAssessmentDate($tobaccoStatusAssessmentDate)
    {
        $this->tobaccoStatusAssessmentDate = $tobaccoStatusAssessmentDate;

        return $this;
    }

    /**
     * Get tobaccoStatusAssessmentDate
     *
     * @return \DateTime
     */
    public function getTobaccoStatusAssessmentDate()
    {
        return $this->tobaccoStatusAssessmentDate;
    }

    /**
     * Set tobaccoCessationAdviceOrTreatmentDate
     *
     * @param \DateTime $tobaccoCessationAdviceOrTreatmentDate
     *
     * @return SubmissionCOPD
     */
    public function setTobaccoCessationAdviceOrTreatmentDate($tobaccoCessationAdviceOrTreatmentDate)
    {
        $this->tobaccoCessationAdviceOrTreatmentDate = $tobaccoCessationAdviceOrTreatmentDate;

        return $this;
    }

    /**
     * Get tobaccoCessationAdviceOrTreatmentDate
     *
     * @return \DateTime
     */
    public function getTobaccoCessationAdviceOrTreatmentDate()
    {
        return $this->tobaccoCessationAdviceOrTreatmentDate;
    }

    /**
     * Set respiratoryFailureDiagnosis
     *
     * @param boolean $respiratoryFailureDiagnosis
     *
     * @return SubmissionCOPD
     */
    public function setRespiratoryFailureDiagnosis($respiratoryFailureDiagnosis)
    {
        $this->respiratoryFailureDiagnosis = $respiratoryFailureDiagnosis;

        return $this;
    }

    /**
     * Get respiratoryFailureDiagnosis
     *
     * @return boolean
     */
    public function getRespiratoryFailureDiagnosis()
    {
        return $this->respiratoryFailureDiagnosis;
    }

    /**
     * Set rightHeartFailureDiagnosis
     *
     * @param boolean $rightHeartFailureDiagnosis
     *
     * @return SubmissionCOPD
     */
    public function setRightHeartFailureDiagnosis($rightHeartFailureDiagnosis)
    {
        $this->rightHeartFailureDiagnosis = $rightHeartFailureDiagnosis;

        return $this;
    }

    /**
     * Get rightHeartFailureDiagnosis
     *
     * @return boolean
     */
    public function getRightHeartFailureDiagnosis()
    {
        return $this->rightHeartFailureDiagnosis;
    }

    /**
     * Set oxygenSaturationLevel
     *
     * @param integer $oxygenSaturationLevel
     *
     * @return SubmissionCOPD
     */
    public function setOxygenSaturationLevel($oxygenSaturationLevel)
    {
        $this->oxygenSaturationLevel = $oxygenSaturationLevel;

        return $this;
    }

    /**
     * Get oxygenSaturationLevel
     *
     * @return integer
     */
    public function getOxygenSaturationLevel()
    {
        return $this->oxygenSaturationLevel;
    }

    /**
     * Set oxygenSaturationLevelDate
     *
     * @param boolean $oxygenSaturationLevelDate
     *
     * @return SubmissionCOPD
     */
    public function setOxygenSaturationLevelDate($oxygenSaturationLevelDate)
    {
        $this->oxygenSaturationLevelDate = $oxygenSaturationLevelDate;

        return $this;
    }

    /**
     * Get oxygenSaturationLevelDate
     *
     * @return boolean
     */
    public function getOxygenSaturationLevelDate()
    {
        return $this->oxygenSaturationLevelDate;
    }

    /**
     * Set paO2Level
     *
     * @param integer $paO2Level
     *
     * @return SubmissionCOPD
     */
    public function setPaO2Level($paO2Level)
    {
        $this->paO2Level = $paO2Level;

        return $this;
    }

    /**
     * Get paO2Level
     *
     * @return integer
     */
    public function getPaO2Level()
    {
        return $this->paO2Level;
    }

    /**
     * Set paO2LevelDate
     *
     * @param \DateTime $paO2LevelDate
     *
     * @return SubmissionCOPD
     */
    public function setPaO2LevelDate($paO2LevelDate)
    {
        $this->paO2LevelDate = $paO2LevelDate;

        return $this;
    }

    /**
     * Get paO2LevelDate
     *
     * @return \DateTime
     */
    public function getPaO2LevelDate()
    {
        return $this->paO2LevelDate;
    }

    /**
     * Set longTermOxygenTherapyDate
     *
     * @param \DateTime $longTermOxygenTherapyDate
     *
     * @return SubmissionCOPD
     */
    public function setLongTermOxygenTherapyDate($longTermOxygenTherapyDate)
    {
        $this->longTermOxygenTherapyDate = $longTermOxygenTherapyDate;

        return $this;
    }

    /**
     * Get longTermOxygenTherapyDate
     *
     * @return \DateTime
     */
    public function getLongTermOxygenTherapyDate()
    {
        return $this->longTermOxygenTherapyDate;
    }

    /**
     * Set influenzaImmunization
     *
     * @param integer $influenzaImmunization
     *
     * @return SubmissionCOPD
     */
    public function setInfluenzaImmunization($influenzaImmunization)
    {
        $this->influenzaImmunization = $influenzaImmunization;

        return $this;
    }

    /**
     * Get influenzaImmunization
     *
     * @return integer
     */
    public function getInfluenzaImmunization()
    {
        return $this->influenzaImmunization;
    }

    /**
     * Set influenzaImmunizationDate
     *
     * @param \DateTime $influenzaImmunizationDate
     *
     * @return SubmissionCOPD
     */
    public function setInfluenzaImmunizationDate($influenzaImmunizationDate)
    {
        $this->influenzaImmunizationDate = $influenzaImmunizationDate;

        return $this;
    }

    /**
     * Get influenzaImmunizationDate
     *
     * @return \DateTime
     */
    public function getInfluenzaImmunizationDate()
    {
        return $this->influenzaImmunizationDate;
    }

    /**
     * Set pneumococcalVaccinePCV13
     *
     * @param integer $pneumococcalVaccinePCV13
     *
     * @return SubmissionCOPD
     */
    public function setPneumococcalVaccinePCV13($pneumococcalVaccinePCV13)
    {
        $this->pneumococcalVaccinePCV13 = $pneumococcalVaccinePCV13;

        return $this;
    }

    /**
     * Get pneumococcalVaccinePCV13
     *
     * @return integer
     */
    public function getPneumococcalVaccinePCV13()
    {
        return $this->pneumococcalVaccinePCV13;
    }

    /**
     * Set pneumococcalVaccinePCV13Date
     *
     * @param \DateTime $pneumococcalVaccinePCV13Date
     *
     * @return SubmissionCOPD
     */
    public function setPneumococcalVaccinePCV13Date($pneumococcalVaccinePCV13Date)
    {
        $this->pneumococcalVaccinePCV13Date = $pneumococcalVaccinePCV13Date;

        return $this;
    }

    /**
     * Get pneumococcalVaccinePCV13Date
     *
     * @return \DateTime
     */
    public function getPneumococcalVaccinePCV13Date()
    {
        return $this->pneumococcalVaccinePCV13Date;
    }

    /**
     * Set pneumococcalVaccinePCV23
     *
     * @param integer $pneumococcalVaccinePCV23
     *
     * @return SubmissionCOPD
     */
    public function setPneumococcalVaccinePCV23($pneumococcalVaccinePCV23)
    {
        $this->pneumococcalVaccinePCV23 = $pneumococcalVaccinePCV23;

        return $this;
    }

    /**
     * Get pneumococcalVaccinePCV23
     *
     * @return integer
     */
    public function getPneumococcalVaccinePCV23()
    {
        return $this->pneumococcalVaccinePCV23;
    }

    /**
     * Set pneumococcalVaccinePCV23Date
     *
     * @param \DateTime $pneumococcalVaccinePCV23Date
     *
     * @return SubmissionCOPD
     */
    public function setPneumococcalVaccinePCV23Date($pneumococcalVaccinePCV23Date)
    {
        $this->pneumococcalVaccinePCV23Date = $pneumococcalVaccinePCV23Date;

        return $this;
    }

    /**
     * Get pneumococcalVaccinePCV23Date
     *
     * @return \DateTime
     */
    public function getPneumococcalVaccinePCV23Date()
    {
        return $this->pneumococcalVaccinePCV23Date;
    }

    /**
     * Set cOPDExacerbationEpisode
     *
     * @param integer $cOPDExacerbationEpisode
     *
     * @return SubmissionCOPD
     */
    public function setCOPDExacerbationEpisode($cOPDExacerbationEpisode)
    {
        $this->COPDExacerbationEpisode = $cOPDExacerbationEpisode;

        return $this;
    }

    /**
     * Get cOPDExacerbationEpisode
     *
     * @return integer
     */
    public function getCOPDExacerbationEpisode()
    {
        return $this->COPDExacerbationEpisode;
    }

    /**
     * Set cOPDExacerbationEpisodeDate
     *
     * @param \DateTime $cOPDExacerbationEpisodeDate
     *
     * @return SubmissionCOPD
     */
    public function setCOPDExacerbationEpisodeDate($cOPDExacerbationEpisodeDate)
    {
        $this->COPDExacerbationEpisodeDate = $cOPDExacerbationEpisodeDate;

        return $this;
    }

    /**
     * Get cOPDExacerbationEpisodeDate
     *
     * @return \DateTime
     */
    public function getCOPDExacerbationEpisodeDate()
    {
        return $this->COPDExacerbationEpisodeDate;
    }

    /**
     * Set cOPDExacerbationEpisodeStateDate
     *
     * @param \DateTime $cOPDExacerbationEpisodeStateDate
     *
     * @return SubmissionCOPD
     */
    public function setCOPDExacerbationEpisodeStateDate($cOPDExacerbationEpisodeStateDate)
    {
        $this->COPDExacerbationEpisodeStateDate = $cOPDExacerbationEpisodeStateDate;

        return $this;
    }

    /**
     * Get cOPDExacerbationEpisodeStateDate
     *
     * @return \DateTime
     */
    public function getCOPDExacerbationEpisodeStateDate()
    {
        return $this->COPDExacerbationEpisodeStateDate;
    }

    /**
     * Set faceToFaceVisit
     *
     * @param \DateTime $faceToFaceVisit
     *
     * @return SubmissionCOPD
     */
    public function setFaceToFaceVisit($faceToFaceVisit)
    {
        $this->faceToFaceVisit = $faceToFaceVisit;

        return $this;
    }

    /**
     * Get faceToFaceVisit
     *
     * @return \DateTime
     */
    public function getFaceToFaceVisit()
    {
        return $this->faceToFaceVisit;
    }

    /**
     * Set lowDoseCTDate
     *
     * @param \DateTime $lowDoseCTDate
     *
     * @return SubmissionCOPD
     */
    public function setLowDoseCTDate($lowDoseCTDate)
    {
        $this->lowDoseCTDate = $lowDoseCTDate;

        return $this;
    }

    /**
     * Get lowDoseCTDate
     *
     * @return \DateTime
     */
    public function getLowDoseCTDate()
    {
        return $this->lowDoseCTDate;
    }

    /**
     * Set lowDoseCTReferral
     *
     * @param boolean $lowDoseCTReferral
     *
     * @return SubmissionCOPD
     */
    public function setLowDoseCTReferral($lowDoseCTReferral)
    {
        $this->lowDoseCTReferral = $lowDoseCTReferral;

        return $this;
    }

    /**
     * Get lowDoseCTReferral
     *
     * @return boolean
     */
    public function getLowDoseCTReferral()
    {
        return $this->lowDoseCTReferral;
    }

    /**
     * Set lowDoseCTDocumented
     *
     * @param boolean $lowDoseCTDocumented
     *
     * @return SubmissionCOPD
     */
    public function setLowDoseCTDocumented($lowDoseCTDocumented)
    {
        $this->lowDoseCTDocumented = $lowDoseCTDocumented;

        return $this;
    }

    /**
     * Get lowDoseCTDocumented
     *
     * @return boolean
     */
    public function getLowDoseCTDocumented()
    {
        return $this->lowDoseCTDocumented;
    }

    /**
     * Set advancedDirective
     *
     * @param integer $advancedDirective
     *
     * @return SubmissionCOPD
     */
    public function setAdvancedDirective($advancedDirective)
    {
        $this->advancedDirective = $advancedDirective;

        return $this;
    }

    /**
     * Get advancedDirective
     *
     * @return integer
     */
    public function getAdvancedDirective()
    {
        return $this->advancedDirective;
    }

    /**
     * Set submissionId
     *
     * @param integer $submissionId
     *
     * @return SubmissionCOPD
     */
    public function setSubmissionId($submissionId)
    {
        $this->submission_id = $submissionId;

        return $this;
    }

    /**
     * Get submissionId
     *
     * @return integer
     */
    public function getSubmissionId()
    {
        return $this->submission_id;
    }

    /**
     * Set tempId
     *
     * @param integer $tempId
     *
     * @return SubmissionCOPD
     */
    public function setTempId($tempId)
    {
        $this->temp_id = $tempId;

        return $this;
    }

    /**
     * Get tempId
     *
     * @return integer
     */
    public function getTempId()
    {
        return $this->temp_id;
    }

    /**
     * Set submision
     *
     * @param \AppBundle\Entity\Submission $submision
     *
     * @return SubmissionCOPD
     */
    public function setSubmision(\AppBundle\Entity\Submission $submision = null)
    {
        $this->submision = $submision;

        return $this;
    }

    /**
     * Get submision
     *
     * @return \AppBundle\Entity\Submission
     */
    public function getSubmision()
    {
        return $this->submision;
    }
}
