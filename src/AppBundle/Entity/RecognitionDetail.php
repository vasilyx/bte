<?php

namespace AppBundle\Entity;

/**
 * RecognitionDetail
 */
class RecognitionDetail
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $recognition_id;

    /**
     * @var integer
     */
    private $measure_id;

    /**
     * @var float
     */
    private $score;

    /**
     * @var \AppBundle\Entity\Recognition
     */
    private $recognition;

    /**
     * @var \AppBundle\Entity\Measure
     */
    private $measure;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set recognitionId
     *
     * @param integer $recognitionId
     *
     * @return RecognitionDetail
     */
    public function setRecognitionId($recognitionId)
    {
        $this->recognition_id = $recognitionId;

        return $this;
    }

    /**
     * Get recognitionId
     *
     * @return integer
     */
    public function getRecognitionId()
    {
        return $this->recognition_id;
    }

    /**
     * Set measureId
     *
     * @param integer $measureId
     *
     * @return RecognitionDetail
     */
    public function setMeasureId($measureId)
    {
        $this->measure_id = $measureId;

        return $this;
    }

    /**
     * Get measureId
     *
     * @return integer
     */
    public function getMeasureId()
    {
        return $this->measure_id;
    }

    /**
     * Set score
     *
     * @param float $score
     *
     * @return RecognitionDetail
     */
    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * Get score
     *
     * @return float
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Set recognition
     *
     * @param \AppBundle\Entity\Recognition $recognition
     *
     * @return RecognitionDetail
     */
    public function setRecognition(\AppBundle\Entity\Recognition $recognition = null)
    {
        $this->recognition = $recognition;

        return $this;
    }

    /**
     * Get recognition
     *
     * @return \AppBundle\Entity\Recognition
     */
    public function getRecognition()
    {
        return $this->recognition;
    }

    /**
     * Set measure
     *
     * @param \AppBundle\Entity\Measure $measure
     *
     * @return RecognitionDetail
     */
    public function setMeasure(\AppBundle\Entity\Measure $measure = null)
    {
        $this->measure = $measure;

        return $this;
    }

    /**
     * Get measure
     *
     * @return \AppBundle\Entity\Measure
     */
    public function getMeasure()
    {
        return $this->measure;
    }
    /**
     * @var integer
     */
    private $numerator;

    /**
     * @var integer
     */
    private $denominator;


    /**
     * Set numerator
     *
     * @param integer $numerator
     *
     * @return RecognitionDetail
     */
    public function setNumerator($numerator)
    {
        $this->numerator = $numerator;

        return $this;
    }

    /**
     * Get numerator
     *
     * @return integer
     */
    public function getNumerator()
    {
        return $this->numerator;
    }

    /**
     * Set denominator
     *
     * @param integer $denominator
     *
     * @return RecognitionDetail
     */
    public function setDenominator($denominator)
    {
        $this->denominator = $denominator;

        return $this;
    }

    /**
     * Get denominator
     *
     * @return integer
     */
    public function getDenominator()
    {
        return $this->denominator;
    }
}
