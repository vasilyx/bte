<?php

namespace AppBundle\Entity;

/**
 * SubmissionIBDCare
 */
class SubmissionIBDCare
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $responsibleProviderID;

    /**
     * @var string
     */
    private $npi;

    /**
     * @var string
     */
    private $groupID;

    /**
     * @var string
     */
    private $individualGroup;

    /**
     * @var string
     */
    private $chartID;

    /**
     * @var \DateTime
     */
    private $lastVisitDate;

    /**
     * @var \DateTime
     */
    private $patientDOB;

    /**
     * @var integer
     */
    private $patientGender;

    /**
     * @var integer
     */
    private $patientRace;

    /**
     * @var boolean
     */
    private $medicarePartB;

    /**
     * @var boolean
     */
    private $IBDDiagnosis;

    /**
     * @var boolean
     */
    private $IBDUnderCareProvider;

    /**
     * @var boolean
     */
    private $IBDDiagnosisOneYear;

    /**
     * @var boolean
     */
    private $IBDDiagnosisEightYear;

    /**
     * @var boolean
     */
    private $IBDType;

    /**
     * @var \DateTime
     */
    private $IBDTypeDate;

    /**
     * @var integer
     */
    private $IBDLocation;

    /**
     * @var integer
     */
    private $IBDExtent;

    /**
     * @var \DateTime
     */
    private $IBDExtentDate;

    /**
     * @var integer
     */
    private $IBDActivity;

    /**
     * @var \DateTime
     */
    private $IBDActivityDate;

    /**
     * @var integer
     */
    private $externalManifestations;

    /**
     * @var \DateTime
     */
    private $externalMainfestationsDateAssessed;

    /**
     * @var boolean
     */
    private $corticosteroid10Mg60Days;

    /**
     * @var boolean
     */
    private $corticosteroidSparingTherapy;

    /**
     * @var boolean
     */
    private $boneLossRiskAssessed;

    /**
     * @var \DateTime
     */
    private $boneLossRiskDateAssessed;

    /**
     * @var boolean
     */
    private $osteoporosisTherapy;

    /**
     * @var boolean
     */
    private $firstCourseAntiTNFTherapy;

    /**
     * @var \DateTime
     */
    private $antiTNFInitiationDate;

    /**
     * @var boolean
     */
    private $tuberculosisScreening;

    /**
     * @var \DateTime
     */
    private $tuberculosisScreeningDate;

    /**
     * @var boolean
     */
    private $positiveTBReatedTB;

    /**
     * @var boolean
     */
    private $hepatitisBStatus;

    /**
     * @var \DateTime
     */
    private $hepatitisBStatusDate;

    /**
     * @var boolean
     */
    private $influenzaImmunization;

    /**
     * @var \DateTime
     */
    private $influenzaImmunizationDate;

    /**
     * @var boolean
     */
    private $pneumococcalVaccine;

    /**
     * @var \DateTime
     */
    private $pneumococccalVaccineDate;

    /**
     * @var boolean
     */
    private $tobaccoStatus;

    /**
     * @var \DateTime
     */
    private $tobaccoStatusAssessmentDate;

    /**
     * @var \DateTime
     */
    private $tobaccoCessationAdviceOrTreatmentDate;

    /**
     * @var boolean
     */
    private $surveillanceColonoscopy;

    /**
     * @var \DateTime
     */
    private $surveillanceColonoscopyDate;

    /**
     * @var boolean
     */
    private $ulcerativeColitisDiagnosis;

    /**
     * @var boolean
     */
    private $crohnColitisDiagnosis;

    /**
     * @var boolean
     */
    private $PHQ2screening;

    /**
     * @var \DateTime
     */
    private $PHQ2screeningDate;

    /**
     * @var boolean
     */
    private $PHQ9screening;

    /**
     * @var \DateTime
     */
    private $PHQ9screeningDate;

    /**
     * @var boolean
     */
    private $IBDBiologics;

    /**
     * @var \DateTime
     */
    private $IBDBiologicsDateAssessed;

    /**
     * @var integer
     */
    private $submission_id;

    /**
     * @var integer
     */
    private $temp_id;

    /**
     * @var \AppBundle\Entity\Submission
     */
    private $submision;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set responsibleProviderID
     *
     * @param integer $responsibleProviderID
     *
     * @return SubmissionIBDCare
     */
    public function setResponsibleProviderID($responsibleProviderID)
    {
        $this->responsibleProviderID = $responsibleProviderID;

        return $this;
    }

    /**
     * Get responsibleProviderID
     *
     * @return integer
     */
    public function getResponsibleProviderID()
    {
        return $this->responsibleProviderID;
    }

    /**
     * Set npi
     *
     * @param string $npi
     *
     * @return SubmissionIBDCare
     */
    public function setNpi($npi)
    {
        $this->npi = $npi;

        return $this;
    }

    /**
     * Get npi
     *
     * @return string
     */
    public function getNpi()
    {
        return $this->npi;
    }

    /**
     * Set groupID
     *
     * @param string $groupID
     *
     * @return SubmissionIBDCare
     */
    public function setGroupID($groupID)
    {
        $this->groupID = $groupID;

        return $this;
    }

    /**
     * Get groupID
     *
     * @return string
     */
    public function getGroupID()
    {
        return $this->groupID;
    }

    /**
     * Set individualGroup
     *
     * @param string $individualGroup
     *
     * @return SubmissionIBDCare
     */
    public function setIndividualGroup($individualGroup)
    {
        $this->individualGroup = $individualGroup;

        return $this;
    }

    /**
     * Get individualGroup
     *
     * @return string
     */
    public function getIndividualGroup()
    {
        return $this->individualGroup;
    }

    /**
     * Set chartID
     *
     * @param string $chartID
     *
     * @return SubmissionIBDCare
     */
    public function setChartID($chartID)
    {
        $this->chartID = $chartID;

        return $this;
    }

    /**
     * Get chartID
     *
     * @return string
     */
    public function getChartID()
    {
        return $this->chartID;
    }

    /**
     * Set lastVisitDate
     *
     * @param \DateTime $lastVisitDate
     *
     * @return SubmissionIBDCare
     */
    public function setLastVisitDate($lastVisitDate)
    {
        $this->lastVisitDate = $lastVisitDate;

        return $this;
    }

    /**
     * Get lastVisitDate
     *
     * @return \DateTime
     */
    public function getLastVisitDate()
    {
        return $this->lastVisitDate;
    }

    /**
     * Set patientDOB
     *
     * @param \DateTime $patientDOB
     *
     * @return SubmissionIBDCare
     */
    public function setPatientDOB($patientDOB)
    {
        $this->patientDOB = $patientDOB;

        return $this;
    }

    /**
     * Get patientDOB
     *
     * @return \DateTime
     */
    public function getPatientDOB()
    {
        return $this->patientDOB;
    }

    /**
     * Set patientGender
     *
     * @param integer $patientGender
     *
     * @return SubmissionIBDCare
     */
    public function setPatientGender($patientGender)
    {
        $this->patientGender = $patientGender;

        return $this;
    }

    /**
     * Get patientGender
     *
     * @return integer
     */
    public function getPatientGender()
    {
        return $this->patientGender;
    }

    /**
     * Set patientRace
     *
     * @param integer $patientRace
     *
     * @return SubmissionIBDCare
     */
    public function setPatientRace($patientRace)
    {
        $this->patientRace = $patientRace;

        return $this;
    }

    /**
     * Get patientRace
     *
     * @return integer
     */
    public function getPatientRace()
    {
        return $this->patientRace;
    }

    /**
     * Set medicarePartB
     *
     * @param boolean $medicarePartB
     *
     * @return SubmissionIBDCare
     */
    public function setMedicarePartB($medicarePartB)
    {
        $this->medicarePartB = $medicarePartB;

        return $this;
    }

    /**
     * Get medicarePartB
     *
     * @return boolean
     */
    public function getMedicarePartB()
    {
        return $this->medicarePartB;
    }

    /**
     * Set iBDDiagnosis
     *
     * @param boolean $iBDDiagnosis
     *
     * @return SubmissionIBDCare
     */
    public function setIBDDiagnosis($iBDDiagnosis)
    {
        $this->IBDDiagnosis = $iBDDiagnosis;

        return $this;
    }

    /**
     * Get iBDDiagnosis
     *
     * @return boolean
     */
    public function getIBDDiagnosis()
    {
        return $this->IBDDiagnosis;
    }

    /**
     * Set iBDUnderCareProvider
     *
     * @param boolean $iBDUnderCareProvider
     *
     * @return SubmissionIBDCare
     */
    public function setIBDUnderCareProvider($iBDUnderCareProvider)
    {
        $this->IBDUnderCareProvider = $iBDUnderCareProvider;

        return $this;
    }

    /**
     * Get iBDUnderCareProvider
     *
     * @return boolean
     */
    public function getIBDUnderCareProvider()
    {
        return $this->IBDUnderCareProvider;
    }

    /**
     * Set iBDDiagnosisOneYear
     *
     * @param boolean $iBDDiagnosisOneYear
     *
     * @return SubmissionIBDCare
     */
    public function setIBDDiagnosisOneYear($iBDDiagnosisOneYear)
    {
        $this->IBDDiagnosisOneYear = $iBDDiagnosisOneYear;

        return $this;
    }

    /**
     * Get iBDDiagnosisOneYear
     *
     * @return boolean
     */
    public function getIBDDiagnosisOneYear()
    {
        return $this->IBDDiagnosisOneYear;
    }

    /**
     * Set iBDDiagnosisEightYear
     *
     * @param boolean $iBDDiagnosisEightYear
     *
     * @return SubmissionIBDCare
     */
    public function setIBDDiagnosisEightYear($iBDDiagnosisEightYear)
    {
        $this->IBDDiagnosisEightYear = $iBDDiagnosisEightYear;

        return $this;
    }

    /**
     * Get iBDDiagnosisEightYear
     *
     * @return boolean
     */
    public function getIBDDiagnosisEightYear()
    {
        return $this->IBDDiagnosisEightYear;
    }

    /**
     * Set iBDType
     *
     * @param boolean $iBDType
     *
     * @return SubmissionIBDCare
     */
    public function setIBDType($iBDType)
    {
        $this->IBDType = $iBDType;

        return $this;
    }

    /**
     * Get iBDType
     *
     * @return boolean
     */
    public function getIBDType()
    {
        return $this->IBDType;
    }

    /**
     * Set iBDTypeDate
     *
     * @param \DateTime $iBDTypeDate
     *
     * @return SubmissionIBDCare
     */
    public function setIBDTypeDate($iBDTypeDate)
    {
        $this->IBDTypeDate = $iBDTypeDate;

        return $this;
    }

    /**
     * Get iBDTypeDate
     *
     * @return \DateTime
     */
    public function getIBDTypeDate()
    {
        return $this->IBDTypeDate;
    }

    /**
     * Set iBDLocation
     *
     * @param integer $iBDLocation
     *
     * @return SubmissionIBDCare
     */
    public function setIBDLocation($iBDLocation)
    {
        $this->IBDLocation = $iBDLocation;

        return $this;
    }

    /**
     * Get iBDLocation
     *
     * @return integer
     */
    public function getIBDLocation()
    {
        return $this->IBDLocation;
    }

    /**
     * Set iBDExtent
     *
     * @param integer $iBDExtent
     *
     * @return SubmissionIBDCare
     */
    public function setIBDExtent($iBDExtent)
    {
        $this->IBDExtent = $iBDExtent;

        return $this;
    }

    /**
     * Get iBDExtent
     *
     * @return integer
     */
    public function getIBDExtent()
    {
        return $this->IBDExtent;
    }

    /**
     * Set iBDExtentDate
     *
     * @param \DateTime $iBDExtentDate
     *
     * @return SubmissionIBDCare
     */
    public function setIBDExtentDate($iBDExtentDate)
    {
        $this->IBDExtentDate = $iBDExtentDate;

        return $this;
    }

    /**
     * Get iBDExtentDate
     *
     * @return \DateTime
     */
    public function getIBDExtentDate()
    {
        return $this->IBDExtentDate;
    }

    /**
     * Set iBDActivity
     *
     * @param integer $iBDActivity
     *
     * @return SubmissionIBDCare
     */
    public function setIBDActivity($iBDActivity)
    {
        $this->IBDActivity = $iBDActivity;

        return $this;
    }

    /**
     * Get iBDActivity
     *
     * @return integer
     */
    public function getIBDActivity()
    {
        return $this->IBDActivity;
    }

    /**
     * Set iBDActivityDate
     *
     * @param \DateTime $iBDActivityDate
     *
     * @return SubmissionIBDCare
     */
    public function setIBDActivityDate($iBDActivityDate)
    {
        $this->IBDActivityDate = $iBDActivityDate;

        return $this;
    }

    /**
     * Get iBDActivityDate
     *
     * @return \DateTime
     */
    public function getIBDActivityDate()
    {
        return $this->IBDActivityDate;
    }

    /**
     * Set externalManifestations
     *
     * @param integer $externalManifestations
     *
     * @return SubmissionIBDCare
     */
    public function setExternalManifestations($externalManifestations)
    {
        $this->externalManifestations = $externalManifestations;

        return $this;
    }

    /**
     * Get externalManifestations
     *
     * @return integer
     */
    public function getExternalManifestations()
    {
        return $this->externalManifestations;
    }

    /**
     * Set externalMainfestationsDateAssessed
     *
     * @param \DateTime $externalMainfestationsDateAssessed
     *
     * @return SubmissionIBDCare
     */
    public function setExternalMainfestationsDateAssessed($externalMainfestationsDateAssessed)
    {
        $this->externalMainfestationsDateAssessed = $externalMainfestationsDateAssessed;

        return $this;
    }

    /**
     * Get externalMainfestationsDateAssessed
     *
     * @return \DateTime
     */
    public function getExternalMainfestationsDateAssessed()
    {
        return $this->externalMainfestationsDateAssessed;
    }

    /**
     * Set corticosteroid10Mg60Days
     *
     * @param boolean $corticosteroid10Mg60Days
     *
     * @return SubmissionIBDCare
     */
    public function setCorticosteroid10Mg60Days($corticosteroid10Mg60Days)
    {
        $this->corticosteroid10Mg60Days = $corticosteroid10Mg60Days;

        return $this;
    }

    /**
     * Get corticosteroid10Mg60Days
     *
     * @return boolean
     */
    public function getCorticosteroid10Mg60Days()
    {
        return $this->corticosteroid10Mg60Days;
    }

    /**
     * Set corticosteroidSparingTherapy
     *
     * @param boolean $corticosteroidSparingTherapy
     *
     * @return SubmissionIBDCare
     */
    public function setCorticosteroidSparingTherapy($corticosteroidSparingTherapy)
    {
        $this->corticosteroidSparingTherapy = $corticosteroidSparingTherapy;

        return $this;
    }

    /**
     * Get corticosteroidSparingTherapy
     *
     * @return boolean
     */
    public function getCorticosteroidSparingTherapy()
    {
        return $this->corticosteroidSparingTherapy;
    }

    /**
     * Set boneLossRiskAssessed
     *
     * @param boolean $boneLossRiskAssessed
     *
     * @return SubmissionIBDCare
     */
    public function setBoneLossRiskAssessed($boneLossRiskAssessed)
    {
        $this->boneLossRiskAssessed = $boneLossRiskAssessed;

        return $this;
    }

    /**
     * Get boneLossRiskAssessed
     *
     * @return boolean
     */
    public function getBoneLossRiskAssessed()
    {
        return $this->boneLossRiskAssessed;
    }

    /**
     * Set boneLossRiskDateAssessed
     *
     * @param \DateTime $boneLossRiskDateAssessed
     *
     * @return SubmissionIBDCare
     */
    public function setBoneLossRiskDateAssessed($boneLossRiskDateAssessed)
    {
        $this->boneLossRiskDateAssessed = $boneLossRiskDateAssessed;

        return $this;
    }

    /**
     * Get boneLossRiskDateAssessed
     *
     * @return \DateTime
     */
    public function getBoneLossRiskDateAssessed()
    {
        return $this->boneLossRiskDateAssessed;
    }

    /**
     * Set osteoporosisTherapy
     *
     * @param boolean $osteoporosisTherapy
     *
     * @return SubmissionIBDCare
     */
    public function setOsteoporosisTherapy($osteoporosisTherapy)
    {
        $this->osteoporosisTherapy = $osteoporosisTherapy;

        return $this;
    }

    /**
     * Get osteoporosisTherapy
     *
     * @return boolean
     */
    public function getOsteoporosisTherapy()
    {
        return $this->osteoporosisTherapy;
    }

    /**
     * Set firstCourseAntiTNFTherapy
     *
     * @param boolean $firstCourseAntiTNFTherapy
     *
     * @return SubmissionIBDCare
     */
    public function setFirstCourseAntiTNFTherapy($firstCourseAntiTNFTherapy)
    {
        $this->firstCourseAntiTNFTherapy = $firstCourseAntiTNFTherapy;

        return $this;
    }

    /**
     * Get firstCourseAntiTNFTherapy
     *
     * @return boolean
     */
    public function getFirstCourseAntiTNFTherapy()
    {
        return $this->firstCourseAntiTNFTherapy;
    }

    /**
     * Set antiTNFInitiationDate
     *
     * @param \DateTime $antiTNFInitiationDate
     *
     * @return SubmissionIBDCare
     */
    public function setAntiTNFInitiationDate($antiTNFInitiationDate)
    {
        $this->antiTNFInitiationDate = $antiTNFInitiationDate;

        return $this;
    }

    /**
     * Get antiTNFInitiationDate
     *
     * @return \DateTime
     */
    public function getAntiTNFInitiationDate()
    {
        return $this->antiTNFInitiationDate;
    }

    /**
     * Set tuberculosisScreening
     *
     * @param boolean $tuberculosisScreening
     *
     * @return SubmissionIBDCare
     */
    public function setTuberculosisScreening($tuberculosisScreening)
    {
        $this->tuberculosisScreening = $tuberculosisScreening;

        return $this;
    }

    /**
     * Get tuberculosisScreening
     *
     * @return boolean
     */
    public function getTuberculosisScreening()
    {
        return $this->tuberculosisScreening;
    }

    /**
     * Set tuberculosisScreeningDate
     *
     * @param \DateTime $tuberculosisScreeningDate
     *
     * @return SubmissionIBDCare
     */
    public function setTuberculosisScreeningDate($tuberculosisScreeningDate)
    {
        $this->tuberculosisScreeningDate = $tuberculosisScreeningDate;

        return $this;
    }

    /**
     * Get tuberculosisScreeningDate
     *
     * @return \DateTime
     */
    public function getTuberculosisScreeningDate()
    {
        return $this->tuberculosisScreeningDate;
    }

    /**
     * Set positiveTBReatedTB
     *
     * @param boolean $positiveTBReatedTB
     *
     * @return SubmissionIBDCare
     */
    public function setPositiveTBReatedTB($positiveTBReatedTB)
    {
        $this->positiveTBReatedTB = $positiveTBReatedTB;

        return $this;
    }

    /**
     * Get positiveTBReatedTB
     *
     * @return boolean
     */
    public function getPositiveTBReatedTB()
    {
        return $this->positiveTBReatedTB;
    }

    /**
     * Set hepatitisBStatus
     *
     * @param boolean $hepatitisBStatus
     *
     * @return SubmissionIBDCare
     */
    public function setHepatitisBStatus($hepatitisBStatus)
    {
        $this->hepatitisBStatus = $hepatitisBStatus;

        return $this;
    }

    /**
     * Get hepatitisBStatus
     *
     * @return boolean
     */
    public function getHepatitisBStatus()
    {
        return $this->hepatitisBStatus;
    }

    /**
     * Set hepatitisBStatusDate
     *
     * @param \DateTime $hepatitisBStatusDate
     *
     * @return SubmissionIBDCare
     */
    public function setHepatitisBStatusDate($hepatitisBStatusDate)
    {
        $this->hepatitisBStatusDate = $hepatitisBStatusDate;

        return $this;
    }

    /**
     * Get hepatitisBStatusDate
     *
     * @return \DateTime
     */
    public function getHepatitisBStatusDate()
    {
        return $this->hepatitisBStatusDate;
    }

    /**
     * Set influenzaImmunization
     *
     * @param boolean $influenzaImmunization
     *
     * @return SubmissionIBDCare
     */
    public function setInfluenzaImmunization($influenzaImmunization)
    {
        $this->influenzaImmunization = $influenzaImmunization;

        return $this;
    }

    /**
     * Get influenzaImmunization
     *
     * @return boolean
     */
    public function getInfluenzaImmunization()
    {
        return $this->influenzaImmunization;
    }

    /**
     * Set influenzaImmunizationDate
     *
     * @param \DateTime $influenzaImmunizationDate
     *
     * @return SubmissionIBDCare
     */
    public function setInfluenzaImmunizationDate($influenzaImmunizationDate)
    {
        $this->influenzaImmunizationDate = $influenzaImmunizationDate;

        return $this;
    }

    /**
     * Get influenzaImmunizationDate
     *
     * @return \DateTime
     */
    public function getInfluenzaImmunizationDate()
    {
        return $this->influenzaImmunizationDate;
    }

    /**
     * Set pneumococcalVaccine
     *
     * @param boolean $pneumococcalVaccine
     *
     * @return SubmissionIBDCare
     */
    public function setPneumococcalVaccine($pneumococcalVaccine)
    {
        $this->pneumococcalVaccine = $pneumococcalVaccine;

        return $this;
    }

    /**
     * Get pneumococcalVaccine
     *
     * @return boolean
     */
    public function getPneumococcalVaccine()
    {
        return $this->pneumococcalVaccine;
    }

    /**
     * Set pneumococccalVaccineDate
     *
     * @param \DateTime $pneumococccalVaccineDate
     *
     * @return SubmissionIBDCare
     */
    public function setPneumococccalVaccineDate($pneumococccalVaccineDate)
    {
        $this->pneumococccalVaccineDate = $pneumococccalVaccineDate;

        return $this;
    }

    /**
     * Get pneumococccalVaccineDate
     *
     * @return \DateTime
     */
    public function getPneumococccalVaccineDate()
    {
        return $this->pneumococccalVaccineDate;
    }

    /**
     * Set tobaccoStatus
     *
     * @param boolean $tobaccoStatus
     *
     * @return SubmissionIBDCare
     */
    public function setTobaccoStatus($tobaccoStatus)
    {
        $this->tobaccoStatus = $tobaccoStatus;

        return $this;
    }

    /**
     * Get tobaccoStatus
     *
     * @return boolean
     */
    public function getTobaccoStatus()
    {
        return $this->tobaccoStatus;
    }

    /**
     * Set tobaccoStatusAssessmentDate
     *
     * @param \DateTime $tobaccoStatusAssessmentDate
     *
     * @return SubmissionIBDCare
     */
    public function setTobaccoStatusAssessmentDate($tobaccoStatusAssessmentDate)
    {
        $this->tobaccoStatusAssessmentDate = $tobaccoStatusAssessmentDate;

        return $this;
    }

    /**
     * Get tobaccoStatusAssessmentDate
     *
     * @return \DateTime
     */
    public function getTobaccoStatusAssessmentDate()
    {
        return $this->tobaccoStatusAssessmentDate;
    }

    /**
     * Set tobaccoCessationAdviceOrTreatmentDate
     *
     * @param \DateTime $tobaccoCessationAdviceOrTreatmentDate
     *
     * @return SubmissionIBDCare
     */
    public function setTobaccoCessationAdviceOrTreatmentDate($tobaccoCessationAdviceOrTreatmentDate)
    {
        $this->tobaccoCessationAdviceOrTreatmentDate = $tobaccoCessationAdviceOrTreatmentDate;

        return $this;
    }

    /**
     * Get tobaccoCessationAdviceOrTreatmentDate
     *
     * @return \DateTime
     */
    public function getTobaccoCessationAdviceOrTreatmentDate()
    {
        return $this->tobaccoCessationAdviceOrTreatmentDate;
    }

    /**
     * Set surveillanceColonoscopy
     *
     * @param boolean $surveillanceColonoscopy
     *
     * @return SubmissionIBDCare
     */
    public function setSurveillanceColonoscopy($surveillanceColonoscopy)
    {
        $this->surveillanceColonoscopy = $surveillanceColonoscopy;

        return $this;
    }

    /**
     * Get surveillanceColonoscopy
     *
     * @return boolean
     */
    public function getSurveillanceColonoscopy()
    {
        return $this->surveillanceColonoscopy;
    }

    /**
     * Set surveillanceColonoscopyDate
     *
     * @param \DateTime $surveillanceColonoscopyDate
     *
     * @return SubmissionIBDCare
     */
    public function setSurveillanceColonoscopyDate($surveillanceColonoscopyDate)
    {
        $this->surveillanceColonoscopyDate = $surveillanceColonoscopyDate;

        return $this;
    }

    /**
     * Get surveillanceColonoscopyDate
     *
     * @return \DateTime
     */
    public function getSurveillanceColonoscopyDate()
    {
        return $this->surveillanceColonoscopyDate;
    }

    /**
     * Set ulcerativeColitisDiagnosis
     *
     * @param boolean $ulcerativeColitisDiagnosis
     *
     * @return SubmissionIBDCare
     */
    public function setUlcerativeColitisDiagnosis($ulcerativeColitisDiagnosis)
    {
        $this->ulcerativeColitisDiagnosis = $ulcerativeColitisDiagnosis;

        return $this;
    }

    /**
     * Get ulcerativeColitisDiagnosis
     *
     * @return boolean
     */
    public function getUlcerativeColitisDiagnosis()
    {
        return $this->ulcerativeColitisDiagnosis;
    }

    /**
     * Set crohnColitisDiagnosis
     *
     * @param boolean $crohnColitisDiagnosis
     *
     * @return SubmissionIBDCare
     */
    public function setCrohnColitisDiagnosis($crohnColitisDiagnosis)
    {
        $this->crohnColitisDiagnosis = $crohnColitisDiagnosis;

        return $this;
    }

    /**
     * Get crohnColitisDiagnosis
     *
     * @return boolean
     */
    public function getCrohnColitisDiagnosis()
    {
        return $this->crohnColitisDiagnosis;
    }

    /**
     * Set pHQ2screening
     *
     * @param boolean $pHQ2screening
     *
     * @return SubmissionIBDCare
     */
    public function setPHQ2screening($pHQ2screening)
    {
        $this->PHQ2screening = $pHQ2screening;

        return $this;
    }

    /**
     * Get pHQ2screening
     *
     * @return boolean
     */
    public function getPHQ2screening()
    {
        return $this->PHQ2screening;
    }

    /**
     * Set pHQ2screeningDate
     *
     * @param \DateTime $pHQ2screeningDate
     *
     * @return SubmissionIBDCare
     */
    public function setPHQ2screeningDate($pHQ2screeningDate)
    {
        $this->PHQ2screeningDate = $pHQ2screeningDate;

        return $this;
    }

    /**
     * Get pHQ2screeningDate
     *
     * @return \DateTime
     */
    public function getPHQ2screeningDate()
    {
        return $this->PHQ2screeningDate;
    }

    /**
     * Set pHQ9screening
     *
     * @param boolean $pHQ9screening
     *
     * @return SubmissionIBDCare
     */
    public function setPHQ9screening($pHQ9screening)
    {
        $this->PHQ9screening = $pHQ9screening;

        return $this;
    }

    /**
     * Get pHQ9screening
     *
     * @return boolean
     */
    public function getPHQ9screening()
    {
        return $this->PHQ9screening;
    }

    /**
     * Set pHQ9screeningDate
     *
     * @param \DateTime $pHQ9screeningDate
     *
     * @return SubmissionIBDCare
     */
    public function setPHQ9screeningDate($pHQ9screeningDate)
    {
        $this->PHQ9screeningDate = $pHQ9screeningDate;

        return $this;
    }

    /**
     * Get pHQ9screeningDate
     *
     * @return \DateTime
     */
    public function getPHQ9screeningDate()
    {
        return $this->PHQ9screeningDate;
    }

    /**
     * Set iBDBiologics
     *
     * @param boolean $iBDBiologics
     *
     * @return SubmissionIBDCare
     */
    public function setIBDBiologics($iBDBiologics)
    {
        $this->IBDBiologics = $iBDBiologics;

        return $this;
    }

    /**
     * Get iBDBiologics
     *
     * @return boolean
     */
    public function getIBDBiologics()
    {
        return $this->IBDBiologics;
    }

    /**
     * Set iBDBiologicsDateAssessed
     *
     * @param \DateTime $iBDBiologicsDateAssessed
     *
     * @return SubmissionIBDCare
     */
    public function setIBDBiologicsDateAssessed($iBDBiologicsDateAssessed)
    {
        $this->IBDBiologicsDateAssessed = $iBDBiologicsDateAssessed;

        return $this;
    }

    /**
     * Get iBDBiologicsDateAssessed
     *
     * @return \DateTime
     */
    public function getIBDBiologicsDateAssessed()
    {
        return $this->IBDBiologicsDateAssessed;
    }

    /**
     * Set submissionId
     *
     * @param integer $submissionId
     *
     * @return SubmissionIBDCare
     */
    public function setSubmissionId($submissionId)
    {
        $this->submission_id = $submissionId;

        return $this;
    }

    /**
     * Get submissionId
     *
     * @return integer
     */
    public function getSubmissionId()
    {
        return $this->submission_id;
    }

    /**
     * Set tempId
     *
     * @param integer $tempId
     *
     * @return SubmissionIBDCare
     */
    public function setTempId($tempId)
    {
        $this->temp_id = $tempId;

        return $this;
    }

    /**
     * Get tempId
     *
     * @return integer
     */
    public function getTempId()
    {
        return $this->temp_id;
    }

    /**
     * Set submision
     *
     * @param \AppBundle\Entity\Submission $submision
     *
     * @return SubmissionIBDCare
     */
    public function setSubmision(\AppBundle\Entity\Submission $submision = null)
    {
        $this->submision = $submision;

        return $this;
    }

    /**
     * Get submision
     *
     * @return \AppBundle\Entity\Submission
     */
    public function getSubmision()
    {
        return $this->submision;
    }
    /**
     * @var \DateTime
     */
    private $osteoporosisTherapyDateAssessed;


    /**
     * Set osteoporosisTherapyDateAssessed
     *
     * @param \DateTime $osteoporosisTherapyDateAssessed
     *
     * @return SubmissionIBDCare
     */
    public function setOsteoporosisTherapyDateAssessed($osteoporosisTherapyDateAssessed)
    {
        $this->osteoporosisTherapyDateAssessed = $osteoporosisTherapyDateAssessed;

        return $this;
    }

    /**
     * Get osteoporosisTherapyDateAssessed
     *
     * @return \DateTime
     */
    public function getOsteoporosisTherapyDateAssessed()
    {
        return $this->osteoporosisTherapyDateAssessed;
    }
}
