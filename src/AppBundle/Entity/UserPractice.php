<?php

namespace AppBundle\Entity;

/**
 * UserPractice
 */
class UserPractice
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $address;

    /**
     * @var string
     */
    private $city;

    /**
     * @var string
     */
    private $state;

    /**
     * @var integer
     */
    private $zipCode;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var string
     */
    private $npi;

    /**
     * @var string
     */
    private $primaryContactName;

    /**
     * @var string
     */
    private $primaryContactEmail;

    /**
     * @var \AppBundle\Entity\User
     */
    private $user;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return UserPractice
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return UserPractice
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return UserPractice
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set state
     *
     * @param string $state
     *
     * @return UserPractice
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set zipCode
     *
     * @param integer $zipCode
     *
     * @return UserPractice
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    /**
     * Get zipCode
     *
     * @return integer
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return UserPractice
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set npi
     *
     * @param string $npi
     *
     * @return UserPractice
     */
    public function setNpi($npi)
    {
        $this->npi = $npi;

        return $this;
    }

    /**
     * Get npi
     *
     * @return string
     */
    public function getNpi()
    {
        return $this->npi;
    }

    /**
     * Set primaryContactName
     *
     * @param string $primaryContactName
     *
     * @return UserPractice
     */
    public function setPrimaryContactName($primaryContactName)
    {
        $this->primaryContactName = $primaryContactName;

        return $this;
    }

    /**
     * Get primaryContactName
     *
     * @return string
     */
    public function getPrimaryContactName()
    {
        return $this->primaryContactName;
    }

    /**
     * Set primaryContactEmail
     *
     * @param string $primaryContactEmail
     *
     * @return UserPractice
     */
    public function setPrimaryContactEmail($primaryContactEmail)
    {
        $this->primaryContactEmail = $primaryContactEmail;

        return $this;
    }

    /**
     * Get primaryContactEmail
     *
     * @return string
     */
    public function getPrimaryContactEmail()
    {
        return $this->primaryContactEmail;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return UserPractice
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    public function __toString(){
        return $this->getName();
    }
    /**
     * @var boolean
     */
    private $dataAggregator;


    /**
     * Set dataAggregator
     *
     * @param boolean $dataAggregator
     *
     * @return UserPractice
     */
    public function setDataAggregator($dataAggregator)
    {
        $this->dataAggregator = $dataAggregator;

        return $this;
    }

    /**
     * Get dataAggregator
     *
     * @return boolean
     */
    public function getDataAggregator()
    {
        return $this->dataAggregator;
    }
}
