<?php

namespace AppBundle\Entity;

/**
 * Recognition
 */
class Recognition
{
    /**
     * @var integer
     */
    private $id;


    /**
     * @var \AppBundle\Entity\Submission
     */
    private $submission;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set submission
     *
     * @param \AppBundle\Entity\Submission $submision
     *
     * @return Recognition
     */
    public function setSubmision(\AppBundle\Entity\Submission $submission = null)
    {
        $this->submission = $submission;

        return $this;
    }

    /**
     * Get submission
     *
     * @return \AppBundle\Entity\Submission
     */
    public function getSubmision()
    {
        return $this->submission;
    }

    /**
     * Set submission
     *
     * @param \AppBundle\Entity\Submission $submission
     *
     * @return Recognition
     */
    public function setSubmission(\AppBundle\Entity\Submission $submission = null)
    {
        $this->submission = $submission;

        return $this;
    }

    /**
     * Get submission
     *
     * @return \AppBundle\Entity\Submission
     */
    public function getSubmission()
    {
        return $this->submission;
    }

    public function __toString(){
        return (string)$this->getId();
    }
    /**
     * @var integer
     */
    private $provider_id;

    /**
     * @var float
     */
    private $score;

    /**
     * @var \AppBundle\Entity\Provider
     */
    private $provider;


    /**
     * Set providerId
     *
     * @param integer $providerId
     *
     * @return Recognition
     */
    public function setProviderId($providerId)
    {
        $this->provider_id = $providerId;

        return $this;
    }

    /**
     * Get providerId
     *
     * @return integer
     */
    public function getProviderId()
    {
        return $this->provider_id;
    }

    /**
     * Set score
     *
     * @param float $score
     *
     * @return Recognition
     */
    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * Get score
     *
     * @return float
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Set provider
     *
     * @param \AppBundle\Entity\Provider $provider
     *
     * @return Recognition
     */
    public function setProvider(\AppBundle\Entity\Provider $provider = null)
    {
        $this->provider = $provider;

        return $this;
    }

    /**
     * Get provider
     *
     * @return \AppBundle\Entity\Provider
     */
    public function getProvider()
    {
        return $this->provider;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $recognitionDetail;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->recognitionDetail = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add recognitionDetail
     *
     * @param \AppBundle\Entity\RecognitionDetail $recognitionDetail
     *
     * @return Recognition
     */
    public function addRecognitionDetail(\AppBundle\Entity\RecognitionDetail $recognitionDetail)
    {
        $this->recognitionDetail[] = $recognitionDetail;

        return $this;
    }

    /**
     * Remove recognitionDetail
     *
     * @param \AppBundle\Entity\RecognitionDetail $recognitionDetail
     */
    public function removeRecognitionDetail(\AppBundle\Entity\RecognitionDetail $recognitionDetail)
    {
        $this->recognitionDetail->removeElement($recognitionDetail);
    }

    /**
     * Get recognitionDetail
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRecognitionDetail()
    {
        return $this->recognitionDetail;
    }
    /**
     * @var int
     */
    private $numerator;

    /**
     * @var int
     */
    private $denominator;


    /**
     * Set numerator.
     *
     * @param int $numerator
     *
     * @return Recognition
     */
    public function setNumerator($numerator)
    {
        $this->numerator = $numerator;

        return $this;
    }

    /**
     * Get numerator.
     *
     * @return int
     */
    public function getNumerator()
    {
        return $this->numerator;
    }

    /**
     * Set denominator.
     *
     * @param int $denominator
     *
     * @return Recognition
     */
    public function setDenominator($denominator)
    {
        $this->denominator = $denominator;

        return $this;
    }

    /**
     * Get denominator.
     *
     * @return int
     */
    public function getDenominator()
    {
        return $this->denominator;
    }
    /**
     * @var string|null
     */
    private $groupID;


    /**
     * Set groupID.
     *
     * @param string|null $groupID
     *
     * @return Recognition
     */
    public function setGroupID($groupID = null)
    {
        $this->groupID = $groupID;

        return $this;
    }

    /**
     * Get groupID.
     *
     * @return string|null
     */
    public function getGroupID()
    {
        return $this->groupID;
    }
    /**
     * @var int|null
     */
    private $starRatingAssesed;

    /**
     * @var int|null
     */
    private $starRatingAchived;

    /**
     * @var \DateTime|null
     */
    private $expirationDate;


    /**
     * Set starRatingAssesed.
     *
     * @param int|null $starRatingAssesed
     *
     * @return Recognition
     */
    public function setStarRatingAssesed($starRatingAssesed = null)
    {
        $this->starRatingAssesed = $starRatingAssesed;

        return $this;
    }

    /**
     * Get starRatingAssesed.
     *
     * @return int|null
     */
    public function getStarRatingAssesed()
    {
        return $this->starRatingAssesed;
    }

    /**
     * Set starRatingAchived.
     *
     * @param int|null $starRatingAchived
     *
     * @return Recognition
     */
    public function setStarRatingAchived($starRatingAchived = null)
    {
        $this->starRatingAchived = $starRatingAchived;

        return $this;
    }

    /**
     * Get starRatingAchived.
     *
     * @return int|null
     */
    public function getStarRatingAchived()
    {
        return $this->starRatingAchived;
    }

    /**
     * Set expirationDate.
     *
     * @param \DateTime|null $expirationDate
     *
     * @return Recognition
     */
    public function setExpirationDate($expirationDate = null)
    {
        $this->expirationDate = $expirationDate;

        return $this;
    }

    /**
     * Get expirationDate.
     *
     * @return \DateTime|null
     */
    public function getExpirationDate()
    {
        return $this->expirationDate;
    }
    /**
     * @var float|null
     */
    private $groupScore;


    /**
     * Set groupScore.
     *
     * @param float|null $groupScore
     *
     * @return Recognition
     */
    public function setGroupScore($groupScore = null)
    {
        $this->groupScore = $groupScore;

        return $this;
    }

    /**
     * Get groupScore.
     *
     * @return float|null
     */
    public function getGroupScore()
    {
        return $this->groupScore;
    }
}
