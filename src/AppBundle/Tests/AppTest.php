<?php
namespace AppBundle\Tests;
use AppBundle\Tests\Controller\AuthControllerTest;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\User;

class AppTest  extends WebTestCase
{
    protected static $client;
    protected static $server;
    protected static $content;
    protected static $token;

    protected static $fake_email;
    protected static $fake_psw;
    protected static $em;

    protected $testUser;
    protected $api_url = 'api';

    public $url_without_token = array('login', 'resetPassword', 'forgotten', 'registration', 'logout');


    /**
     * @return void
     */
    public static function setUpBeforeClass()
    {
        static::$client = static::createClient( array());
        static::$server = array();
        static::$content = '{"param": "value"}';
        static::$fake_email = time() . '@test.com';
        static::$fake_psw = '123';

        $kernel = static::createKernel();
        $kernel->boot();
        static::$em = $kernel->getContainer()->get('doctrine.orm.entity_manager');

        $user = new User();
        $user->email = static::$fake_email;
        $user->setPassword(123);

        static::$em->persist($user);
        static::$em->flush();

        $aa = new AuthControllerTest();
        $aa->testLogin();
    }

    public function setUp()
    {
        $this->send('login', array('email' => static::$fake_email, 'password' => static::$fake_psw), 'POST');
        $response = json_decode(static::$client->getResponse()->getContent());
        static::$token = $response->token;
    }

    public function tearDown()
    {
        $this->send('logout', array(), 'POST');
    }

    /**
     * @param $url
     * @param array $params
     * @param string $method
     * @param string $custom_token
     * @return mixed
     */
    protected function send($url, $params = array(), $method = 'GET', $custom_token = '')
    {
        $server = static::$server;

        if (!in_array($url, $this->url_without_token)) {

            if ($custom_token) {
                $server = array_merge($server, array('HTTP_token' => $custom_token));

            } else {
                $server = $this->addToken($server);
            }
        }

        return static::$client->request($method, '/api/' . $url, $params, array(), $server, static::$content);
    }



    protected function addToken($array = array())
    {
        return array_merge($array, array('HTTP_token' => static::$token));
    }

    public function testLoginScreen()
    {
        $this->send('login');
        $this->assertEquals(Response::HTTP_METHOD_NOT_ALLOWED, static::$client->getResponse()->getStatusCode());
    }


    /**
     * @return void
     */
    public static function tearDownAfterClass()
    {
        $repository = static::$em->getRepository('AppBundle:User');
        $user = $repository->findOneBy(array('email' => static::$fake_email));

        static::$em->remove($user);
        static::$em->flush();
    }
}
