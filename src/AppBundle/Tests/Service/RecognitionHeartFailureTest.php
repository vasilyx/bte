<?php

namespace AppBundle\Tests\Service;
//use PHPUnit\Framework\TestCase;
use AppBundle\Entity\Submission;
use AppBundle\Service\RecognitionCADCare;

use AppBundle\Service\RecognitionHeartFailure;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class RecognitionHeartFailureTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    private $service;

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        self::bootKernel();

        $this->em = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->service = new RecognitionHeartFailure($this->em);
    }


    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();

        $this->em->close();
        $this->em = null; // avoid memory leaks
    }


    public function testGetTotal()
    {
        $submission = $this->em->getRepository(Submission::class)->find(128);
        $result = $this->service->getTotal($submission);

        // assert that your calculator added the numbers correctly!
         $this->assertEquals(52.65, $result);
    }
}