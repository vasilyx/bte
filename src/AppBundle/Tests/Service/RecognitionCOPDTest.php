<?php

namespace AppBundle\Tests\Service;
//use PHPUnit\Framework\TestCase;
use AppBundle\Entity\Submission;
use AppBundle\Service\RecognitionCOPD;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class RecognitionCOPDTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    private $service;

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        self::bootKernel();

        $this->em = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->service = new RecognitionCOPD($this->em);
    }


    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();

        $this->em->close();
        $this->em = null; // avoid memory leaks
    }


    public function testGetTotal()
    {
        $submission = $this->em->getRepository(Submission::class)->find(234);
        $result = $this->service->getTotal($submission);

        var_dump($result);
        // assert that your calculator added the numbers correctly!
         $this->assertEquals(52.65, $result);
    }
}