<?php

namespace AppBundle\Tests\Service;
//use PHPUnit\Framework\TestCase;
use AppBundle\Entity\Submission;

use AppBundle\Service\RecognitionHypertension;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class RecognitionHypertensionTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    private $service;

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        self::bootKernel();

        $this->em = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        $this->service = new RecognitionHypertension($this->em);
    }


    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();

        $this->em->close();
        $this->em = null; // avoid memory leaks
    }


    public function testGetTotal()
    {
        $submission = $this->em->getRepository(Submission::class)->find(166);
        $result = $this->service->getTotal($submission);

        // assert that your calculator added the numbers correctly!
         $this->assertEquals(61.55, $result);
    }

/*    public function testGetDenominator()
    {
        $result = $this->service->getDenominator(3);
        $this->assertEquals(150, $result);
    }*/
    /*
        public function testCalc_A1c()
        {
            $result = $this->service->calc_A1c();
            $this->assertEquals(25.07, $result);
        }

        public function testCalc_A1c2x()
        {
            $result = $this->service->calc_A1c2x();
            $this->assertEquals(0.9, $result);
        }

        public function testCalc_LDL()
        {
            $result = $this->service->calc_LDL();
            $this->assertEquals(13.1, $result);
        }

        public function testCalc_BloodPressure()
        {
            $result = $this->service->calc_BloodPressure();
            $this->assertEquals(28.13, $result);
        }

        public function testCalc_BloodPressure2x()
        {
            $result = $this->service->calc_BloodPressure2x();
            $this->assertEquals(4.3, $result);
        }

        public function testCalc_SmokingCessation()
        {
            $result = $this->service->calc_SmokingCessation();
            $this->assertEquals(12.9, $result);
        }

        public function testCalc_FootExam()
        {
            $result = $this->service->calc_FootExam();
            $this->assertEquals(8.6, $result);
        }

        public function testCalc_EyeExam()
        {
            $result = $this->service->calc_EyeExam();
            $this->assertEquals(1.17, $result);
        }

        public function testCalc_Nephropathy()
        {
            $result = $this->service->calc_Nephropathy();
            $this->assertEquals(1.43, $result);
        }

        public function testCalc_ACEI()
        {
            $result = $this->service->calc_ACEI();
            $this->assertEquals(3.82, $result);
        }

        public function testCalc_BMI()
        {
            $result = $this->service->calc_BMI();
            $this->assertEquals(7.93, $result);
        }

        public function testCalc_ASCVD()
        {
            $result = $this->service->calc_ASCVD();
            $this->assertEquals(0.2, $result);
        }

        public function testCalc_AspirinUse()
        {
            $result = $this->service->calc_AspirinUse();
            $this->assertEquals(0.4, $result);
        }

        public function testCalc_DepressionScreening()
        {
            $result = $this->service->calc_DepressionScreening();
            $this->assertEquals(1.55, $result);
        }*/


}