<?php
namespace AppBundle\Tests\Controller;
use AppBundle\Tests\AppTest;
use Symfony\Component\HttpFoundation\Response;

class AuthControllerTest extends AppTest
{
    public function testLogin()
    {
        $this->send('login', array(), 'POST');

        $this->assertEquals(Response::HTTP_NOT_FOUND, static::$client->getResponse()->getStatusCode());

        $this->send('login', array('email' => static::$fake_email, 'password' => static::$fake_psw), 'POST');

        $this->assertEquals(Response::HTTP_OK, static::$client->getResponse()->getStatusCode());
        $arr = json_decode(static::$client->getResponse()->getContent());
        $this->assertNotEmpty($arr->token);
        static::$token = $arr->token;
    }

    public function testCheck()
    {
        $this->send('check');
        $this->assertEquals(Response::HTTP_OK, static::$client->getResponse()->getStatusCode());

        $this->send('check', array(), 'GET', 'FAILED');
        $this->assertEquals(Response::HTTP_UNAUTHORIZED, static::$client->getResponse()->getStatusCode());
    }

    public function testForgotten()
    {

    }

    public function testResetPassword()
    {

    }

    public function testRegistration()
    {

    }

    public function testLogout()
    {
        $this->send('logout', array(), 'POST');

        $this->send('check');
        $this->assertEquals(Response::HTTP_UNAUTHORIZED, static::$client->getResponse()->getStatusCode());
    }
}
