<?php
namespace AppBundle\Security\TwoFactorAuth;


class FixedBitNotation {
    protected $_chars;
    protected $_bitsPerCharacter;
    protected $_radix;
    protected $_rightPadFinalBits;
    protected $_padFinalGroup;
    protected $_padCharacter;
    protected $_charmap;

    /**
     * Constructor
     *
     * @param integer $bitsPerCharacter Bits to use for each encoded
     *                character
     * @param string $chars Base character alphabet
     * @param boolean $rightPadFinalBits How to encode last character
     * @param boolean $padFinalGroup Add padding to end of encoded
     *                output
     * @param string $padCharacter Character to use for padding
     */
    public function __construct(
        $bitsPerCharacter, $chars = NULL, $rightPadFinalBits = FALSE,
        $padFinalGroup = FALSE, $padCharacter = '=')
    {
        if (!is_string($chars) || ($charLength = strlen($chars)) < 2) {
            $chars =
                '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-,';
            $charLength = 64;
        }

        if ($bitsPerCharacter < 1) {
            $bitsPerCharacter = 1;
            $radix = 2;

        } elseif ($charLength < 1 << $bitsPerCharacter) {

            $bitsPerCharacter = 1;
            $radix = 2;

            while ($charLength >= ($radix <<= 1) && $bitsPerCharacter < 8) {
                $bitsPerCharacter++;
            }

            $radix >>= 1;

        } elseif ($bitsPerCharacter > 8) {

            $bitsPerCharacter = 8;
            $radix = 256;

        } else {
            $radix = 1 << $bitsPerCharacter;
        }

        $this->_chars = $chars;
        $this->_bitsPerCharacter = $bitsPerCharacter;
        $this->_radix = $radix;
        $this->_rightPadFinalBits = $rightPadFinalBits;
        $this->_padFinalGroup = $padFinalGroup;
        $this->_padCharacter = $padCharacter[0];
    }

    /**
     * Encode a string
     *
     * @param  string $rawString Binary data to encode
     * @return string
     */
    public function encode($rawString)
    {
        $bytes = unpack('C*', $rawString);
        $byteCount = count($bytes);

        $encodedString = '';
        $byte = array_shift($bytes);
        $bitsRead = 0;

        $chars = $this->_chars;
        $bitsPerCharacter = $this->_bitsPerCharacter;
        $rightPadFinalBits = $this->_rightPadFinalBits;
        $padFinalGroup = $this->_padFinalGroup;
        $padCharacter = $this->_padCharacter;

        for ($c = 0; $c < $byteCount * 8 / $bitsPerCharacter; $c++) {
            if ($bitsRead + $bitsPerCharacter > 8) {

                $oldBitCount = 8 - $bitsRead;
                $oldBits = $byte ^ ($byte >> $oldBitCount << $oldBitCount);
                $newBitCount = $bitsPerCharacter - $oldBitCount;

                if (!$bytes) {
                    if ($rightPadFinalBits) $oldBits <<= $newBitCount;
                    $encodedString .= $chars[$oldBits];

                    if ($padFinalGroup) {
                        $lcmMap = array(1 => 1, 2 => 1, 3 => 3, 4 => 1,
                            5 => 5, 6 => 3, 7 => 7, 8 => 1);
                        $bytesPerGroup = $lcmMap[$bitsPerCharacter];
                        $pads = $bytesPerGroup * 8 / $bitsPerCharacter
                            - ceil((strlen($rawString) % $bytesPerGroup)
                                * 8 / $bitsPerCharacter);
                        $encodedString .= str_repeat($padCharacter[0], $pads);
                    }
                    break;
                }

                $byte = array_shift($bytes);
                $bitsRead = 0;

            } else {
                $oldBitCount = 0;
                $newBitCount = $bitsPerCharacter;
            }

            $bits = $byte >> 8 - ($bitsRead + ($newBitCount));
            $bits ^= $bits >> $newBitCount << $newBitCount;
            $bitsRead += $newBitCount;

            if ($oldBitCount) {
                $bits = ($oldBits << $newBitCount) | $bits;
            }

            $encodedString .= $chars[$bits];
        }

        return $encodedString;
    }

    /**
     * Decode a string
     *
     * @param  string $encodedString Data to decode
     * @param  boolean $caseSensitive
     * @param  boolean $strict Returns NULL if $encodedString contains
     *                 an undecodable character
     * @return string|NULL
     */
    public function decode($encodedString, $caseSensitive = TRUE,
                           $strict = FALSE)
    {
        if (!$encodedString || !is_string($encodedString)) {
            return '';
        }

        $chars = $this->_chars;
        $bitsPerCharacter = $this->_bitsPerCharacter;
        $radix = $this->_radix;
        $rightPadFinalBits = $this->_rightPadFinalBits;
        $padFinalGroup = $this->_padFinalGroup;
        $padCharacter = $this->_padCharacter;

        if ($this->_charmap) {
            $charmap = $this->_charmap;

        } else {
            $charmap = array();

            for ($i = 0; $i < $radix; $i++) {
                $charmap[$chars[$i]] = $i;
            }

            $this->_charmap = $charmap;
        }

        $lastNotatedIndex = strlen($encodedString) - 1;

        while ($encodedString[$lastNotatedIndex] == $padCharacter[0]) {
            $encodedString = substr($encodedString, 0, $lastNotatedIndex);
            $lastNotatedIndex--;
        }

        $rawString = '';
        $byte = 0;
        $bitsWritten = 0;

        for ($c = 0; $c <= $lastNotatedIndex; $c++) {

            if (!isset($charmap[$encodedString[$c]]) && !$caseSensitive) {
                if (isset($charmap[$cUpper
                        = strtoupper($encodedString[$c])])) {
                    $charmap[$encodedString[$c]] = $charmap[$cUpper];

                } elseif (isset($charmap[$cLower
                        = strtolower($encodedString[$c])])) {
                    $charmap[$encodedString[$c]] = $charmap[$cLower];
                }
            }

            if (isset($charmap[$encodedString[$c]])) {
                $bitsNeeded = 8 - $bitsWritten;
                $unusedBitCount = $bitsPerCharacter - $bitsNeeded;

                if ($bitsNeeded > $bitsPerCharacter) {

                    $newBits = $charmap[$encodedString[$c]] << $bitsNeeded
                        - $bitsPerCharacter;
                    $bitsWritten += $bitsPerCharacter;

                } elseif ($c != $lastNotatedIndex || $rightPadFinalBits) {

                    $newBits = $charmap[$encodedString[$c]] >> $unusedBitCount;
                    $bitsWritten = 8;

                } else {
                    $newBits = $charmap[$encodedString[$c]];
                    $bitsWritten = 8;
                }

                $byte |= $newBits;

                if ($bitsWritten == 8 || $c == $lastNotatedIndex) {
                    $rawString .= pack('C', $byte);

                    if ($c != $lastNotatedIndex) {
                        $bitsWritten = $unusedBitCount;
                        $byte = ($charmap[$encodedString[$c]]
                                ^ ($newBits << $unusedBitCount)) << 8 - $bitsWritten;
                    }
                }

            } elseif ($strict) {
                return NULL;
            }
        }

        return $rawString;
    }
}
