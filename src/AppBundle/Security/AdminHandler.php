<?php

namespace AppBundle\Security;

use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Security\Handler\RoleSecurityHandler;


class AdminHandler extends RoleSecurityHandler
{
    protected $session;

    protected $superAdminRoles;

    protected $roles;

    /**
     * @param $session
     * @param array $superAdminRoles
     * @param $roles
     */
    public function __construct($session, array $superAdminRoles, $roles)
    {
        $this->session = $session;
        $this->superAdminRoles = $superAdminRoles;
        $this->roles = $roles;
    }

    /**
     * {@inheritDoc}
     */
    public function isGranted( AdminInterface $admin, $attributes, $object = null )
    {
        $hideArray = ['app.admin.program', 'app.admin.user_session', 'app.admin.user_role', 'app.admin.user_role', 'app.admin.submission_diabete'];
        $userId = $this->session->get('currentUser');
        if($userId && $userId == 1){
            if(in_array($admin->getCode(), $hideArray)){
                return false;
            }else{
                return true;
            }
        }else{
            return false;
        }
    }
}