<?php
namespace AppBundle\Security;

use AppBundle\Security\TwoFactorAuth\GoogleAuthenticator;

class TwoFactorAuth{
    private $googleAuthenticator;

    public function __construct()
    {
        $this->googleAuthenticator = new GoogleAuthenticator;
    }

    public function checkCode($secret, $code)
    {

        return $this->googleAuthenticator->checkCode($secret, $code);
    }

    public function getUrl($user, $hostname, $secret)
    {
        $hcode = 'BTE Scoring';
        return $this->googleAuthenticator->getUrl($user, $hostname, $secret, $hcode);
    }

    public function generateSecret()
    {
        return $this->googleAuthenticator->generateSecret();
    }
}