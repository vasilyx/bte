<?php

namespace AppBundle\Controller;

use AppBundle\AppBundle;
use AppBundle\Entity\User;
use AppBundle\Entity\Submission;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;

use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Delete;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Validation;
use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\Controller\Annotations\Prefix;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\Annotations\QueryParam;

use AppBundle\Manager;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class SubmissionController extends ApiController
{
    /**
     * @ApiDoc(
     *  section="Submission",
     *  tags={"Auth"},
     *  headers={
     *         {
     *             "name"="token",
     *             "description"="Authorization key",
     *             "required"=true
     *         }
     *  },
     *  description="Get user's submissions. Headers 'X-Total-Count' - total count",
     *  parameters={
     *      {"name"="page", "dataType"="string", "required"=true, "description"="Default 1"},
     *      {"name"="perPage", "dataType"="string", "required"=true, "description"="Default 10"}
     *  }
     * )
     * @GET("/submissions")
     * @return Response
     */
    public function getSubmissionsAction(Request $request)
    {
        $order = ['field' => 'createdAt', 'direction' => 'ASC'];

        $pagination = ['page' => $request->get('page', 1), 'per_page' => $request->get('perPage', 10),];

        $result = $this->get('bte.manager.submission')->getAllSubmissions($this->getCurrentUser(), $order, $pagination);
        $list = $this->container->get('jms_serializer')->serialize($result['data'], 'json');

        return new Response($list, Response::HTTP_OK, array('X-Total-Count' => $result['totalCount']));
    }

    /**
     * @ApiDoc(
     *  section="Submission",
     *  tags={"Auth"},
     *  headers={
     *         {
     *             "name"="token",
     *             "description"="Authorization key",
     *             "required"=true
     *         }
     *  },
     *  description="Get submission by id.",
     *  parameters={
     *      {"name"="id", "dataType"="string", "required"=true, "description"=""}
     *  }
     * )
     * @GET("/submission")
     */
    public function getSubmissionAction(Request $request)
    {
        return $this->get('bte.manager.submission')->getSubmissionById($request->get('id'));
    }

    /**
     * @ApiDoc(
     *  section="Submission",
     *  tags={"Auth"},
     *  headers={
     *         {
     *             "name"="token",
     *             "description"="Authorization key",
     *             "required"=true
     *         }
     *  },
     *  description="Add submission.",
     *  parameters={
     *       {"name"="from_ehr", "dataType"="boolean", "description"="Was this data extracted from an EHR System", "required"=false},
     *       {"name"="ehr", "dataType"="string", "description"="Name of EHR System", "required"=false},
     *       {"name"="ehr_id", "dataType"="string", "description"="CMS EHR Certification ID", "required"=false},
     *       {"name"="reporting_for", "dataType"="boolean", "description"="Reporting for(0 - individual, 1- group)", "required"=true},
     *       {"name"="reportingStart", "dataType"="string", "description"="Reporting start(Reporting date Range) (2017/03/23)", "required"=false},
     *       {"name"="reportingEnd", "dataType"="string", "description"="Reporting end(Reporting date Range) (2017/03/25)", "required"=false},
     *       {"name"="program_id", "dataType"="string", "description"="Program id", "required"=true},
     *       {"name"="temp_id", "dataType"="string", "description"="Temporary id, it is a id in a /submission/data", "required"=true},
     *  }
     * )
     * @POST("/submission")
     * @ParamConverter("submissionEntity", converter="fos_rest.request_body", options={
     *  })
     * @return Response
     * @throws NotFoundHttpException
     */
    public function postSubmissionAction(Request $request, Submission $submissionEntity)
    {
        $date_end = new \DateTime($request->get('reportingEnd'));
        $date_start = new \DateTime($request->get('reportingStart'));

        $end = explode('/', $request->get('reportingEnd_date'));
        $start = explode('/', $request->get('reportingStart_date'));

        $date_start->setDate('20' . $start[2], $start[0], $start[1]);
        $date_start->setTime(0, 0, 0);

        $date_end->setDate('20' . $end[2], $end[0], $end[1]);
        $date_end->setTime(23, 59, 59);

        $submissionEntity->setReportingEnd($date_end);
        $submissionEntity->setReportingStart($date_start);

        $submissionEntity = $this->get('bte.manager.submission')->createSubmission($submissionEntity, $request->request->get('temp_id'), $request->request->get('providers'), $this->getCurrentUser());

        if ($this->container->getParameter('enable_sql_recognition')) {
            $logger = new \Doctrine\DBAL\Logging\DebugStack();
            $this->get('doctrine')->getConnection()->getConfiguration()->setSQLLogger($logger);
        }


        $log_manager = $this->get('bte.manager.log_manager');
        $log_manager->setMessage('Recognition started for program ' . $submissionEntity->getProgram()->getTableName() ,
            null, $request->request->get('temp_id'), Manager\LogManager::TYPE_RECOGNITION,  Manager\LogManager::STATUS_STARTED);

        /**
         * @var $submissionEntity Submission
         */
        $score_array = $this->get('bte.recognition_' . $submissionEntity->getProgram()->getTableName())->getTotal($submissionEntity);

        if ($this->container->getParameter('enable_sql_recognition')) {
            file_put_contents('../var/logs/sql_' . $submissionEntity->getProgram()->getTableName() . '.log',
                print_r($this->getInterpolatedQueryList($logger->queries), true),
            FILE_APPEND);
        }

        $log_manager->setMessage('Recognition finished for program ' . $submissionEntity->getProgram()->getTableName() ,
            null, $request->request->get('temp_id'), Manager\LogManager::TYPE_RECOGNITION,  Manager\LogManager::STATUS_DONE);


        $log_manager->setMessage('Updating recognition rating ',
            null, $request->request->get('temp_id'), Manager\LogManager::TYPE_RECOGNITION,  Manager\LogManager::STATUS_STARTED);

        $submissionEntity = $this->get('bte.manager.recognition')->setRecognition($submissionEntity, $score_array);
        $log_manager->setMessage('Updating recognition rating - done.',
            null, $request->request->get('temp_id'), Manager\LogManager::TYPE_RECOGNITION,  Manager\LogManager::STATUS_DONE);

        $log_manager->updateByTempId($submissionEntity, $request->request->get('temp_id'));

        $this->get('bte.manager.submission')->updateSubmissionSumScore($submissionEntity, $score_array);


        return $submissionEntity;
    }


    /**
     * @ApiDoc(
     *  section="Submission",
     *  tags={"Auth"},
     *  headers={
     *         {
     *             "name"="token",
     *             "description"="Authorization key",
     *             "required"=true
     *         }
     *  },
     *  description="Delete submission by id.",
     *  parameters={
     *      {"name"="id", "dataType"="string", "required"=true, "description"="Submission Id"}
     *  }
     * )
     * @DELETE("/submission")
     * @param Request $request
     * @return Response
     * @internal param $id
     */
    public function deleteSubmissionAction(Request $request)
    {
        $Submission = $this->getDoctrine()->getRepository('AppBundle:Submission')->find($request->get('id'));

        if (!$Submission instanceof Submission) {
            throw new NotFoundHttpException('Submission not found');
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($Submission);
        $em->flush();

        return new Response(null);
    }

    /**
     * @ApiDoc(
     *  section="Submission",
     *  tags={"Auth"},
     *  headers={
     *         {
     *             "name"="token",
     *             "description"="Authorization key",
     *             "required"=true
     *         }
     *  },
     *  description="Add data for submission. Json - not finished",
     *  parameters={
     *      {"name"="data", "dataType"="textarea", "required"=true, "description"="Submission Data"},
     *      {"name"="providers", "dataType"="string", "required"=true, "description"="providers"},
     *      {"name"="submissionReportingEnd", "dataType"="string", "required"=true, "description"="submissionReportingEnd"},
     *      {"name"="type", "dataType"="boolean", "required"=true, "description"="0 - Json, 1 - base64"},
     *      {"name"="program_id", "dataType"="string", "description"="Program id", "required"=true},
     *  },
     *  statusCodes={
     *      200="Returned when successful",
     *      422="Wrong file. File can not be parsed.",
     *  }
     * )
     * @POST("/submission/data")
     * @param Request $request
     * @return Response
     */
    public function postSubmissionDataAction(Request $request)
    {
        $resp = $this->get('bte.manager.submission')->addSubmissionData($request, $this->getCurrentUser());

        return new Response(json_encode($resp), Response::HTTP_OK);
    }

    /**
     * @ApiDoc(
     *  section="Submission",
     *  tags={"Auth"},
     *  headers={
     *         {
     *             "name"="token",
     *             "description"="Authorization key",
     *             "required"=true
     *         }
     *  },
     *  description="Get data by submission id",
     *  parameters={
     *      {"name"="id", "dataType"="string", "required"=true, "description"="Submission Id"}
     *  },
     *  statusCodes={
     *      200="Returned when successful",
     *  }
     * )
     * @GET("/submission/data")
     * @param Request $request
     * @return Response
     */
    public function getSubmissionDataAction(Request $request)
    {
        $data = $this->get('bte.manager.submission')->getSubmissionDataById($request->get('id'), $this->getCurrentUser());

        if ($data === false) {
            throw new NotFoundHttpException('Sorry, original file is unavailable');
        } else {
            return new Response($data, Response::HTTP_OK);
        }
    }

    /**
     * @ApiDoc(
     *  section="Submission",
     *  tags={"Auth"},
     *  headers={
     *         {
     *             "name"="token",
     *             "description"="Authorization key",
     *             "required"=true
     *         }
     *  },
     *  description="Delete submission data by temp_id.",
     *  parameters={
     *      {"name"="fileId", "dataType"="string", "required"=true, "description"="Submission Temp Id"},
     *      {"name"="program_id", "dataType"="string", "required"=false, "description"="Program Id. At this moment doesnt work"},
     *  }
     * )
     * @DELETE("/submission/data")
     * @param Request $request
     * @return Response
     */
    public function deleteSubmissionDataAction(Request $request)
    {
        $this->get('bte.manager.submission')->deleteSubmissionData($request->get('fileId'));

        return new Response(null);
    }

    /**
     * Replaces any parameter placeholders in a query with the value of that
     * parameter. Useful for debugging. Assumes anonymous parameters from
     * $params are are in the same order as specified in $query
     *
     * @param string $query The sql query with parameter placeholders
     * @param array $params The array of substitution parameters
     * @return string The interpolated query
     */
    protected function interpolateQuery($query, $params) {
        $keys = array();
        $values = $params;
        if (!$params) {
            return $query;
        }
        # build a regular expression for each parameter
        foreach ($params as $key => $value) {
            if (is_string($key)) {
                $keys[] = '/:'.$key.'/';
            } else {
                $keys[] = '/[?]/';
            }
            if (is_string($value))
                $values[$key] = "'" . $value . "'";
            if (is_array($value))
                $values[$key] = "'" . implode("','", $value) . "'";
            if (is_null($value))
                $values[$key] = 'NULL';
            if ($value instanceof \DateTime) $values[$key] = "'" . $value->format(DATE_ATOM) . "'";;
        }
        $query = preg_replace($keys, $values, $query, 1, $count);
        return $query;
    }


    protected function getInterpolatedQueryList($queries)
    {
        $result = [];

        foreach ($queries as $query) {
            array_push($result, $this->interpolateQuery($query['sql'], $query['params']));
        }

        return $result;
    }
}