<?php

namespace AppBundle\Controller;

use AppBundle\AppBundle;
use AppBundle\Entity\User;

use AppBundle\Entity\UserPractice;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;

use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Delete;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Validation;
use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\Controller\Annotations\Prefix;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\Annotations\QueryParam;

use AppBundle\Manager;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserController extends ApiController
{
    /**
     * @ApiDoc(
     *  section="User",
     *  description="User registration. Returns Auth token on success in element 'token' of the array ",
     *  parameters={
     *      {"name"="firstname", "dataType"="string", "required"=true, "description"="First name"},
     *      {"name"="lastname", "dataType"="string", "required"=true, "description"="Last name"},
     *      {"name"="username", "dataType"="string", "required"=true, "description"="Email"},
     *      {"name"="phone", "dataType"="string", "required"=true, "description"="phone"},
     *      {"name"="password", "dataType"="string", "required"=true, "description"="Password"},
     *      {"name"="password_conf", "dataType"="string", "required"=true, "description"="Password Confirmation"},
     *  }
     * )
     * @Post("/user/registration")
     * @ParamConverter("userEntity", converter="fos_rest.request_body", options={
     *      "validator"={"groups"={"details"}}
     *  })
     */
    public function registrationAction(
        Request $request,
        User $userEntity
     //   ConstraintViolationListInterface $validationErrors
    ) {
        /*
        if (count($validationErrors) > 0) {
            return $this->getValidationErrorResponse($validationErrors);
        }
        */

        $user = $this->get('bte.manager.user_manager')->registerUser($userEntity);

         $this->get('bte_mailer')->sendRegistrationConfirm(
             $user->getUsername(),
            [
                'name'  => $user->getFullName(),
                'link'  => $this->container->get('request_stack')->getCurrentRequest()->getSchemeAndHttpHost() .
                    '/confirm_email?token=' . $userEntity->getConfirmEmailToken() .
                    '&username=' . $userEntity->getUsername()
            ]
        );

        return new Response(null);
    }

    /**
     * @ApiDoc(
     *  section="User",
     *  tags={"Auth"},
     *  headers={
     *         {
     *             "name"="token",
     *             "description"="Authorization key",
     *             "required"=true
     *         }
     *  },
     *  description="Get Practice by for user.",
     *  parameters={
     *  }
     * )
     * @GET("/user/practice")
     */
    public function getPracticeAction()
    {
        return $this->get('bte.manager.user_practice')->getPracticeByUser(
            $this->getCurrentUser()
        );
    }

    /**
     * @ApiDoc(
     *  section="User",
     *  tags={"Auth"},
     *  headers={
     *         {
     *             "name"="token",
     *             "description"="Authorization key",
     *             "required"=true
     *         }
     *  },
     *  description="Create Practice. tbd",
     *  parameters={
     *       {"name"="id", "dataType"="string", "required"=true, "description"="id for update"},
     *       {"name"="name", "dataType"="string", "required"=true, "description"="First name"},
     *       {"name"="address", "dataType"="string", "required"=true, "description"="Last name"},
     *       {"name"="city", "dataType"="string", "required"=true, "description"="Last name"},
     *       {"name"="state", "dataType"="string", "required"=true, "description"="Last name"},
     *       {"name"="zip_code", "dataType"="string", "required"=true, "description"="Last name"},
     *       {"name"="phone", "dataType"="string", "required"=true, "description"="Last name"},
     *       {"name"="npi", "dataType"="string", "required"=true, "description"="Last name"},
     *       {"name"="primary_contact_name", "dataType"="string", "required"=true, "description"="Last name"},
     *       {"name"="primary_contact_email", "dataType"="string", "required"=true, "description"="Last name"},
     *  }
     * )
     * @POST("/user/practice")
     * @ParamConverter("practiceEntity", converter="fos_rest.request_body",
     *  options={
     *      "validator"={"groups"={"update"}}
     *  })
     * @return Response
     */
    public function postPracticeAction(Request $request, UserPractice $practiceEntity, ConstraintViolationListInterface $validationErrors)
    {
        if (count($validationErrors) > 0) {
            return $this->getValidationErrorResponse($validationErrors);
        }
        return $this->get('bte.manager.user_practice')->updatePracticeByUser(
            $practiceEntity,
            $this->getCurrentUser()
        );
    }

    /**
     * @ApiDoc(
     *  section="User",
     *  tags={"Auth"},
     *  headers={
     *         {
     *             "name"="token",
     *             "description"="Authorization key",
     *             "required"=true
     *         }
     *  },
     *  description="Get current user."
     * )
     * @Rest\View(serializerGroups={"details"})
     * @GET("/user")
     */
    public function getUserAction()
    {
        return $this->getCurrentUser();
    }
}
