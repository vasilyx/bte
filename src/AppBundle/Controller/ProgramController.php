<?php

namespace AppBundle\Controller;

use AppBundle\AppBundle;
use AppBundle\Entity\User;
use AppBundle\Entity\Program;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;

use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Delete;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Validation;
use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\Controller\Annotations\Prefix;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\Annotations\QueryParam;

use AppBundle\Manager;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ProgramController extends ApiController
{

    /**
     * @ApiDoc(
     *  section="Program",
     *  description="Get list of all programs.",
     *  parameters={
     *  }
     * )
     * @GET("/programs")
     */
    public function getProgramsAction()
    {
        return $this->get('bte.manager.program')->getAllPrograms();
    }

    /**
     * @ApiDoc(
     *  section="Program",
     *  description="Get program by id.",
     *  parameters={
     *      {"name"="id", "dataType"="string", "required"=true, "description"="Program Id"}
     *  }
     * )
     * @GET("/program")
     */
    public function getProgramAction(Request $request)
    {
        return $this->get('bte.manager.program')->getProgramById( $request->get('id') );
    }
}
