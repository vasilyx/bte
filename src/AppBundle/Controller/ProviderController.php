<?php

namespace AppBundle\Controller;

use AppBundle\AppBundle;
use AppBundle\Entity\User;
use AppBundle\Entity\Provider;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;

use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Delete;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Validation;
use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\Controller\Annotations\Prefix;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\Annotations\QueryParam;

use AppBundle\Manager;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;


class ProviderController extends ApiController
{
    /**
     * @ApiDoc(
     *  section="Provider",
     *  tags={"Auth"},
     *  headers={
     *         {
     *             "name"="token",
     *             "description"="Authorization key",
     *             "required"=true
     *         }
     *  },
     *  description="Get providers of current user.",
     *  parameters={
     *     {"name"="program_id", "dataType"="string", "required"=false, "description"="Program Id"}
     *  }
     * )
     * @GET("/providers")
     * @Rest\View(serializerGroups={"providers_list"})
     * @return Response
     */
    public function getProvidersAction(Request $request)
    {
        /*
        $search = $this->getRequest()->get('query', '');

        $order = [
            'field'     => $this->getRequest()->get('orderBy'),
            'direction' => $this->getRequest()->get('order', 'ASC')
        ];

        $pagination = [
            'offset'    => $this->getRequest()->get('offset', 0),
            'page'      => $this->getRequest()->get('page', 1),
            'per_page'  => $this->getRequest()->get('perPage', 0),
        ];

        return $this->get('address_book.manager.accounts_manager')->getAllAccounts($search, $order, $pagination);
        */
        return $this->get('bte.manager.provider')->getProvidersByProgram($this->getCurrentUser(), $request->get('program_id'), $this->getParameter('provider_expire_days'));

    }

    /**
     * @ApiDoc(
     *  section="Provider",
     *  tags={"Auth"},
     *  headers={
     *         {
     *             "name"="token",
     *             "description"="Authorization key",
     *             "required"=true
     *         }
     *  },
     *  description="Get provider by id.",
     *  parameters={
     *      {"name"="id", "dataType"="string", "required"=true, "description"="Provider Id"}
     *  }
     * )
     * @Rest\View(serializerGroups={"provider_details"})
     * @GET("/provider")
     */
    public function getProviderAction(Request $request)
    {
        return $this->get('bte.manager.provider')->getProviderById($request->get('id'));
    }

    /**
     * @ApiDoc(
     *  section="Provider",
     *  tags={"Auth"},
     *  headers={
     *         {
     *             "name"="token",
     *             "description"="Authorization key",
     *             "required"=true
     *         }
     *  },
     *  description="Add provider.",
     *  parameters={
     *      {"name"="clinician_id", "dataType"="string", "description"="Responsible Clinician Id", "required"=false},
     *      {"name"="npi", "dataType"="string", "description"="Clinician NPI", "required"=false},
     *      {"name"="dea", "dataType"="string", "description"="Clinician DEA", "required"=false},
     *      {"name"="license", "dataType"="string", "description"="Clinician Medical License", "required"=false},
     *      {"name"="lastname", "dataType"="string", "description"="Clinician Last Name", "required"=false},
     *      {"name"="firstname", "dataType"="string", "description"="Clinician First name", "required"=false},
     *      {"name"="middlename", "dataType"="string", "description"="Clinician Middle Name", "required"=false},
     *      {"name"="degree_id", "dataType"="string", "description"="Clinician Degree", "required"=false},
     *      {"name"="speciality_id", "dataType"="string", "description"="Clinician Speciality", "required"=false},
     *      {"name"="gender", "dataType"="string", "description"="Clinician Gender (Male/Female) ", "required"=false},
     *      {"name"="date_birth", "dataType"="string", "description"="Clinician Date of Birth (2017/03/23)", "required"=false},
     *      {"name"="active", "dataType"="boolean", "description"="Active", "required"=false},
     *  }
     * )
     * @POST("/provider")
     * @ParamConverter("providerEntity", converter="fos_rest.request_body")
     *  options={
     *    "deserializationContext"={"groups"="provider_create"},
     *    "serializerGroups"={"provider_details"},
     *    "validator"={"groups"={"provider_create"}}
     *  }
     * @return Response
     * @throws NotFoundHttpException
     */
    public function postProviderAction(Request $request, Provider $providerEntity)
    {
        $date = new \DateTime($request->get('date_birth'));// TODO rebuild
        $date->modify('+1 day'); // hack for ui
        $providerEntity->setDateBirth($date);

        return $this->get('bte.manager.provider')->createProvider(
            $providerEntity,
            $this->getCurrentUser()
        );
    }

    /**
     * @ApiDoc(
     *  section="Provider",
     *  description="Update provider.",
     *  tags={"Auth"},
     *  headers={
     *         {
     *             "name"="token",
     *             "description"="Authorization key",
     *             "required"=true
     *         }
     *  },
     *  parameters={
     *      {"name"="id", "dataType"="string", "required"=true, "description"=""},
     *      {"name"="clinician_id", "dataType"="string", "description"="Responsible Clinician Id", "required"=false},
     *      {"name"="npi", "dataType"="string", "description"="Clinician NPI", "required"=false},
     *      {"name"="dea", "dataType"="string", "description"="Clinician DEA", "required"=false},
     *      {"name"="license", "dataType"="string", "description"="Clinician Medical License", "required"=false},
     *      {"name"="lastname", "dataType"="string", "description"="Clinician Last Name", "required"=false},
     *      {"name"="firstname", "dataType"="string", "description"="Clinician First name", "required"=false},
     *      {"name"="middlename", "dataType"="string", "description"="Clinician Middle Name", "required"=false},
     *      {"name"="degree_id", "dataType"="string", "description"="Clinician Degree", "required"=false},
     *      {"name"="speciality_id", "dataType"="string", "description"="Clinician Speciality", "required"=false},
     *      {"name"="gender", "dataType"="string", "description"="Clinician Gender (Male/Female) ", "required"=false},
     *      {"name"="date_birth", "dataType"="string", "description"="Clinician Date of Birth (2017/03/23)", "required"=false},
     *      {"name"="active", "dataType"="boolean", "description"="Active", "required"=false},
     *  }
     * )
     * @PUT("/provider")
     * @ParamConverter("providerEntity", converter="fos_rest.request_body")
     *  options={
     *    "deserializationContext"={"groups"="provider_create"},
     *    "serializerGroups"={"provider_details"},
     *    "validator"={"groups"={"provider_create"}}
     *  }
     * @return Response
     * @throws NotFoundHttpException
     */
    public function putProviderAction(Request $request, Provider $providerEntity)
    {
        $date = new \DateTime($request->get('date_birth'));// TODO rebuild
        $date->modify('+1 day'); // hack for ui
        $providerEntity->setDateBirth($date);
        return $this->get('bte.manager.provider')->updateProvider(
            $providerEntity,
            $this->getCurrentUser()
        );
    }

    /**
     * @ApiDoc(
     *  section="Provider",
     *  tags={"Auth"},
     *  headers={
     *         {
     *             "name"="token",
     *             "description"="Authorization key",
     *             "required"=true
     *         }
     *  },
     *  description="Delete provider by id.",
     *  parameters={
     *      {"name"="id", "dataType"="string", "required"=true, "description"="Provider Id"}
     *  }
     * )
     * @DELETE("/provider")
     * @param Request $request
     * @return Response
     * @internal param $id
     */
    public function deleteProviderAction(Request $request)
    {
        $Provider = $this->getDoctrine()->getRepository('AppBundle:Provider')->find($request->get('id'));

        if (!$Provider instanceof Provider) {
            throw new NotFoundHttpException('Provider not found');
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($Provider);
        $em->flush();

        return new Response(null);
    }

    /**
     * @ApiDoc(
     *  section="Provider",
     *  tags={"Auth"},
     *  headers={
     *         {
     *             "name"="token",
     *             "description"="Authorization key",
     *             "required"=true
     *         }
     *  },
     *  description="Add provider.",
     *  parameters={
     *      {"name"="value", "dataType"="string", "description"="Clinician NPI", "required"=true},
     *      {"name"="id", "dataType"="string", "description"="Record id", "required"=false},
     *  },
     *  statusCodes={
     *      200="Returned when successful",
     *      409="Conflict",
     *  }
     * )
     * @POST("/provider/check/npi")
     * @return Response
     * @throws NotFoundHttpException
     */
    public function postCheckNPIAction(Request $request)
    {

       if ($this->getDoctrine()->getRepository('AppBundle:Provider')->isUniqueNpi(
            $this->getCurrentUser(),
            $request->get('value'),
            $request->get('id')
            ) === false
       ){
           throw new ConflictHttpException('Npi is not unique.');
       }

        return new Response();
    }

    /**
     * @ApiDoc(
     *  section="Provider",
     *  tags={"Auth"},
     *  headers={
     *         {
     *             "name"="token",
     *             "description"="Authorization key",
     *             "required"=true
     *         }
     *  },
     *  description="Add provider.",
     *  parameters={
     *      {"name"="value", "dataType"="string", "description"="Clinician Id", "required"=true},
     *      {"name"="id", "dataType"="string", "description"="Record id", "required"=false},
     *  },
     *  statusCodes={
     *      200="Returned when successful",
     *      409="Conflict",
     *  }
     * )
     * @POST("/provider/check/clinicianId")
     * @return Response
     * @throws NotFoundHttpException
     */
    public function postCheckClinicianIdAction(Request $request)
    {

        if ($this->getDoctrine()->getRepository('AppBundle:Provider')->isUniqueClinicianId(
                $this->getCurrentUser(),
                $request->get('value'),
                $request->get('id')
            ) === false
        ){
            throw new ConflictHttpException('ClinicianId is not unique.');
        }

        return new Response();
    }

    /**
     * @ApiDoc(
     *  section="Provider",
     *  tags={"Auth"},
     *  headers={
     *         {
     *             "name"="token",
     *             "description"="Authorization key",
     *             "required"=true
     *         }
     *  },
     *  description="Get list of clinician degree.",
     * )
     * @GET("/provider/clinician_degree")
     */
    public function getProviderDegreeAction()
    {
        return $this->getDoctrine()->getRepository('AppBundle:ClinicianDegree')->findAll();
    }

    /**
     * @ApiDoc(
     *  section="Provider",
     *  description="Get list of clinician speciality.",
     *  tags={"Auth"},
     *  headers={
     *         {
     *             "name"="token",
     *             "description"="Authorization key",
     *             "required"=true
     *         }
     *  },
     * )
     * @GET("/provider/clinician_speciality")
     */
    public function getProviderSpecialityAction()
    {
        return $this->getDoctrine()->getRepository('AppBundle:ClinicianSpeciality')->findAll();
    }


    /**
     * @ApiDoc(
     *  section="Provider",
     *  tags={"Auth"},
     *  headers={
     *         {
     *             "name"="token",
     *             "description"="Authorization key",
     *             "required"=true
     *         }
     *  },
     *  description="Add data for submission. Json - not finished",
     *  parameters={
     *      {"name"="data", "dataType"="textarea", "required"=true, "description"="Submission Data"},
     *      {"name"="program_id", "dataType"="string", "required"=false, "description"="Program Id"}
     *  },
     *  statusCodes={
     *      200="Returned when successful",
     *      422="Wrong file. File can not be parsed.",
     *  }
     * )
     * @POST("/provider/data")
     * @param Request $request
     * @return Response
     */
    public function postProviderDataAction(Request $request)
    {

        $resp = $this->get('bte.manager.provider')->addProviderData(
            $request,
            $this->getCurrentUser(),
            $this->getParameter('provider_expire_days')
        );

        return $resp;
    }
}