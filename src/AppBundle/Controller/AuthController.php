<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use AppBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;
use Symfony\Component\Routing\RequestContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;


class AuthController extends ApiController
{
    /**
     * @ApiDoc(
     *  section="Auth",
     *  description="Login. Returns Auth token on success in element 'token' of the array ",
     *  parameters={
     *      {"name"="username", "dataType"="string", "required"=true, "description"="Username"},
     *      {"name"="password", "dataType"="string", "required"=true, "description"="Password"},
     *  }
     * )
     *     statusCodes={
     *         200="Returned when successful",
     *         400="Wrong password",
     *         403="User is not activated",
     *         404="Wrong username",
     *     },
     * @Post("/auth/login")
     */
    public function loginAction(Request $request)
    {
        $code = $request->request->get('code');
        if($code){
            $session = $this->get('bte.manager.user_manager')->login(
                $request->request->get('username'),
                $request->request->get('password'),
                $code
            );
        }else{
            $session = $this->get('bte.manager.user_manager')->login(
                $request->request->get('username'),
                $request->request->get('password')
            );
        }

        if($session) {
            $userProvider = $this->container->get('api_key_user_provider');
            $user = $userProvider->loadUserByToken($session->getToken());
            $request->getSession()->set('currentUser', $user->getId());
        }
        return $session;
     }



    /**
     * @ApiDoc(
     *  section="Auth",
     *  tags={"Auth"},
     *  headers={
     *         {
     *             "name"="token",
     *             "description"="Authorization key",
     *             "required"=true
     *         }
     *  },
     *     statusCodes={
     *         200="Returned when successful",
     *         401="Returned when the user is not authorized to say hello",
     *     },
     *  description="Return 200 response if user logged",
     *  parameters={
     *  }
     * )
     * @Get("/auth/check")
     */
    public function checkAction()
    {
        $userProvider = $this->container->get('bte.manager.user_manager');
        $userProvider->checkSessionLifetime($this->getCurrentUser());
        return new Response(null);
    }

    /**
     * @ApiDoc(
     *  section="Auth",
     *  tags={"Auth"},
     *  headers={
     *         {
     *             "name"="token",
     *             "description"="Authorization key",
     *             "required"=true
     *         }
     *  },
     *  description="Logout. Return 200 if OK. Auth token must be removed.",
     *  parameters={
     *  }
     * )
     * @Post("/auth/logout")
     */
    public function logoutAction(Request $request)
    {
        try {
            $token = $this->container->get('security.token_storage')->getToken()->getUser();

            if ($token) {
                $request->getSession()->remove('currentUser');
                $this->getDoctrine()->getRepository('AppBundle:UserSession')->removeByUser($token);
            }

        } catch(\Exception $e) {
        }

        return new Response(null);
    }

    /**
     * @ApiDoc(
     *  section="Auth",
     *  description="Forgot password. Enter your username and check email",
     *  parameters={
     *      {"name"="username", "dataType"="string", "required"=true, "description"="Username"},
     *  }
     * )
     * @Post("/auth/forgotten")
     */
    public function forgottenAction(Request $request)
    {
        $user = $this->get('bte.manager.user_manager')->getUserByUsername(
            $request->request->get('username')
        );

        if (!$user instanceof User) {
            throw new NotFoundHttpException('Email not found');
        }

        $this->get('bte.manager.user_manager')->updateForgotPasswordToken($user);

        $this->sendResetPasswordMail($user);

        return new Response(null);
    }

    /**
     * @ApiDoc(
     *  section="Auth",
     *  description="Reset password. Enter your username and new email",
     *  parameters={
     *      {"name"="username", "dataType"="string", "required"=true, "description"="Parameter 'e' in email"},
     *      {"name"="password", "dataType"="string", "required"=true, "description"="Password"},
     *      {"name"="token", "dataType"="string", "required"=true, "description"="Parameter 't' in email"},
     *  }
     * )
     * @Post("/auth/reset_password")
     * @ParamConverter("userEntity", converter="fos_rest.request_body", options={
     *      "mapping": {"token": "forgottenPasswordToken"}
     *  })
     */
    public function resetPasswordAction(Request $request, User $userEntity, ConstraintViolationListInterface $validationErrors)
    {
        $user = $this->get('bte.manager.user_manager')->getUserByForgottenPasswordToken(
            $request->request->get('token')
        );

        if (!$user instanceof User) {
            throw new NotFoundHttpException('Invitation key is invalid');
        }

        $this->get('bte.manager.user_manager')->updatePassword(
            $user,
            $request->request->get('password')
        );

        $this->sendPasswordUpdatedMail($userEntity);

        return new Response(null);
    }
    /**
     * @ApiDoc(
     *  section="Auth",
     *  description="User two factor authentication confirmation",
     *  parameters={
     *      {"name"="username", "dataType"="string", "required"=true, "description"="Username"},
     *      {"name"="token", "dataType"="string", "required"=true, "description"="Token from Email"}
     *  }
     * )
     * @Post("/auth/twofactorauth")
     */
    public function getTwoFactorAuthCodeAction(Request $request)
    {

        return $this->get('bte.manager.user_manager')->getTwoFactorAuthInfo(
                    $request->request->get('username'),
                    $request->request->get('token'),
                    $request->server->get('HTTP_HOST')
                );
    }

    /**
     * @ApiDoc(
     *  section="Auth",
     *  description="User email confirmation",
     *  parameters={
     *      {"name"="username", "dataType"="string", "required"=true, "description"="Username"},
     *      {"name"="token", "dataType"="string", "required"=true, "description"="Token from Email"}
     *  }
     * )
     *
     * @Post("/auth/email_confirmation")
     */
    public function emailConfirmationAction(Request $request)
    {

        $this->get('bte.manager.user_manager')->setUserEmailConfirmation(
            $request->request->get('username'),
            $request->request->get('token'),
            $request->request->get('code')
        );
        return new Response(null);
    }

    private function sendResetPasswordMail(User $userEntity)
    {
        return $this->get('bte_mailer')->sendForgottenPassword(
            $userEntity->getUsername(), [
                'name'  => $userEntity->getFullName(),
                'link'  => $this->container->get('request_stack')->getCurrentRequest()->getSchemeAndHttpHost() . '/password_reset?token=' .
                    $userEntity->getForgottenPasswordToken() . '&username=' .
                    $userEntity->getUsername()
            ]
        );
    }

    private function sendPasswordUpdatedMail(User $userEntity)
    {
        return $this->get('bte_mailer')->sendResetPassword(
            $userEntity->getUsername(),
            ['name'=> $userEntity->getUsername()]
        );
    }
}