<?php

namespace AppBundle\Controller;


use AppBundle\Entity\Recognition;
use AppBundle\Repository\RecognitionRepository;
use Doctrine\ORM\EntityNotFoundException;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;

use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Delete;
use Proxies\__CG__\AppBundle\Entity\Submission;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Validation;
use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\Controller\Annotations\Prefix;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\Annotations\QueryParam;

use AppBundle\Manager;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;

class RecognitionController extends ApiController
{

    /**
     * @ApiDoc(
     *  section="Recognition",
     *  tags={"Auth"},
     *  headers={
     *         {
     *             "name"="token",
     *             "description"="Authorization key",
     *             "required"=true
     *         }
     *  },
     *  description="Get list of all recognitions of current user. Headers 'X-Total-Count' - total count",
     *  parameters={
     *      {"name"="page", "dataType"="string", "required"=true, "description"="Default 1"},
     *      {"name"="perPage", "dataType"="string", "required"=true, "description"="Default 10"}
     *  }
     * )
     * @GET("/recognitions")
     */
    public function getRecognitionsAction(Request $request)
    {
        /**
         * @var RecognitionRepository
         */
        $repository = $this->getDoctrine()->getRepository('AppBundle:Recognition');

        $order = [
            'field'     => 'createdAt',
            'direction' => 'ASC'
        ];

        $pagination = [
            'page'      => $request->get('page', 1),
            'per_page'  => $request->get('perPage', 10),
        ];

        $result = $repository->getRecognitionsByUser($this->getCurrentUser(), $order, $pagination);
        $list = $this->container->get('jms_serializer')->serialize(
            $result['data'],
            'json'
        );

        return new Response($list, Response::HTTP_OK , array('X-Total-Count' => $result['totalCount']));
    }

    /**
     * @ApiDoc(
     *  section="Recognition",
     *  tags={"Auth"},
     *  headers={
     *         {
     *             "name"="token",
     *             "description"="Authorization key",
     *             "required"=true
     *         }
     *  },
     *  description="Get pdf by recognition id.",
     *  parameters={
     *      {"name"="id", "dataType"="string", "required"=true, "description"="Recognition Id"}
     *  }
     * )
     * @GET("/recognition/pdf")
     */
    public function getRecognitionPDFAction(Request $request)
    {
        /**
         * @var Submission $submission
         */
        if ($submission = $this->get('bte.manager.submission')->getSubmissionById($request->get('id'))) {
            $submission = $submission[0];
        } else {
            throw new EntityNotFoundException('Wrong submission id.');
        }


       $html = $this->renderView('AppBundle:pdf:recognition_pdf.html.twig', [
           'submission' => $submission
        ]);

        $file_name = $submission->getProgram()->getName() . ' ' . date('m_d_Y');

        return new PdfResponse(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
            $file_name .'.pdf'
        );
    }

    /**
     * @ApiDoc(
     *  section="Recognition",
     *  tags={"Auth"},
     *  headers={
     *         {
     *             "name"="token",
     *             "description"="Authorization key",
     *             "required"=true
     *         }
     *  },
     *  description="Get csv by recognition id.",
     *  parameters={
     *      {"name"="id", "dataType"="string", "required"=true, "description"="Recognition Id"}
     *  }
     * )
     * @GET("/recognition/csv/program")
     */
    public function getRecognitionCSVProgramAction(Request $request)
    {
        /**
         * @var Submission $submission
         */
        if ($submission = $this->get('bte.manager.submission')->getSubmissionById(
            $request->get('id')
        )) {
            $data = $this->get('bte.manager.submission')->getSubmissionProgramList($submission[0]);
        } else {
            throw new EntityNotFoundException('Wrong submission id.');
        }

        $file_name = $submission[0]->getProgram()->getName() . ' - aggregated by program ' . date('m_d_Y');

        $response = new Response(implode("\n",$data));
        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-Disposition', 'attachment; filename="' . $file_name . '.csv"');

        return $response;
    }

    /**
     * @ApiDoc(
     *  section="Recognition",
     *  tags={"Auth"},
     *  headers={
     *         {
     *             "name"="token",
     *             "description"="Authorization key",
     *             "required"=true
     *         }
     *  },
     *  description="Get csv by recognition id.",
     *  parameters={
     *      {"name"="id", "dataType"="string", "required"=true, "description"="Recognition Id"}
     *  }
     * )
     * @GET("/recognition/csv/measure")
     */
    public function getRecognitionCSVMeasureAction(Request $request)
    {
        /**
         * @var Submission $submission
         */
        if ($submission = $this->get('bte.manager.submission')->getSubmissionById(
            $request->get('id')
        )) {
            $data = $this->get('bte.manager.submission')->getSubmissionMeasureList($submission[0]);

        } else {
            throw new EntityNotFoundException('Wrong submission id.');
        }

        $file_name = $submission[0]->getProgram()->getName() . ' - aggregated by measure ' . date('m_d_Y');

        $response = new Response(implode("\n", $data));
        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-Disposition', 'attachment; filename="' . $file_name . '.csv"');

        return $response;
    }
}
