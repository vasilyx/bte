<?php

namespace AppBundle\Controller;

use FOS\RestBundle\View\View;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;

use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Delete;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\Controller\Annotations\Prefix;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\Annotations\QueryParam;


abstract class ApiController extends FOSRestController
{
    protected $container;

    /**
     * Get array of constraint violations messages
     *
     * @param  ConstraintViolationListInterface $violations
     * @return array
     */
    protected function getConstraintViolations(ConstraintViolationListInterface $violations)
    {
        $errors = [];

        if (count($violations) > 0) {
            /* @var ConstraintViolationInterface $violation */
            foreach ($violations as $violation) {
                $errors[$violation->getPropertyPath()] = $violation->getMessage();
            }
        }

        return $errors;
    }

    /**
     * Response view for request data validation violations
     *
     * @param  ConstraintViolationListInterface $violations
     * @return View
     */
    protected function getValidationErrorResponse(ConstraintViolationListInterface $violations)
    {
        return View::create(array(
            'status' => 'error',
            'message' => $this->getConstraintViolations($violations),
        ), 400);
    }

    /**
     * @return \AppBundle\Entity\User
     */
    protected function getCurrentUser()
    {
        return $this->container->get('security.token_storage')->getToken()->getUser();
    }
}