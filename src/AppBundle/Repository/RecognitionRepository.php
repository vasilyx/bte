<?php

namespace AppBundle\Repository;
use \AppBundle\Entity\Recognition;
use \AppBundle\Entity\User;
use AppBundle\Entity\Provider;


class RecognitionRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param User $user
     * @return array
     */
    public function getRecognitionsByUser(User $user, $order, $pagination)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('s')
            ->from('AppBundle\Entity\Submission', 's')
            ->leftJoin(
                'AppBundle\Entity\Recognition',
                'r',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                's.id = r.submission'
            )
            ->where('s.user = :user and r.id is not null AND (s.reportingFor = 2 OR (s.sumScore > 50 AND s.reportingFor <> 2))')
            ->setParameter('user', $user)
            ->orderBy('s.id', 'DESC')
            ->groupBy('s.id');


        return [
            'totalCount' => count(
                $qb->getQuery()->getResult()
            ),
            'data' => $qb
                        ->setFirstResult($pagination['per_page'] * ($pagination['page'] - 1))
                        ->setMaxResults($pagination['per_page'])
                        ->getQuery()
                        ->getResult(),
        ];
    }

    /**
     * @param User $user
     * @return array
     */
    public function getRecognitionForPDFProgram(User $user, $program, $submission_id, Provider $provider)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb
            ->select('r')
            ->from('AppBundle\Entity\Recognition', 'r')
            ->leftJoin(
                'AppBundle\Entity\Submission',
                's',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'r.submission = s.id'
            )
            ->where('
                s.user = :user and 
                s.program = :program and 
                s.id <> :submission_id and 
                r.id is not null AND r.provider = :provider
            ')
            ->setParameter('user', $user)
            ->setParameter('program', $program)
            ->setParameter('submission_id', $submission_id)
            ->setParameter('provider', $provider)
            ->orderBy('r.id', 'DESC');


        $res = $qb->getQuery()->getResult();

        return $res;
    }
}
