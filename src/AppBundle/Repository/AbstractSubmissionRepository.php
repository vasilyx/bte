<?php

namespace AppBundle\Repository;


class AbstractSubmissionRepository extends \Doctrine\ORM\EntityRepository
{
    public function updateByTempId($subm_id, $temp_id)
    {
        $result =  $this->createQueryBuilder('u')
            ->update()
            ->set('u.submission_id', '?1')
            ->setParameter(1, $subm_id)
            ->where('u.temp_id = ?2')
            ->setParameter(2, $temp_id)
            ->getQuery()->getSingleScalarResult();


        $this->createQueryBuilder('u')
            ->update()
            ->set('u.temp_id', 'null')
            ->where('u.temp_id = ?2')
            ->setParameter(2, $temp_id)
            ->getQuery()->getSingleScalarResult();

        return $result;
    }


    public function deleteByTempId($temp_id)
    {
        return $this->createQueryBuilder('u')
            ->delete()
            ->where('u.temp_id = ?2')
            ->setParameter(2, $temp_id)
            ->getQuery()->getSingleScalarResult();
    }
}