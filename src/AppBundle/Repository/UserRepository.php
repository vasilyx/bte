<?php

namespace AppBundle\Repository;

use AppBundle\Entity\UserSession;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    public function loadByToken($token)
    {
        return $this->getEntityManager()->getRepository(UserSession::class)
            ->findUserByToken($token);
    }

    public function getInactiveAccounts($duration = 'P120D')
    {
        $date = new \DateTime();
        $date->sub(new \DateInterval($duration));

        $users = $this->createQueryBuilder('u')
            ->where('u.id <> 1')
            ->andWhere('u.datetimeLastLogin < :date')
            ->setParameter('date', $date)
            ->getQuery()
            ->getResult()
        ;

        return $users;
    }

    public function loadByUsername($username)
    {
        return $this->findOneByUsername($username);
    }

    public function isUniqueUsername(User $user)
    {
        $res = $this->findBy(['username' => $user->getUsername()]);
        return count($res) > 0 ? false : true;
    }
}
