<?php

namespace AppBundle\Monolog;

use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\OptimisticLockException;
use Monolog\Handler\FingersCrossed\ActivationStrategyInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpFoundation\Request;
use Monolog\Logger;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Exception\InsufficientAuthenticationException;

class ApiActivationStrategy implements ActivationStrategyInterface
{
    public function isHandlerActivated(array $record)
    {
            if (isset($record['context']) && isset($record['context']['exception']) && (
                ($record['context']['exception'] instanceof HttpException) ||
                ($record['context']['exception'] instanceof EntityNotFoundException) ||
                ($record['context']['exception'] instanceof ResourceNotFoundException) ||
                ($record['context']['exception'] instanceof AccessDeniedException) ||
                ($record['context']['exception'] instanceof BadCredentialsException) ||
                ($record['context']['exception'] instanceof InsufficientAuthenticationException) ||
                ($record['context']['exception'] instanceof OptimisticLockException))
            ) {
            return false;
        }

        return $record['level'] >= Logger::ERROR;
    }
    }