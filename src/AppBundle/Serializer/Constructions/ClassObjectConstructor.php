<?php
/**
 * Created by PhpStorm.
 * User: starinskij
 * Date: 3/31/15
 * Time: 13:17
 */

namespace AppBundle\Serializer\Constructions;

use JMS\Serializer\Construction\ObjectConstructorInterface;
use JMS\Serializer\VisitorInterface;
use JMS\Serializer\Metadata\ClassMetadata;
use JMS\Serializer\DeserializationContext;


class ClassObjectConstructor implements ObjectConstructorInterface
{
    /**
     * {@inheritDoc}
     */
    public function construct(
        VisitorInterface $visitor,
        ClassMetadata $metadata,
        $data,
        array $type,
        DeserializationContext $context
    )
    {
        return new $metadata->name;
    }
}