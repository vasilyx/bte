<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\AdminInterface as AdminInterface;

class SubmissionAdmin extends AbstractAdmin
{
    /**
     * @param MenuItemInterface $menu
     * @param string $action
     * @param null|AdminInterface $childAdmin
     *
     * @return void
     */
    protected function configureTabMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        $name = ($action == 'edit')? 'View Submission' : 'Edit Submission';
        $options =  array('uri' => $this->generateUrl($action == 'edit' ? 'show' : 'edit', array('id' => $this->getRequest()->get('id'))));
        $menu->addChild($name, $options);
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('from_ehr')
            ->add('ehr')
            ->add('ehr_id')
            ->add('reportingStart')
            ->add('reportingEnd')
            ->add('reportingFor')
            ->add('reportingMethod')
            ->add('modifiedAt')
            ->add('user')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('user', null, [
                'sortable' => 'user_id',
            ])
            ->add('from_ehr')
            ->add('ehr')
            ->add('ehr_id')
            ->add('reportingStart')
            ->add('reportingEnd')
            ->add('reportingFor', null, ['label' => 'Type(0-Ind;1-Group;2-DA)'])
            ->add('reportingMethod')
            ->add('modifiedAt','datetime', ['label' => 'Created'])
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
            ->add('user')
            ->add('from_ehr')
            ->add('ehr')
            ->add('ehr_id')
            ->add('reportingStart')
            ->add('reportingEnd')
            ->add('reportingFor', null, ['label' => 'Type(0-Ind;1-Group;2-DA)'])
            ->add('reportingMethod')
            ->add('program')
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('user')
            ->add('provider', null, ['associated_property' => 'npi', 'label' => 'Providers NPI'])
            ->add('from_ehr')
            ->add('ehr')
            ->add('ehr_id')
            ->add('reportingStart')
            ->add('reportingEnd')
            ->add('reportingFor', null, ['label' => 'Type(0-Ind;1-Group;2-DA)'])
            ->add('reportingMethod')
            ->add('modifiedAt','datetime', ['label' => 'Created'])
        ;
    }
}
