<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Admin\AdminInterface as AdminInterface;

use Knp\Menu\ItemInterface as MenuItemInterface;

class UserPracticeAdmin extends AbstractAdmin
{
    /**
     * @param \Knp\Menu\ItemInterface $menu
     * @param $action
     * @param null|\Sonata\AdminBundle\Admin\Admin $childAdmin
     *
     * @return void
     */
    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        $name = ($action == 'edit')? 'View User Practice' : 'Edit User Practice';
        $options =  array('uri' => $this->generateUrl($action == 'edit' ? 'show' : 'edit', array('id' => $this->getRequest()->get('id'))));
        $menu->addChild($name, $options);
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('user')
            ->add('name')
            ->add('address')
            ->add('city')
            ->add('state')
            ->add('zipCode')
            ->add('phone')
            ->add('npi')
            ->add('primaryContactName')
            ->add('primaryContactEmail')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('user', null, [
                'sortable' => 'user_id',
            ])
            ->add('name')
            ->add('address')
            ->add('city')
            ->add('state')
            ->add('zipCode')
            ->add('phone')
            ->add('npi')
            ->add('primaryContactName')
            ->add('primaryContactEmail')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('user')
            ->add('name')
            ->add('address')
            ->add('city')
            ->add('state')
            ->add('zipCode')
            ->add('phone')
            ->add('npi')
            ->add('primaryContactName')
            ->add('primaryContactEmail')
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('user')
            ->add('name')
            ->add('address')
            ->add('city')
            ->add('state')
            ->add('zipCode')
            ->add('phone')
            ->add('npi')
            ->add('primaryContactName')
            ->add('primaryContactEmail')
        ;
    }
}
