<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\AdminInterface as AdminInterface;

class ProviderAdmin extends AbstractAdmin
{
    /**
     * @param MenuItemInterface $menu
     * @param string $action
     * @param null|AdminInterface $childAdmin
     *
     * @return void
     */
    protected function configureTabMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        $name = ($action == 'edit')? 'View Provider' : 'Edit Provider';
        $options =  array('uri' => $this->generateUrl($action == 'edit' ? 'show' : 'edit', array('id' => $this->getRequest()->get('id'))));
        $menu->addChild($name, $options);
    }
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('clinicianId')
            ->add('npi')
            ->add('dea')
            ->add('license')
            ->add('lastname')
            ->add('firstname')
            ->add('middlename')
            ->add('degree')
            ->add('speciality')
            ->add('gender')
            ->add('dateBirth')
            ->add('active')
            ->add('createdAt')
            ->add('modifiedAt')
            ->add('user_id')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('clinicianId')
            ->add('npi')
            ->add('dea')
            ->add('license')
            ->add('lastname')
            ->add('firstname')
            ->add('middlename')
            ->add('degree')
            ->add('speciality')
            ->add('gender')
            ->add('dateBirth')
            ->add('active')
            ->add('createdAt')
            ->add('modifiedAt')
            ->add('user', 'url', array(
                'attributes' => array('target' => '_blank'),
                'route' => array(
                    'name' => 'admin_app_user_show',
                    'identifier_parameter_name' => 'id'
                )
            ))
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('clinicianId')
            ->add('npi')
            ->add('dea')
            ->add('license')
            ->add('lastname')
            ->add('firstname')
            ->add('middlename')
            ->add('degree')
            ->add('speciality')
            ->add('gender')
            ->add('dateBirth')
            ->add('active')
            ->add('user')
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('clinicianId')
            ->add('npi')
            ->add('dea')
            ->add('license')
            ->add('lastname')
            ->add('firstname')
            ->add('middlename')
            ->add('degree')
            ->add('speciality')
            ->add('gender')
            ->add('dateBirth')
            ->add('active')
            ->add('createdAt')
            ->add('modifiedAt')
            ->add('user_id')
        ;
    }
}
