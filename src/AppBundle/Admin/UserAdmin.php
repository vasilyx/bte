<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\AdminInterface as AdminInterface;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class UserAdmin extends AbstractAdmin
{
    /**
     * @param MenuItemInterface $menu
     * @param string $action
     * @param null|AdminInterface $childAdmin
     *
     * @return void
     */
    protected function configureTabMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        $name = ($action == 'edit')? 'View User' : 'Edit User';
        $options =  array('uri' => $this->generateUrl($action == 'edit' ? 'show' : 'edit', array('id' => $this->getRequest()->get('id'))));
        $menu->addChild($name, $options);
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('username')
            ->add('firstname')
            ->add('lastname')
            ->add('phone')
            ->add('isTest', null, ['label' => 'Test account'])
            ->add('status', 'doctrine_orm_choice',  array(), ChoiceType::class, array(
                'choices' => $this->getUserStatuses(),
                'expanded' => true,
                'choice_translation_domain' => false,
            ))
            ->add('datetimeRegistration')
            ->add('datetimeLastLogin')
            ->add('notes')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('username')
            ->add('firstname')
            ->add('lastname')
            ->add('phone')
            ->add('isTest', null, ['label' => 'Test account'])
            ->add('status', 'choice', array('choices' => $this->getUserStatuses(true)))
            ->add('hasAgreement')
            ->add('notes')
            ->add('datetimeRegistration')
            ->add('datetimeLastLogin')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('username')
            ->add('firstname')
            ->add('lastname')
            ->add('phone')
          //  ->add('password')
            ->add('isTest', null, ['label' => 'Test account'])
            ->add('status', ChoiceType::class, array('choices' => $this->getUserStatuses()))
            ->add('datetimeRegistration')
            ->add('datetimeLastLogin')
            ->add('twoFactorAuthenticationToken')
            ->add('hasAgreement')
            ->add('notes', TextareaType::class)
        ;
    }
    public static function getUserStatuses($flip = false)
    {
        $result = array(
            'Inactive' => 0,
            'Active' => 1,
            'Wrong password' => 2,
            'Long time inactive' => 3
        );
        if($flip){
            $result = array_flip($result);
        }
        return $result;
    }
    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('username')
       //     ->add('password')
            ->add('firstname')
            ->add('lastname')
            ->add('phone')
            ->add('isTest', null, ['label' => 'Test account'])
            ->add('status', 'choice', array('choices' => $this->getUserStatuses(true)))
            ->add('datetimeRegistration')
            ->add('datetimeLastLogin')
            ->add('twoFactorAuthenticationToken')
            ->add('notes', TextareaType::class)
        ;
    }
}
