<?php

namespace AppBundle\Manager;


use AppBundle\Entity\User;
use AppBundle\Entity\UserSession;
use AppBundle\Entity\Provider;
use AppBundle\Entity\Recognition;
use AppBundle\Entity\Submission;
use AppBundle\Entity\SubmissionAsthma;
use AppBundle\Entity\SubmissionCADCare;
use AppBundle\Entity\SubmissionCOPD;
use AppBundle\Entity\SubmissionDepression;
use AppBundle\Entity\SubmissionDiabete;
use AppBundle\Entity\SubmissionHeartFailure;
use AppBundle\Entity\SubmissionHypertension;
use AppBundle\Entity\SubmissionIBDCare;
use AppBundle\Entity\UserPractice;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;

use AppBundle\Exception\TwoFactorAuthException;
use AppBundle\Security\TwoFactorAuth;



class CommonManager
{
    private $em;


    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function cleanDb(){
        $connection = $this->em->getConnection();
        $platform = $this->em->getConnection()->getDatabasePlatform();
        $connection->query('SET FOREIGN_KEY_CHECKS=0');
        $connection->executeUpdate($platform->getTruncateTableSQL('user_session', true ));
        $connection->executeUpdate($platform->getTruncateTableSQL('log', true ));
        $connection->executeUpdate($platform->getTruncateTableSQL('provider', true ));
        $connection->executeUpdate($platform->getTruncateTableSQL('recognition', true ));
        $connection->executeUpdate($platform->getTruncateTableSQL('submission', true ));
        $connection->executeUpdate($platform->getTruncateTableSQL('submission_asthma', true ));
        $connection->executeUpdate($platform->getTruncateTableSQL('submission_cadcare', true ));
        $connection->executeUpdate($platform->getTruncateTableSQL('submission_copd', true ));
        $connection->executeUpdate($platform->getTruncateTableSQL('submission_depression', true ));
        $connection->executeUpdate($platform->getTruncateTableSQL('submission_diabete', true ));
        $connection->executeUpdate($platform->getTruncateTableSQL('submission_heartfailure', true ));
        $connection->executeUpdate($platform->getTruncateTableSQL('submission_hypertension', true ));
        $connection->executeUpdate($platform->getTruncateTableSQL('submission_ibdcare', true ));
        $connection->executeUpdate($platform->getTruncateTableSQL('user_practice', true ));
        $connection->executeUpdate($platform->getTruncateTableSQL('provider_submission', true ));
        $connection->query('SET FOREIGN_KEY_CHECKS=1');
        $repository = $this->em->getRepository(User::class);
        $this->cleanTable($repository, true);
    }

    private function cleanTable($repository, $exceptFirst = fasle){
        $items = $repository->findAll();
        foreach ($items as $item) {
            if($exceptFirst && $item->getId() == 1){
                continue;
            }
            $this->em->remove($item);
        }
        $this->em->flush();
    }

}