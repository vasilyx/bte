<?php

namespace AppBundle\Manager;

use AppBundle\Entity\ClinicianDegree;
use AppBundle\Entity\ClinicianSpeciality;
use AppBundle\Entity\User;

use AppBundle\Entity\Provider;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use AppBundle\Service\CSVImport;


use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class ProviderManager
{
    private $em;
    private $repository;
    private $authorizationChecker;
    private $dataService;


    public function __construct(EntityManager $em,
                                CSVImport $dataService,
                                AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->em = $em;
        $this->dataService = $dataService;
        // @var AppBundle\Repository
        $this->repository = $this->em->getRepository(Provider::class);
        $this->authorizationChecker = $authorizationChecker;
    }

    public function getAllProviders(User $user)
    {
        return $this->repository->findByUser($user);
    }

    public function getProvidersByProgram(User $user, $program_id, $providerExpireDays)
    {
        return $this->repository->findEnabledProviders($user, $program_id, $providerExpireDays);
    }

    public function getProviderById($id)
    {
        return $this->repository->findById($id);
    }


    public function updateProvider(Provider $providerEntity, User $user, $andFlush = true)
    {
        $degree = $this->em->getRepository(ClinicianDegree::class)->find($providerEntity->getDegreeId());
        $providerEntity->setDegree($degree);

        $speciality = $this->em->getRepository(ClinicianSpeciality::class)->find($providerEntity->getSpecialityId());
        $providerEntity->setSpeciality($speciality);
        $this->checkUniqueFields($providerEntity, $user, $providerEntity->getId());

        $providerEntity->setModifiedAt();

        return $this->saveProvider($providerEntity, $andFlush);
    }

    public function createProvider(Provider $providerEntity, User $user, $andFlush = true)
    {
        $degree = $this->em->getRepository(ClinicianDegree::class)->find($providerEntity->getDegreeId());
        $providerEntity->setDegree($degree);

        $speciality = $this->em->getRepository(ClinicianSpeciality::class)->find($providerEntity->getSpecialityId());
        $providerEntity->setSpeciality($speciality);

        $this->checkUniqueFields($providerEntity, $user, false);
        $providerEntity->setUser($user);

        return $this->saveProvider($providerEntity, $andFlush);
    }

    public function deleteProvider(Provider $providerEntity, $andFlush = true)
    {
        $this->em->remove($providerEntity);

        if (true === $andFlush) {
            $this->em->flush();
        }

        return true;
    }

    public function getCleanDataByUserIdAndNpi($userId, $npi){

        $provider = $this->repository->findProviderByUserAndNpi($userId, $npi);
        
        if($provider) {
            foreach ($provider->getSubmission() as $submission) {
                $this->em->remove($submission);
            }
            $this->em->flush();
        }else{
            throw new \LogicException('Provider with NPI: "'.$npi.'" and userId: "'.$userId.'" was not found.' );
        }

    }

    private function saveProvider(Provider $providerEntity, $andFlush)
    {
        $this->em->persist($providerEntity);

        if (true === $andFlush) {
            $this->em->flush();
        }

        return $providerEntity;
    }

    /**
     * @param Provider $providerEntity
     * @param User $user
     */
    private function checkUniqueFields(Provider $providerEntity, User $user, $id)
    {
        if ($this->repository->isUniqueNpi($user, $providerEntity->getNpi(), $id) === false) {
            throw new ConflictHttpException('Npi is not unique.');
        }

        if ($this->repository->isUniqueClinicianId($user, $providerEntity->getClinicianId(), $id) === false) {
            throw new ConflictHttpException('Clinician ID is not unique.');
        }
    }

    /**
     * @param $request
     * @param User $user
     * @return array
     */
    public function addProviderData($request, User $user, $providerExpireDays){
        $source_file = base64_decode($request->request->get('data'));
        if(!mb_check_encoding($source_file, 'UTF-8')){
            throw new UnprocessableEntityHttpException('Your data could not be encoded because it contains invalid UTF8 characters.');
        }
        $file = preg_split("/\\r\\n|\\r|\\n/", $source_file);

        $class_csv = new \AppBundle\Service\CSV\Provider($this->em);

        $delimiter = $this->detectDelimiter($file[0]);

        if (count(str_getcsv($file[0], $delimiter)) !== $class_csv->getPropertiesCount()) {

            throw new UnprocessableEntityHttpException('File can not be parsed. File has incorrect count of fields '.
                count(str_getcsv($file[0], $delimiter)) . ' , but expected ' .  $class_csv->getPropertiesCount() . '.' );

        }

        try {
            $program_id = $request->request->get('program_id');
            $returnData =  $this->dataService->importProvider($file, $user, $program_id, $providerExpireDays);
        } catch (\Exception $e) {
            throw new UnprocessableEntityHttpException('File can not be parsed. Wrong format.');
        }
/*
        $this->backupService->putFile($returnData['id'], $returnData['data']);
        $this->backupService->putFileSource($returnData['id'], $source_file);*/

        return $returnData;
    }

    protected function detectDelimiter($str)
    {
        $delimiters = array(
            ';' => 0,
            ',' => 0,
            "\t" => 0,
            "|" => 0
        );

        foreach ($delimiters as $delimiter => &$count) {
            $count = count(str_getcsv($str, $delimiter));
        }

        return array_search(max($delimiters), $delimiters);
    }
}