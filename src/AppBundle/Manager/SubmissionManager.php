<?php

namespace AppBundle\Manager;

use AppBundle\Entity\Measure;
use AppBundle\Entity\Program;
use AppBundle\Entity\Recognition;
use AppBundle\Entity\RecognitionDetail;
use AppBundle\Entity\User;

use AppBundle\Entity\Submission;
use AppBundle\Entity\Provider;
use AppBundle\Entity\SubmissionDiabete;
use AppBundle\Entity\SubmissionCADCare;
use AppBundle\Entity\SubmissionHypertension;
use AppBundle\Repository\SubmissionRepository;
use AppBundle\Service\BackupSubmission;
use AppBundle\Service\CSV\CSVInterface;
use AppBundle\Service\RecognitionDiabete;

use AppBundle\Service\CSVImport;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class SubmissionManager
{
    private $em;

    /**
     * @var $repository SubmissionRepository
     */
    private $repository;
    private $authorizationChecker;
    private $dataService;
    private $backupService;

    public function __construct(EntityManager $em,
                                AuthorizationCheckerInterface $authorizationChecker,
                                CSVImport $dataService,
                                BackupSubmission $backupService)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(Submission::class);
        $this->authorizationChecker = $authorizationChecker;
        $this->dataService = $dataService;
        $this->backupService = $backupService;
    }

    public function getAllSubmissions(User $user, $order, $pagination)
    {
        $offset = $pagination['per_page'] * ($pagination['page'] - 1);
        $data = $this->repository->findBy(['user' => $user], ['id' => 'DESC'], $pagination['per_page'], $offset);

        $fullData = $this->repository->findByUser($user);

        return array('data' => $data, 'totalCount' => count($fullData));
    }


    /**
     * @param $id
     * @return mixed
     */
    public function getSubmissionById($id)
    {
        return $this->repository->findById($id);
    }

    /**
     * @param Submission $submission
     * @return array
     */
    public function getSubmissionMeasureList(Submission $submission)
    {
        $arr = [];

        array_push($arr, ['ResponsibleProviderID', 'NPI', 'MeasureName', 'Denominator', 'Numerator', 'Score']);

        /**
         * @var Recognition $recognition
         */
        foreach ($submission->getRecognition() as $recognition) {

            /**
             * @var RecognitionDetail $detail
             */
            foreach ($recognition->getRecognitionDetail() as $detail) {

                array_push($arr, [
                    $recognition->getProvider()->getClinicianId(),
                    $recognition->getProvider()->getNpi(),
                    $detail->getMeasure()->getName(),
                    $detail->getDenominator(),
                    $detail->getNumerator(),
                    $detail->getScore()
                ]);
            }
        }

        foreach ($arr as &$r) {
            $r = implode(',', $r );
        }

        return $arr;
    }

    /**
     * @param Submission $submission
     * @return array
     */
    public function getSubmissionProgramList(Submission $submission)
    {
        $arr = [];
        array_push($arr, ['NPI', 'Program', 'Score', 'StarRatingAssessed', 'StarRatingAchieved',
            'ExpirationDate', 'PreviousRating', 'PreviousAssessed']);

        /**
         * @var Recognition $recognition
         */

        foreach ($submission->getRecognition() as $recognition) {

            $score = $recognition->getGroupID() ? $recognition->getGroupScore() : $recognition->getScore();

            array_push($arr, [
                 $recognition->getProvider()->getNpi(),
                 $submission->getProgram()->getTableName(),
                 $score,
                 $this->createScoreLabel($recognition->getStarRatingAssesed()),
                 $this->createScoreLabel($recognition->getStarRatingAchived()),
                 $recognition->getExpirationDate()->format('m/d/Y'),
                 '',
                 ''
            ]);
        }

        foreach ($arr as &$r) {
            $r = implode(',', $r );
        }

        return $arr;
    }

    public function createScoreLabel($score)
    {
        return $score < 3 ? 'Not Recognized' : ($score . ' Stars');
    }

    /**
     * @param Submission $submissionEntity
     * @param $temp_id
     * @param $providers
     * @param User $user
     * @param bool|true $andFlush
     * @return Submission
     */
    public function createSubmission(Submission $submissionEntity, $temp_id, $providers, User $user,  $andFlush = true)
    {
        $submissionEntity->setUser($user);
        $program = $this->em->getRepository(Program::class)->find($submissionEntity->getProgramId());
        $submissionEntity->setProgram($program);
        $this->setProviders($submissionEntity, $providers, $program, $temp_id);
        $this->saveSubmission($submissionEntity, $andFlush);
        $this->em->getRepository('AppBundle\Entity\Submission' . $program->getTableName())->updateByTempId($submissionEntity->getId(), $temp_id);
        $this->backupService->renameFile($temp_id, $submissionEntity->getId());
        $this->backupService->renameFileSource($temp_id, $submissionEntity->getId());
        $this->backupService->renameRenderFileSource($temp_id, $submissionEntity->getId());

        return $submissionEntity;
    }

    /**
     * @param Submission $submissionEntity
     * @param array $score_array
     * @param bool|true $andFlush
     * @return Submission
     */
    public function updateSubmissionSumScore(Submission $submissionEntity, $score_array, $andFlush = true)
    {
        $score = 0;
        $numerator = [];
        $denominator = [];

        foreach ($score_array as $item) {
            if(isset($item['details']) && count($item['details'])){
                foreach ($item['details'] as $recognitionDetail) {
                    if ($recognitionDetail->getDenominator()) {
                        $measure = $recognitionDetail->getMeasure();
                        $numerator[$measure->getId()] += $recognitionDetail->getNumerator();
                        $denominator[$measure->getId()] += $recognitionDetail->getDenominator();
                        $total[$measure->getId()] = $measure->getTotal();
                    }
                }
            }
        }
        foreach ($denominator as $measureId => $item){
            $score += ($numerator[$measureId]/$denominator[$measureId])*$total[$measureId];
        }
        $score = round($score, 2);

        if (count($score_array)) {
            $submissionEntity->setSumScore($score);
        } else {
            $submissionEntity->setSumScore(0);
        }

        $submissionEntity->setModifiedAt();

        return $this->saveSubmission($submissionEntity, $andFlush);
    }

    /**
     * @param Submission $submissionEntity
     * @param bool|true $andFlush
     * @return bool
     */
    public function deleteAccount(Submission $submissionEntity, $andFlush = true)
    {
        $this->em->remove($submissionEntity);

        if (true === $andFlush) {
            $this->em->flush();
        }

        return true;
    }

    /**
     * @param $temp_id
     * @return mixed
     */
    public function deleteSubmissionData($temp_id)
    {
        $this->backupService->removeFile($temp_id);
        // TODO fix
        return $this->em->getRepository(SubmissionDiabete::class)->deleteByTempId($temp_id);
    }

    /**
     * @param $request
     * @param User $user
     * @return array
     */
    public function addSubmissionData($request, User $user)
    {
        $source_file = base64_decode($request->request->get('data'));

        if(!mb_check_encoding($source_file, 'UTF-8')){
            throw new UnprocessableEntityHttpException('Your data could not be encoded because it contains invalid UTF8 characters.');
        }

        $program = $this->em->getRepository(Program::class)->find($request->request->get('program_id'));
        $submissionReportingEndDate = $this->convertJSDateFormat($request->request->get('submissionReportingEnd'));

        $file = preg_split("/\\r\\n|\\r|\\n/", $source_file);

        $name_scv = '\AppBundle\Service\CSV\\' . $program->getTableName();

        /**
         * @var $class_csv CSVInterface
         */
        $class_csv = new $name_scv();
        $class_csv->setSubmissionReportingEndDate($submissionReportingEndDate); //very important here !!!

        $delimiter = $this->detectDelimiter($file[0]);

        if (count(str_getcsv($file[0], $delimiter)) !== $class_csv->getPropertiesCount()) {

            throw new UnprocessableEntityHttpException('File can not be parsed. File has incorrect count of fields '.
                count(str_getcsv($file[0], $delimiter)) . ' , but expected ' .  $class_csv->getPropertiesCount() . '.' );

        }

        if ($request->request->get('reporting_for') != 2) {
            $provider_list = $this->dataService->getProviderList($file);
            $this->checkProviderList(explode($delimiter, $request->request->get('providers')), $provider_list);
        }


        try {
            $providers = $this->getProviderListFromIDs(explode($delimiter, $request->request->get('providers')));
            $returnData = $this->dataService->importSubmission($file, $program, $submissionReportingEndDate, $providers);
        } catch (\Exception $e) {
            throw new UnprocessableEntityHttpException('File can not be parsed. Wrong format.');
        }

        $this->backupService->putFile($returnData['id'], $returnData['data']);
        $this->backupService->putFileSource($returnData['id'], $source_file);
        $this->backupService->putRenderFile($returnData['id'], $returnData);

        return $returnData;
    }

    private function convertJSDateFormat($date){
        $date = str_replace('T', ' ', $date);
        $date = str_replace('.000Z', '', $date);
        return \DateTime::createFromFormat("Y-m-d H:i:s", $date);
    }

    /**
     * @param Submission $submissionEntity
     * @param $andFlush
     * @return Submission
     */
    private function saveSubmission(Submission $submissionEntity, $andFlush)
    {
        $this->em->persist($submissionEntity);

        if (true === $andFlush) {
            $this->em->flush();
        }

        return $submissionEntity;
    }

    /**
     * @param Submission $submissionEntity
     * @param $providers
   `  *
     * @return Submission
     */
    private function setProviders(Submission $submissionEntity, $providers, Program $program, $temp_id)
    {
        $ids = array_unique(explode(',', $providers));

        if ($ids) {

            foreach($ids as $id) {
                $prov = $this->em->getRepository(Provider::class)->find($id);

                if ($prov &&
                    $this->em->getRepository('AppBundle\Entity\Submission' . $program->getTableName())->findOneBy(['temp_id' => $temp_id, 'npi' => $prov->getNpi()])
                ) {
                    $submissionEntity->addProvider($prov);
                }
            }
        }

        return $submissionEntity;
    }


    /**
     * @param $provider_selected
     * @param $providers_cvs
     */
    private function checkProviderList($provider_selected, $providers_cvs)
    {
        $providerExtra = $providers_cvs;

        foreach ($provider_selected as $provider) {
            $prov = $this->em->getRepository(Provider::class)->find($provider);
            if (!isset($providers_cvs[$prov->getNpi()])) {
                throw new UnprocessableEntityHttpException("File does not contain provider {$prov->getNpi()}.");
            } else {
               unset($providerExtra[$prov->getNpi()]);
            }

            if ($providers_cvs[$prov->getNpi()] < 10) {
                throw new UnprocessableEntityHttpException("File does not contain enough records for provider {$prov->getNpi()}.");
            }
        }

        unset($providerExtra['']);

        if ($providerExtra) {
            $keys = array_keys($providerExtra);
            throw new UnprocessableEntityHttpException("File contains extra records with providers " . implode(', ' , $keys). '.');
        }

        return;
    }

    /**
     * @param string $providers
     * @return array
     */
    private function getProviderListFromIDs($providers)
    {
       $res = [];
        foreach ($providers as $provider) {
            $prov = $this->em->getRepository(Provider::class)->find($provider);
            array_push($res, $prov->getNpi());
        }

        return $res;
    }


    /**
     * @param $id
     * @param User $user
     * @return string
     */
    public function getSubmissionDataById($id, User $user)
    {
        // TODO add checking on user
        return $this->backupService->getFile($id);
    }

    protected function detectDelimiter($str)
    {
        $delimiters = array(
            ';' => 0,
            ',' => 0,
            "\t" => 0,
            "|" => 0
        );

        foreach ($delimiters as $delimiter => &$count) {
            $count = count(str_getcsv($str, $delimiter));
        }

        return array_search(max($delimiters), $delimiters);
    }

    public function getDataAggregatorSubmission()
    {
        return $this->repository->getDataAggregatorSubmission();
    }

    public function getCleanDataBySubmissionId($submission_id){

        $submission = $this->repository->find($submission_id);

        if($submission) {
            $this->em->remove($submission);
            $this->em->flush();
        }else{
            throw new \LogicException('Submission with Id : ' . $submission_id . ' was not found.' );
        }
    }
}