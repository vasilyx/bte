<?php

namespace AppBundle\Manager;

use AppBundle\Entity\Provider;
use AppBundle\Entity\RecognitionDetail;
use AppBundle\Entity\Recognition;

use Doctrine\ORM\EntityManager;
use \AppBundle\Entity\Submission;


class RecognitionManager
{
    private $em;
    private $repository;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(Recognition::class);
    }


    /**
     * @return \AppBundle\Entity\Recognition[]|array
     */
    public function getAllRecognitions()
    {
        return $this->repository->findAll();
    }


    /**
     * @param Submission $submission
     * @param $score_array
     * @return Submission
     */
    public function setRecognition(Submission $submission, $score_array)
    {
        $modifiedAt = clone $submission->getModifiedAt();
        $modifiedAt->modify('+2 year');

        /**
         * @var $provider Provider
         */
        foreach ($submission->getProvider() as $provider) {

            $npi = $score_array[$provider->getNpi()];

            $recognition = new Recognition();
            $recognition->setSubmision($submission);
            $recognition->setProvider($provider);
            $recognition->setScore($npi['total'] ?? 0);
            $recognition->setNumerator($npi['numerator'] ?? 0);
            $recognition->setDenominator($npi['denominator'] ?? 0);
            $recognition->setGroupID($npi['group_id'] ?? null);


            $sub_recognition = $this->repository->getRecognitionForPDFProgram(
                $recognition->getSubmission()->getUser(),
                $recognition->getSubmission()->getProgram(),
                $recognition->getSubmission()->getId(),
                $recognition->getProvider()
            );

            $asset = $this->getScoreAsset($recognition->getScore());
            $score_achieved = $this->getScoreAchieved($asset, $sub_recognition);

            $recognition->setStarRatingAssesed($asset);
            $recognition->setStarRatingAchived($score_achieved);
            $recognition->setExpirationDate($modifiedAt);

            $this->em->persist($recognition);
       //     $this->em->flush();

            if ($npi['details']) {

                /**
                 * @var RecognitionDetail $recognitionDetail
                 */
                foreach ($npi['details'] as $recognitionDetail) {
                    $recognitionDetail->setRecognition($recognition);
                    $this->em->persist($recognitionDetail);

                }
            }
        }
        $this->em->flush();
        $this->em->refresh($submission);
        $this->setSubmissionGroupScore($submission, true);

        return $submission;
    }

    public function getScoreAchieved($q3, $recognition)
    {
        if (count($recognition)>1) {

            $q1 = $this->getScoreAsset($recognition[1]->getScore());
            $q2 = $this->getScoreAsset($recognition[0]->getScore());


            if ($q3 >$q1 && $q2 > $q1) {
                return ($q2 + 1);
            }

            if ($q3 < $q2 && $q2 < $q1) {
                return ($q2 - 1);
            }
        } elseif(count($recognition) == 1) {
            return $this->getScoreAsset($recognition[0]->getScore());
        }

        return $q3;
    }

    public function getScoreAsset($score)
    {
        if ($score <= 50) {
            return 2;
        } elseif ($score > 50 && $score < 65) {
            return 3;
        } elseif ($score >= 65 && $score < 85) {
            return 4;
        } elseif ($score >= 85) {
            return 5;
        }
    }

    protected function getGroupScore($arr)
    {
        $sum = 0;
        foreach ($arr as $item) {
            if ($item['den']) {
                $sum = $sum + ($item['num'] / $item['den'] * $item['score']);
            }
        }

        return number_format($sum,2);
    }

    /**
     * @param Submission $submission
     * @return Submission
     */
    public function setSubmissionGroupScore(Submission $submission, $refresh = false)
    {
        $temp_arr = [];

        /**
         * @var $recognition Recognition
         */
        foreach ($submission->getRecognition() as $recognition) {
            if ($recognition->getGroupID()) {

                if ($refresh) {
                    $this->em->refresh($recognition);
                }

                /**
                 * @var $detail RecognitionDetail
                 */
                foreach ($recognition->getRecognitionDetail() as $detail) {

                    if ($detail) {
                        $this->em->refresh($detail);
                    }

                    if (isset($temp_arr[$recognition->getGroupID()][$detail->getMeasureId()])) {

                        $temp_arr[$recognition->getGroupID()][$detail->getMeasureId()]['num'] += $detail->getNumerator();
                        $temp_arr[$recognition->getGroupID()][$detail->getMeasureId()]['den'] += $detail->getDenominator();

                    } else {
                        $temp_arr[$recognition->getGroupID()][$detail->getMeasureId()] = [
                            'num' => $detail->getNumerator(),
                            'den' => $detail->getDenominator(),
                            'score' => $detail->getMeasure()->getTotal()];
                    }
                }
            }
        }


        if ($temp_arr) {
            foreach ($submission->getRecognition() as $recognition) {
                if ($recognition->getGroupID()) {
                    $groupScore = $this->getGroupScore($temp_arr[$recognition->getGroupID()]);
                    $recognition->setGroupScore($groupScore);
                    $this->em->persist($recognition);
                    $this->em->flush();
                }
            }
        }

        return $submission;
    }
}