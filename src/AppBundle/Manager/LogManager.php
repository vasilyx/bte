<?php

namespace AppBundle\Manager;

use AppBundle\Entity\Log;

use AppBundle\Entity\Submission;
use Doctrine\ORM\EntityManager;




class LogManager
{
    private $em;
    private $repository;
    private $token_storage;

    public $start_time = null;

    const STATUS_STARTED = 0;
    const STATUS_PROGRESS = 1;
    const STATUS_DONE = 2;

    const TYPE_SUBMISSION = 0;
    const TYPE_RECOGNITION = 1;
    const TYPE_PROVIDER = 2;

    const STATUS_TEXT = [self::STATUS_STARTED => 'Started', self::STATUS_PROGRESS => 'In progress', self::STATUS_DONE => 'Done'];
    const TYPE_TEXT = [self::TYPE_SUBMISSION => 'Submission', self::TYPE_RECOGNITION => 'Recognition', self::TYPE_PROVIDER => 'Provider'];

    public function __construct(EntityManager $em, $token_storage)
    {
        $this->em = $em;
        $this->start_time = microtime(true);
        $this->repository = $this->em->getRepository(Log::class);
        $this->token_storage = $token_storage;
    }


    /**
     * @param string $description
     * @param null $submission_id
     * @param null $temp_id
     * @param null $type_id
     * @param null $status_id
     * @return Log
     * @internal param null $submission_temp_id
     */
    public function setMessage($description = '', $submission_id = null, $temp_id = null, $type_id = null, $status_id = null)
    {
        $logEntity = new Log();
        $time = number_format(microtime(true) - $this->start_time, 2);
        $memory = number_format((memory_get_peak_usage(true)/1024/1024), 2);
        $logEntity->setDescription($description . ' [time: ' . $time . '; memory: ' . $memory . ' Mb]');

        $logEntity->setSubmissionId($submission_id);
        $logEntity->setTempId($temp_id);
        $logEntity->setTypeId($type_id);
        $logEntity->setType(self::TYPE_TEXT[$type_id]);
        $logEntity->setStatusId($status_id);
        $logEntity->setStatus(self::STATUS_TEXT[$status_id]);

        return $this->saveLog($logEntity, true);
    }

    private function saveLog(Log $logEntity, $andFlush = false)
    {
        $logEntity->setUserId($this->token_storage->getToken()->getUser()->getId());
        $logEntity->setCreatedAt();
        $this->em->persist($logEntity);

        if (true === $andFlush) {
            $this->em->flush();
        }

        return $logEntity;
    }

    /**
     * @param Submission $submissionEntity
     * @param $temp_id
     */
    public function updateByTempId(Submission $submissionEntity, $temp_id)
    {
        $this->repository->updateByTempId($submissionEntity->getId(), $temp_id);
    }
}