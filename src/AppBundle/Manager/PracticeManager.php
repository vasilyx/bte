<?php

namespace AppBundle\Manager;

use AppBundle\Entity\User;
//use AppBundle\Entity\UserSession;
use AppBundle\Entity\UserPractice;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;


class PracticeManager
{
    private $em;
    private $repository;
    private $authorizationChecker;

    public function __construct(EntityManager $em, AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(UserPractice::class);
        $this->authorizationChecker = $authorizationChecker;
    }

    public function getPracticeByUser(User $user)
    {
        return $this->repository->findByUser($user);
    }

    public function updatePracticeByUser(UserPractice $entity, User $user)
    {
        $entity->setUser($user);
        $this->em->persist($entity);
        $this->em->flush();

        return $entity;
    }
}