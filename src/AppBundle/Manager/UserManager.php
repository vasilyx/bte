<?php

namespace AppBundle\Manager;

use AppBundle\Entity\User;
use AppBundle\Entity\UserSession;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;


use AppBundle\Exception\AccessDeniedSessionException;
use AppBundle\Exception\AccessDeniedStatusException;
use AppBundle\Exception\TwoFactorAuthException;
use AppBundle\Security\TwoFactorAuth;



class UserManager
{
    private $em;
    private $repository;
    private $authorizationChecker;
    private $encoderFactory;

    public function __construct(EntityManager $em, AuthorizationCheckerInterface $authorizationChecker, EncoderFactory $encoderFactory)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(User::class);
        $this->authorizationChecker = $authorizationChecker;
        $this->encoderFactory = $encoderFactory;
    }

    public function registerUser(User $userEntity)
    {
        if ($this->repository->isUniqueUsername($userEntity) === false) {
            throw new ConflictHttpException('Account with this e-mail is already exists.');
        }

        $encoder = $this->getEncoder($userEntity);
        $userEntity->setPassword($encoder->encodePassword($userEntity->getPassword(), $userEntity->getSalt()));
        $userEntity->generateConfirmationToken();
        $userEntity->setDatetimeRegistration(new \DateTime);
        $userEntity->setDatetimeLastLogin(new \DateTime);
        $userEntity->setStatus(User::STATUS_INACTIVE);

        $user = $this->saveUser($userEntity, true);
        return $user;
    }

    public function saveUser(User $userEntity, $andFlush = false)
    {
        $this->em->persist($userEntity);

        if (true === $andFlush) {
            $this->em->flush();
        }

        return $userEntity;
    }

    public function updatePassword(User $userEntity, $password, $andFlush = true)
    {
        $encoder = $this->getEncoder($userEntity);

        $userEntity
            ->setForgottenPasswordToken(null)
            ->setPassword($encoder->encodePassword($password, $userEntity->getSalt()));

        return $this->saveUser($userEntity, $andFlush);
    }

    public function updatePasswordForAdminUser($password)
    {
        $userEntity = $this->repository->findOneById(1);
        return $this->updatePassword($userEntity, $password);
    }

    public function getAllUser()
    {
        return $this->repository->findAll();
    }



    public function getUserByUsername($username, $active = true)
    {
        return $this->repository->findOneByUsername($username);
    }

    public function getUserById($id, $active = true)
    {
        return $this->repository->findOneById($id);
    }


    public function getUserByForgottenPasswordToken($token)
    {
        return $this->repository->findOneByForgottenPasswordToken($token);
    }


    public function updateForgotPasswordToken(User $userEntity, $andFlush = true)
    {
        $userEntity->generateForgotPasswordToken();

        return $this->saveUser($userEntity, $andFlush);
    }


    public function login($username, $password, $code = false)
    {
        /**
         * @var User $user
         */
        $user = $this->repository->findOneByUsername($username);

        if (!$user) {
            throw new EntityNotFoundException('Email is not found.');
        }

        switch ($user->getStatus()) {
            case User::STATUS_INACTIVE:
                throw new AccessDeniedHttpException('Account is not activated');
                break;
            case User::STATUS_LONG_TIME_INACTIVE:
                throw new AccessDeniedStatusException('Account was locked after 120 consecutive days of no log ins.');
                break;
            case User::STATUS_WRONG_PASSWORD:
                throw new AccessDeniedStatusException('Three consecutive failed log in attempts.');
                break;
        }

        if (!$this->encoderFactory->getEncoder($user)->isPasswordValid(
            $user->getPassword(),
            $password,
            $user->getSalt()
        )){
            $wrongLoginQuantity = $user->getWrongLoginQuantity();
            if( $wrongLoginQuantity >= 3 ) {
                if($user->getStatus() == User::STATUS_ACTIVE){
                    $user->setWrongLoginQuantity(0);
                    $user->setStatus(User::STATUS_WRONG_PASSWORD);
                    $this->saveUser($user, true);
                }
                throw new AccessDeniedStatusException('Three consecutive failed log in attempts.');
            } else {
                $user->setWrongLoginQuantity( $wrongLoginQuantity + 1);
                $this->saveUser($user, true);
                throw new BadRequestHttpException('Wrong password.');
            }
        }

        if($code){
            $auth = new TwoFactorAuth();
            if(!$auth->checkCode($user->getTwoFactorAuthenticationToken(), $code)){
                throw new TwoFactorAuthException('Verification code is incorrect.');
            }
        }else{
            throw new TwoFactorAuthException('');
        }

        $user->setDatetimeLastLogin(new \DateTime);
        $user->setWrongLoginQuantity(0);
        $this->saveUser($user, true);

        $userSessionRep = $this->em->getRepository(UserSession::class);
        $userSessionList = $userSessionRep->findByUser($user);
        if(is_array($userSessionList)) {
            foreach ($userSessionList as $userSession){
                $this->em->remove($userSession);
            }
            $this->em->flush();
        }
        $session = new UserSession($user);
        $this->em->getRepository(UserSession::class)->save($session);

        return $session;
    }

    public function processUserStatus(){
        $userList = $this->repository->getInactiveAccounts();
        if(is_array($userList) && count($userList)){
            foreach ($userList as $user){
                $user->setStatus(User::STATUS_LONG_TIME_INACTIVE);
                $this->em->persist($user);
            }
            $this->em->flush();
        }
    }

    public function checkSessionLifetime(User $user)
    {
        $userSession = $this->em->getRepository(UserSession::class);
        $session = $userSession->findOneByUser($user);
        if($session){
            $date = new \DateTime();
            $date->sub(new \DateInterval('PT15M'));

            if( $session->getLastChange() && ($session->getLastChange() < $date)) {
                $this->em->remove($session);
                $this->em->flush();
                $session = null;
            }
            if($session) {
                $session->setLastChange(new \DateTime());
                $this->em->persist($session);
                $this->em->flush();
            }else{
                throw new AccessDeniedSessionException('Session was broken.');
            }
        }else{
            throw new AccessDeniedSessionException('Session was not found');
        }
    }

    public function getTwoFactorAuthInfo($username, $token, $host)
    {

        $user = $this->repository->findOneBy([
            'username' => $username,
            'confirmEmailToken' => $token
        ]);

        if (!$user instanceof User) {
            throw new NotFoundHttpException('Registration key is invalid');
        }

        $result = array();
        $auth = new TwoFactorAuth();
        if(!$user->getTwoFactorAuthenticationToken()){
            $secret = $auth->generateSecret();
            $user->setTwoFactorAuthenticationToken($secret);
            $this->saveUser($user, true);
        }
        $result['two_factor_auth_qr_code'] = $auth->getUrl($user->getUsername(), $host, $user->getTwoFactorAuthenticationToken());
        $result['two_factor_auth_key'] = $user->getTwoFactorAuthenticationToken();

        return $result;
    }

    public function setUserEmailConfirmation($username, $token, $code)
    {
        $user = $this->repository->findOneBy([
            'username' => $username,
            'confirmEmailToken' => $token
        ]);

        $auth = new TwoFactorAuth();


        if (!$user instanceof User) {
            throw new NotFoundHttpException('Registration key is invalid');
        }

        if(!$auth->checkCode($user->getTwoFactorAuthenticationToken(), $code)){
            throw new TwoFactorAuthException('Verification code is incorrect.');
        }

        $user->setConfirmEmailToken(null)
            ->setStatus(User::STATUS_ACTIVE);

        return $this->saveUser($user, true);

    }

    protected function getEncoder(UserInterface $user)
    {
        return $this->encoderFactory->getEncoder($user);
    }
}