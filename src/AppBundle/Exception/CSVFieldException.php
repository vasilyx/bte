<?php
namespace AppBundle\Exception;


class CSVFieldException extends \Exception
{
    protected $isCritical;

    public function __construct($message = "", $isCritical = false, $code = 0, Throwable $previous = null) {

        $this->isCritical = $isCritical;
        parent::__construct($message, $code, $previous);

    }

    /**
     * @return mixed
     */
    public function getIsCritical()
    {
        return $this->isCritical;
    }
}