<?php

namespace AppBundle\Service;
use AppBundle\Entity\SubmissionDiabete;
use AppBundle\Manager\LogManager;
use Doctrine\ORM\EntityManager;

class RecognitionDiabete extends AbstractRecognition
{
    public function __construct(EntityManager $em, LogManager $logManager)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(SubmissionDiabete::class);
        $this->logManager = $logManager;
    }

    /**
     * @param string $andSql
     * @param int $minAge
     * @param int $maxAge
     * @return string
     */
    public function getDenominatorSQL($andSql = '', $minAge = 18, $maxAge = 75)
    {
        $result = 'sd.submission_id = :submissionId
                    AND sd.npi = :npi
                    AND sd.diabetesDiagnosis = 1
                    AND TIMESTAMPDIFF(YEAR, sd.patientDOB, sd.lastVisitDate) >= ' . $minAge . '
                    AND TIMESTAMPDIFF(YEAR, sd.patientDOB, sd.lastVisitDate) <= ' . $maxAge . '
                    AND sd.lastVisitDate IS NOT NULL
                    AND (sd.lastVisitDate BETWEEN  :fromDate AND :toDate)';

        if ($andSql) {
            $result = $result . ' AND ' . $andSql;
        }

        return $result;
    }


    /**
     * Hemoglobin A1c Control (HbA1c)
     * Hemoglobin A1c < 8%
     */
    public function calculateA1c()
    {
        return $this->getNumerator($this->getDenominatorSQL() . '
                    AND sd.HbA1c2 IS NOT NULL
                    AND sd.HbA1c2 < 8
                    AND sd.HbA1cDate2 IS NOT NULL
                    AND (sd.HbA1cDate2 BETWEEN  :fromDate AND :toDate)
                    ');
    }

    /**
     * Hemoglobin A1c (HbA1c) Measurement Twice Annually
     * A1C measured 2x annually
     */
    public function calculateA1c2x()
    {
        return $this->getNumerator($this->getDenominatorSQL() . '
                    AND sd.HbA1c1 IS NOT NULL
                    AND sd.HbA1c2 IS NOT NULL
                    AND sd.HbA1cDate2 IS NOT NULL
                    AND (sd.HbA1cDate1 BETWEEN  :fromDate AND :toDate)
                    AND DATE_DIFF(sd.HbA1cDate2, sd.HbA1cDate1) > 90
                    ');
    }

    /**
     * Lipid Control
     * LDL<70 or Mod/High intensity statin
     */
    public function calculateLDL()
    {
        return $this->getNumerator($this->getDenominatorSQL() . ' AND
                    (
                        ((sd.LDLDate BETWEEN  :fromDate AND :toDate)
                            AND sd.LDLLevel >= 70
                        )
                    OR
                        sd.statinStatus = 1
                    )');
    }

    /**
     * Blood Pressure Control
     * Blood Pressure <140/90
     */
    public function calculateBloodPressure()
    {
        return $this->getNumerator($this->getDenominatorSQL() . ' AND
                    (
                        ((sd.bloodPressureDate2 BETWEEN  :fromDate AND :toDate)
                            AND sd.systolic2 IS NOT NULL
                            AND sd.systolic2 < 140
                            AND sd.diastolic2 IS NOT NULL
                            AND sd.diastolic2 < 90
                        )
                    )');
    }

    /**
     * Blood Pressure Measurement Twice Annually
     * Blood Pressure measured 2x annually
     */
    public function calculateBloodPressure2x()
    {
        return $this->getNumerator($this->getDenominatorSQL() . ' AND
                    (
                        ((sd.bloodPressureDate1 BETWEEN  :fromDate AND :toDate)
                            AND sd.systolic1 IS NOT NULL
                            AND sd.diastolic1 IS NOT NULL
                        )
                    AND
                       ((sd.bloodPressureDate2 BETWEEN  :fromDate AND :toDate)
                            AND sd.systolic2 IS NOT NULL
                            AND sd.diastolic2 IS NOT NULL
                            AND sd.bloodPressureDate2 IS NOT NULL
                            AND DATE_DIFF(sd.bloodPressureDate2, sd.bloodPressureDate1) >= 90
                        )
                    )');
    }

    /**
     * Tobacco Use and Cessation Advice and Treatment(sd.retinalExamDate BETWEEN  :fromDate AND :toDate)
     * Smoking Cessation discussed and documented
     */
    public function calculateTobacco()
    {
        return $this->getNumerator($this->getDenominatorSQL() . ' AND
                    (
                        ((sd.tobaccoStatusAssessmentDate BETWEEN  :fromDate AND :toDate)
                            AND sd.tobaccoStatus=0
                        )
                    OR
                       ((sd.tobaccoStatusAssessmentDate BETWEEN  :fromDate AND :toDate)
                            AND (sd.tobaccoCessationAdviceOrTreatmentDate BETWEEN  :fromDate AND :toDate)
                            AND sd.tobaccoStatus=1
                        )
                    )');
    }

    /**
     * Podiatry Examination
     * Foot Exam done annually
     */
    public function calculatePodiatry()
    {
        return $this->getNumerator($this->getDenominatorSQL() . ' AND
                    (
                        (sd.footExamDate BETWEEN  :fromDate AND :toDate)

                    OR
                            sd.bilateralAmputation=1
                    )');
    }

    /**
     * Ophthalmologic Examination
     * Eye exam annually if retinopathy, every 2 years if no retinopathy
     */
    public function calculateOphthalmologic()
    {
        $retinalExamDate = clone $this->submission->getReportingEnd();


        return $this->getNumerator($this->getDenominatorSQL() . ' AND
                    (
                        (sd.retinalExamDate IS NOT NULL
                            AND (sd.retinalExamDate BETWEEN :retinalExamDate AND :toDate)
                            AND sd.retinopathy=0
                        )
                    OR
                       (sd.retinalExamDate IS NOT NULL
                            AND (sd.retinalExamDate BETWEEN :fromDate AND :toDate)
                          AND sd.retinopathy=1
                        )
                    OR
                        sd.blindness = 1
                    )', ['retinalExamDate' => $retinalExamDate->modify('-24 month')]);
    }

    /**
     * Nephropathy Assessment annually if not previously diagnosed
     */
    public function calculateNephropathy()
    {
        return $this->getNumerator($this->getDenominatorSQL() . ' AND
                    (
                       sd.nephropathyDiagnosis = 1
                    OR
                       sd.nephropathyEvidence = 1
                    OR
                       (sd.microalbuminuriaDate BETWEEN :fromDate AND :toDate)
                    OR
                       (sd.macroalbuminuriaDate BETWEEN :fromDate AND :toDate)
                    )');
    }

    /**
     * ACEI/ARB Therapy
     * On ACEI/ARB if hypertension or nephropathy
     */
    public function calculateACEI()
    {
        if (!($denominator = $this->getDenominatorACEI())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLACEI() . ' AND
                    (
                         sd.aceiArbTherapy = 1
                    )');

        return [$numerator, $denominator];
    }

    /**
     * Body Mass Index/Weight/Nutrition Counseling
     */
    public function calculateBMI()
    {
        return $this->getNumerator($this->getDenominatorSQL() . ' AND
                    (
                        (
                        sd.bmiCalculation IS NOT NULL
                        AND sd.bmiCalculation <= 25
                        AND (sd.bmiDate BETWEEN :fromDate AND :toDate)
                        )
                    OR
                        (
                          sd.bmiCalculation > 25
                        AND sd.bmiCounseling = 1
                        AND (sd.bmiDate BETWEEN :fromDate AND :toDate)
                        AND (sd.bmiCounselingDate BETWEEN :fromDate AND :toDate)
                        )
                    )');
    }

    /**
     * Cardiovascular Risk Assessment
     * Assess 10 year ASCVD risk annually
     */
    public function calculateASCVD()
    {
        return $this->getNumerator($this->getDenominatorSQL() . ' AND
                    (
                        sd.ascvdAssessment = 1
                        AND sd.ascvdDate IS NOT NULL
                        AND (sd.ascvdDate BETWEEN :fromDate AND :toDate)
                    )');
    }

    /**
     * Give aspirin if 10-year risk >10%
     */
    public function calculateAspirinUse()
    {
        if (!($denominator = $this->getDenominatorAspirin())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLAspirin() . '
                                    AND (sd.aspirinTherapy = 1 OR
                                    sd.aspirinUseConsidered = 1 OR
                                    sd.aspirinTherapy = 3
                                    )'
            );
        return [$numerator, $denominator];
    }

    /**
     * Depression Screening Annually
     */
    public function calculateDepressionScreening()
    {
        return $this->getNumerator($this->getDenominatorSQL() . ' AND
                    (
                        (
                        sd.PHQ2screening = 1
                        AND sd.PHQ2screeningDate IS NOT NULL
                        AND (sd.PHQ2screeningDate BETWEEN :fromDate AND :toDate)
                        )
                    OR
                        (
                        sd.PHQ9screening = 1
                        AND sd.PHQ9screeningDate IS NOT NULL
                        AND (sd.PHQ9screeningDate BETWEEN :fromDate AND :toDate)
                        )
                    )');
    }


    protected function getDenominatorACEI()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLACEI());

        return $this->getSingleResultWithParams($qb);
    }

    protected function getDenominatorSQLACEI()
    {
        return $this->getDenominatorSQL('(sd.hypertensionDiagnosis = 1 OR sd.nephropathyDiagnosis = 1)');
    }


    protected function getDenominatorAspirin()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLAspirin());

        return $this->getSingleResultWithParams($qb);
    }

    protected function getDenominatorSQLAspirin()
    {
        return $this->getDenominatorSQL('(sd.ascvdDate BETWEEN :fromDate AND :toDate) AND (sd.ascvdAssessment = 1 AND sd.ascvdResults >= 10)', 40);
    }
}