<?php
namespace AppBundle\Service;
use AppBundle\Entity\SubmissionHypertension;

use AppBundle\Manager\LogManager;
use Doctrine\ORM\EntityManager;


class RecognitionHypertension extends AbstractRecognition
{
    public function __construct(EntityManager $em, LogManager $logManager)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(SubmissionHypertension::class);
        $this->logManager = $logManager;
    }

    /**
     * @param string $andSql
     * @param int $minAge
     * @param int $maxAge
     * @return string
     */
    public function getDenominatorSQL($andSql = '', $minAge = 18, $maxAge = 75)
    {
        $result = 'sd.submission_id = :submissionId
                    AND sd.npi = :npi
                    AND sd.hypertensionDiagnosis = 1
                    AND TIMESTAMPDIFF(YEAR, sd.patientDOB, sd.lastVisitDate) >= ' . $minAge . '
                    AND TIMESTAMPDIFF(YEAR, sd.patientDOB, sd.lastVisitDate) <= ' . $maxAge . '
                    AND sd.lastVisitDate IS NOT NULL
                    AND (sd.lastVisitDate BETWEEN  :fromDate AND :toDate)';

        if ($andSql) {
            $result = $result . ' AND ' . $andSql;
        }

        return $result;
    }

    /**
     * Denominator for
     * Blood Pressure Control in Patients < 60
     */
    private function getDenominatorBloodPressureUnderSixty()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLBloodPressureUnderSixty());

        return $this->getSingleResultWithParams($qb);
    }

    /**
     * Denominator for
     * Blood Pressure Control in Patients < 60
     */
    private function getDenominatorSQLBloodPressureUnderSixty()
    {
        return $this->getDenominatorSQL('', 18, 59);
    }

    /**
     * Blood Pressure Control in Patients < 60
     */
    public function calculateBloodPressureUnderSixty()
    {
        if (!($denominator = $this->getDenominatorBloodPressureUnderSixty())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLBloodPressureUnderSixty() . ' AND
                    (
                        (sd.bloodPressureDate2 BETWEEN  :fromDate AND :toDate)
                            AND sd.systolic2 IS NOT NULL
                            AND sd.systolic2 < 140
                            AND sd.diastolic2 IS NOT NULL
                            AND sd.diastolic2 < 90
                        
                    )');

        return [$numerator, $denominator];
    }

    /**
     * Denominator for
     * Blood Pressure Control in Patients > 60
     */
    private function getDenominatorBloodPressureOverSixty()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLBloodPressureOverSixty());

        return $this->getSingleResultWithParams($qb);
    }

    public function getDenominatorSQLBloodPressureOverSixty()
    {
        return $this->getDenominatorSQL('', 60, 75);
    }

    /**
     * Blood Pressure Control in Patients > 60
     */
    public function calculateBloodPressureOverSixty()
    {
        if (!($denominator = $this->getDenominatorBloodPressureOverSixty())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLBloodPressureOverSixty() . ' AND
                    (
                        (sd.bloodPressureDate2 BETWEEN  :fromDate AND :toDate)
                            AND sd.systolic2 IS NOT NULL
                            AND sd.systolic2 < 150
                            AND sd.diastolic2 IS NOT NULL
                            AND sd.diastolic2 < 90
                    )');
        return [$numerator, $denominator];
    }

    /**
     * Blood Pressure Measurement Twice Annually
     */
    public function calculateBloodPressureTwice()
    {
        return $this->getNumerator($this->getDenominatorSQL() . ' AND
                    (
                        (
                            (sd.bloodPressureDate1 BETWEEN  :fromDate AND :toDate)
                            AND sd.systolic1 IS NOT NULL
                            AND sd.diastolic1 IS NOT NULL
                        )
                    AND
                       (
                            (sd.bloodPressureDate2 BETWEEN  :fromDate AND :toDate)
                            AND sd.systolic2 IS NOT NULL
                            AND sd.diastolic2 IS NOT NULL
                            AND DATE_DIFF(sd.bloodPressureDate2, sd.bloodPressureDate1) >= 90
                        )
                    )');
    }

    /**
     * Denominator for
     * Blood Pressure Control in Patients with Chronic Kidney Disease
     */
    private function getDenominatorKidneyDisease()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLKidneyDisease());

        return $this->getSingleResultWithParams($qb);
    }

    public function getDenominatorSQLKidneyDisease()
    {
        return $this->getDenominatorSQL('sd.chronicKidneyDiseaseDiagnosis=1');
    }

    /**
     * Blood Pressure Control in Patients with Chronic Kidney Disease
     */
    public function calculateBloodPressureKidneyDisease()
    {
        if (!($denominator = $this->getDenominatorKidneyDisease())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLKidneyDisease() . '
                    AND
                    (
                            sd.systolic2 IS NOT NULL
                            AND sd.diastolic2 IS NOT NULL
                            AND (sd.bloodPressureDate2 BETWEEN  :fromDate AND :toDate)
                            AND sd.systolic2 < 140
                            AND sd.diastolic2 < 90
                    )');

        return [$numerator, $denominator];
    }

    /**
     * ACEI/ARB Treatment for Hypertensive Patients with CKD
     */
    public function calculateACEI()
    {
        if (!($denominator = $this->getDenominatorKidneyDisease())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLKidneyDisease() . '
                    AND
                    (
                        sd.aceiArbTherapy=1 OR sd.aceiArbTherapy=3
                    )');

        return [$numerator, $denominator];
    }

    /**
     * Denominator for
     * Blood Pressure Control in Patients with Diabetes
     */
    private function getDenominatorDiabetes()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLDiabetes());

       return $this->getSingleResultWithParams($qb);
    }

    public function getDenominatorSQLDiabetes()
    {
        return $this->getDenominatorSQL('sd.diabetesDiagnosis = 1');
    }

    /**
     * Blood Pressure Control in Patients with Diabetes
     */
    public function calculateBloodPressureDiabetes()
    {
        if (!($denominator = $this->getDenominatorDiabetes())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLDiabetes() . '
                    AND
                    (
                        sd.systolic2 < 140
                        AND sd.diastolic2 < 90
                        AND sd.bloodPressureDate2 IS NOT NULL
                        AND (sd.bloodPressureDate2 BETWEEN  :fromDate AND :toDate)
                        
                    )');
        return [$numerator, $denominator];
    }

    /**
     * Blood Pressure Treatment in Patients Requiring Pharmacotherapy
     */
    public function calculateBloodPressurePharmacotherapy()
    {
        return $this->getNumerator($this->getDenominatorSQL() . ' AND (
        sd.aceiArbTherapy=1 OR sd.thiazide=1 OR sd.calciumChannelBlocker=1 OR
        sd.aceiArbTherapy=3 OR sd.thiazide=3 OR sd.calciumChannelBlocker=3
        )');
    }

    /**
     * Renal Function Testing in Hypertensive Patients
     */
    public function calculateHypertensive()
    {
        return $this->getNumerator($this->getDenominatorSQL() . '
                     AND sd.creatinine=1
                     AND (sd.creatinineDate BETWEEN :fromDate AND :toDate)
                    ');
    }

    /**
     * Nephropathy Assessment Urine
     */
    public function calculateNephropathy()
    {
        return $this->getNumerator($this->getDenominatorSQL() . ' AND
                    (
                        sd.nephropathyDiagnosis=1
                        OR (sd.nephropathyScreeningDate BETWEEN :fromDate AND :toDate)
                    )');
    }

    /**
     * Tobacco Status
     */
    public function calculateTobacco()
    {
        return $this->getNumerator($this->getDenominatorSQL() . '
                        AND (sd.tobaccoStatusAssessmentDate BETWEEN :fromDate AND :toDate)
                     ');
    }


    /**
     * Denominator for tobacco
     */
    private function getDenominatorTobaccoCessation()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLTobaccoCessation());

        return $this->getSingleResultWithParams($qb);
    }

    public function getDenominatorSQLTobaccoCessation()
    {
        return $this->getDenominatorSQL('sd.tobaccoStatus = 1');
    }



    /**
     * Tobacco Cessation counseling if user – and Treatment
     */
    public function calculateTobaccoCessation()
    {
        if (!($denominator = $this->getDenominatorTobaccoCessation())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLTobaccoCessation() . '
                        AND (sd.tobaccoCessationAdviceOrTreatmentDate BETWEEN :fromDate AND :toDate)
                     ');
        return [$numerator, $denominator];
    }

    /**
     * Body Mass Index/Weight/Nutrition Counseling
     */
    public function calculateBMI()
    {
        return $this->getNumerator($this->getDenominatorSQL() . ' AND
                    (
                        sd.bmiValue IS NOT NULL
                        AND (sd.bmiValueDate BETWEEN :fromDate AND :toDate)
                    )');
    }

    /**
     * Nutrition Counseling for Low Salt (DASH) Diet
     */
    public function calculateNutritionDash()
    {
        return $this->getNumerator($this->getDenominatorSQL() . ' AND
                    (
                        (sd.DASHDietCounseling=1 AND (sd.DASHDietDate BETWEEN :fromDate AND :toDate)) AND
                        (sd.DASHSodiumCounseling=1 AND (sd.DASHSodiumDate BETWEEN :fromDate AND :toDate)) AND
                        (sd.activityStatus IS NOT NULL AND (sd.activityStatusDate BETWEEN :fromDate AND :toDate)) AND
                        (sd.activityCounseling=1 AND (sd.activityCounselingDate BETWEEN :fromDate AND :toDate))
                    )');
    }
}