<?php
namespace AppBundle\Service\CSV;
use AppBundle\Entity\SubmissionHeartFailure;
use AppBundle\Service\CSV\Field\{
    BooleanField, DateField, GenderField, HearFailureTypeField, NumericField, AlphanumericField,
    IndividualGroupField, TobaccoStatusField, ActivityStatusField, BirthdayDateField, LimitedDateField, RaceField
};
use AppBundle\Service\CSV\FieldRelation\BloodPressureDate;


class HeartFailure extends AbstractCSV
{
    static $cache = null;

    public function getEmptyEntity()
    {
        return new SubmissionHeartFailure();
    }

    /**
     * @return mixed
     */
    public function getProperties():array
    {
        if (self::$cache === null) {
            self::$cache = [
            'setResponsibleProviderID' => new AlphanumericField(true, true, 26),
            'setNpi' => new NumericField(true, true, 10, 10),
            'setGroupID' => new AlphanumericField(false, false, 50),
            'setIndividualGroup' => new IndividualGroupField(false, false),
            'setChartID' => new AlphanumericField(true, true, 50),
            'setLastVisitDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true, true),
            'setPatientDOB' => new BirthdayDateField($this->getSubmissionReportingEndDate(),true, true),
            'setPatientGender' => new GenderField(true, true),
            'setPatientRace' => new RaceField(true),
            'setMedicarePartB' => new BooleanField(true),

            'setHFDiagnosis' => new BooleanField(true,true),
            'setLVSDDiagnosis' => new BooleanField(true),
            'setLvfAssessDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setBetablockerTherapy' => new BooleanField(true),
            'setAceiArbTherapy' => new BooleanField(true),

            'setBloodPressureDate1' => new DateField(true),
            'setSystolic1' => new NumericField(true, false,false, false,  300, 60),
            'setDiastolic1' => new NumericField(true, false,false, false,  150, 40),
            'setBloodPressureDate2' => new DateField(true),
            'setSystolic2' => new NumericField(true, false,false, false,  300, 60),
            'setDiastolic2' => new NumericField(true, false,false, false,  150, 40),
            'setWeightMeasureDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setPeripheraledemaAssessment' => new BooleanField(true),
            'setOrthopneaAssessment' => new BooleanField(true),
            'setJVPAssessment' => new BooleanField(true),
            'setTobaccoStatus' => new TobaccoStatusField(true),
            'setTobaccoStatusAssessmentDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setTobaccoCessationAdviceOrTreatment' => new BooleanField(true),
            'setTobaccoCessationAdviceOrTreatmentDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),

            'setBmiValue' => new NumericField(true, false, false, false, 50, 15),
            'setBmiValueDate' =>  new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setNutritionCounseling' => new BooleanField(true),
            'setNutritionCounselingDate' =>  new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setActivityStatus' => new ActivityStatusField(true),
            'setActivityStatusDate' =>  new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setActivityCounseling' => new BooleanField(true),
            'setActivityCounselingDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setHeartfailureType' => new HearFailureTypeField(true),
            'setHeartfailureAssessedDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setHeartfailureSeverity' => new BooleanField(true),
            'setEsrdPatient' => new BooleanField(true),
            'setDialysisPatient' => new BooleanField(true),
            ];
        }

        return self::$cache;
    }

    /**
     * @return array
     */
    public function getFieldRelations(): array
    {
        return [
            'setBloodPressureDate1' => BloodPressureDate::class,
            'setBloodPressureDate2' => BloodPressureDate::class
        ];
    }
}