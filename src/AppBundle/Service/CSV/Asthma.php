<?php
namespace AppBundle\Service\CSV;
use AppBundle\Entity\SubmissionAsthma;
use AppBundle\Service\CSV\Field\{
    AlphanumericField, BooleanField, DiseaseSeverityField, ExacerbationSeverityField, GenderField,
    NumericField, RaceField, TobaccoStatusField, IndividualGroupField, BirthdayDateField, LimitedDateField
};


class Asthma extends AbstractCSV
{
    static $cache = null;

    public function getEmptyEntity()
    {
        return new SubmissionAsthma();
    }

    /**
     * @return mixed
     */
    public function getProperties():array
    {
        if (self::$cache === null) {
            self::$cache = [
            'setResponsibleProviderID' => new AlphanumericField(true, true, 26),
            'setNpi' => new NumericField(true, true, 10, 10),
            'setGroupID' => new AlphanumericField(false, false, 50),
            'setIndividualGroup' => new IndividualGroupField(false, false),
            'setChartID' => new AlphanumericField(true, true, 50),
            'setLastVisitDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true, true),
            'setPatientDOB' => new BirthdayDateField($this->getSubmissionReportingEndDate(),true, true, false, false, 5, 80),
            'setPatientGender' => new GenderField(true, true),
            'setPatientRace' => new RaceField(true),
            'setMedicarePartB' => new BooleanField(true),

            'setAsthmaDiagnosis'=> new BooleanField(true, true),
            'setAsthmaDiseaseSeverity' => new DiseaseSeverityField(true),
            'setAsthmaDiseaseSeverityDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setSpirometryEvalResults' => new NumericField(true),
            'setSpirometryEvalDate' => new LimitedDateField($this->getSubmissionReportingEndDate(), true),

            'setSABAPrescribedDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setInhaledCorticosteroidDispensed' => new BooleanField(true),
            'setInhaledCorticosteroidDispensedDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setInhaledCorticosteroidPrescribed' => new BooleanField(true),
            'setInhaledCorticosteroidPrescribedDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),

            'setOralCorticosteroidPrescribed' => new BooleanField(true),
            'setOralCorticosteroidPrescribedDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),

            'setInhaledBronchodilatorPrescribedDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setOralBeta3AgonistsDispensedDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setOralBeta3AgonistsPrescribedDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),

            'setAsthmaMedicationPrescribedDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setAsthmaExacerbationEpisode' => new BooleanField(true),
            'setAsthmaExacerbationSeverity' => new ExacerbationSeverityField(true),
            'setAsthmaExacerbationDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setAsthmaExacerbationReferralDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),

            'setFaceToFaceVisit1' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setFaceToFaceVisit2' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setAsthmaActionPlanDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setAsthmaSelfManagementPlanDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setInhalerEducationDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setTobaccoExposureStatus' => new BooleanField(true),
            'setTobaccoExposureStatusDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setTobaccoStatus' => new TobaccoStatusField(true),
            'setTobaccoStatusAssessmentDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setTobaccoCessationAdviceOrTreatmentDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setInfluenzaImmunization' => new BooleanField(true),
            'setInfluenzaImmunizationDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            ];
        }

        return self::$cache;
    }


    /**
     * @return array
     */
    public function getFieldRelations(): array
    {
        return [
        ];
    }
}