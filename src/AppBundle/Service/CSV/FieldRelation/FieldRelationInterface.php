<?php

namespace AppBundle\Service\CSV\FieldRelation;

interface FieldRelationInterface
{
    public function getResult();
}