<?php
namespace AppBundle\Service\CSV\FieldRelation;


class HbA1cDate implements FieldRelationInterface
{
    public $date1;
    public $date2;

    public function __construct($entity)
    {
        $this->date1 = $entity->getHbA1cDate1();
        $this->date2 = $entity->getHbA1cDate2();
    }

    public function getResult()
    {
        return ($this->date2 < $this->date1);
    }
}