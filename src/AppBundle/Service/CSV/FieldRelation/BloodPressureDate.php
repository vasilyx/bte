<?php
namespace AppBundle\Service\CSV\FieldRelation;


class BloodPressureDate implements FieldRelationInterface
{
    public $bloodPressureDate1;
    public $bloodPressureDate2;


    public function __construct($entity)
    {
        $this->bloodPressureDate1 = $entity->getBloodPressureDate1();
        $this->bloodPressureDate2 = $entity->getBloodPressureDate2();
    }

    public function getResult()
    {
        return ($this->bloodPressureDate2 < $this->bloodPressureDate1);
    }
}