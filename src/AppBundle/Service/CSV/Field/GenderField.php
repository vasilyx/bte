<?php
namespace AppBundle\Service\CSV\Field;


class GenderField extends AbstractEnumField
{
    protected $enumArray = [
        'male' => 0,
        'm' => 0,
        'female' => 1,
        'f' => 1,
        'u' => 2,
        'unknown' => 2
    ];
}