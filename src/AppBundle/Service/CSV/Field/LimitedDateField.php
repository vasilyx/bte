<?php
namespace AppBundle\Service\CSV\Field;


use AppBundle\Exception\CSVFieldFormatException;
use AppBundle\Exception\CSVFieldLimitedDateException;
use Symfony\Component\Validator\Constraints\Date;

class LimitedDateField extends AbstractField
{

    private $submissionReportingEndDate;

    public function __construct($submissionReportingEndDate, $required = false, $isCritical = false, $maxLength = false, $minLength = false)
    {
        $this->submissionReportingEndDate = $submissionReportingEndDate;
        parent::__construct($required, $isCritical, $maxLength, $minLength);
    }

    /**
     * @param $value
     * @return mixed
     * @throws CSVFieldFormatException
     * @throws CSVFieldLimitedDateException
     */
    protected function convertValue($value)
    {
        if (!$value) return null;

        $date_arr = explode('/', $value);

        if (count($date_arr) < 3) {
            throw new CSVFieldFormatException('Wrong format', $this->isCritical);
        }


        if (strlen($date_arr[0]) === 1) $date_arr[0] = '0' . $date_arr[0];
        if (strlen($date_arr[1]) === 1) $date_arr[1] = '0' . $date_arr[1];

        if (strlen($date_arr[2]) === 2) {
            if ($date_arr[2] < 25) {
                $date_arr[2] = '20' . $date_arr[2];
            } else {
                $date_arr[2] = '19' . $date_arr[2];
            }
        }

        $date = \DateTime::createFromFormat('m/d/Y', implode('/', $date_arr));

        if (!($date instanceof \DateTime)) {
            throw new CSVFieldFormatException('Wrong format', $this->isCritical);
        }

        if ($this->submissionReportingEndDate) {
            $currDate = clone $date;
            $currDate->sub(new \DateInterval('P1D'));
            if ($currDate > $this->submissionReportingEndDate) {
                throw new CSVFieldLimitedDateException('The date cannot be after the end of the reporting period.', $this->isCritical);
            }
        }

        unset($currDate);
        unset($date_arr);

        if ($date === false) {
            $date = null;
        }

        return $date;
    }
}