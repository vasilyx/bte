<?php
namespace AppBundle\Service\CSV\Field;


use AppBundle\Exception\CSVFieldFormatException;

class DeaField extends AbstractField
{
    /**
     * @return mixed
     */
    protected function convertValue($value)
    {
        if (!$value) return null;

        if(!ctype_alnum($value)){
            throw new CSVFieldFormatException('This field should be Alphanumeric.', $this->isCritical);
        }
        $firstSymbolsArray = array("A", "B", "F" ,"M");

        if(!in_array($value[0], $firstSymbolsArray)){
            throw new CSVFieldFormatException('First symbol in DEA must be "A", "B", "F" or "M".', $this->isCritical);
        }

        return  ctype_alnum($value)?$value:null;
    }
}