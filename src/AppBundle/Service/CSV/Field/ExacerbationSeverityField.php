<?php
namespace AppBundle\Service\CSV\Field;


class ExacerbationSeverityField extends AbstractEnumField
{
    /**
     * "Mild", "Moderate", "Severe", "Life Threatening"
     * @var array
     */
    protected $enumArray = [
        'mild' => 1,
        'moderate' => 2,
        'severe' => 3,
        'life threatening' => 4
    ];
}