<?php
namespace AppBundle\Service\CSV\Field;


class TobaccoStatusField extends AbstractEnumField
{
    protected $enumArray = [
        'tobacco free' => 0,
        'current user' => 1,
        'tobacco user' => 1,
        'current tobacco user' => 1,
        'light smoker' => 2,
        'moderate cigarette smoker' => 3,
        'moderate smoker' => 3,
        'heavy smoker' => 4,
        'very heavy smoker' => 5,
        'smoking reduced' => 6,
        'no documentation' => null,
        '' => null,
        'unknown' => null,
    ];
}