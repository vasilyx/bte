<?php
namespace AppBundle\Service\CSV\Field;


use AppBundle\Exception\CSVFieldFormatException;
use Symfony\Component\Validator\Constraints\Date;

class DateField extends AbstractField
{

    /**
     * @param $value
     * @return bool|\DateTime|null
     * @throws CSVFieldFormatException
     */
    protected function convertValue($value)
    {
        $date = $this->createDate($value);

        if(!($date instanceof \DateTime)){
            throw new CSVFieldFormatException('wrong format', $this->isCritical);
        }
        if ($date === false) {
            $date = null;
        }

        return $date;
    }

    /**
     * @param $value
     * @return bool|\DateTime|null
     */
    public function createDate($value) {
        if (!$value) return null;

        $date_arr = explode('/', $value);

        if (strlen($date_arr[0]) === 1) $date_arr[0] = '0' . $date_arr[0];
        if (strlen($date_arr[1]) === 1) $date_arr[1] = '0' . $date_arr[1];

        if (strlen($date_arr[2]) === 2) {
            if ($date_arr[2] < 25) {
                $date_arr[2] = '20' . $date_arr[2];
            } else {
                $date_arr[2] = '19' . $date_arr[2];
            }
        }

        return \DateTime::createFromFormat('m/d/Y', implode('/', $date_arr));
    }
}