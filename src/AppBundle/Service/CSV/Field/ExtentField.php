<?php
namespace AppBundle\Service\CSV\Field;


class ExtentField extends AbstractEnumField
{
    /**
     * "Less than 1/3 of colon", "Greater than 1/3 of colon"
     * @var array
     */
    protected $enumArray = [
        "less than 1/3 of colon" => 1,
        "greater than 1/3 of colon" => 2
    ];
}