<?php
namespace AppBundle\Service\CSV\Field;

use Doctrine\ORM\EntityManager;
use AppBundle\Exception\CSVFieldFormatException;

class DegreeField extends AbstractField
{
    private $em;

    public function __construct($em, $required = false, $isCritical = false, $maxLength = false, $minLength = false)
    {
        $this->em = $em;
        parent::__construct($required, $isCritical, $maxLength, $minLength);
    }

    /**
     * @return mixed
     */
    protected function convertValue($value)
    {
        if (!$value) return null;

        $id = (int)$value;

        $value = $this->em->getRepository(\AppBundle\Entity\ClinicianDegree::class)->find($id);

        if(!$value){
            throw new CSVFieldFormatException('Degree with id '.$id.' was not found.', $this->isCritical);
        }

        return  $value;
    }
}