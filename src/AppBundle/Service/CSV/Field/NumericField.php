<?php
namespace AppBundle\Service\CSV\Field;

use AppBundle\Exception\CSVFieldFormatException;
use AppBundle\Exception\CSVFieldLimitedNumericException;

class NumericField extends AbstractField
{
    private $min;
    private $max;
    public function __construct($required = false, $isCritical = false, $maxLength = false, $minLength = false, $maxValue = false, $minValue = false)
    {
        $this->max = $maxValue;
        $this->min = $minValue;
        parent::__construct($required, $isCritical, $maxLength, $minLength);
    }

    /**
     * @param $value
     * @return mixed
     * @throws CSVFieldFormatException
     * @throws CSVFieldLimitedNumericException
     */
    protected function convertValue($value)
    {

        if ($value === '') return null;
        $value = trim($value);
        if (!is_numeric($value)) {
            throw new CSVFieldFormatException('This field should be Numeric.', $this->isCritical);
        }
        $value = (float)$value;
        if ($this->max && $value > $this->max ) {
            throw new CSVFieldLimitedNumericException('Maximum value of the field is '. $this->max. '.', $this->isCritical);
        }
        if ($this->min && $value < $this->min ) {
            throw new CSVFieldLimitedNumericException('Minimum value of the field is '. $this->min. '.', $this->isCritical);
        }
        return is_numeric($value) ? (float)$value : null;
    }
}