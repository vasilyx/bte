<?php
namespace AppBundle\Service\CSV\Field;


class ManifestationField extends AbstractEnumField
{
    /**
     * "Arthritis", "Biliary", "Dermatologic", "None", "Ocular", "Thromboembolic"
     * @var array
     */
    protected $enumArray = [
        'none' => 0,
        'arthritis' => 1,
        'biliary' => 2,
        'dermatologic' => 3,
        'ocular' => 4,
        'thromboembolic' => 5
    ];
}