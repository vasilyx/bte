<?php

namespace AppBundle\Service\CSV\Field;


use AppBundle\Exception\CSVFieldFormatException;
use AppBundle\Exception\CSVFieldRequireException;

abstract class AbstractField
{
    protected $required;
    protected $maxLength;
    protected $minLength;
    protected $isCritical;

    public function __construct($required = false, $isCritical = false, $maxLength = false, $minLength = false)
    {
        $this->required = $required;
        $this->maxLength = $maxLength;
        $this->minLength = $minLength;
        $this->isCritical = $isCritical;
    }

    public function getValue($value)
    {
        $value = trim($value);

        if ($this->required == true && $value == '') {
            if($this->isCritical) {
                throw new CSVFieldRequireException('Field can not be null', $this->isCritical);
            }else{
                throw new CSVFieldRequireException('Field can not be null');
            }
        }

        if ($this->maxLength && strlen($value) > $this->maxLength ) {
            throw new CSVFieldFormatException('Maximum length of the field is '. $this->maxLength. ' symbols.', $this->isCritical);
        }
        if ($this->minLength && strlen($value) < $this->minLength ) {
            throw new CSVFieldFormatException('Minimum length of the field is '. $this->minLength. ' symbols.', $this->isCritical);
        }
        return $this->convertValue($value);
    }

    abstract protected function convertValue($value);
}