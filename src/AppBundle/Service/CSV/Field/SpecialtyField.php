<?php
namespace AppBundle\Service\CSV\Field;


use AppBundle\Exception\CSVFieldFormatException;

class SpecialtyField extends AbstractField
{

    private $em;

    public function __construct($em, $required = false, $isCritical = false, $maxLength = false, $minLength = false)
    {
        $this->em = $em;
        parent::__construct($required, $isCritical, $maxLength, $minLength);
    }

    /**
     * @return mixed
     */
    protected function convertValue($value)
    {
        if (!$value) return null;

        $id = (int)$value;

        $value = $this->em->getRepository(\AppBundle\Entity\ClinicianSpeciality::class)->find($id);

        if (!$value) {
            throw new CSVFieldFormatException('Specialty with id ' . $id . ' was not found.', $this->isCritical);
        }
        return  $value;
    }
}