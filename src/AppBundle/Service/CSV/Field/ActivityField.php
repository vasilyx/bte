<?php
namespace AppBundle\Service\CSV\Field;


class ActivityField extends AbstractEnumField
{
    /**
     * "Mild", "Moderate", "Quiescent", "Severe"
     * @var array
     */
    protected $enumArray = [
        'mild' => 1,
        'moderate' => 2,
        'quiescent' => 3,
        'severe' => 4
    ];
}