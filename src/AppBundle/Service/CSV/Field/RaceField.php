<?php
namespace AppBundle\Service\CSV\Field;

use AppBundle\Exception\CSVFieldFormatException;

class RaceField extends AbstractField
{
    protected function convertValue($value)
    {
        if (!$value) return null;

        $raceList = [
            'american indian or alaskan native',
            'asian',
            'black or african american',
            'native hawaiian or other pacific islander',
            'other race',
            'white',
            'declined to identify'
        ];
        $value = mb_strtolower(trim($value));
        if (!in_array($value, $raceList)) {
            throw new CSVFieldFormatException("The race is incorrect.", $this->isCritical);
        }

        return $value;
    }
}