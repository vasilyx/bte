<?php
namespace AppBundle\Service\CSV\Field;


use AppBundle\Exception\CSVFieldFormatException;

class AlphanumericField extends AbstractField
{
    /**
     * @param $value
     * @return mixed
     * @throws CSVFieldFormatException
     */
    protected function convertValue($value)
    {
        if (!$value) return null;

        $pattern = "/^[a-z0-9\- ]*$/i";
        if (!preg_match($pattern, (string)$value)) {
            throw new CSVFieldFormatException($value.'This field should be Alphanumeric.', $this->isCritical);
        }
        return  ctype_alnum($value)?$value:null;
    }
}