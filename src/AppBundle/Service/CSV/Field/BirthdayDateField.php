<?php
namespace AppBundle\Service\CSV\Field;


use AppBundle\Exception\CSVFieldFormatException;

class BirthdayDateField extends AbstractField
{
    /**
     * @var  \DateTime
     */
    public $lastVisitDate;
    public $minAge;
    public $maxAge;

    public function __construct($lastVisitDate = false, $required = false, $isCritical = false, $maxLength = false, $minLength = false, $minAge = 18, $maxAge = 75)
    {
        $this->lastVisitDate = $lastVisitDate;
        $this->minAge = $minAge;
        $this->maxAge = $maxAge;
        parent::__construct($required, $isCritical, $maxLength, $minLength);
    }

    /**
     * @param $value
     * @return mixed
     * @throws CSVFieldFormatException
     */
    protected function convertValue($value)
    {
        if (!$value) return null;

        $date_arr = explode('/', $value);

        if (strlen($date_arr[0]) === 1) $date_arr[0] = '0' . $date_arr[0];
        if (strlen($date_arr[1]) === 1) $date_arr[1] = '0' . $date_arr[1];

        if (strlen($date_arr[2]) === 2) {
            if ($date_arr[2] < 25) {
                $date_arr[2] = '20' . $date_arr[2];
            } else {
                $date_arr[2] = '19' . $date_arr[2];
            }
        }
        $date = \DateTime::createFromFormat('m/d/Y', implode('/', $date_arr));

        if(!($date instanceof \DateTime)){
            throw new CSVFieldFormatException('Wrong datetime format', $this->isCritical);
        }



        if($this->lastVisitDate && $this->lastVisitDate instanceof \DateTime) {
            $currDate = clone $this->lastVisitDate;
            $currDate->modify('-1 year');
        }else{
            $currDate = new \DateTime();
        }
        $age = $date ->diff($currDate)->y;
        if($age > $this->maxAge || $age < $this->minAge){
            throw new CSVFieldFormatException("It's not real age ($age)", $this->isCritical);
        }
        if ($date === false) {
            $date = null;
        }

        return $date;
    }
}