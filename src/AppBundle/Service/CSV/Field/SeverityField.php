<?php
namespace AppBundle\Service\CSV\Field;


class SeverityField extends AbstractEnumField
{
    protected $enumArray = [
        'mild' => 1,
        'severe' => 2,
        'moderate' => 3,
        'life threatening' => 4,
        'intermittent' => 5,
        'no documentation' => null,
        '' => null,
    ];
}