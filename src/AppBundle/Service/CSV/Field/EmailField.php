<?php
namespace AppBundle\Service\CSV\Field;

use AppBundle\Exception\CSVFieldFormatException;

class EmailField extends AbstractField
{
    /**
     * @return mixed
     */
    protected function convertValue($value)
    {
        if (!$value) return null;

        if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
            throw new CSVFieldFormatException('It is not valid email.', $this->isCritical);
        }
        return $value;
    }
}