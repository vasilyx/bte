<?php
namespace AppBundle\Service\CSV\Field;


class LocationField extends AbstractEnumField
{
    /**
     * "Upper GI", "Small Intestine", "Large Intestine"
     * @var array
     */
    protected $enumArray = [
        'upper gi' => 1,
        'small intestine' => 2,
        'large intestine' => 3
    ];
}