<?php
namespace AppBundle\Service\CSV\Field;


class BooleanField extends AbstractEnumField
{
    protected $enumArray = [
        'no' => 0,
        'yes' => 1,
        'ascvd risk assessed' => 1,
        'positive' => 1,
        'negative' => 0,
        'not active' => 0,
        'active' => 1,
        '1' => 1,
        'not known' => null,
        '' => null,
        'patient exclusion present' => 2, // CW578-344
        'no- documented allergy or contraindication' => 2,
        'documented allergy or contraindication' => 3,
        'contraindication or allergy' => 3,
        'patient risk less than 10' => 4
    ];
}