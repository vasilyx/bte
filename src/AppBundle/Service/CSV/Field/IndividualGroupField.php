<?php
namespace AppBundle\Service\CSV\Field;

use AppBundle\Exception\CSVFieldFormatException;

class IndividualGroupField extends AbstractField
{
    /**
     * @return mixed
     */
    protected function convertValue($value)
    {
        if (!$value) return null;

        $value = trim(mb_strtolower($value));
        if($value == 'g' || $value == 'i' ){
            if($value == 'g'){
                $value = 'G';
            }else{
                $value = 'I';
            }
        }else{
            throw new CSVFieldFormatException('In this field should be I or G.', $this->isCritical);
        }
        return $value;
    }
}