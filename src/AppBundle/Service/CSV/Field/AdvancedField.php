<?php
namespace AppBundle\Service\CSV\Field;


class AdvancedField extends AbstractEnumField
{
    protected $enumArray = [
        'active' => 1,
        'unavailable' => 2,
        'requested' => 3,
        'discussed' => 4,
        'not active' => 0,
        '' => null,
    ];
}