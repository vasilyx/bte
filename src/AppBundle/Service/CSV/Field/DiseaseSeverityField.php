<?php
namespace AppBundle\Service\CSV\Field;


class DiseaseSeverityField extends AbstractEnumField
{
    /**
     * "Intermittent", "Persistent Mild", "Persistent Moderate", "Persistent Severe"
     * @var array
     */
    protected $enumArray = [
        'intermittent' => 1,
        'persistent mild' => 2,
        'persistent moderate' => 3,
        'persistent severe' => 4,
    ];
}