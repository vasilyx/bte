<?php
namespace AppBundle\Service\CSV\Field;


class IBDTypeField extends AbstractEnumField
{
    protected $enumArray = [
        'crohn\'s disease' => 1,
        'indeterminate colitis' => 2,
        'ulcerative colitis' => 3
    ];
}