<?php
namespace AppBundle\Service\CSV\Field;

use AppBundle\Exception\CSVFieldFormatException;

class ZipCodeField extends AbstractField
{
    /**
     * @return mixed
     */
    protected function convertValue($value)
    {
        if (!$value) return null;

        $pattern = "/^\d{5}(?:\-?\d{4})?$/i";

        if (!preg_match($pattern, $value)) {
            throw new CSVFieldFormatException('It is not valid Zip Code.', $this->isCritical);
        }

        return $value;
    }
}