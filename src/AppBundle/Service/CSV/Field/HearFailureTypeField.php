<?php
namespace AppBundle\Service\CSV\Field;


class HearFailureTypeField extends AbstractEnumField
{
    /**
     * "Systolic", "Diastolic", "Persistent", "Preserved EF", "Reduced EF"
     * @var array
     */
    protected $enumArray = [
        'systolic' => 1,
        'diastolic' => 2,
        'persistent' => 3,
        'preserved ef' => 4,
        'reduced ef' => 5
    ];
}