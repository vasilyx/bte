<?php
namespace AppBundle\Service\CSV\Field;

use AppBundle\Exception\CSVFieldFormatException;

class ListField extends AbstractField
{
    private $options;

    public function __construct($options, $required = false, $isCritical = false, $maxLength = false, $minLength = false)
    {
        $this->options = $options;
        parent::__construct($required, $isCritical, $maxLength, $minLength);
    }

    protected function convertValue($value)
    {
        if (!$value) return null;

        $value = trim($value);
        if (!in_array($value, $this->options)) {
            throw new CSVFieldFormatException("The value is not included in the list.", $this->isCritical);
        }

        return $value;
    }
}