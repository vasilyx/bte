<?php
namespace AppBundle\Service\CSV\Field;


use AppBundle\Exception\CSVFieldFormatException;

abstract class AbstractEnumField extends AbstractField
{
    protected $enumArray = [
    ];

    /**
     * AbstractEnumField constructor.
     * @param bool|false $required
     * @param array $array
     */
    public function __construct($required = false, $isCritical = false, $maxLength = false, $minLength = false, $array = [])
    {
        $required = $required ?? false;

        if ($array) {
            $this->enumArray = $array;
        }

        parent::__construct($required, $isCritical, $maxLength, $minLength);
    }

    protected function convertValue($value)
    {
        $value = trim(mb_strtolower($value));

        if (!$value || $value == 'null') {
            return null;
        }

        if (is_array($this->enumArray) && $value && array_key_exists($value, $this->enumArray) === false) {
            throw new CSVFieldFormatException('wrong format', $this->isCritical);
        } else {
            return $this->enumArray[$value];
        }
    }
}