<?php
namespace AppBundle\Service\CSV\Field;


class ActivityStatusField extends AbstractEnumField
{
    protected $enumArray = [
        'active' => 1,
        'not active' => 0
    ];
}