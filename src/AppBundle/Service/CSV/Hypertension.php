<?php
namespace AppBundle\Service\CSV;
use AppBundle\Entity\SubmissionHypertension;
use AppBundle\Service\CSV\Field\{
    BooleanField, DateField, GenderField, NumericField, AlphanumericField, IndividualGroupField,
    TobaccoStatusField, ActivityStatusField, LimitedDateField, BirthdayDateField
};
use AppBundle\Service\CSV\FieldRelation\BloodPressureDate;


class Hypertension extends AbstractCSV
{
    static $cache = null;

    public function getEmptyEntity()
    {
        return new SubmissionHypertension();
    }

    /**
     * @return mixed
     */
    public function getProperties():array
    {
        if (self::$cache === null) {
            self::$cache = [
            'setResponsibleProviderID' => new AlphanumericField(true, true, 26),
            'setNpi' => new NumericField(true, true, 10, 10),
            'setGroupID' => new AlphanumericField(false, false, 50),
            'setIndividualGroup' => new IndividualGroupField(false, false),
            'setChartID' => new AlphanumericField(true, true, 50),
            'setLastVisitDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true, true),
            'setPatientDOB' => new BirthdayDateField($this->getSubmissionReportingEndDate(),true, true),
            'setPatientGender' => new GenderField(true,true),
            'setMedicarePartB' => new BooleanField(true),

            'setBloodPressureDate1' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setSystolic1' => new NumericField(true, false, false, false, 300, 60),
            'setDiastolic1' => new NumericField(true, false, false, false, 150, 40),
            'setBloodPressureDate2' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setSystolic2' => new NumericField(true, false, false, false, 300, 60),
            'setDiastolic2' => new NumericField(true, false,false, false,  150, 40),
            'setHypertensionDiagnosis' => new BooleanField(true, true),
            'setChronicKidneyDiseaseDiagnosis' => new BooleanField(true),
            'setDiabetesDiagnosis' => new BooleanField(true),
            'setAceiArbTherapy' => new BooleanField(true),
            'setCalciumChannelBlocker' => new BooleanField(true),
            'setThiazide' => new BooleanField(true),
            'setNephropathyDiagnosis' => new BooleanField(true),
            'setNephropathyScreening' => new BooleanField(true),
            'setNephropathyScreeningDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setEsrdPatient' => new BooleanField(true),
            'setDialysisPatient' => new BooleanField(true),
            'setCreatinine' => new BooleanField(true),
            'setCreatinineDate' => new DateField(true),
            'setTobaccoStatus' => new TobaccoStatusField(true),
            'setTobaccoStatusAssessmentDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setTobaccoCessationAdviceOrTreatmentDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setBmiValue' => new NumericField(true),

            'setBmiValueDate' => new DateField(true),
            'setDASHDietCounseling' => new BooleanField(true),
            'setDASHDietDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setDASHSodiumCounseling' => new BooleanField(true),
            'setDASHSodiumDate' =>  new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setActivityStatus' => new ActivityStatusField(true),
            'setActivityStatusDate' =>  new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setActivityCounseling' => new BooleanField(true),
            'setActivityCounselingDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            ];
        }

        return self::$cache;
    }

    /**
     * @return array
     */
    public function getFieldRelations(): array
    {
        return [
            'setBloodPressureDate1' => BloodPressureDate::class,
            'setBloodPressureDate2' => BloodPressureDate::class
        ];
    }
}