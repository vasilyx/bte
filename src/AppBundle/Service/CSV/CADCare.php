<?php
namespace AppBundle\Service\CSV;
use AppBundle\Entity\SubmissionCADCare;
use AppBundle\Service\CSV\Field\{
    BooleanField, DateField, GenderField, NumericField, IndividualGroupField, LimitedDateField,
    AlphanumericField, TobaccoStatusField, BirthdayDateField
};
use AppBundle\Service\CSV\FieldRelation\BloodPressureDate;


class CADCare extends AbstractCSV
{
    static $cache = null;

    public function getEmptyEntity()
    {
        return new SubmissionCADCare();
    }

    /**
     * @return mixed
     */
    public function getProperties():array
    {
        if (self::$cache === null) {
            self::$cache = [
            'setResponsibleProviderID' => new AlphanumericField(true, true, 26),
            'setNpi' => new NumericField(true, true, 10, 10),
            'setGroupID' => new AlphanumericField(false, false, 50),
            'setIndividualGroup' => new IndividualGroupField(false, false),
            'setChartID' => new AlphanumericField(true, true, 50),
            'setLastVisitDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true, true),
            'setPatientDOB' => new BirthdayDateField($this->getSubmissionReportingEndDate(),true, true),
            'setPatientGender' => new GenderField(true, true),
            'setMedicarePartB' => new BooleanField(true),

            'setCadDiagnosis' => new BooleanField(true, true),
            'setSystolic1' => new NumericField(true, false, false,false, 300, 60),
            'setDiastolic1' => new NumericField(true, false, false,false, 150, 40),
            'setBloodPressureDate1' => new DateField(true),
            'setSystolic2' => new NumericField(true, false,false,false, 300, 60),
            'setDiastolic2' => new NumericField(true, false,false,false, 150, 40),
            'setBloodPressureDate2' => new DateField(true),
            'setStatinStatus' => new BooleanField(true),
            'setAntiPlatelets' => new BooleanField(true),
            'setBetaBlockers' => new BooleanField(true),
            'setAceiArbTherapy' => new BooleanField(true),
            'setEsrdPatient' => new BooleanField(true),
            'setDialysisPatient' => new BooleanField(true),
            'setDiabetesDiagnosis' => new BooleanField(true),
            'setHypertensionDiagnosis' => new BooleanField(true),

            'setChronicKidneyDiseaseDiagnosis' => new BooleanField(true),
            'setChfDiagnosis' => new BooleanField(true),
            'setTobaccoStatus' => new TobaccoStatusField(true),
            'setTobaccoStatusAssessmentDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setTobaccoCessationAdviceOrTreatment' => new BooleanField(true),
            'setTobaccoCessationAdviceOrTreatmentDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setBmiValue' => new NumericField(true),

            'setBmiValueDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setNutritionCounseling' => new BooleanField(true),
            'setNutritionCounselingDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setActivityCounseling' => new BooleanField(true),
            'setActivityCounselingDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setPHQ2screening' => new BooleanField(true),
            'setPHQ2screeningDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setPHQ9screening' => new BooleanField(true),
            'setPHQ9screeningDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setInfluenzaImmunization' => new BooleanField(true),
            'setInfluenzaImmunizationDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            ];
        }

        return self::$cache;
    }

    /**
     * @return array
     */
    public function getFieldRelations(): array
    {
        return [
            'setBloodPressureDate1' => BloodPressureDate::class,
            'setBloodPressureDate2' => BloodPressureDate::class
        ];
    }

}