<?php

namespace AppBundle\Service\CSV;
use AppBundle\Entity\SubmissionDiabete;
use AppBundle\Service\CSV\Field\{
    BooleanField, AlphanumericField, TobaccoStatusField, NumericField, IndividualGroupField,
    LimitedDateField, BirthdayDateField, RaceField
};
use AppBundle\Service\CSV\FieldRelation\BloodPressureDate;
use AppBundle\Service\CSV\FieldRelation\HbA1cDate;


class Diabete extends AbstractCSV
{
    static $cache = null;

    public function getEmptyEntity()
    {
        return new SubmissionDiabete();
    }

    /**
     * @return array
     */
    public function getProperties()
    {
        if (self::$cache === null) {
            self::$cache = [
                'setResponsibleProviderID' => new AlphanumericField(true, true, 26),
                'setNpi' => new NumericField(true, true, 10, 10),
                'setGroupID' => new AlphanumericField(false, false, 50),
                'setIndividualGroup' => new IndividualGroupField(false, false),
                'setChartID' => new AlphanumericField(true, true, 50),
                'setLastVisitDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true, true),
                'setPatientDOB' => new BirthdayDateField($this->getSubmissionReportingEndDate(),true, true),
                'setPatientRace' => new RaceField(true),
                'setMedicarePartB' => new BooleanField(true),
                'setDiabetesDiagnosis' => new BooleanField(true, true),
                'setHbA1c1' => new NumericField(true, false,false, false, 16, 4),
                'setHbA1cDate1' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
                'setHbA1c2' => new NumericField(true),
                'setHbA1cDate2' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
                'setLDLLevel' => new NumericField(true),
                'setLDLDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
                'setStatinStatus' => new BooleanField(true),
                'setSystolic1' => new NumericField(true, false, false, false, 300, 60),
                'setDiastolic1' => new NumericField(true, false,false, false,  150, 40),
                'setBloodPressureDate1' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
                'setSystolic2' => new NumericField(true, false, false, false, 300, 60),
                'setDiastolic2' => new NumericField(true, false, false, false, 150, 40),
                'setBloodPressureDate2' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
                'setTobaccoStatus' => new TobaccoStatusField(true),
                'setTobaccoStatusAssessmentDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
                'setTobaccoCessationAdviceOrTreatmentDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
                'setFootExam' => new BooleanField(true),
                'setFootExamDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
                'setBilateralAmputation' => new BooleanField(true),
                'setRetinopathy' => new BooleanField(true),
                'setRetinalExamDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
                'setBlindness' => new BooleanField(true),
                'setNephropathyDiagnosis' => new BooleanField(true),
                'setNephropathyEvidence' => new BooleanField(true),
                'setMicroalbuminuriaLab' => new BooleanField(true),
                'setMicroalbuminuriaDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
                'setMacroalbuminuriaLab' => new BooleanField(true),
                'setMacroalbuminuriaDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
                'setHypertensionDiagnosis' => new BooleanField(true),
                'setAceiArbTherapy' => new BooleanField(true),
                'setBmiCalculation' => new NumericField(true),
                'setBmiDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
                'setBmiCounseling' => new BooleanField(true),
                'setBmiCounselingDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
                'setAscvdAssessment' => new BooleanField(true),
                'setAscvdResults' => new NumericField(true),
                'setAscvdDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
                'setAspirinTherapy' => new BooleanField(true),
                'setAspirinUseConsidered' => new BooleanField(true),
                'setPHQ2screening' => new BooleanField(true),
                'setPHQ2screeningDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
                'setPHQ9screening' => new BooleanField(true),
                'setPHQ9screeningDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            ];
        }

        return self::$cache;
    }

    /**
     * @return array
     */
    public function getFieldRelations(): array
    {
        return [
            'setBloodPressureDate1' => BloodPressureDate::class,
            'setBloodPressureDate2' => BloodPressureDate::class,
            'setHbA1cDate1' => HbA1cDate::class,
            'setHbA1cDate2' => HbA1cDate::class
        ];
    }

}