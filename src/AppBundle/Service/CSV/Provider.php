<?php
namespace AppBundle\Service\CSV;

use Doctrine\ORM\EntityManager;
use AppBundle\Service\CSV\Field\{
    BooleanField, GenderField, IndividualGroupField, NumericField, AlphanumericField, AlphaField, DegreeField, StringField, ZipCodeField, EmailField, BirthdayDateField, DeaField, SpecialtyField
};


class Provider extends AbstractCSV
{
    protected $em;
    protected $npi = [];
    protected $responsibleProviderID = [];


    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getEmptyEntity()
    {
        return new \AppBundle\Entity\Provider();
    }

    public function getFieldRelations()
    {
        return [];
    }

    /**
     * @return mixed
     */
    public function getProperties():array
    {
        return [
            'responsibleProviderID' => new AlphanumericField(true, true, 26),
            'npi' => new NumericField(true, true, 10, 10),
            'dea' => new DeaField(true, false, 9, 9),
            'license' => new AlphanumericField(false, true, 10),
            'lastname' => new AlphaField(true, true, 50),
            'firstname' => new AlphaField(true, true, 50),
            'middlename' => new AlphaField(false, true, 50),
            'degree' => new DegreeField($this->em,true, true),
            'practiceAddress1' => new StringField(true, true, 100),
            'practiceAddress2' => new StringField(false, false, 100),
            'practiceCity' => new AlphanumericField(true, true, 100),
            'practiceState'  => new AlphaField(true, true, 2, 2),
            'practiceZipCode'  => new ZipCodeField(false, true),
            'emailaddress' => new EmailField(false, true),
            'practicePhone' => new StringField(false, true, 30),
            'dateBirth' => new BirthdayDateField(false,false, true),
            'gender' => new GenderField(false, true),
            'speciality' => new SpecialtyField($this->em,false, true),
            'practiceID' => new AlphanumericField(true, true, 26),
            'practiceName' => new StringField(true, true, 100),
            'individualGroup' => new IndividualGroupField(true, true),
            'CCHITCertified' => new BooleanField(true, false),
            'fullPatientPanel' => new BooleanField(true, false),
        ];
    }

    /**
     * @return array
     */
    public function getNpi(): array
    {
        return $this->npi;
    }

    /**
     * @param array $npi
     */
    public function setNpi(array $npi)
    {
        $this->npi = $npi;
    }

    /**
     * @return array
     */
    public function getResponsibleProviderID(): array
    {
        return $this->responsibleProviderID;
    }

    /**
     * @param array $responsibleProviderID
     */
    public function setResponsibleProviderID(array $responsibleProviderID)
    {
        $this->responsibleProviderID = $responsibleProviderID;
    }

    /**
     * @param $array
     */
    public function fillValidationArray($array)
    {
        $npi_list = [];
        $responsible_list = [];

        foreach ($array as $line_number => $csv_record) {

            if ($line_number == 0) continue;

            $responsibleProviderID = $csv_record[0];
            $npi = $csv_record[1];

            if (isset($npi_list[$npi])) {
                $npi_list[$npi] = $npi_list[$npi] + 1;
            } else {
                $npi_list[$npi] = 1;
            }

            if (isset($responsible_list[$responsibleProviderID])) {
                $responsible_list[$responsibleProviderID] = $responsible_list[$responsibleProviderID] + 1;
            } else {
                $responsible_list[$responsibleProviderID] = 1;
            }
        }

        $this->setNpi($npi_list);
        $this->setResponsibleProviderID($responsible_list);
    }


    /**
     * @param $npi
     * @return bool
     */
    public function checkNpi($npi)
    {
        return isset($this->getNpi()[$npi]) && $this->getNpi()[$npi] < 2 ? false : true;
    }

    public function checkResponsibleProviderID($responsibleProviderID)
    {
        return isset($this->getResponsibleProviderID()[$responsibleProviderID]) && $this->getResponsibleProviderID()[$responsibleProviderID] < 2 ? false : true;
    }
}