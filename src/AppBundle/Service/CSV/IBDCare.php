<?php
namespace AppBundle\Service\CSV;
use AppBundle\Entity\SubmissionIBDCare;
use AppBundle\Service\CSV\Field\{
    ActivityField, BooleanField, ExtentField, GenderField, IBDTypeField,
    LocationField, ManifestationField, NumericField, TobaccoStatusField,
    AlphanumericField, RaceField, IndividualGroupField, BirthdayDateField, LimitedDateField
};


class IBDCare extends AbstractCSV
{
    static $cache = null;

    public function getEmptyEntity()
    {
        return new SubmissionIBDCare();
    }

    /**
     * @return mixed
     */
    public function getProperties():array
    {
        if (self::$cache === null) {
            self::$cache = [
            'setResponsibleProviderID' => new AlphanumericField(true, true, 26),
            'setNpi' => new NumericField(true, true, 10, 10),
            'setGroupID' => new AlphanumericField(false, false, 50),
            'setIndividualGroup' => new IndividualGroupField(false, false),
            'setChartID' => new AlphanumericField(true, true, 50),
            'setLastVisitDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true, true),
            'setPatientDOB' => new BirthdayDateField($this->getSubmissionReportingEndDate(),true, true), // IBD only
            'setPatientGender' => new GenderField(true,true),
            'setPatientRace' => new RaceField(true),
            'setMedicarePartB' => new BooleanField(true),

            'setIBDDiagnosis'=> new BooleanField(true, true),
            'setIBDUnderCareProvider' => new BooleanField(true),
            'setIBDDiagnosisOneYear' => new BooleanField(true),
            'setIBDDiagnosisEightYear' => new BooleanField(true),
            'setIBDType' => new IBDTypeField(true),
            'setIBDTypeDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setIBDLocation' => new LocationField(true),
            'setIBDExtent' => new ExtentField(true),
            'setIBDExtentDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),

            'setIBDActivity' => new ActivityField(true),
            'setIBDActivityDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setExternalManifestations' => new ManifestationField(true),
            'setExternalMainfestationsDateAssessed' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setCorticosteroid10Mg60Days' => new BooleanField(true),
            'setCorticosteroidSparingTherapy' => new BooleanField(true),
            'setBoneLossRiskAssessed' => new BooleanField(true),
            'setBoneLossRiskDateAssessed' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setOsteoporosisTherapy' => new BooleanField(true),
            'setOsteoporosisTherapyDateAssessed' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),

            'setTuberculosisScreening' => new BooleanField(true),
            'setTuberculosisScreeningDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setPositiveTBReatedTB' => new BooleanField(true),
            'setFirstCourseAntiTNFTherapy' => new BooleanField(true),
            'setAntiTNFInitiationDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setHepatitisBStatus' => new BooleanField(true),
            'setHepatitisBStatusDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setInfluenzaImmunization' => new BooleanField(true),
            'setInfluenzaImmunizationDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),

            'setPneumococcalVaccine' => new BooleanField(true),
            'setPneumococccalVaccineDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setTobaccoStatus' => new TobaccoStatusField(true),
            'setTobaccoStatusAssessmentDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setTobaccoCessationAdviceOrTreatmentDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setSurveillanceColonoscopy' => new BooleanField(true),
            'setSurveillanceColonoscopyDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setUlcerativeColitisDiagnosis' => new BooleanField(true),
            'setCrohnColitisDiagnosis' => new BooleanField(true),
            'setPHQ2screening' => new BooleanField(true),
            'setPHQ2screeningDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),

            'setPHQ9screening' => new BooleanField(true),
            'setPHQ9screeningDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setIBDBiologics' => new BooleanField(true),
            'setIBDBiologicsDateAssessed' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            ];
        }

        return self::$cache;
    }

    /**
     * @return array
     */
    public function getFieldRelations(): array
    {
        return [
        ];
    }
}