<?php
namespace AppBundle\Service\CSV;
use AppBundle\Entity\SubmissionDepression;
use AppBundle\Service\CSV\Field\{
    BooleanField, AlphanumericField, GenderField, NumericField, BirthdayDateField,
    IndividualGroupField, LimitedDateField, RaceField, ActivityStatusField
};


class Depression extends AbstractCSV
{
    static $cache = null;

    public function getEmptyEntity()
    {
        return new SubmissionDepression();
    }

    /**
     * @return mixed
     */
    public function getProperties():array
    {
        if (self::$cache === null) {
            self::$cache = [
                'setResponsibleProviderID' => new AlphanumericField(true, true, 26),
                'setNpi' => new NumericField(true, true, 10, 10),
                'setGroupID' => new AlphanumericField(false, false, 50),
                'setIndividualGroup' => new IndividualGroupField(false, false),
                'setChartID' => new AlphanumericField(true, true, 50),
                'setLastVisitDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true, true),
                'setPatientDOB' => new BirthdayDateField($this->getSubmissionReportingEndDate(),true, true, false, false, 12),
                'setPatientGender' => new GenderField(true, true),
                'setPatientRace' => new RaceField(true),
                'setMedicarePartB' => new BooleanField(true),

                'setPHQ2ScreeningScore' => new NumericField(true, false,false, false,  6),
                'setPHQ2ScreeningDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
                'setPHQ9ScreeningScore' => new NumericField(true, false,false, false,  27),
                'setPHQ9ScreeningDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
                'setPHQ9Screening2Score' => new NumericField(true, false,false, false,  27),
                'setPHQ9Screening2Date' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),

                'setPHQAdolescentScreeningScore' => new NumericField(true, false,false, false,  27),
                'setPHQAdolescentScreeningDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
                'setCognitiveBehaviorTherapyDate' =>  new LimitedDateField($this->getSubmissionReportingEndDate(),true),
                'setSpecialistReferralDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
                'setPsychotherapyDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),

                'setAntidepressantActiveDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
                'setAntidepressantInitialPrescribedDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
                'setDepressionDiagnosis' => new BooleanField(true),
                'setDysthymiaDiagnosis' => new BooleanField(true),
                'setSubstanceUseScreening' => new BooleanField(true),
                'setSubstanceUseScreeningDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
                'setSubstanceUseCounselingDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),

                'setActivityCounseling' => new BooleanField(true),
                'setActivityCounselingDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
                'setCareCoordination' => new BooleanField(true),
                'setMonitoringandTreatment' => new BooleanField(true),
                'setCaseLoadReviews' => new BooleanField(true)
            ];
        }

        return self::$cache;
    }

    /**
     * @return array
     */
    public function getFieldRelations(): array
    {
        return [
        ];
    }
}