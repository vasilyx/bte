<?php
namespace AppBundle\Service\CSV;
use AppBundle\Entity\SubmissionCOPD;
use AppBundle\Service\CSV\Field\{
    AdvancedField, BooleanField, AlphanumericField, GenderField, NumericField, IndividualGroupField, TobaccoStatusField,
    BirthdayDateField, LimitedDateField, RaceField
};


class COPD extends AbstractCSV
{
    static $cache = null;

    public function getEmptyEntity()
    {
        return new SubmissionCOPD();
    }

    /**
     * @return mixed
     */
    public function getProperties():array
    {
        if (self::$cache === null) {
            self::$cache = [
            'setResponsibleProviderID' => new AlphanumericField(true, true, 26),
            'setNpi' => new NumericField(true, true, 10, 10),
            'setGroupID' => new AlphanumericField(false, false, 50),
            'setIndividualGroup' => new IndividualGroupField(false, false),
            'setChartID' => new AlphanumericField(true, true, 50),
            'setLastVisitDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true, true),
            'setPatientDOB' => new BirthdayDateField($this->getSubmissionReportingEndDate(),true, true),
            'setPatientGender' => new GenderField(true, true),
            'setPatientRace' => new RaceField(true),
            'setMedicarePartB' => new BooleanField(true),

            'setCOPDDiagnosis' => new BooleanField(true, true),
            'setCOPDSymptoms' => new BooleanField(true),
            'setSpirometryEvalResults' => new NumericField(true, false, false, false, 100),
            'setSpirometryEvalDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setInhaledBronchodilatorPrescribedDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setInhaledCorticosteroidDispensedDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setInhaledCorticosteroidPrescribedDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setTobaccoStatus' => new TobaccoStatusField(true),
            'setTobaccoStatusAssessmentDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),

            'setTobaccoCessationAdviceOrTreatmentDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setRespiratoryFailureDiagnosis'=> new BooleanField(true),
            'setRightHeartFailureDiagnosis'=> new BooleanField(true),
            'setOxygenSaturationLevel' => new NumericField(true),
            'setOxygenSaturationLevelDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setPaO2Level' => new NumericField(true),
            'setPaO2LevelDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setLongTermOxygenTherapyDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setInfluenzaImmunization' => new BooleanField(true),
            'setInfluenzaImmunizationDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setPneumococcalVaccinePCV13' => new BooleanField(true),

            'setPneumococcalVaccinePCV13Date' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setPneumococcalVaccinePCV23' => new BooleanField(true),
            'setPneumococcalVaccinePCV23Date' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setCOPDExacerbationEpisode' => new NumericField(true),
            'setCOPDExacerbationEpisodeDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setCOPDExacerbationEpisodeStateDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setFaceToFaceVisit' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setLowDoseCTDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true),
            'setLowDoseCTReferral' => new BooleanField(true),
            'setLowDoseCTDocumented' => new BooleanField(true),
            'setAdvancedDirective' => new AdvancedField(true)
            ];
        }

        return self::$cache;
    }

    /**
     * @return array
     */
    public function getFieldRelations(): array
    {
        return [
        ];
    }
}