<?php

namespace AppBundle\Service\CSV;

interface CSVInterface
{
    public function getProperties();
    public function getPropertiesCount();
    public function getPropertyNameByIndex($index);
    public function getPropertyObjectByIndex($index);
}