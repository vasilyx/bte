<?php

namespace AppBundle\Service\CSV;

use AppBundle\Exception\CSVFieldException;
use AppBundle\Exception\CSVFieldLimitedDateException;
use AppBundle\Exception\CSVFieldLimitedNumericException;

abstract class AbstractCSV implements CSVInterface
{
    protected $submissionReportingEndDate;
    protected $submissionReportingStartDate;

    /**
     * @return mixed
     */
    public function getSubmissionReportingStartDate()
    {
        return $this->submissionReportingStartDate;
    }

    /**
     * @param mixed $submissionReportingStartDate
     */
    public function setSubmissionReportingStartDate($submissionReportingStartDate)
    {
        $this->submissionReportingStartDate = $submissionReportingStartDate;
    }

    protected $lastVisitDate;

    protected $groupSubmission;

    protected $warningData;

    protected $chartList;

    protected $logger;

    protected $errors = 0;

    protected $warnings = 0;

    const STATUS_ERROR = 2;
    const STATUS_WARNING = 1;
    const STATUS_OK = 0;

    /**
     * @return int
     */
    public function getErrors(): int
    {
        return $this->errors;
    }

    /**
     * @param int $errors
     */
    public function setErrors(int $errors)
    {
        $this->errors = $errors;
    }

    /**
     * @return int
     */
    public function getWarnings(): int
    {
        return $this->warnings;
    }

    /**
     * @param int $warnings
     */
    public function setWarnings(int $warnings)
    {
        $this->warnings = $warnings;
    }

    /**
     * @return mixed
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * @param mixed $logger
     */
    public function setLogger($logger)
    {
        $this->logger = $logger;
    }

    /**
     * @return mixed
     */
    public function getWarningData()
    {
        return $this->warningData;
    }

    /**
     * @param mixed $warningData
     */
    public function setWarningData($warningData)
    {
        $this->warningData = $warningData;
    }

    /**
     * @return mixed
     */
    public function getChartList()
    {
        return $this->chartList;
    }

    /**
     * @param mixed $chartList
     */
    public function setChartList($chartList)
    {
        $this->chartList = $chartList;
    }


    abstract public function getEmptyEntity();
    abstract public function getFieldRelations();

    /**
     * @return mixed
     */
    public function getGroupSubmission()
    {
        return $this->groupSubmission;
    }

    /**
     * @param mixed $groupSubmission
     */
    public function setGroupSubmission($groupSubmission)
    {
        $this->groupSubmission = $groupSubmission;
    }
    /**
     * @return mixed
     */
    abstract public function getProperties();

    /**
     * @return mixed
     */
    public function getPropertiesCount()
    {
        return count($this->getProperties());
    }


    /**
     * @return mixed
     */
    public function setSubmissionReportingEndDate($submissionReportingEndDate)
    {
        $this->submissionReportingEndDate = $submissionReportingEndDate;
    }
    /**
     * @return mixed
     */
    public function getSubmissionReportingEndDate()
    {
        return $this->submissionReportingEndDate;
    }
    /**
     * @return mixed
     */
    public function setLastVisitDate($lastVisitDate)
    {
        $this->lastVisitDate = $lastVisitDate;
    }

    /**
     * @return mixed
     */
    public function getLastVisitDate()
    {
        return $this->lastVisitDate;
    }

    /**
     * @param $index
     * @return mixed
     */
    public function getPropertyNameByIndex($index)
    {
        $allKeys = array_keys($this->getProperties());

        return $allKeys[$index];
    }


    /**
     * @param $index
     * @return mixed
     */
    public function getPropertyObjectByIndex($index)
    {
        $array_values = array_values($this->getProperties());

        return $array_values[$index];
    }

    /**
     * @param $csv_record
     * @return mixed
     */
    public function generateEntity($csv_record)
    {
        $entity = $this->getEmptyEntity();
        $countWarning = $this->getWarnings();
        $countError = $this->getErrors();
        $dateField = new \AppBundle\Service\CSV\Field\DateField();

        foreach($csv_record as $k => $val) {

            $warning = false;
            $error = false;
            $wrong_data_format = false;

            if ($this->getPropertyNameByIndex($k)) {
                $propertyName = $this->getPropertyNameByIndex($k);

                $field = $this->getPropertyObjectByIndex($k);

                try {
                    $new_val = $field->getValue($val);
                } catch (CSVFieldLimitedDateException $e) {
                    if($e->getIsCritical()){
                        $error = true;
                    } else {
                        $warning = true;
                    }
                    $new_val = $dateField->createDate($val);
                } catch (CSVFieldLimitedNumericException $e) {
                    if($e->getIsCritical()){
                        $error = true;
                    } else {
                        $warning = true;
                    }
                    $new_val = (float)$val;
                } catch (CSVFieldException $e) {
                    if($e->getIsCritical()){
                        $error = true;
                    }else {
                        $warning = true;
                    }
                    $wrong_data_format = true;

                } catch (\Exception $e) {
                    $this->getLogger()->error($e->getMessage());
                }

                if (method_exists($entity, $propertyName))  {
                    if ($wrong_data_format === false) {
                        $entity->$propertyName($new_val);

                        if($propertyName == 'setLastVisitDate') {
                            $this->setLastVisitDate($new_val);
                        }
                    }

                } else {
                    $this->getLogger()->error('Property "' . $propertyName . '" is not exist.');
                }
            }

            $this->countError($error);
            $this->countWarning($warning);

            $return_data[$k] = [$val, self::STATUS_OK];


            if ($warning) {
                $return_data[$k] = [$val, self::STATUS_WARNING];
            }

            if ($error) {
                $return_data[$k] = [$val, self::STATUS_ERROR];
            }
        }

        $return_data = $this->checkEntityPostWarning($entity, $return_data);
        $return_data = $this->checkChartIdAndNpi($entity, $return_data);

        if (($countWarning < $this->getWarnings()) || ($countError < $this->getErrors())) {
           $this->setWarningData($return_data);
        }

        unset($return_data);

        return $entity;
    }

    public function fillChartId($array)
    {
        $chartList = [];

        foreach($array as $line) {

            $npi = $line[array_search('setNpi', array_keys($this->getProperties()))];
            $chartID = $line[array_search('setChartID', array_keys($this->getProperties()))];

            if (isset($chartList[$chartID])) {

                if (isset($chartList[$chartID][$npi])) {

                    $chartList[$chartID][$npi] = $chartList[$chartID][$npi] + 1;

                } else {
                    $chartList[$chartID][$npi] = 1;
                }

            } else {
                $chartList[$chartID][$npi] = 1;
            }
        }

        $this->setChartList($chartList);
    }

    /**
     * @param $entity
     * @param $warning_array
     * @return mixed
     */
    protected function checkChartIdAndNpi($entity, $warning_array)
    {
        $chartList = $this->getChartList();

        if (isset($chartList[$entity->getChartID()][$entity->getNpi()])
            && $chartList[$entity->getChartID()][$entity->getNpi()] > 1) {

            $k = array_search('setChartID', array_keys($this->getProperties()));
            $warning_array[$k][1] = self::STATUS_ERROR;


            $k = array_search('setNpi', array_keys($this->getProperties()));
            $warning_array[$k][1] = self::STATUS_ERROR;

            $this->countError(true);

            return $warning_array;
        }


        if (count($chartList[$entity->getChartID()]) == 1
            && $chartList[$entity->getChartID()][$entity->getNpi()] == 1) {

            return $warning_array;

        } else {
            $k = array_search('setChartID', array_keys($this->getProperties()));

            if ($warning_array[$k][1] !== self::STATUS_ERROR) {
                $warning_array[$k][1] = self::STATUS_WARNING;
            }

            $this->countWarning(true);

            return $warning_array;
        }
    }


    /**
     * @param $entity
     * @param $warning_array
     * @return mixed
     */
    protected function checkEntityPostWarning($entity, $warning_array)
    {
        $relations = $this->getFieldRelations();

        if (!$relations) return $warning_array;

        foreach ($relations as $f => $class) {

            $k = array_search($f, array_keys($this->getProperties()));
            $obj = new $class($entity);
            $warning = $obj->getResult();


            if ($warning_array[$k][1] !== self::STATUS_ERROR) {
                $warning_array[$k][1] = $warning ? self::STATUS_WARNING : self::STATUS_OK;
            }

            $this->countWarning($warning);

        }

        return $warning_array;
    }


    protected function countError($error)
    {
        if ($error) {
            $this->setErrors($this->getErrors()+1);
        }
    }

    protected function countWarning($warning)
    {
        if ($warning) {
            $this->setWarnings($this->getWarnings()+1);
        }
    }

}

