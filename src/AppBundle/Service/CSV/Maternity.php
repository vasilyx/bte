<?php
namespace AppBundle\Service\CSV;
use AppBundle\Entity\SubmissionAsthma;
use AppBundle\Entity\SubmissionMaternity;
use AppBundle\Service\CSV\Field\{
    AlphanumericField, BooleanField, DiseaseSeverityField, ExacerbationSeverityField, GenderField,
    NumericField, RaceField, TobaccoStatusField, IndividualGroupField, BirthdayDateField, LimitedDateField
};


class Maternity extends AbstractCSV
{
    static $cache = null;

    public function getEmptyEntity()
    {
        return new SubmissionMaternity();
    }

    /**
     * @return mixed
     */
    public function getProperties():array
    {
        if (self::$cache === null) {
            self::$cache = [
            'setResponsibleProviderID' => new AlphanumericField(true, true, 26),
            'setNpi' => new NumericField(true, true, 10, 10),
            'setGroupID' => new AlphanumericField(false, false, 50),
            'setIndividualGroup' => new IndividualGroupField(false, false),
            'setChartID' => new AlphanumericField(true, true, 50),
            'setDeliveryDate' => new LimitedDateField($this->getSubmissionReportingEndDate(),true, true),
            'setPatientDOB' => new BirthdayDateField($this->getSubmissionReportingEndDate(),true, true, false, false, 5, 80),
            'setPatientGender' => new GenderField(true, true),
            'setPatientRace' => new RaceField(true),
            'setMedicarePartB' => new BooleanField(true),

            'setLmpDate' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setDueDate' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setInitialVisitDate' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setTri2VisitDate1' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setTri2VisitDate2' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setTri2VisitDate3' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setTri2VisitDate4' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setTri2VisitDate5' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setTri2VisitDate6' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setTri3VisitDate1' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setTri3VisitDate2' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setTri3VisitDate3' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setTri3VisitDate4' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setTri3VisitDate5' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setTri3VisitDate6' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setTri3VisitDate7' => new LimitedDateField($this->getSubmissionReportingEndDate()),

            'setPostpartumVisitDate' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setAboRhScreenDate' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setCbcDate1' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setVdrlScreenDate' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setChlamydiaScreenDate' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setGonorrheaScreenDate' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setHivScreenDate' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setUrineCultureSensitivityDate' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setHbA1Cdate' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setRubellaScreenDate' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setFetalAneuploidyScreenDate' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setAfpScreenDate' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setQuadScreenDate' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setCbcDate2' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setGlucoseScreenDate' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setAntibodiesScreenDate' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setRhoGAMScreenDate' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setGroupBStrepScreenDate' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setCysticFibrosisScreeningDate' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setSpinalMuscularAtrophyDate' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setHemoglobinopathiesDate' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setPanEthicExpandedCarrierScreenDate' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setGenticCarrierScreen' => new BooleanField(),
            'setInfluenzaDate' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setTetanusDate' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setDiphtheriaDate' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setPertussisTdapDate' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setHxPreeclampsia' =>  new BooleanField(),
            'setHxPretermdelivery' => new NumericField(),
            'setLowDoseAspirinDate' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setUltrasoundDate' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setGroupBStrep' =>  new BooleanField(),
            'setAntibioticProphylaxisDate' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setCorticoSteroidDate' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setHxCSection' => new BooleanField(),
            'setBirthPlanDate' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setDecisionMakingConsultDate' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setNtsv' => new BooleanField(),
            'setElectivecsectionDate' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setInfantWeight' =>  new NumericField(),
            'setNICUlevel' =>  new NumericField(),
            'setPostPartumDepressionScreenDate' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setDrugAlcoholScreenDate' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            'setInterpersonalViolenceScreenDate' => new LimitedDateField($this->getSubmissionReportingEndDate()),
            ];
        }

        return self::$cache;
    }


    /**
     * @return array
     */
    public function getFieldRelations(): array
    {
        return [
        ];
    }
}