<?php
namespace AppBundle\Service;

use AppBundle\Entity\SubmissionCADCare;
use AppBundle\Manager\LogManager;
use Doctrine\ORM\EntityManager;


class RecognitionCADCare extends AbstractRecognition
{

    public function __construct(EntityManager $em, LogManager $logManager)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(SubmissionCADCare::class);
        $this->logManager = $logManager;
    }


    /**
     * @param string $andSql
     * @param int $minAge
     * @param int $maxAge
     * @return string
     */
    public function getDenominatorSQL($andSql = '', $minAge = 18, $maxAge = 75)
    {
        $result = 'sd.submission_id = :submissionId
                    AND sd.npi = :npi
                    AND sd.cadDiagnosis = 1
                    AND TIMESTAMPDIFF(YEAR, sd.patientDOB, sd.lastVisitDate) >= ' . $minAge . '
                    AND TIMESTAMPDIFF(YEAR, sd.patientDOB, sd.lastVisitDate) <= ' . $maxAge . '
                    AND sd.lastVisitDate IS NOT NULL
                    AND (sd.lastVisitDate BETWEEN  :fromDate AND :toDate)';

        if ($andSql) {
            $result = $result . ' AND ' . $andSql;
        }

        return $result;
    }

    /**
     * Blood Pressure Control
     */
    public function calculateBloodPressure()
    {
        return $this->getNumerator($this->getDenominatorSQL() . ' AND
                    (
                        (sd.bloodPressureDate2 BETWEEN  :fromDate AND :toDate)
                            AND sd.systolic2 IS NOT NULL
                            AND sd.systolic2 < 140
                            AND sd.diastolic2 IS NOT NULL
                            AND sd.diastolic2 < 90
                    )');
    }

    /**
     * Blood Pressure Measurement Twice Annually
     */
    public function calculateBloodPressureTwice()
    {
        return $this->getNumerator($this->getDenominatorSQL() . ' AND
                    (
                        (
                            (sd.bloodPressureDate1 BETWEEN  :fromDate AND :toDate)
                            AND sd.systolic1 IS NOT NULL
                            AND sd.diastolic1 IS NOT NULL
                        )
                    AND
                       (
                            sd.bloodPressureDate2 IS NOT NULL
                            AND (sd.bloodPressureDate2 BETWEEN  :fromDate AND :toDate)
                            AND sd.systolic2 IS NOT NULL
                            AND sd.diastolic2 IS NOT NULL
                            AND DATE_DIFF(sd.bloodPressureDate2, sd.bloodPressureDate1) >= 90
                        )
                    )');
    }

    /**
     * High Intensity Statin Therapy
     */
    public function calculateStatin()
    {
        return $this->getNumerator($this->getDenominatorSQL() . ' AND
                    (
                        sd.statinStatus = 1
                    OR
                        sd.statinStatus = 3
                    )');
    }

    /**
     * Antiplatelet Therapy
     */
    public function calculateAntiPlatelet()
    {
        return $this->getNumerator($this->getDenominatorSQL() . ' AND
                    (
                        sd.antiPlatelets = 1
                    OR
                        sd.antiPlatelets = 3
                    )');
    }

    /**
     * Beta Blocker Therapy
     */
    public function calculateBetaBlocker()
    {
        return $this->getNumerator($this->getDenominatorSQL() . ' AND (sd.betaBlockers = 1 OR sd.betaBlockers = 3)');
    }

    /**
     * ACEI/ARB Therapy
     */
    public function calculateACEI()
    {
        if (!($denominator = $this->getDenominatorACEI())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLACEI() . ' AND
                    (
                         sd.aceiArbTherapy = 1
                         OR sd.aceiArbTherapy = 3
                         OR sd.dialysisPatient = 1
                         OR sd.esrdPatient = 1
                    )');

        return [$numerator, $denominator];
    }

    /**
     * Tobacco Status
     */
    public function calculateTobacco()
    {
        return $this->getNumerator($this->getDenominatorSQL() . '
                    AND (
                         sd.tobaccoStatus IN (0, 1)
                         AND (sd.tobaccoStatusAssessmentDate BETWEEN :fromDate AND :toDate)
                     )');
    }


    private function getDenominatorTobaccoCessation()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLTobaccoCessation());

        return $this->getSingleResultWithParams($qb);
    }

    private function getDenominatorSQLTobaccoCessation()
    {
        return $this->getDenominatorSQL('sd.tobaccoStatus = 1');
    }

    /**
     * Tobacco Cessation counseling if user – and Treatment
     */
    public function calculateTobaccoCessation()
    {
        if (!($denominator = $this->getDenominatorTobaccoCessation())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLTobaccoCessation() . '
                    AND (sd.tobaccoCessationAdviceOrTreatmentDate BETWEEN :fromDate AND :toDate)
                     ');
        return [$numerator, $denominator];
    }

    /**
     * Body Mass Index/Weight/Nutrition Counseling
     */
    public function calculateBMI()
    {
        return $this->getNumerator($this->getDenominatorSQL() . ' AND
                    (
                        sd.bmiValue IS NOT NULL 
                        AND (sd.bmiValueDate BETWEEN :fromDate AND :toDate)
                    )');
    }

    /**
     * Nutrition Counseling
     */
    public function calculateNutrition()
    {
        return $this->getNumerator($this->getDenominatorSQL() . ' AND
                    (
                        (sd.nutritionCounseling=1 AND (sd.nutritionCounselingDate BETWEEN :fromDate AND :toDate)) 
                        AND
                        (sd.activityCounseling=1 AND (sd.activityCounselingDate BETWEEN :fromDate AND :toDate))
                    )');
    }

    /**
     * Depression Screening Annually
     */
    public function calculateDepressionScreening()
    {
        return $this->getNumerator($this->getDenominatorSQL() . ' AND
                    (
                        (
                        sd.PHQ2screening = 1
                        AND sd.PHQ2screeningDate IS NOT NULL
                        AND (sd.PHQ2screeningDate BETWEEN :fromDate AND :toDate)
                        )
                    OR
                        (
                        sd.PHQ9screening = 1
                        AND sd.PHQ9screeningDate IS NOT NULL
                        AND (sd.PHQ9screeningDate BETWEEN :fromDate AND :toDate)
                        )
                    )');
    }

    /**
     * Influenza Immunization
     */
    public function calculateInfluenzaImmunization()
    {
        return $this->getNumerator($this->getDenominatorSQL() . '
                     AND (
                        (sd.influenzaImmunization=3)
                        OR   
                        (sd.influenzaImmunization=1 AND sd.influenzaImmunizationDate BETWEEN :fromDate AND :toDate)
                     )');
    }

    private function getDenominatorACEI()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLACEI());

        return $this->getSingleResultWithParams($qb);
    }

    protected function getDenominatorSQLACEI()
    {
        return $this->getDenominatorSQL('(sd.diabetesDiagnosis = 1 OR sd.hypertensionDiagnosis = 1 OR
        sd.chronicKidneyDiseaseDiagnosis = 1 OR sd.chfDiagnosis = 1 )');
    }
}