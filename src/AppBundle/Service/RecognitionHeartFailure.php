<?php

namespace AppBundle\Service;

use AppBundle\Entity\SubmissionHeartFailure;
use AppBundle\Manager\LogManager;
use Doctrine\ORM\EntityManager;

class RecognitionHeartFailure extends AbstractRecognition
{
    public function __construct(EntityManager $em, LogManager $logManager)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(SubmissionHeartFailure::class);
        $this->logManager = $logManager;
    }


    /**
     * @param string $andSql
     * @param int $minAge
     * @param int $maxAge
     * @return string
     */
    public function getDenominatorSQL($andSql = '', $minAge = 18, $maxAge = 75)
    {
        $result = 'sd.submission_id = :submissionId
                    AND sd.npi = :npi
                    AND sd.HFDiagnosis = 1
                    AND TIMESTAMPDIFF(YEAR, sd.patientDOB, sd.lastVisitDate) >= ' . $minAge . '
                    AND TIMESTAMPDIFF(YEAR, sd.patientDOB, sd.lastVisitDate) <= ' . $maxAge . '
                    AND sd.lastVisitDate IS NOT NULL
                    AND (sd.lastVisitDate BETWEEN  :fromDate AND :toDate)';

        if ($andSql) {
            $result = $result . ' AND ' . $andSql;
        }

        return $result;
    }

    private function getDenominatorBetaBlocker()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLBetaBlocker());

        return $this->getSingleResultWithParams($qb);
    }

    private function getDenominatorSQLBetaBlocker()
    {
        return $this->getDenominatorSQL('sd.LVSDDiagnosis = 1');
    }

    /**
     * Beta Blocker Therapy
     */
    public function calculateBetaBlocker()
    {
        if (!($denominator = $this->getDenominatorBetaBlocker())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLBetaBlocker() . ' AND (
            sd.betablockerTherapy = 1
                OR
            sd.betablockerTherapy = 3
        )');

        return [$numerator, $denominator];
    }

    private function getDenominatorACEI()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLACEI());

        return $this->getSingleResultWithParams($qb);
    }

    private function getDenominatorSQLACEI()
    {
        return $this->getDenominatorSQL('sd.LVSDDiagnosis = 1');
    }

    /**
     * ACEI/ARB Therapy
     */
    public function calculateAcei()
    {
        if (!($denominator = $this->getDenominatorACEI())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLACEI() . '
                    AND (sd.aceiArbTherapy=1 OR sd.aceiArbTherapy=3) ');
        return [$numerator, $denominator];
    }

    /**
     * Denominator for
     * Blood Pressure Control in Patients < 60
     */
    private function getDenominatorBloodPressureUnderSixty()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLBloodPressureUnderSixty());

        return $this->getSingleResultWithParams($qb);
    }

    /**
     * Denominator for
     * Blood Pressure Control in Patients < 60
     */
    private function getDenominatorSQLBloodPressureUnderSixty()
    {
        return $this->getDenominatorSQL('', 18, 59);
    }

    /**
     * Denominator for
     * Blood Pressure Control in Patients > 60
     */
    private function getDenominatorBloodPressureOverSixty()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLBloodPressureOverSixty());

        return $this->getSingleResultWithParams($qb);
    }

    public function getDenominatorSQLBloodPressureOverSixty()
    {
        return $this->getDenominatorSQL('', 60, 75);
    }

    /**
     * Blood Pressure Control in Patients < 60
     */
    public function calculateBloodPressureUnderSixty()
    {
        if (!($denominator = $this->getDenominatorBloodPressureUnderSixty())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLBloodPressureUnderSixty() . ' AND
                    (
                        (sd.bloodPressureDate2 BETWEEN  :fromDate AND :toDate)
                            AND sd.systolic2 IS NOT NULL
                            AND sd.systolic2 < 140
                            AND sd.diastolic2 IS NOT NULL
                            AND sd.diastolic2 < 90
                    )');
        return [$numerator, $denominator];
    }


    /**
     * Blood Pressure Control in Patients > 60
     */
    public function calculateBloodPressureOverSixty()
    {
        if (!($denominator = $this->getDenominatorBloodPressureOverSixty())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLBloodPressureOverSixty() . ' AND
                    (
                        (sd.bloodPressureDate2 BETWEEN  :fromDate AND :toDate)
                            AND sd.systolic2 IS NOT NULL
                            AND sd.systolic2 < 150
                            AND sd.diastolic2 IS NOT NULL
                            AND sd.diastolic2 < 90
                    )');

        return [$numerator, $denominator];
    }

    /**
     * Clinical Assessment of Fluid Status
     */
    public function calculateFluidStatus()
    {
        if (!($denominator = $this->getDenominatorBloodPressureOverSixty())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLBloodPressureOverSixty() . ' AND (
                    (sd.weightMeasureDate IS NOT NULL AND sd.weightMeasureDate BETWEEN  :fromDate AND :toDate)
                        OR sd.peripheraledemaAssessment = 1
                        OR sd.orthopneaAssessment = 1
                        OR sd.JVPAssessment = 1
                    )');
        return [$numerator, $denominator];
    }


    /**
     * Tobacco Status
     */
    public function calculateTobacco()
    {
        return $this->getNumerator($this->getDenominatorSQL() . '
                    AND (sd.tobaccoStatusAssessmentDate BETWEEN :fromDate AND :toDate)
                     ');
    }


    /**
     * Denominator for tobacco
     */
    private function getDenominatorTobaccoCessation()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLTobaccoCessation());

        return $this->getSingleResultWithParams($qb);
    }

    private function getDenominatorSQLTobaccoCessation()
    {
        return $this->getDenominatorSQL('sd.tobaccoStatus = 1');
    }


    /**
     * Tobacco Cessation counseling if user – and Treatment
     */
    public function calculateTobaccoCessation()
    {
        if (!($denominator = $this->getDenominatorTobaccoCessation())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLTobaccoCessation() . '
                    AND (sd.tobaccoCessationAdviceOrTreatmentDate BETWEEN :fromDate AND :toDate)
                     ');

        return [$numerator, $denominator];
    }

    /**
     * Body Mass Index/Weight/Nutrition Counseling
     */
    public function calculateBMI()
    {
        return $this->getNumerator($this->getDenominatorSQL() . ' AND
                    (
                        sd.bmiValue IS NOT NULL
                        AND (sd.bmiValueDate BETWEEN :fromDate AND :toDate)
                    )');
    }

    /**
     * Nutrition Counseling for Low Salt (DASH) Diet
     */
    public function calculateNutrition()
    {
        return $this->getNumerator($this->getDenominatorSQL() . ' AND
                    (
                        (sd.nutritionCounseling=1 AND (sd.nutritionCounselingDate BETWEEN :fromDate AND :toDate)) AND
                        (sd.activityStatus IS NOT NULL AND (sd.activityStatusDate BETWEEN :fromDate AND :toDate)) AND
                        (sd.activityCounseling=1 AND (sd.activityCounselingDate BETWEEN :fromDate AND :toDate))
                    )');
    }

    /**
     * Type of Heart Failure
     */
    public function calculateTypeHeartfailure()
    {
        return $this->getNumerator($this->getDenominatorSQL() . ' AND
                    (
                        sd.heartfailureType IN (1, 2, 4, 5)
                        AND (sd.heartfailureAssessedDate BETWEEN :fromDate AND :toDate)
                    )');
    }

    /**
     * Severity of Heart Failure
     */
    public function calculateSeverityHeartfailure()
    {
        return $this->getNumerator($this->getDenominatorSQL() . ' AND
                    (
                        sd.heartfailureSeverity=1
                        AND (sd.heartfailureAssessedDate BETWEEN :fromDate AND :toDate)
                    )');
    }
}