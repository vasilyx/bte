<?php

namespace AppBundle\Service;

use AppBundle\Entity\Program;
use AppBundle\Entity\User;

use AppBundle\Exception\CSVFieldException;
use AppBundle\Manager\LogManager;
use AppBundle\Service\CSV\Provider;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\Exception;
use Psr\Log\LoggerInterface;



class CSVImport
{
    private $em;
    private $logger;
    private $logManager;

    const STATUS_ERROR = 2;
    const STATUS_WARNING = 1;
    const STATUS_OK = 0;
    const LIMIT_RESPONSE = 99999;

    public function __construct(EntityManager $em, LoggerInterface $logger, LogManager $logManager)
    {
        $this->em = $em;
        $this->logger = $logger;
        $this->logManager = $logManager;
    }


    /**
     * @param $data
     * @return array
     */
    public function getProviderList($data)
    {
        $array = $this->csvToArray($data);

        $result = [];

        foreach ($array as $line_number => $csv_record) {

            if ($line_number != 0 && $csv_record) {

                if (isset($result[$csv_record[1]])){
                    $result[$csv_record[1]] ++;
                } else {
                    $result[$csv_record[1]] = 1;
                }
            }
        }

        return $result;
    }

    protected function csvToArray($data)
    {
        ini_set("auto_detect_line_endings", true);
        return array_map('str_getcsv', $data);
    }

    /**
     * @param $data
     * @param Program $program
     * @param bool $submissionReportingEndDate
     * @param $providerList
     * @return array
     * @throws \Exception
     */
    public function importSubmission($data, Program $program, $submissionReportingEndDate = false, $providerList)
    {
        $array = $this->csvToArray($data);
        $temp_id = random_int(0, 999999);

        $name_scv = '\AppBundle\Service\CSV\\' . $program->getTableName();

        $this->logManager->setMessage('Started submission, number of rows ' . count($array),
            null,
            $temp_id,
            LogManager::TYPE_SUBMISSION,
            LogManager::STATUS_STARTED);

        /**
         * @var $class_csv CSV\AbstractCSV
         */
        $class_csv = new $name_scv();
        if($submissionReportingEndDate) {
            $class_csv->setLogger($this->logger);
            $class_csv->setGroupSubmission((count($providerList) > 1 ? true : false));
        }

        $return_data = [];

        $class_csv->fillChartId($array);
        $numPersist = 0;

        foreach ($array as $line_number => $csv_record) {

            if ($line_number == 0) {

                $temp = array_map(function($n) { return [$n, 0];}, $csv_record);
                array_push($return_data, $temp);

            } else {
                if (!$this->checkEmptyArray($csv_record)
                    || count($csv_record) !== $class_csv->getPropertiesCount()
                    || !in_array($csv_record[1], $providerList) // not in providerList
                ) {
                    continue;
                } else {
                   $entity = $class_csv->generateEntity($csv_record);

                    if (count($return_data) < self::LIMIT_RESPONSE) {
                        array_push($return_data,  $class_csv->getWarningData());
                    }

                    $entity->setTempId($temp_id);

                    $this->em->persist($entity);
                    $numPersist++;
                    unset($entity);

                    if (($line_number%2000) == 0) {
                        try {

                            $this->em->flush();
                            $this->em->clear();
                            $this->logManager->setMessage('Validated and inserted ' . $numPersist . ' rows.',
                                null, $temp_id,LogManager::TYPE_SUBMISSION,LogManager::STATUS_PROGRESS);

                        } catch (\Exception $e) {
                            $this->logger->error($e->getMessage());
                            throw new \Exception('Wrong file format.');
                        }
                    }
                }
            }
        }

        try {
            $this->em->flush();
            $this->logManager->setMessage('Done. Validated and inserted ' . $numPersist . ' rows',
                null, $temp_id,LogManager::TYPE_SUBMISSION, LogManager::STATUS_DONE);
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            throw new \Exception('Wrong file format.');
        }


        return [
            'count' => $numPersist,
            'id' => $temp_id,
            'errors' => $class_csv->getErrors(),
            'warnings' => $class_csv->getWarnings(),
            'data' => $return_data
        ];
    }


    public function importProvider($data, User $user, $program_id = false, $providerExpireDays = 90)
    {
        $array = $this->csvToArray($data);

        $this->logManager->setMessage('Started provider_import, number of rows ' . count($array),
            null, null,LogManager::TYPE_PROVIDER,LogManager::STATUS_STARTED);


        /**
         * @var $class_csv CSV\CSVInterface
         */
        $class_csv = new Provider($this->em);

        $class_csv->fillValidationArray($array);

        $return_data = [];
        $count_inserted = 0;
        $count_warning = 0;
        $count_error = 0;

        foreach ($array as $line_number => $csv_record) {

            if (!$this->checkEmptyArray($csv_record) || count($csv_record) !== $class_csv->getPropertiesCount()) { // empty line
                continue;
            }
            if ($line_number == 0) {
                $temp = array_map(function($n) { return [$n, 0];}, $csv_record);
                array_push($return_data, $temp);

            } else {

                if (!($item = $this->em->getRepository(\AppBundle\Entity\Provider::class)->isUniqueNpi($user, $csv_record[1], 0, true, $program_id, $providerExpireDays))) {
                    $item = $this->em->getRepository(\AppBundle\Entity\Provider::class)->isUniqueClinicianId($user, $csv_record[0], 0, true, $program_id, $providerExpireDays);
                }

                if(isset($item[0]) && $item[0] instanceof \AppBundle\Entity\Provider) {
                    $entity = $item[0];
                } else {
                    $entity = new \AppBundle\Entity\Provider();
                }

                if(isset($item['disabled']) && $item['disabled']) {
                    $count_warning++;
                }

                foreach ($csv_record as $key => $val) {
                    $value = '';
                    $warning = false;
                    $error = false;

                    if ($propertyName = $class_csv->getPropertyNameByIndex($key)) {
                        try {
                            $field = $class_csv->getPropertyObjectByIndex($key);
                            $value = $field->getValue($val);

                        } catch (CSVFieldException $e) {
                            if($e->getIsCritical()){
                                $error = true;
                                $count_error++;
                            }else {
                                $warning = true;
                                $count_warning++;
                            }
                        } catch (Exception $e) {
                            $this->logger->error($e->getMessage());
                        }

                        $methodName = 'set' . ucfirst($propertyName);
                        if (method_exists($entity, $methodName)) {
                            if ($error === false && $warning === false) {
                                $entity->$methodName($value);
                            }
                        } else {
                            $this->logger->error('Property "' . $propertyName . '" is not exist.');
                        }
                    }

                    if ($key == 1 && !$error) {
                        if ($error = $class_csv->checkNpi($val)) {
                            $count_error++;
                        }
                    }

                    if ($key == 0 && !$error) {
                        if ($error = $class_csv->checkResponsibleProviderID($val)) {
                            $count_error++;
                        }
                    }

                    $return_data[$line_number][$key] = [$val, self::STATUS_OK];

                    if ($warning) {
                        $return_data[$line_number][$key] = [$val, self::STATUS_WARNING];
                    }

                    if ($error) {
                        $return_data[$line_number][$key] = [$val, self::STATUS_ERROR];
                    }
                }

                $entity->setUser($user);
                $entity->setActive(1);
                $entity->setCreatedAt(new \DateTime());
                $otherDate = array();

                if(isset($item['last_submission_date']) && $item['last_submission_date']){
                    $date = \DateTime::createFromFormat('Y-m-d H:i:s', $item['last_submission_date']) ;
                    $otherDate['last_submission_date'] = $date;
                }

                $otherDate['disabled'] = $item['disabled'];
                $otherDate['entity'] = $entity;
                $return_data[$line_number][] = $otherDate;
                
                $this->em->persist($entity);
            }
        }

        try {
            $this->em->flush();
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            throw new \Exception($e->getMessage() . 'Wrong file format.');
        }

        $this->logManager->setMessage('Validated and inserted ' . count($return_data) . ' rows',
            null, null,LogManager::TYPE_PROVIDER,LogManager::STATUS_DONE);

        return [
            'count' => $count_inserted,
            'errors' => $count_error,
            'warnings' => $count_warning,
            'data' => $return_data
        ];
    }

    /**
     * @param $array
     * @return bool
     */
    protected function checkEmptyArray($array) {

        foreach ($array as $value) {
            if ($value) return true;
        }

        return false;
    }
}