<?php
namespace AppBundle\Service;

class BackupSubmission
{
    protected $file_path;
    protected $file_path_source;
    protected $twig;

    public function __construct($folder, $file_path_source, $templating)
    {
        $this->file_path = $folder;
        $this->file_path_source = $file_path_source;
        $this->twig = $templating;


        if (file_exists($this->file_path) === false) {
            mkdir($this->file_path);
        }

        if (file_exists($this->file_path_source) === false) {
            mkdir($this->file_path_source);
        }
    }

    /**
     * @param $file_name
     * @param $string
     * @return void
     */
    public function putFile($file_name, $string)
    {
        file_put_contents($this->file_path . $file_name, json_encode($string));
    }

    /**
     * @param $file_name
     * @param $string
     * @return void
     */
    public function putFileSource($file_name, $string)
    {
        file_put_contents($this->file_path_source . $file_name . '.csv', $string);
    }

    /**
     * @param $file_name
     * @param $string
     * @return void
     */
    public function putRenderFile($file_name, $string)
    {
        $html = $this->twig->render(
            'AppBundle:common:backup_submission.html.twig',[
            'data' => $string
            ]
        );

        file_put_contents($this->file_path_source . $file_name . '.html', $html);
    }


    /**
     * @param $name
     * @return string | bool
     */
    public function getFile($name)
    {
        if (file_exists($this->file_path . $name)) {
            return file_get_contents($this->file_path . $name);
        } else {
            return false;
        }
    }

    /**
     * @param $name
     */
    public function removeFile($name)
    {
        unlink($this->file_path . $name);
        unlink($this->file_path_source . $name . '.csv');
        unlink($this->file_path_source . $name . '.html');
    }

    /**
     * @param $old_name
     * @param $new_name
     * @return bool | string
     */
    public function renameFile($old_name, $new_name)
    {
        if (file_exists($this->file_path . $old_name)) {
            return rename($this->file_path . $old_name, $this->file_path . $new_name);
        } else {
            return false;
        }
    }

    /**
     * @param $old_name
     * @param $new_name
     * @return bool | string
     */
    public function renameFileSource($old_name, $new_name)
    {
        if (file_exists($this->file_path_source . $old_name .'.csv')) {
            return rename($this->file_path_source . $old_name .'.csv', $this->file_path_source . $new_name .'.csv');
        } else {
            return false;
        }
    }

    /**
     * @param $old_name
     * @param $new_name
     * @return bool | string
     */
    public function renameRenderFileSource($old_name, $new_name)
    {
        if (file_exists($this->file_path_source . $old_name .'.html')) {
            return rename($this->file_path_source . $old_name .'.html', $this->file_path_source . $new_name .'.html');
        } else {
            return false;
        }
    }
}