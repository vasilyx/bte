<?php
namespace AppBundle\Service;

use AppBundle\Entity\SubmissionMaternity;
use AppBundle\Manager\LogManager;
use Doctrine\ORM\EntityManager;


class RecognitionMaternity extends AbstractRecognition
{
    public function __construct(EntityManager $em, LogManager $logManager)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(SubmissionMaternity::class);
        $this->logManager = $logManager;
    }

    /**
     * This rules of denominator use for almost all tests
     * @return mixed
     */
    public function getDenominator()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQL());

        return $this->getSingleResultWithParams($qb);
    }

    /**
     * @param string $andSql
     * @param int $minAge
     * @param int $maxAge
     * @return string
     */
    public function getDenominatorSQL($andSql = '', $minAge = 15, $maxAge = 45, $postPartumVisitDate = false)
    {
        $result = 'sd.submission_id = :submissionId
                    AND sd.npi = :npi
                    AND sd.patientGender = 1
                    AND TIMESTAMPDIFF(YEAR, sd.patientDOB, sd.deliveryDate) >= ' . $minAge . '
                    AND TIMESTAMPDIFF(YEAR, sd.patientDOB, sd.deliveryDate) <= ' . $maxAge ;

        if ($postPartumVisitDate) {
            $result .= ' AND (sd.postPartumDepressionScreenDate >= :fromDate AND sd.postPartumDepressionScreenDate <= :toDate)';
        } else {
            $result .= ' AND (sd.deliveryDate >= :fromDate AND sd.deliveryDate <= :toDate)';
        }

        if ($andSql) {
            $result = $result . ' AND ' . $andSql;
        }

        return $result;
    }

    /**
     * Frequency of Prenatal & Postpartum Visits
     * @return float
     */
    public function calculateVisits()
    {
        return $this->getNumerator($this->getDenominatorSQL() . ' AND 
        (
            (sd.initialVisitDate BETWEEN  :fromDate AND :toDate)
            AND
            (
                (sd.tri2VisitDate1 BETWEEN  :fromDate AND :toDate) AND
                (sd.tri2VisitDate2 BETWEEN  :fromDate AND :toDate) AND
                (sd.tri2VisitDate3 BETWEEN  :fromDate AND :toDate) AND
                (sd.tri2VisitDate4 BETWEEN  :fromDate AND :toDate) AND
                (sd.tri2VisitDate5 BETWEEN  :fromDate AND :toDate) AND
                (sd.tri2VisitDate6 BETWEEN  :fromDate AND :toDate) AND
                (sd.tri3VisitDate1 BETWEEN  :fromDate AND :toDate) AND
                (sd.tri3VisitDate2 BETWEEN  :fromDate AND :toDate) AND
                (sd.tri3VisitDate3 BETWEEN  :fromDate AND :toDate) AND
                (sd.tri3VisitDate4 BETWEEN  :fromDate AND :toDate) AND
                (sd.tri3VisitDate5 BETWEEN  :fromDate AND :toDate) AND
                (sd.tri3VisitDate6 BETWEEN  :fromDate AND :toDate) AND
                (sd.tri3VisitDate7 BETWEEN  :fromDate AND :toDate)
            )
            AND
            (
                (sd.postpartumVisitDate BETWEEN  :fromDate AND :toDate) AND 
                (DATE_DIFF(sd.postpartumVisitDate, sd.deliveryDate) >= 21 AND  DATE_DIFF(sd.postpartumVisitDate, sd.deliveryDate) <= 56) 
            )
        )');
    }

    /**
     * Risk-Appropriate Screenings During Pre-Natal Care Visits
     * @return float
     */
    public function calculateRiskVisits()
    {
        return $this->getNumerator($this->getDenominatorSQL() . ' AND
            (
                (
                    (sd.aboRhScreenDate BETWEEN  :fromDate AND :toDate) AND 
                    (DATE_DIFF(sd.aboRhScreenDate, sd.lmpDate)/7 >= 10 AND  DATE_DIFF(sd.aboRhScreenDate, sd.lmpDate)/7 <= 12) 
                ) AND 
                (
                    (sd.cbcDate1 BETWEEN  :fromDate AND :toDate) AND 
                    (DATE_DIFF(sd.cbcDate1, sd.lmpDate)/7 >= 10 AND  DATE_DIFF(sd.cbcDate1, sd.lmpDate)/7 <= 12) 
                ) AND 
                (
                    (sd.vdrlScreenDate BETWEEN  :fromDate AND :toDate) AND 
                    (DATE_DIFF(sd.vdrlScreenDate, sd.lmpDate)/7 >= 10 AND  DATE_DIFF(sd.vdrlScreenDate, sd.lmpDate)/7 <= 12) 
                ) AND 
                (
                    (sd.chlamydiaScreenDate BETWEEN  :fromDate AND :toDate) AND 
                    (DATE_DIFF(sd.chlamydiaScreenDate, sd.lmpDate)/7 >= 10 AND  DATE_DIFF(sd.chlamydiaScreenDate, sd.lmpDate)/7 <= 12) 
                ) AND 
                (
                    (sd.gonorrheaScreenDate BETWEEN  :fromDate AND :toDate) AND 
                    (DATE_DIFF(sd.gonorrheaScreenDate, sd.lmpDate)/7 >= 10 AND  DATE_DIFF(sd.gonorrheaScreenDate, sd.lmpDate)/7 <= 12) 
                ) AND 
                (
                    (sd.hivScreenDate BETWEEN  :fromDate AND :toDate) AND 
                    (DATE_DIFF(sd.hivScreenDate, sd.lmpDate)/7 >= 10 AND  DATE_DIFF(sd.hivScreenDate, sd.lmpDate)/7 <= 12) 
                ) AND 
                (
                    (sd.urineCultureSensitivityDate BETWEEN  :fromDate AND :toDate) AND 
                    (DATE_DIFF(sd.urineCultureSensitivityDate, sd.lmpDate)/7 >= 10 AND  DATE_DIFF(sd.urineCultureSensitivityDate, sd.lmpDate)/7 <= 12) 
                ) AND 
                (
                    (sd.HbA1Cdate BETWEEN  :fromDate AND :toDate) AND 
                    (DATE_DIFF(sd.HbA1Cdate, sd.lmpDate)/7 >= 10 AND  DATE_DIFF(sd.HbA1Cdate, sd.lmpDate)/7 <= 12) 
                ) AND 
                (
                    (sd.rubellaScreenDate BETWEEN  :fromDate AND :toDate) AND 
                    (DATE_DIFF(sd.rubellaScreenDate, sd.lmpDate)/7 >= 10 AND  DATE_DIFF(sd.rubellaScreenDate, sd.lmpDate)/7 <= 12) 
                ) AND 
                (
                    (sd.fetalAneuploidyScreenDate BETWEEN  :fromDate AND :toDate) AND 
                    (DATE_DIFF(sd.fetalAneuploidyScreenDate, sd.lmpDate)/7 >= 10 AND  DATE_DIFF(sd.fetalAneuploidyScreenDate, sd.lmpDate)/7 <= 12) 
                ) AND 
                (
                    (sd.afpScreenDate BETWEEN  :fromDate AND :toDate) AND 
                    (DATE_DIFF(sd.afpScreenDate, sd.lmpDate)/7 >= 15 AND  DATE_DIFF(sd.afpScreenDate, sd.lmpDate)/7 <= 22) 
                ) AND 
                (
                    (sd.quadScreenDate BETWEEN  :fromDate AND :toDate) AND 
                    (DATE_DIFF(sd.quadScreenDate, sd.lmpDate)/7 >= 15 AND  DATE_DIFF(sd.quadScreenDate, sd.lmpDate)/7 <= 22) 
                ) AND 
                (
                    (sd.cbcDate2 BETWEEN  :fromDate AND :toDate) AND 
                    (DATE_DIFF(sd.cbcDate2, sd.lmpDate)/7 >= 24 AND  DATE_DIFF(sd.cbcDate2, sd.lmpDate)/7 <= 28) 
                ) AND 
                (
                    (sd.glucoseScreenDate BETWEEN  :fromDate AND :toDate) AND 
                    (DATE_DIFF(sd.glucoseScreenDate, sd.lmpDate)/7 >= 24 AND  DATE_DIFF(sd.glucoseScreenDate, sd.lmpDate)/7 <= 28) 
                ) AND 
                (
                    (sd.antibodiesScreenDate BETWEEN  :fromDate AND :toDate) AND 
                    (DATE_DIFF(sd.antibodiesScreenDate, sd.lmpDate)/7 >= 24 AND  DATE_DIFF(sd.antibodiesScreenDate, sd.lmpDate)/7 <= 28) 
                ) AND 
                (
                    (sd.rhoGAMScreenDate BETWEEN  :fromDate AND :toDate) AND 
                    (DATE_DIFF(sd.rhoGAMScreenDate, sd.lmpDate)/7 >= 24 AND  DATE_DIFF(sd.rhoGAMScreenDate, sd.lmpDate)/7 <= 28) 
                ) AND 
                (
                    (sd.groupBStrepScreenDate BETWEEN  :fromDate AND :toDate) AND 
                    (DATE_DIFF(sd.groupBStrepScreenDate, sd.lmpDate)/7 >= 35 AND  DATE_DIFF(sd.groupBStrepScreenDate, sd.lmpDate)/7 <= 37) 
                )
         )');
    }


    /**
     * Genetic Carrier Screenings
     * @return float
     */
    public function calculateGeneticCarrier()
    {
        return $this->getNumerator($this->getDenominatorSQL() . ' AND 
        (
            (
                (sd.cysticFibrosisScreeningDate BETWEEN  :fromDate AND :toDate) AND 
                (sd.spinalMuscularAtrophyDate BETWEEN  :fromDate AND :toDate) AND 
                (sd.hemoglobinopathiesDate BETWEEN  :fromDate AND :toDate)
            )
            AND
            (sd.panEthicExpandedCarrierScreenDate BETWEEN  :fromDate AND :toDate)
            OR
            (
                sd.genticCarrierScreen=1
            )
        )');
    }

    /**
     * Pre-Natal Immunizations
     * @return float
     */
    public function calculateImmunizations()
    {
        return $this->getNumerator($this->getDenominatorSQL() . ' AND
         (
                 (sd.influenzaDate BETWEEN  :fromDate AND :toDate) AND
                 (
                    (sd.tetanusDate BETWEEN  :fromDate AND :toDate) AND 
                    (DATE_DIFF(sd.tetanusDate, sd.lmpDate)/7 >= 27 AND  DATE_DIFF(sd.tetanusDate, sd.lmpDate)/7 <= 35) 
                 ) AND 
                 (
                    (sd.diphtheriaDate BETWEEN  :fromDate AND :toDate) AND 
                    (DATE_DIFF(sd.diphtheriaDate, sd.lmpDate)/7 >= 27 AND  DATE_DIFF(sd.diphtheriaDate, sd.lmpDate)/7 <= 35) 
                 ) AND 
                 (
                    (sd.pertussisTdapDate BETWEEN  :fromDate AND :toDate) AND 
                    (DATE_DIFF(sd.pertussisTdapDate, sd.lmpDate)/7 >= 27 AND  DATE_DIFF(sd.pertussisTdapDate, sd.lmpDate)/7 <= 35) 
                 ) 
        )') ;
    }

    private function getDenominatorAspirin()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLAspirin());

        return $this->getSingleResultWithParams($qb);
    }

    private function getDenominatorSQLAspirin()
    {
        return $this->getDenominatorSQL( '(sd.hxPreeclampsia = 1 AND sd.hxPretermdelivery <=34)');
    }


    /**
     * Low-Dose Aspirin for Prevention of Pre-Eclampsia
     * @return float
     */
    public function calculateAspirin()
    {
        if (!($denominator = $this->getDenominatorAspirin())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLAspirin() . ' 
                        AND (sd.lowDoseAspirinDate BETWEEN  :fromDate AND :toDate)');

        return [$numerator, $denominator];
    }

    /**
     * Performed ultrasound at 18–22 weeks of pregnancy
     * @return float
     */
    public function calculateUltrasound()
    {
        return $this->getNumerator($this->getDenominatorSQL() . ' 
                        AND (
                        (sd.ultrasoundDate BETWEEN  :fromDate AND :toDate) AND 
                        (DATE_DIFF(sd.ultrasoundDate, sd.lmpDate)/7 >= 18 AND  DATE_DIFF(sd.ultrasoundDate, sd.lmpDate)/7 <= 22) 
                        )');
    }

    private function getDenominatorAntibiotic()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLAntibiotic());

        return $this->getSingleResultWithParams($qb);
    }

    private function getDenominatorSQLAntibiotic()
    {
        return $this->getDenominatorSQL( 'sd.groupBStrep = 1');
    }

    /**
     * Antibiotic Prophylaxis if GBS (Group B Streptococcus) Positive
     * @return float
     */
    public function calculateAntibiotic()
    {
        if (!($denominator = $this->getDenominatorAntibiotic())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLAntibiotic() . ' 
                        AND (
                            (sd.antibioticProphylaxisDate BETWEEN  :fromDate AND :toDate) AND 
                            (DATE_DIFF(sd.antibodiesScreenDate, sd.lmpDate)/7 >= 35 AND  DATE_DIFF(sd.antibodiesScreenDate, sd.lmpDate)/7 <= 37) 
                        )');

        return [$numerator, $denominator];
    }

    private function getDenominatorCorticosteroid()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLCorticosteroid());

        return $this->getSingleResultWithParams($qb);
    }

    private function getDenominatorSQLCorticosteroid()
    {
        // TODO MARCELA
        return $this->getDenominatorSQL('(
            (DATE_DIFF(sd.deliveryDate, sd.lmpDate)/7 >= 24 AND  DATE_DIFF(sd.deliveryDate, sd.lmpDate)/7 < 37) OR 
            (DATE_DIFF(sd.deliveryDate, sd.lmpDate)/7 >= 34 AND  DATE_DIFF(sd.deliveryDate, sd.lmpDate)/7 < 37)  
        )');
    }


    /**
     * Optimal Antenatal Corticosteroid Administration
     * @return float
     */
    public function calculateCorticosteroid()
    {
        if (!($denominator = $this->getDenominatorCorticosteroid())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLCorticosteroid() . ' 
                        AND (sd.corticoSteroidDate BETWEEN  :fromDate AND :toDate)');

        return [$numerator, $denominator];
    }

    private function getDenominatorVBAC()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLVBAC());

        return $this->getSingleResultWithParams($qb);
    }

    private function getDenominatorSQLVBAC()
    {
        return $this->getDenominatorSQL( 'sd.hxCSection = 1');
    }

    /**
     * Vaginal Birth After Cesarean (VBAC) Consent
     * @return float
     */
    public function calculateVBAC()
    {
        if (!($denominator = $this->getDenominatorVBAC())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLVBAC() . ' 
                        AND 
                        (
                            ((sd.birthPlanDate < sd.deliveryDate) AND (sd.birthPlanDate BETWEEN  :fromDate AND :toDate))
                            OR
                            ((sd.decisionMakingConsultDate < sd.deliveryDate) AND (sd.decisionMakingConsultDate BETWEEN  :fromDate AND :toDate))
                        )');

        return [$numerator, $denominator];
    }


    private function getDenominatorNTSV()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLNTSV());

        return $this->getSingleResultWithParams($qb);
    }

    private function getDenominatorSQLNTSV()
    {
        return $this->getDenominatorSQL( 'sd.ntsv = 1');
    }

    /**
     * Primary C-Section Rates (NTSV Rates)
     * @return float
     */
    public function calculateNTSV()
    {
        if (!($denominator = $this->getDenominatorNTSV())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLNTSV() . ' 
                        AND (sd.electivecsectionDate BETWEEN  :fromDate AND :toDate)');

        return [$numerator, $denominator];
    }


    private function getDenominatorVLBW()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLVLBW());

        return $this->getSingleResultWithParams($qb);
    }

    private function getDenominatorSQLVLBW()
    {
        return $this->getDenominatorSQL( '(DATE_DIFF(sd.deliveryDate, sd.lmpDate)/7 < 26 )');
    }

    /**
     * VLBW Babies Managed in NICU level 3 or 4
     * @return float
     */
    public function calculateVLBW()
    {
        if (!($denominator = $this->getDenominatorVLBW())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLVLBW() . ' 
                    AND 
                        (sd.infantWeight < 1500 and (sd.NICUlevel = 3 OR sd.NICUlevel = 4))
                        ');

        return [$numerator, $denominator];
    }

    private function getDenominatorDepressionScreening()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLDepressionScreening());

        return $this->getSingleResultWithParams($qb);
    }

    private function getDenominatorSQLDepressionScreening()
    {
        return $this->getDenominatorSQL( '', 15, 45, true);
    }

    /**
     * Postpartum Depression Screening (optional)
     * @return float
     */
    public function calculateDepressionScreening()
    {
        if (!($denominator = $this->getDenominatorDepressionScreening())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLDepressionScreening() . '
                     AND (sd.postPartumDepressionScreenDate BETWEEN :fromDate AND :toDate)'
        );

        return [$numerator, $denominator];
    }

    /**
     * Drug and Alcohol Screening (optional)
     * @return float
     */
    public function calculateAlcoholScreening()
    {
        return $this->getNumerator($this->getDenominatorSQL(). '
                     AND (sd.drugAlcoholScreenDate BETWEEN  :fromDate AND :toDate)'
        );
    }

    /**
     * Interpersonal Violence Screening (optional)
     * @return float
     */
    public function calculateViolenceScreening()
    {
        return $this->getNumerator($this->getDenominatorSQL() . '
                     AND (sd.interpersonalViolenceScreenDate BETWEEN  :fromDate AND :toDate)'
        );
    }
}