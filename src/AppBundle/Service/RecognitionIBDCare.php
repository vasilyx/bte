<?php
namespace AppBundle\Service;
use AppBundle\Entity\SubmissionIBDCare;
use AppBundle\Manager\LogManager;
use Doctrine\ORM\EntityManager;

class RecognitionIBDCare extends AbstractRecognition
{
    public function __construct(EntityManager $em, LogManager $logManager)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(SubmissionIBDCare::class);
        $this->logManager = $logManager;

    }

    /**
     * This rules of denominator use for almost all tests
     * @return mixed
     */
    public function getDenominator()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQL());

        return $this->getSingleResultWithParams($qb);
    }

    /**
     * @param string $andSql
     * @param int $minAge
     * @param int $maxAge
     * @return string
     */
    public function getDenominatorSQL($andSql = '', $minAge = 18, $maxAge = 75)
    {
        $result = 'sd.submission_id = :submissionId
                    AND sd.npi = :npi
                    AND sd.IBDDiagnosis = 1
                    AND TIMESTAMPDIFF(YEAR, sd.patientDOB, sd.lastVisitDate) >= ' . $minAge . '
                    AND TIMESTAMPDIFF(YEAR, sd.patientDOB, sd.lastVisitDate) <= ' . $maxAge . '
                    AND sd.lastVisitDate IS NOT NULL
                    AND (sd.lastVisitDate BETWEEN  :fromDate AND :toDate)';

        if ($andSql) {
            $result = $result . ' AND ' . $andSql;
        }

        return $result;
    }


    /**
     * IBD Type, Anatomic Location, and Disease Activity
     */
    public function calculateIBDType()
    {
        return $this->getNumerator($this->getDenominatorSQL() . " AND
                    (
                        ((sd.IBDTypeDate BETWEEN  :fromDate AND :toDate)
                            AND sd.IBDType IN (1, 2, 3)
                        )
                        AND
                        ((sd.IBDExtentDate BETWEEN  :fromDate AND :toDate)
                            AND sd.IBDExtent IN (1, 2)
                            AND sd.IBDLocation IN (1, 2, 3)
                        )
                        AND
                        ((sd.IBDActivityDate BETWEEN  :fromDate AND :toDate)
                            AND sd.IBDActivity IN (1, 2, 3, 4)
                        )
                    )");
    }

    /**
     * IBD External Manifestations Assessed
     */
    public function calculateExternalManifestations()
    {
        return $this->getNumerator($this->getDenominatorSQL() . " AND
                    (sd.externalMainfestationsDateAssessed BETWEEN :fromDate AND :toDate)
                    AND
                    (sd.externalManifestations IS NOT NULL AND sd.externalManifestations < 6)
                    ");
    }

    /**
     * Corticosteroid Sparing Therapy Prescribed
     */
    public function calculateCorticosteroid()
    {
        return $this->getNumerator($this->getDenominatorSQL() . '
             AND sd.corticosteroid10Mg60Days=1
             AND sd.corticosteroidSparingTherapy=1
             ');
    }

    /**
     * Bone Loss Assessment for Patients Receiving Corticosteroid Therapy
     */
    public function calculateBoneLoss()
    {
        $fromBoneDate = clone $this->submission->getReportingEnd();

        return $this->getNumerator($this->getDenominatorSQL() . '
                 AND sd.corticosteroid10Mg60Days=1
                 AND sd.boneLossRiskAssessed=1
                 AND (sd.boneLossRiskDateAssessed BETWEEN :fromBoneDate AND :toDate)
             ', ['fromBoneDate' => $fromBoneDate->modify('-24 month')]);
    }

    /**
     * Osteoporosis Therapy in Patients with Bone Loss
     */
    public function calculateOsteoporosis()
    {
        $fromBoneDate = clone $this->submission->getReportingEnd();

        return $this->getNumerator($this->getDenominatorSQL() . '
                 AND sd.boneLossRiskAssessed=1
                 AND (sd.boneLossRiskDateAssessed BETWEEN :fromBoneDate AND :toDate)
                 AND (sd.osteoporosisTherapyDateAssessed BETWEEN :fromDate AND :toDate)
             ', ['fromBoneDate' => $fromBoneDate->modify('-24 month')]);
    }

    /**
     * Testing for latent TB before initiation of anti-TNF Therapy
     */
    public function calculateAntiTNF()
    {
        return $this->getNumerator($this->getDenominatorSQL() . '
                    AND sd.antiTNFInitiationDate IS NOT NULL
                    AND (sd.tuberculosisScreeningDate BETWEEN :fromDate AND :toDate)
                    AND DATE_DIFF(sd.antiTNFInitiationDate, sd.tuberculosisScreeningDate) <= 183
            ');
    }

    /**
     * Assessment of Hepatitis B Virus Exposure Before initiating Anti-TNF therapy
     */
    public function calculateHepatitisB()
    {
        return $this->getNumerator($this->getDenominatorSQL() . '
                    AND sd.hepatitisBStatus=1
                    AND (sd.hepatitisBStatusDate BETWEEN :fromDate AND :toDate)
                    AND DATE_DIFF(sd.antiTNFInitiationDate, sd.hepatitisBStatusDate) <= 365
                    AND DATE_DIFF(sd.antiTNFInitiationDate, sd.hepatitisBStatusDate) >= 1
            ');
    }

    /**
     * Influenza Immunization
     */
    public function calculateInfluenzaImmunization()
    {
        return $this->getNumerator($this->getDenominatorSQL() . '
                     AND (
                        (sd.influenzaImmunization=3)
                        OR   
                        (sd.influenzaImmunization=1 AND sd.influenzaImmunizationDate BETWEEN :fromDate AND :toDate)
                     )');
    }

    /**
     * Pneumococcal Immunization
     */
    public function calculatePneumococcalImmunization()
    {
        return $this->getNumerator($this->getDenominatorSQL() . '
                    AND (
                        (sd.pneumococcalVaccine=1 AND (sd.pneumococccalVaccineDate BETWEEN  :fromDate AND :toDate))
                        OR
                        sd.pneumococcalVaccine=3)
                    ');
    }


    private function getDenominatorTobaccoCessation()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLTobaccoCessation());

        return $this->getSingleResultWithParams($qb);
    }

    public function getDenominatorSQLTobaccoCessation()
    {
        return $this->getDenominatorSQL('sd.tobaccoStatus=1');


    }

    /**
     * Tobacco Status
     */
    public function calculateTobacco()
    {
        return $this->getNumerator($this->getDenominatorSQL() . '
                    AND (sd.tobaccoStatusAssessmentDate BETWEEN :fromDate AND :toDate)
            ');
    }

    /**
     * Tobacco Cessation counseling if user – and Treatment
     */
    public function calculateTobaccoCessation()
    {
        if (!($denominator = $this->getDenominatorTobaccoCessation())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLTobaccoCessation() . '
                    AND (sd.tobaccoCessationAdviceOrTreatmentDate BETWEEN :fromDate AND :toDate)
            ');

        return [$numerator, $denominator];
    }

    /**
     * Denominator for
     * Blood Pressure Control in Patients with Chronic Kidney Disease
     */
    private function getDenominatorColonoscopy()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLColonoscopy());

        return $this->getSingleResultWithParams($qb);
    }

    public function getDenominatorSQLColonoscopy()
    {
        return $this->getDenominatorSQL('(sd.crohnColitisDiagnosis=1 OR sd.ulcerativeColitisDiagnosis=1)');
    }

    /**
     * Appropriate Use of Surveillance Colonoscopy for the Prevention of Colorectal Cancer
     */
    public function calculateColonoscopy()
    {
        if (!($denominator = $this->getDenominatorColonoscopy())) return [0, 0];

        $toColonoscopyDate = clone $this->submission->getReportingEnd();

        $numerator = $this->getNumerator($this->getDenominatorSQLColonoscopy() . ' AND
                       (sd.surveillanceColonoscopy=1 AND (sd.surveillanceColonoscopyDate BETWEEN :toColonoscopyDate AND :toDate)
            )', ['toColonoscopyDate' => $toColonoscopyDate->modify('-36 month')]);

        return [$numerator, $denominator];
    }


    /**
     * Depression Screening Annually
     */
    public function calculateDepressionScreening()
    {
        return $this->getNumerator($this->getDenominatorSQL() . ' AND
                    (
                        (
                        sd.PHQ2screening = 1
                        AND sd.PHQ2screeningDate IS NOT NULL
                        AND (sd.PHQ2screeningDate BETWEEN :fromDate AND :toDate)
                        )
                    OR
                        (
                        sd.PHQ9screening = 1
                        AND sd.PHQ9screeningDate IS NOT NULL
                        AND (sd.PHQ9screeningDate BETWEEN :fromDate AND :toDate)
                        )
                    )');
    }


    /**
     * Pneumococcal Immunization
     */
    public function calculateIBDBiologics()
    {
        return $this->getNumerator($this->getDenominatorSQL() . '
                    AND sd.IBDBiologics=1
                    AND (sd.IBDBiologicsDateAssessed BETWEEN  :fromDate AND :toDate)
                    ');
    }
}