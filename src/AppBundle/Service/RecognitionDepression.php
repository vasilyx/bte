<?php
namespace AppBundle\Service;
use AppBundle\Entity\SubmissionDepression;
use AppBundle\Manager\LogManager;
use Doctrine\ORM\EntityManager;


class RecognitionDepression extends AbstractRecognition
{
    public function __construct(EntityManager $em, LogManager $logManager)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(SubmissionDepression::class);
        $this->logManager = $logManager;
    }

    /**
     * This rules of denominator use for almost all tests
     * @return mixed
     */
    public function getDenominator()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQL());

        return $this->getSingleResultWithParams($qb);
    }

    /**
     * @param string $andSql
     * @param int $minAge
     * @param int $maxAge
     * @return string
     */
    public function getDenominatorSQL($andSql = '', $minAge = 18, $maxAge = 75)
    {
        $result = 'sd.submission_id = :submissionId
                    AND sd.npi = :npi
                    AND TIMESTAMPDIFF(YEAR, sd.patientDOB, sd.lastVisitDate) >= ' . $minAge . '
                    AND TIMESTAMPDIFF(YEAR, sd.patientDOB, sd.lastVisitDate) <= ' . $maxAge . '
                    AND sd.lastVisitDate IS NOT NULL
                    AND (sd.lastVisitDate BETWEEN  :fromDate AND :toDate)';

        if ($andSql) {
            $result = $result . ' AND ' . $andSql;
        }

        return $result;
    }

    protected function getDenominatorSQLPHQ2Screening()
    {
        return $this->getDenominatorSQL('', 12);
    }

    protected function getDenominatorPHQ2Screening()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLPHQ2Screening());

        return $this->getSingleResultWithParams($qb);
    }

    public function calculatePHQ2Screening()
    {
        if (!($denominator = $this->getDenominatorPHQ2Screening())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLPHQ2Screening() . ' AND
                    (
                        sd.PHQ2ScreeningScore IS NOT NULL
                        AND sd.PHQ2ScreeningDate IS NOT NULL
                        AND (sd.PHQ2ScreeningDate BETWEEN :fromDate AND :toDate)
                    )');

        return [$numerator, $denominator];
    }

    protected function getDenominatorSQLPHQ9Screening()
    {
        return $this->getDenominatorSQL('(sd.PHQ2ScreeningScore >= 3 AND (sd.PHQ2ScreeningDate BETWEEN :fromDate AND :toDate))');
    }

    protected function getDenominatorPHQ9Screening()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLPHQ9Screening());

        return $this->getSingleResultWithParams($qb);
    }

    public function calculatePHQ9Screening()
    {
        if (!($denominator = $this->getDenominatorPHQ9Screening())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLPHQ9Screening() . ' AND
                    (
                        sd.PHQ9ScreeningScore > 0
                        AND sd.PHQ9ScreeningDate IS NOT NULL
                        AND (sd.PHQ9ScreeningDate BETWEEN :fromDate AND :toDate)
                    )');

        return [$numerator, $denominator];
    }

    protected function getDenominatorSQLPHQAdolescentScreening()
    {
        return $this->getDenominatorSQL('(sd.PHQ2ScreeningScore > 3 AND (sd.PHQ2ScreeningDate BETWEEN :fromDate AND :toDate))', 12, 17);
    }

    protected function getDenominatorPHQAdolescentScreening()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLPHQAdolescentScreening());

        return $this->getSingleResultWithParams($qb);
    }

    public function calculatePHQAdolescentScreening()
    {
        if (!($denominator = $this->getDenominatorPHQAdolescentScreening())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLPHQAdolescentScreening() . ' AND
                    (
                        sd.PHQAdolescentScreeningScore > 0
                        AND sd.PHQAdolescentScreeningDate IS NOT NULL
                        AND (sd.PHQAdolescentScreeningDate BETWEEN :fromDate AND :toDate)
                    )');
        return [$numerator, $denominator];
    }


    protected function getDenominatorSQLPositiveDepressionScreening()
    {
        $result = 'sd.submission_id = :submissionId
                    AND sd.npi = :npi
                    AND (sd.lastVisitDate BETWEEN  :fromDate AND :toDate)

                    AND ((
                        TIMESTAMPDIFF(YEAR, sd.patientDOB, sd.lastVisitDate) >= 18
                        AND TIMESTAMPDIFF(YEAR, sd.patientDOB, sd.lastVisitDate) <= 75
                        AND sd.PHQ9ScreeningScore >= 10
                        AND (sd.PHQ9ScreeningDate BETWEEN :fromDate AND :toDate)
                    ) OR (
                        TIMESTAMPDIFF(YEAR, sd.patientDOB, sd.lastVisitDate) >= 12
                        AND TIMESTAMPDIFF(YEAR, sd.patientDOB, sd.lastVisitDate) <= 17
                        AND sd.PHQAdolescentScreeningScore >= 10
                        AND (sd.PHQAdolescentScreeningDate BETWEEN :fromDate AND :toDate)
                    ))';

        return $result;
    }

    protected function getDenominatorPositiveDepressionScreening()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLPositiveDepressionScreening());

        return $this->getSingleResultWithParams($qb);
    }

    /**
     * Positive Depression Screening Follow Up
     * @return float|int
     */
    public function calculatePositiveDepressionScreening()
    {
        if (!($denominator = $this->getDenominatorPositiveDepressionScreening())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLPositiveDepressionScreening() . ' AND
                    (
                        (
                            DATE_DIFF(sd.cognitiveBehaviorTherapyDate, sd.PHQ9ScreeningDate) <= 30 
                            OR
                            DATE_DIFF(sd.PHQAdolescentScreeningDate, sd.PHQAdolescentScreeningDate) <= 30
                        ) OR
                        (
                            DATE_DIFF(sd.specialistReferralDate, sd.PHQ9ScreeningDate) <= 30 
                            OR
                            DATE_DIFF(sd.specialistReferralDate, sd.PHQAdolescentScreeningDate) <= 30
                        ) OR
                        (
                            DATE_DIFF(sd.antidepressantActiveDate, sd.PHQ9ScreeningDate) <= 30 
                            OR
                            DATE_DIFF(sd.antidepressantActiveDate, sd.PHQAdolescentScreeningDate) <= 30
                        ) OR
                        (
                            DATE_DIFF(sd.psychotherapyDate, sd.PHQ9ScreeningDate) <= 30 
                            OR
                            DATE_DIFF(sd.psychotherapyDate, sd.PHQAdolescentScreeningDate) <= 30
                        )
                    )');

        return [$numerator, $denominator];
    }

    protected function getDenominatorSQLPHQ9Screening6month()
    {
        return $this->getDenominatorSQL('(
                                (sd.depressionDiagnosis = 1 OR sd.dysthymiaDiagnosis = 1)
                                AND 
                                (
                                    (sd.cognitiveBehaviorTherapyDate BETWEEN :fromDate AND :toDate)
                                    OR (sd.specialistReferralDate BETWEEN :fromDate AND :toDate)
                                    OR (sd.antidepressantActiveDate BETWEEN :fromDate AND :toDate)
                                    OR (sd.psychotherapyDate BETWEEN :fromDate AND :toDate)
                                )
                    )', 12);
    }

    protected function getDenominatorPHQ9Screening6month()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLPHQ9Screening6month());

        return $this->getSingleResultWithParams($qb);
    }

    /**
     * PHQ-9 Screening - 6 Month Follow Up
     * @return float|int
     */
    public function calculatePositiveDepressionScreening6month()
    {
        if (!($denominator = $this->getDenominatorPHQ9Screening6month())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLPHQ9Screening6month() . ' AND
                    (
                        DATE_DIFF(sd.PHQ9Screening2Date, sd.cognitiveBehaviorTherapyDate) <= 184 
                        OR DATE_DIFF(sd.PHQ9Screening2Date, sd.specialistReferralDate) <= 184 
                        OR DATE_DIFF(sd.PHQ9Screening2Date, sd.antidepressantActiveDate) <= 184 
                        OR DATE_DIFF(sd.PHQ9Screening2Date, sd.psychotherapyDate) <= 184 
                    )');

        return [$numerator, $denominator];
    }

    protected function getDenominatorSQLAntidepressant()
    {
        return $this->getDenominatorSQL('((sd.antidepressantActiveDate BETWEEN :fromDate AND :toDate) AND (sd.depressionDiagnosis = 1 OR sd.dysthymiaDiagnosis = 1))');
    }

    protected function getDenominatorAntidepressant()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLAntidepressant());

        return $this->getSingleResultWithParams($qb);
    }

    /**
     * Continuous Antidepressant Treatment- if prescribed and effective
     * @return float|int
     */
    public function calculateAntidepressant()
    {
        if (!($denominator = $this->getDenominatorAntidepressant())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLAntidepressant() . ' AND
                    (
                        DATE_DIFF(:toDate, sd.antidepressantInitialPrescribedDate) >= 122  AND
                        DATE_DIFF(:toDate, sd.antidepressantInitialPrescribedDate) <= 365 
                    )');

        return [$numerator, $denominator];
    }

    protected function getDenominatorSQLSubstanceScreening()
    {
        return $this->getDenominatorSQL('(
        (sd.depressionDiagnosis = 1 OR sd.dysthymiaDiagnosis = 1)
            AND 
        (sd.PHQ9ScreeningScore >=5  AND sd.PHQ9ScreeningDate BETWEEN :fromDate AND :toDate)
        )');
    }

    protected function getDenominatorSubstanceScreening()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLSubstanceScreening());

        return $this->getSingleResultWithParams($qb);
    }

    /**
     * Substance Use Screening
     * @return float|int
     */
    public function calculateSubstanceScreening()
    {
        if (!($denominator = $this->getDenominatorSubstanceScreening())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLSubstanceScreening() . ' AND 
                    (sd.substanceUseScreeningDate BETWEEN :fromDate AND :toDate)
                    ');

        return [$numerator, $denominator];
    }

    protected function getDenominatorSQLSubstanceIntervention()
    {
        return $this->getDenominatorSQL('(
        sd.substanceUseScreening=1 AND 
         (sd.substanceUseScreeningDate BETWEEN :fromDate AND :toDate) AND 
         (sd.depressionDiagnosis = 1 OR sd.dysthymiaDiagnosis = 1)
         )');
    }

    protected function getDenominatorSubstanceIntervention()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLSubstanceIntervention());

        return $this->getSingleResultWithParams($qb);
    }

    /**
     * Documentation of Substance Use Intervention/Counseling - if user
     * @return float|int
     */
    public function calculateSubstanceIntervention()
    {
        if (!($denominator = $this->getDenominatorSubstanceIntervention())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLSubstanceIntervention() . ' AND 
                    (sd.substanceUseCounselingDate BETWEEN :fromDate AND :toDate)
                    ');

        return [$numerator, $denominator];
    }

    protected function getDenominatorSQLPhysicalActivity()
    {
        return $this->getDenominatorSQL('(
                        (sd.depressionDiagnosis = 1 OR sd.dysthymiaDiagnosis = 1)
                        AND (sd.PHQ9ScreeningScore >= 5 AND sd.PHQ9ScreeningDate BETWEEN :fromDate AND :toDate)
                    )');
    }

    protected function getDenominatorPhysicalActivity()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLPhysicalActivity());

        return $this->getSingleResultWithParams($qb);
    }

    /**
     * Documentation of Physical Activity Counseling
     * @return float|int
     */
    public function calculatePhysicalActivity()
    {
        if (!($denominator = $this->getDenominatorPhysicalActivity())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLPhysicalActivity() . ' AND
                    (
                        (sd.activityCounselingDate BETWEEN :fromDate AND :toDate)
                         AND sd.activityCounseling = 1
                    )');
        return [$numerator, $denominator];
    }

    protected function getDenominatorSQLCollaborativeCareModel()
    {
        return $this->getDenominatorSQL('(sd.depressionDiagnosis = 1 OR sd.dysthymiaDiagnosis=1)');
    }

    protected function getDenominatorCollaborativeCareModel()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLCollaborativeCareModel());

        return $this->getSingleResultWithParams($qb);
    }

    /**
     * Collaborative Care Model Participation
     * @return float|int
     */
    public function calculateCollaborativeCareModel()
    {
        if (!($denominator = $this->getDenominatorCollaborativeCareModel())) return [0, 0];

        $numerator1 = $this->getNumerator($this->getDenominatorSQLCollaborativeCareModel() . ' AND
                    (
                        sd.careCoordination = 1
                     )');

        $numerator2 = $this->getNumerator($this->getDenominatorSQLCollaborativeCareModel() . ' AND
                    (
                        sd.monitoringandTreatment = 1
                    )');

        $numerator3 = $this->getNumerator($this->getDenominatorSQLCollaborativeCareModel() . ' AND
                    (
                        sd.caseLoadReviews = 1
                    )');

        return [$numerator1*1 + $numerator2*2 + $numerator3*2, $denominator*5];
    }
}