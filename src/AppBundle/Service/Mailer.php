<?php

namespace AppBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;


class Mailer
{
    private $mailer;
    private $twig;

    private $container;
    private $defaultParams = [];

    private $logFileName;

    public function __construct(\Swift_Mailer $mailer, $templating, Container $container, $logFileName)
    {
        $this->mailer      = $mailer;
        $this->twig        = $templating;
        $this->container    = $container;
        $this->logFileName    = $logFileName;

        $this->setDefaultParams();
    }

    protected function setDefaultParams()
    {
        $this->defaultParams = [
            'baseUrl'       => $this->container->get('router')->getContext()->getScheme() . "://" .
                $this->container->get('router')->getContext()->getHost(),
            'frontEndUrl'   => $this->container->get('router')->getContext()->getScheme() . "://" .
                $this->container->get('router')->getContext()->getHost() . "/frontend/index.html#/"
        ];
    }

    public function sendForgottenPassword($to, $renderParams = array())
    {
        $message = $this->mailer->createMessage()
            ->setSubject('Forgotten password')
            ->setFrom($this->container->getParameter('mail_send_from'))
            ->setTo($to)
            ->setBody(
                $this->twig->render(
                    'AppBundle:emails:forgotten_password.html.twig',
                    $renderParams
                ),
                'text/html'
            );

        return $this->send($message);
    }

    public function sendResetPassword($to, $renderParams = array())
    {
        $message = $this->mailer->createMessage()
            ->setSubject('Reset password')
            ->setFrom($this->container->getParameter('mail_send_from'))
            ->setTo($to)
            ->setBody(
                $this->twig->render(
                    'AppBundle:emails:resseting_password.html.twig',
                    array_merge($renderParams, $this->defaultParams)
                ),
                'text/html'
            );

        return $this->send($message);
    }

    public function sendRegistrationConfirm($to, $renderParams = array())
    {
        $message = $this->mailer->createMessage()
            ->setSubject('Registration confirmation')
            ->setFrom($this->container->getParameter('mail_send_from'))
            ->setTo($to)
            ->setBody(
                $this->twig->render(
                    'AppBundle:emails:confirm_reg.html.twig',
                    array_merge($renderParams, $this->defaultParams)
                ),
                'text/html'
            );

        return $this->send($message);
    }

    private function send($message)
    {
        if (!$this->mailer->getTransport()->isStarted()) {
            $this->mailer->getTransport()->start();
        }

        $sent = $this->mailer->send($message);
        $this->saveLog($message);
        $this->mailer->getTransport()->stop(); // should fix: Expected response code 250 but got code "451", with message "451 4.4.2 Timeout - closing connection. l6sm253049wjz.4 - gsmtp
        return $sent;
    }

    private function saveLog($messages){
        $content = '';
        if(file_exists($this->logFileName)){
            $content = file_get_contents($this->logFileName);
        }
        file_put_contents($this->logFileName, $content . $messages);
    }
}
