<?php

namespace AppBundle\Service;

use AppBundle\Entity\Measure;
use AppBundle\Entity\Provider;
use AppBundle\Entity\RecognitionDetail;
use AppBundle\Entity\Submission;
use AppBundle\Manager\LogManager;
use Doctrine\ORM\QueryBuilder;

abstract class AbstractRecognition
{
    protected $em;
    protected $repository;
    protected $globalDenominator;

    /**
     * @var LogManager
     */
    protected $logManager;

    /**
     * @var Submission
     */
    protected $submission;
    protected $npi; // current npi

    /**
     * @var Measure $current_measure
     */
    protected $current_measure;

    public function getTotal(Submission $submission)
    {
        $this->submission = $submission;

        $result = array();

        /**
         * @var $provider Provider
         */
        foreach ($this->submission->getProvider() as $provider) {

            $this->npi = $provider->getNpi();

            $this->globalDenominator = $this->getDenominator();

            $this->logManager->setMessage('Recognition for provider ' . $provider->getId() . '(npi: ' . $this->npi . ') - started',
                $this->submission->getId(),
                null,
                LogManager::TYPE_RECOGNITION,
                LogManager::STATUS_PROGRESS);

            if ($this->globalDenominator < 1) {
                $result[$this->npi] = null;
                $description = 'Recognition for provider ' . $provider->getId() . '(npi: ' . $this->npi . ') - no records. ';
            } else {
                $result[$this->npi] = $this->calculateAllMeasures();
                $description = 'Recognition for provider ' . $provider->getId() . '(npi: ' . $this->npi . ') - completed. ';
            }

            $this->logManager->setMessage($description,
                $this->submission->getId(),
                null,
                LogManager::TYPE_RECOGNITION,
                LogManager::STATUS_PROGRESS);
        }

        return $result;
    }

    abstract protected function getDenominatorSQL();

    /**
     *
     */
    protected function calculateAllMeasures()
    {
        $measures = $this->submission->getProgram()->getMeasures();
        $total = 0;
        $sum_num = 0;
        $sum_den = 0;
        $details = [];

        /**
         * @var Measure $measure
         */
        foreach ($measures as $measure) {

            if (!$measure->getPublished()) continue;
                $func_name = 'calculate' . $measure->getFuncName();
                $this->current_measure = $measure;

                if (method_exists($this, $func_name)) {
                    $calc_res = $this->$func_name();

                    $entity = new RecognitionDetail();
                    $entity->setMeasure($measure);

                    if (is_array($calc_res)) {
                        $res = $this->intervalResult($calc_res[0], $calc_res[1]);
                        $entity->setNumerator($calc_res[0]);
                        $entity->setDenominator($calc_res[1]);
                    } else {
                        $res = $this->intervalResult($calc_res);
                        $entity->setNumerator($calc_res);
                        $entity->setDenominator($this->globalDenominator);
                    }

                    $total += $res;
                    $sum_num = $sum_num + $entity->getNumerator();
                    $sum_den = $sum_den + $entity->getDenominator();

                    $entity->setScore($res);
                    array_push($details, $entity);
                }
        }

        return ['total' => $total,
                'numerator' => $sum_num,
                'denominator' => $sum_den,
                'group_id' => ($this->submission->getReportingFor() == 2) ? $this->getDAGroup() : null,
                'details' => $details
        ];
    }

    /**
     * @param QueryBuilder $qb
     * @param null $toDate
     * @return mixed
     */
    protected function getSingleResultWithParams(QueryBuilder $qb, $toDate = null)
    {
        $toDate = $toDate ?? $this->submission->getReportingEnd();

        $qb->setParameter('submissionId', $this->submission->getId())
            ->setParameter('npi', $this->npi)
            ->setParameter('toDate', $toDate)
            ->setParameter('fromDate', $this->submission->getReportingStart());

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param string $where
     * @param array $params
     */
    protected function getNumerator($where = '', $params = [])
    {

        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($where);

        $qb->setParameter('submissionId', $this->submission->getId())
            ->setParameter('npi', $this->npi)
            ->setParameter('toDate', $this->submission->getReportingEnd())
            ->setParameter('fromDate', $this->submission->getReportingStart());

        if ($params) {
            foreach ($params as $param => $value) {
                $qb->setParameter($param, $value);
            }
        }

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * This rules of denominator use for almost all tests
     * @return mixed
     */
    protected function getDenominator()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQL());

        return $this->getSingleResultWithParams($qb);
    }

    /**
     * @param $numerator
     * @param null $denominator
     * @return float
     * @internal param $total
     */
    protected function intervalResult($numerator, $denominator = null)
    {
        $total = $this->current_measure->getTotal();

        if ($denominator) {
            if ($denominator == 0) return 0;

            $result = ($numerator / $denominator) * $total;
        } else {
            if ($this->globalDenominator == 0) return 0;
            $result = ($numerator / $this->globalDenominator) * $total;
        }

        return round($result, 2);
    }

    /**
     * Get dataAggregator group
     * @return mixed
     */
    protected function getDAGroup()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('sd.groupID')
            ->where('sd.submission_id = ' . $this->submission->getId() . '
                AND sd.npi = ' . $this->npi)
        //        ' AND sd.individualGroup = \'G\'')
            ->groupBy('sd.groupID');
// TODO ADD ' AND sd.individualGroup = \'G\'')

        $result = $qb->getQuery()->getResult();

        if (isset($result[0]) && isset($result[0]['groupID'])) {
            return $result[0]['groupID'];
        } else {
            return null;
        }
    }
}
