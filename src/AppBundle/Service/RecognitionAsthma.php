<?php
namespace AppBundle\Service;

use AppBundle\Entity\SubmissionAsthma;
use AppBundle\Manager\LogManager;
use Doctrine\ORM\EntityManager;


class RecognitionAsthma extends AbstractRecognition
{
    public function __construct(EntityManager $em, LogManager $logManager)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(SubmissionAsthma::class);
        $this->logManager = $logManager;
    }

    /**
     * This rules of denominator use for almost all tests
     * @return mixed
     */
    public function getDenominator()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQL());

        return $this->getSingleResultWithParams($qb);
    }

    /**
     * @param string $andSql
     * @param int $minAge
     * @param int $maxAge
     * @return string
     */
    public function getDenominatorSQL($andSql = '', $minAge = 5, $maxAge = 80)
    {
        $result = 'sd.submission_id = :submissionId
                    AND sd.npi = :npi
                    AND sd.asthmaDiagnosis = 1
                    AND TIMESTAMPDIFF(YEAR, sd.patientDOB, sd.lastVisitDate) >= ' . $minAge . '
                    AND TIMESTAMPDIFF(YEAR, sd.patientDOB, sd.lastVisitDate) <= ' . $maxAge . '
                    AND sd.lastVisitDate IS NOT NULL
                    AND (sd.lastVisitDate >= :fromDate AND sd.lastVisitDate <= :toDate)';

        if ($andSql) {
            $result = $result . ' AND ' . $andSql;
        }

        return $result;
    }

    /**
     * Disease Severity (including spirometry)
     * @return float
     */
    public function calculateDiseaseSeverity()
    {
        return $this->getNumerator($this->getDenominatorSQL() . '
                     AND 
                     (
                        (
                            TIMESTAMPDIFF(YEAR, sd.patientDOB, sd.lastVisitDate) <= ' . 17 . '
                            AND sd.spirometryEvalResults IS NOT NULL
                            AND (sd.spirometryEvalDate BETWEEN  :fromDate AND :toDate)
                        )
                        OR
                        (
                            TIMESTAMPDIFF(YEAR, sd.patientDOB, sd.lastVisitDate) >= ' . 18 . "
                            AND (sd.asthmaDiseaseSeverityDate BETWEEN  :fromDate AND :toDate)
                            AND sd.asthmaDiseaseSeverity IN (1, 2, 3, 4)
                        )
                     )");
    }

    /**
     * Short Acting Beta Agonist Prescribed to All Patients with Asthma
     * @return float
     */
    public function calculateBetaAgonistPrescribed()
    {
        return $this->getNumerator($this->getDenominatorSQL() . '
                     AND (sd.SABAPrescribedDate BETWEEN  :fromDate AND :toDate)');
    }

    private function getDenominatorAsthmaControllerMedication()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLAsthmaControllerMedication());

        return $this->getSingleResultWithParams($qb);
    }

    private function getDenominatorSQLAsthmaControllerMedication()
    {
        return $this->getDenominatorSQL('(sd.asthmaDiseaseSeverity IN (2, 3, 4) 
                        AND (sd.asthmaDiseaseSeverityDate BETWEEN  :fromDate AND :toDate))');
    }

    /**
     * Patients with moderate/severe asthma exacerbations should be prescribed oral corticosteroids
     */
    public function calculateAsthmaControllerMedication()
    {
        if (!($denominator = $this->getDenominatorAsthmaControllerMedication())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLAsthmaControllerMedication() . ' 
                    AND (
                        (sd.oralCorticosteroidPrescribedDate BETWEEN  :fromDate AND :toDate)
                        OR (sd.inhaledCorticosteroidDispensedDate BETWEEN  :fromDate AND :toDate)
                        OR (sd.inhaledCorticosteroidPrescribedDate BETWEEN  :fromDate AND :toDate)
                        )');

        return [$numerator, $denominator];
    }

    /**
     * Patients with asthma should not be taking long-acting beta agonists (LABA) alone
     * @return float
     */
    public function calculateAsthmaBetaAgonist()
    {
        return $this->getNumerator($this->getDenominatorSQL() . '
                     AND (
                        (sd.asthmaMedicationPrescribedDate BETWEEN  :fromDate AND :toDate) AND 
                        (   
                            (sd.inhaledBronchodilatorPrescribedDate BETWEEN  :fromDate AND :toDate)
                            OR (sd.oralBeta3AgonistsDispensedDate BETWEEN  :fromDate AND :toDate)
                            OR (sd.oralBeta3AgonistsPrescribedDate BETWEEN  :fromDate AND :toDate)
                        )
                     )');
    }

    private function getDenominatorAsthmaExacerbations()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLAsthmaExacerbations());

        return $this->getSingleResultWithParams($qb);
    }

    private function getDenominatorSQLAsthmaExacerbations()
    {
        return $this->getDenominatorSQL('(sd.asthmaDiseaseSeverity IN (3, 4) 
                        AND (sd.asthmaDiseaseSeverityDate BETWEEN  :fromDate AND :toDate))');
    }

    /**
     * Patients with moderate/severe asthma exacerbations should be prescribed oral corticosteroids
     */
    public function calculateAsthmaExacerbations()
    {
        if (!($denominator = $this->getDenominatorAsthmaExacerbations())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLAsthmaExacerbations() . ' 
                    AND 
                        (sd.oralCorticosteroidPrescribedDate BETWEEN  :fromDate AND :toDate)
                        ');

        return [$numerator, $denominator];
    }


    private function getDenominatorAsthmaExacerbations10days()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLAsthmaExacerbations10days());

        return $this->getSingleResultWithParams($qb);
    }

    private function getDenominatorSQLAsthmaExacerbations10days()
    {
        return $this->getDenominatorSQL('(sd.asthmaExacerbationSeverity IN (2, 3, 4) 
                        AND (sd.asthmaExacerbationDate BETWEEN  :fromDate AND :toDate))');
    }

    /**
     * Patients with moderate/ severe asthma exacerbation should have follow-up within 10 days
     */
    public function calculateAsthmaExacerbations10days()
    {
        if (!($denominator = $this->getDenominatorAsthmaExacerbations10days())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLAsthmaExacerbations10days() . ' 
                     AND (
                        (sd.asthmaExacerbationReferralDate IS NOT NULL AND DATE_DIFF(sd.asthmaExacerbationReferralDate, sd.asthmaExacerbationDate) <=10)
                        OR (sd.faceToFaceVisit1 IS NOT NULL AND DATE_DIFF(sd.faceToFaceVisit1, sd.asthmaExacerbationDate) <=10)
                        OR (sd.faceToFaceVisit2 IS NOT NULL AND DATE_DIFF(sd.faceToFaceVisit2, sd.asthmaExacerbationDate) <=10)
                     )');

        return [$numerator, $denominator];
    }

    private function getDenominatorPersistentAsthma()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLPersistentAsthma());

        return $this->getSingleResultWithParams($qb);
    }

    private function getDenominatorSQLPersistentAsthma()
    {
        return $this->getDenominatorSQL('(sd.asthmaDiseaseSeverity IN (2, 3, 4))');
    }

    /**
     * Patients with persistent asthma (any severity) should have 2 annual visits with their care team
     */
    public function calculatePersistentAsthma()
    {
        if (!($denominator = $this->getDenominatorPersistentAsthma())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLPersistentAsthma() . '
                            AND (sd.faceToFaceVisit1 BETWEEN  :fromDate AND :toDate)
                            AND (sd.faceToFaceVisit2 BETWEEN  :fromDate AND :toDate)
                     ');

        return [$numerator, $denominator];
    }

    private function getDenominatorAsthmaActionPlan()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLAsthmaActionPlan());

        return $this->getSingleResultWithParams($qb);
    }

    private function getDenominatorSQLAsthmaActionPlan()
    {
        return $this->getDenominatorSQL('', 12);
    }

    /**
     * Patients with asthma should have an “Asthma Action Plan” or “Asthma Self-Management Plan” documented
     */
    public function calculateAsthmaActionPlan()
    {
        if (!($denominator = $this->getDenominatorAsthmaActionPlan())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLAsthmaActionPlan() . ' 
                     AND (
                            (sd.asthmaActionPlanDate BETWEEN  :fromDate AND :toDate)
                            AND (sd.asthmaSelfManagementPlanDate BETWEEN  :fromDate AND :toDate)
                     )');

        return [$numerator, $denominator];
    }

    private function getDenominatorInhalerTechnique()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLInhalerTechnique());

        return $this->getSingleResultWithParams($qb);
    }

    private function getDenominatorSQLInhalerTechnique()
    {
        return $this->getDenominatorSQL('', 5, 11);
    }

    /**
     * Documentation of proper inhaler technique should be provided (5-11 years old)
     */
    public function calculateInhalerTechnique()
    {
        if (!($denominator = $this->getDenominatorInhalerTechnique())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLInhalerTechnique() . ' 
                     AND (
                        (sd.inhalerEducationDate BETWEEN  :fromDate AND :toDate)
                        AND (
                            (sd.inhaledCorticosteroidDispensedDate BETWEEN  :fromDate AND :toDate)
                            OR (sd.inhaledCorticosteroidPrescribedDate BETWEEN  :fromDate AND :toDate)
                            OR (sd.inhaledBronchodilatorPrescribedDate BETWEEN  :fromDate AND :toDate)
                        )
                     )');
        return [$numerator, $denominator];
    }

    /**
     * Tobacco Use / Tobacco Exposure Status should be assessed and documented
     */
    public function calculateTobacco()
    {
        return $this->getNumerator($this->getDenominatorSQL() . '
                     AND 
                     (
                       (
                            TIMESTAMPDIFF(YEAR, sd.patientDOB, sd.lastVisitDate) <=  11
                            AND sd.tobaccoExposureStatus IS NOT NULL
                            AND (sd.tobaccoExposureStatusDate BETWEEN  :fromDate AND :toDate)
                        )
                        OR
                        (
                            TIMESTAMPDIFF(YEAR, sd.patientDOB, sd.lastVisitDate) >= 12
                            AND sd.tobaccoStatus IN (0, 2, 3, 4, 5)
                            AND (sd.tobaccoStatusAssessmentDate BETWEEN  :fromDate AND :toDate)
                        )
                     )');
    }

    private function getDenominatorTobaccoCessation()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLTobaccoCessation());

        return $this->getSingleResultWithParams($qb);
    }

    private function getDenominatorSQLTobaccoCessation()
    {
        return $this->getDenominatorSQL('(sd.tobaccoStatus IN (2,3,4,5) AND (sd.tobaccoStatusAssessmentDate BETWEEN :fromDate AND :toDate)) ', 12);
    }


    public function calculateTobaccoCessation()
    {
        if (!($denominator = $this->getDenominatorTobaccoCessation())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLTobaccoCessation() . '
                    AND (sd.tobaccoCessationAdviceOrTreatmentDate BETWEEN :fromDate AND :toDate)
                     ');

        return [$numerator, $denominator];
    }

    /**
     * Influenza Immunization
     */
    public function calculateInfluenzaImmunization()
    {
        return $this->getNumerator($this->getDenominatorSQL() . '
                     AND (
                            ((sd.influenzaImmunizationDate BETWEEN :fromDate AND :toDate) 
                                AND sd.influenzaImmunization = 1)
                                OR sd.influenzaImmunization = 3
                        )');
    }
}