<?php

namespace AppBundle\Service;

use AppBundle\Entity\SubmissionCOPD;
use AppBundle\Manager\LogManager;
use Doctrine\ORM\EntityManager;


class RecognitionCOPD extends AbstractRecognition
{
    public function __construct(EntityManager $em, LogManager $logManager)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(SubmissionCOPD::class);
        $this->logManager = $logManager;
    }

    /**
     * This rules of denominator use for almost all tests
     * @return mixed
     */
    public function getDenominator()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQL());

        return $this->getSingleResultWithParams($qb);
    }

    /**
     * @param string $andSql
     * @param int $minAge
     * @param int $maxAge
     * @return string
     */
    public function getDenominatorSQL($andSql = '', $minAge = 18, $maxAge = 80)
    {
        $result = 'sd.submission_id = :submissionId
                    AND sd.npi = :npi
                    AND sd.COPDDiagnosis = 1
                    AND TIMESTAMPDIFF(YEAR, sd.patientDOB, sd.lastVisitDate) >= ' . $minAge . '
                    AND TIMESTAMPDIFF(YEAR, sd.patientDOB, sd.lastVisitDate) <= ' . $maxAge . '
                    AND sd.lastVisitDate IS NOT NULL
                    AND (sd.lastVisitDate BETWEEN  :fromDate AND :toDate)';

        if ($andSql) {
            $result = $result . ' AND ' . $andSql;
        }

        return $result;
    }

    /**
     * COPD Severity Assessed and Recorded, including Spirometry
     */
    public function calculateSeverityAssessed()
    {
        return $this->getNumerator($this->getDenominatorSQL() . ' AND
                    (
                        sd.spirometryEvalResults IS NOT NULL
                        AND (sd.spirometryEvalDate BETWEEN :fromDate AND :toDate)
                    )');
    }

    private function getDenominatorInhaledBronchodilatorTherapyFEV1()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLInhaledBronchodilatorTherapyFEV1());

        return $this->getSingleResultWithParams($qb);
    }

    private function getDenominatorSQLInhaledBronchodilatorTherapyFEV1()
    {
        return $this->getDenominatorSQL('(sd.spirometryEvalResults <60 AND sd.COPDSymptoms=1)');
    }

    /**
     * Inhaled Bronchodilator Therapy: LAMA/LABA for Patients with FEV1 <60% Predicted
     */
    public function calculateInhaledBronchodilatorTherapyFEV1()
    {
        if (!($denominator = $this->getDenominatorInhaledBronchodilatorTherapyFEV1())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLInhaledBronchodilatorTherapyFEV1() . ' 
                    AND (sd.inhaledBronchodilatorPrescribedDate BETWEEN  :fromDate AND :toDate)
                     ');

        return [$numerator, $denominator];
    }

    private function getDenominatorInhaledBronchodilatorTherapy()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLInhaledBronchodilatorTherapy());

        return $this->getSingleResultWithParams($qb);
    }

    private function getDenominatorSQLInhaledBronchodilatorTherapy()
    {
        return $this->getDenominatorSQL('(sd.spirometryEvalResults <60 AND sd.COPDSymptoms=1) AND ((sd.inhaledCorticosteroidDispensedDate BETWEEN  :fromDate AND :toDate) OR (sd.inhaledCorticosteroidPrescribedDate BETWEEN  :fromDate AND :toDate))');
    }

    /**
     * Inhaled Bronchodilator Therapy: Inhaled Corticosteroid Therapy only in Combination with LAMA/LABA
     */
    public function calculateInhaledBronchodilatorTherapy()
    {
        if (!($denominator = $this->getDenominatorInhaledBronchodilatorTherapy())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLInhaledBronchodilatorTherapy() . ' 
                      AND (sd.inhaledBronchodilatorPrescribedDate BETWEEN  :fromDate AND :toDate)
                     ');

        return [$numerator, $denominator];
    }

    /**
     * Tobacco Status
     */
    public function calculateTobacco()
    {
        return $this->getNumerator($this->getDenominatorSQL() . ' 
                    AND sd.tobaccoStatus IN (0, 2,3,4,5,6)
                    AND (sd.tobaccoStatusAssessmentDate BETWEEN :fromDate AND :toDate)
                     ');
    }

    private function getDenominatorTobaccoCessation()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLTobaccoCessation());

        return $this->getSingleResultWithParams($qb);
    }

    private function getDenominatorSQLTobaccoCessation()
    {
        return $this->getDenominatorSQL('((sd.tobaccoStatusAssessmentDate BETWEEN :fromDate AND :toDate) AND sd.tobaccoStatus > 0)');
    }


    public function calculateTobaccoCessation()
    {
        if (!($denominator = $this->getDenominatorTobaccoCessation())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLTobaccoCessation() . '
                    AND (sd.tobaccoCessationAdviceOrTreatmentDate BETWEEN :fromDate AND :toDate)
                     ');
        return [$numerator, $denominator];
    }

    private function getDenominatorAssessmentOxygenSaturation()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLAssessmentOxygenSaturation());

        return $this->getSingleResultWithParams($qb);
    }

    private function getDenominatorSQLAssessmentOxygenSaturation()
    {
        return $this->getDenominatorSQL('sd.spirometryEvalResults <40 AND (sd.respiratoryFailureDiagnosis=1 OR sd.rightHeartFailureDiagnosis=1)');
    }


    /**
     * Assessment of Oxygen Saturation
     * @return float|int
     */
    public function calculateAssessmentOxygenSaturation()
    {
        if (!($denominator = $this->getDenominatorAssessmentOxygenSaturation())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLAssessmentOxygenSaturation() . '
                    AND (
                        sd.oxygenSaturationLevel IS NOT NULL AND (sd.oxygenSaturationLevelDate BETWEEN :fromDate AND :toDate)
                     )');
        return [$numerator, $denominator];
    }

    private function getDenominatorLongTermOxygenTherapy()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLLongTermOxygenTherapy());

        return $this->getSingleResultWithParams($qb);
    }

    private function getDenominatorSQLLongTermOxygenTherapy()
    {
        return $this->getDenominatorSQL('(
                                (
                                    sd.spirometryEvalResults <= 88 AND (sd.spirometryEvalDate BETWEEN :fromDate AND :toDate)
                                )
                             OR (
                                    sd.paO2Level <= 55 AND (sd.paO2LevelDate BETWEEN :fromDate AND :toDate)
                                )
        )');
    }


    /**
     * Long Term Oxygen Therapy
     * @return float|int
     */
    public function calculateLongTermOxygenTherapy()
    {
        if (!($denominator = $this->getDenominatorLongTermOxygenTherapy())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLLongTermOxygenTherapy() . '
                    AND (sd.longTermOxygenTherapyDate BETWEEN :fromDate AND :toDate)
                     ');
        return [$numerator, $denominator];
    }

    /**
     * Influenza Immunization
     */
    public function calculateInfluenzaImmunization()
    {
        return $this->getNumerator($this->getDenominatorSQL() . '
                     AND (
                        (sd.influenzaImmunization=1 OR sd.influenzaImmunization=3)
                        AND   
                        (sd.influenzaImmunizationDate BETWEEN :fromDate AND :toDate)
                     )');
    }

    private function getDenominatorPneumoniaImmunizationPCV13()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLPneumoniaImmunizationPCV13());

        return $this->getSingleResultWithParams($qb);
    }

    private function getDenominatorSQLPneumoniaImmunizationPCV13()
    {
        return $this->getDenominatorSQL('', 66, 80);
    }

    /**
     * Pneumonia Immunization (PCV 13)
     */
    public function calculatePneumoniaImmunizationPCV13()
    {
        if (!($denominator = $this->getDenominatorPneumoniaImmunizationPCV13())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLPneumoniaImmunizationPCV13() . '
                    AND (
                        (sd.pneumococcalVaccinePCV13=1 OR sd.pneumococcalVaccinePCV13=3)
                        AND   
                        (sd.pneumococcalVaccinePCV13Date BETWEEN :fromDate AND :toDate)
                     )');

        return [$numerator, $denominator];
    }

    private function getDenominatorPneumoniaImmunizationPCV23()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLPneumoniaImmunizationPCV23());

        return $this->getSingleResultWithParams($qb);
    }

    private function getDenominatorSQLPneumoniaImmunizationPCV23()
    {
        return $this->getDenominatorSQL('', 67, 80);
    }

    /**
     * Pneumonia Immunization (PCV 23)
     */
    public function calculatePneumoniaImmunizationPCV23()
    {
        if (!($denominator = $this->getDenominatorPneumoniaImmunizationPCV23())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLPneumoniaImmunizationPCV23() . '
                    AND (
                        (sd.pneumococcalVaccinePCV23=1 OR sd.pneumococcalVaccinePCV23=3)
                        AND   
                        (sd.pneumococcalVaccinePCV23Date BETWEEN :fromDate AND :toDate)
                     )');
        return [$numerator, $denominator];
    }

    private function getDenominatorFollowUpHospitalAdmission()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLFollowUpHospitalAdmission());

        return $this->getSingleResultWithParams($qb);
    }

    private function getDenominatorSQLFollowUpHospitalAdmission()
    {
        return $this->getDenominatorSQL('(sd.COPDExacerbationEpisodeDate  BETWEEN :fromDate AND :toDate)');
    }


    /**
     * Follow-Up after Hospital Admission with COPD exacerbation
     * @return float|int
     */
    public function calculateFollowUpHospitalAdmission()
    {
        if (!($denominator = $this->getDenominatorFollowUpHospitalAdmission())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLFollowUpHospitalAdmission() . '
                    AND (
                        DATE_DIFF(sd.COPDExacerbationEpisodeStateDate, sd.COPDExacerbationEpisodeDate) <= 30
                        OR DATE_DIFF(sd.faceToFaceVisit, sd.COPDExacerbationEpisodeDate) <= 30
                     )');
        return [$numerator, $denominator];
    }

    private function getDenominatorLungCancerScreening()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLLungCancerScreening());

        return $this->getSingleResultWithParams($qb);
    }

    private function getDenominatorSQLLungCancerScreening()
    {
        return $this->getDenominatorSQL('(sd.tobaccoStatus IN (3, 4, 5))', 55);
    }


    /**
     * Lung Cancer Screening with Low Dose CT
     * @return float|int
     */
    public function calculateLungCancerScreening()
    {
        if (!($denominator = $this->getDenominatorLungCancerScreening())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLLungCancerScreening() . '
                    AND (
                        (sd.lowDoseCTDate BETWEEN :fromDate AND :toDate)
                        OR sd.lowDoseCTReferral=1 
                        OR sd.lowDoseCTDocumented=1
                     )');
        return [$numerator, $denominator];
    }


    private function getDenominatorAdvanceHealthCareDirectives()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLAdvanceHealthCareDirectives());

        return $this->getSingleResultWithParams($qb);
    }

    private function getDenominatorSQLAdvanceHealthCareDirectives()
    {
        return $this->getDenominatorSQL('', 60);
    }

    /**
     * Advance Health Care Directives for Patients with Severe COPD
     */
    public function calculateAdvanceHealthCareDirectives()
    {
        if (!($denominator = $this->getDenominatorAdvanceHealthCareDirectives())) return [0, 0];

        $numerator = $this->getNumerator($this->getDenominatorSQLAdvanceHealthCareDirectives() . '
                     AND (
                            sd.spirometryEvalResults <50 
                            AND (sd.spirometryEvalDate BETWEEN :fromDate AND :toDate)
                            AND sd.advancedDirective=1
                     )');
        return [$numerator, $denominator];
    }

    private function getDenominatorAssessmentExacerbations()
    {
        $qb = $this->repository->createQueryBuilder('sd');
        $qb->select('count(sd.id)')
            ->where($this->getDenominatorSQLAssessmentExacerbations());

        return $this->getSingleResultWithParams($qb);
    }

    private function getDenominatorSQLAssessmentExacerbations()
    {
        return $this->getDenominatorSQL('', 60, 80);
    }


    /**
     * Assessment of COPD Exacerbations
     * @return float|int
     */
    public function calculateAssessmentExacerbations()
    {
        if (!($denominator = $this->getDenominatorAssessmentExacerbations())) return [0, 0];


        $numerator = $this->getNumerator($this->getDenominatorSQLAssessmentExacerbations() . '
                    AND (
                       sd.COPDExacerbationEpisode IS NOT NULL
                     )');

        return [$numerator, $denominator];
    }
}