<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

use Symfony\Component\Console\Input\InputArgument;


class ChangePasswordCommand extends ContainerAwareCommand
{

    protected function configure(){

        $this
            ->setName('app:change-password')
            ->setDescription('Change password for admin user.')
            ->setHelp('This command allows you to change password for admin user')
            ->addArgument('password', InputArgument::REQUIRED, 'The new password of the admin user.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output){

        $this->getContainer()->get('bte.manager.user_manager')->updatePasswordForAdminUser($input->getArgument('password'));
        $output->writeln('Password for admin user was changed.');
    }
}