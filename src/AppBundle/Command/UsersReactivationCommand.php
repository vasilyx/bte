<?php

namespace AppBundle\Command;

use AppBundle\Entity\User;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

use Symfony\Component\Console\Input\InputArgument;


class UsersReactivationCommand extends ContainerAwareCommand
{

    protected function configure(){

        $this
            ->setName('app:users-reactivation')
            ->setDescription('Deactivate all user and send email confirmation letter.')
            ->setHelp('This command allows you to deactivate all user and send email confirmation letter')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output){

        $bteMailer = $this->getContainer()->get('bte_mailer');
        $userManager = $this->getContainer()->get('bte.manager.user_manager');
        $users = $userManager->getAllUser();
        foreach ($users as $user){
            if($user->getId() == 1){
                continue;
            }
            $user->generateConfirmationToken();
            $user->setStatus(User::STATUS_INACTIVE);
            $userManager->saveUser($user, true);
            $bteMailer->sendRegistrationConfirm(
                $user->getUsername(),
                [
                    'name'  => $user->getFullName(),
                    'link'  => "http://portal.bridgestoexcellence.org" .
                        '/confirm_email?token=' . $user->getConfirmEmailToken() .
                        '&username=' . $user->getUsername()
                ]
            );
        }
        $output->writeln('Users was deactivated and confirmation letters was sent.');
    }
}