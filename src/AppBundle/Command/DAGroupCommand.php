<?php

namespace AppBundle\Command;

use AppBundle\Entity\RecognitionDetail;
use AppBundle\Entity\Submission;
use AppBundle\Entity\User;
use Proxies\__CG__\AppBundle\Entity\Recognition;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

use Symfony\Component\Console\Input\InputArgument;


class DAGroupCommand extends ContainerAwareCommand
{

    protected function configure(){

        $this
            ->setName('app:da-group')
            ->setDescription('Data Aggregator fix group.')
            ->setHelp('This command allows you to fix already loaded data aggregator submissions')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $submissionManager = $this->getContainer()->get('bte.manager.submission');
        $recognitionManager = $this->getContainer()->get('bte.manager.recognition');
        $doctrine = $this->getContainer()->get('doctrine');
        $submissions = $submissionManager->getDataAggregatorSubmission();

        $recognition = $doctrine->getRepository('AppBundle:Recognition');

        /**
         * @var $submission Submission
         */
        foreach ($submissions as $submission){
            $repository = $doctrine->getRepository('AppBundle:Submission' . $submission->getProgram()->getTableName());
            foreach ($submission->getProvider() as $provider) {
                $qb = $repository->createQueryBuilder('sd');
                $qb->select('sd.groupID')
                    ->where('sd.submission_id = ' . $submission->getId() . '
                         AND sd.npi = ' . $provider->getNpi())
                    //        )
                    ->groupBy('sd.groupID');
// TODO ADD ' AND sd.individualGroup = \'G\'')

                $groupID = $qb->getQuery()->getOneOrNullResult();

                if ($groupID && $groupID['groupID']) {
                    $recognition->createQueryBuilder('u')
                        ->update()
                        ->set('u.groupID', '?1')
                        ->setParameter(1, $groupID['groupID'])
                        ->where('u.submission = ?2 and u.provider_id = ?3')
                        ->setParameter(2, $submission)
                        ->setParameter(3, $provider->getId())
                        ->getQuery()->getResult();
                }
            }
        }

        foreach ($submissions as $submission){
            $recognitionManager->setSubmissionGroupScore($submission);

        }

        $output->writeln('Table recognition(group_id, group_score) was filled.');
    }
}