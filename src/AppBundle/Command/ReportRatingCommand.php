<?php

namespace AppBundle\Command;

use AppBundle\Entity\Recognition;
use AppBundle\Entity\Submission;
use AppBundle\Entity\User;
use AppBundle\Manager\SubmissionManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

use Symfony\Component\Console\Input\InputArgument;


class ReportRatingCommand extends ContainerAwareCommand
{

    protected function configure(){

        $this
            ->setName('app:report-fill')
            ->setDescription('Recognition fill star rating.')
            ->setHelp('This command allows you to fix already loaded data aggregator submissions')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        /**
         * @var $submissionManager SubmissionManager
         */
        $recognitionManager = $this->getContainer()->get('bte.manager.recognition');
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getEntityManager();


        $repository = $doctrine->getRepository(Recognition::class);
        $qb = $repository->createQueryBuilder('sd');
        $qb->select()
             ->orderBy('sd.id', 'DESC');

        $recognitions = $qb->getQuery()->getResult();

        /**
         * @var $recognition Recognition
         */
        foreach ($recognitions as $recognition){
            $modifiedAt = clone $recognition->getSubmission()->getModifiedAt();
            $modifiedAt->modify('+2 year');


            $sub_recognition = $repository->getRecognitionForPDFProgram(
                $recognition->getSubmission()->getUser(),
                $recognition->getSubmission()->getProgram(),
                $recognition->getSubmission()->getId(),
                $recognition->getProvider()
            );

            $asset = $recognitionManager->getScoreAsset($recognition->getScore());
            $score_achieved = $recognitionManager->getScoreAchieved($asset, $sub_recognition);

            $recognition->setStarRatingAssesed($asset);
            $recognition->setStarRatingAchived($score_achieved);
            $recognition->setExpirationDate($modifiedAt);

            $em->persist($recognition);
            $em->flush();
        }

        $output->writeln('Table recognition(star_rating_asset, star_rating_achived) was filled.');
    }
}