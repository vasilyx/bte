<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;



class CleanDbCommand extends ContainerAwareCommand
{

    protected function configure(){

        $this
            ->setName('app:clean-db')
            ->setDescription('Clean data base.')
            ->setHelp('This command allows you to clean data base')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output){
        $this->getContainer()->get('bte.manager.common')->cleanDb();
        $output->writeln('Database was cleaned.');
    }
}