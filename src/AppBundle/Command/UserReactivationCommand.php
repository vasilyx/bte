<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

use Symfony\Component\Console\Input\InputArgument;

use AppBundle\Entity\User;


class UserReactivationCommand extends ContainerAwareCommand
{

    protected function configure(){

        $this
            ->setName('app:user-reactivation')
            ->setDescription('Deactivate user and send email confirmation letter.')
            ->setHelp('This command allows you to deactivate user and send email confirmation letter')
            ->addArgument('user_id', InputArgument::REQUIRED, 'Reactivation user username')
            ->addArgument('url', InputArgument::REQUIRED, 'Url of th site')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output){

        $bteMailer = $this->getContainer()->get('bte_mailer');
        $userManager = $this->getContainer()->get('bte.manager.user_manager');
        $userId = $input->getArgument('user_id');
        $url = $input->getArgument('url');

        $user = $userManager->getUserById($userId);

        if(is_array($user)){
            $user = $user[0];
        }
        if($user instanceof User ) {
            $user->generateConfirmationToken();
            $user->setStatus(User::STATUS_INACTIVE);
            $userManager->saveUser($user, true);
            $bteMailer->sendRegistrationConfirm(
                $user->getUsername(),
                [
                    'name' => $user->getFullName(),
                    'link' => $url .
                        '/confirm_email?token=' . $user->getConfirmEmailToken() .
                        '&username=' . $user->getUsername()
                ]
            );
        }else{
            $output->writeln("Can't find user with this id on username");
        }

        $output->writeln('User was deactivated and confirmation letter was sent.');
    }
}