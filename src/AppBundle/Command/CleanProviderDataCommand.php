<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

use Symfony\Component\Console\Input\InputArgument;

use AppBundle\Entity\User;


class CleanProviderDataCommand extends ContainerAwareCommand
{

    protected function configure(){

        $this
            ->setName('app:clean-provider-data')
            ->setDescription('Clean provider data by NPI and User ID.')
            ->setHelp('This command allows you to clean provider data by NPI and User ID')
            ->addArgument('user_id', InputArgument::REQUIRED, 'User ID')
            ->addArgument('npi', InputArgument::REQUIRED, 'NPI')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output){


        $provider = $this->getContainer()->get('bte.manager.provider');
        $userId = $input->getArgument('user_id');
        $npi = $input->getArgument('npi');

        $provider->getCleanDataByUserIdAndNpi($userId, $npi);

        $output->writeln('The data was cleaned.');
    }
}