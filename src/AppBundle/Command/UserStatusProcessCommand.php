<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;


class UserStatusProcessCommand extends ContainerAwareCommand
{

    protected function configure(){

        $this
            ->setName('app:user-status-process')
            ->setDescription('Process inactive users and change status of the users.')
            ->setHelp('This command allows you to change password for admin user')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output){

        $this->getContainer()->get('bte.manager.user_manager')->processUserStatus();
        $output->writeln('Users were processed.');
    }
}