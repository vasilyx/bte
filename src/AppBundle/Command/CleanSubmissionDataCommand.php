<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

use Symfony\Component\Console\Input\InputArgument;


class CleanSubmissionDataCommand extends ContainerAwareCommand
{

    protected function configure(){

        $this
            ->setName('app:clean-submission-data')
            ->setDescription('Clean submission data by Submission ID.')
            ->setHelp('This command allows you to clean submission data by ID')
            ->addArgument('submission_id', InputArgument::REQUIRED, 'Submission ID')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output){


        $submission = $this->getContainer()->get('bte.manager.submission');
        $submission_id = $input->getArgument('submission_id');

        $submission->getCleanDataBySubmissionId($submission_id);

        $output->writeln('Submission data was removed.');
    }
}