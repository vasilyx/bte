-- MySQL dump 10.13  Distrib 5.7.20, for Linux (x86_64)
--
-- Host: localhost    Database: bte_example2
-- ------------------------------------------------------
-- Server version	5.7.20-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `clinician_degree`
--

DROP TABLE IF EXISTS `clinician_degree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clinician_degree` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clinician_degree`
--

LOCK TABLES `clinician_degree` WRITE;
/*!40000 ALTER TABLE `clinician_degree` DISABLE KEYS */;
INSERT INTO `clinician_degree` VALUES (1,'M.D.'),(2,'D.O.'),(3,'N.P.'),(4,'P.A.');
/*!40000 ALTER TABLE `clinician_degree` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clinician_speciality`
--

DROP TABLE IF EXISTS `clinician_speciality`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clinician_speciality` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clinician_speciality`
--

LOCK TABLES `clinician_speciality` WRITE;
/*!40000 ALTER TABLE `clinician_speciality` DISABLE KEYS */;
INSERT INTO `clinician_speciality` VALUES (1,'Allergy/Immunology'),(2,'Cardiology'),(3,'Critical Care Services'),(4,'Dermatology'),(5,'Endocrinology'),(6,'Gastroenterology'),(7,'Gen/Fam Practice'),(8,'Geriatric Medicine'),(9,'Hematology'),(10,'Infectious Disease'),(11,'Internal Medicine'),(12,'Nephrology'),(13,'Neurology'),(14,'Neurosurgery'),(15,'Obstetrics/Gynecology'),(16,'Occ. Medicine'),(17,'Oncology'),(18,'Ophthalmology'),(19,'Orthopedics'),(20,'Otolaryngology'),(21,'Pediatrics'),(22,'Phys/Rehab Medicine'),(23,'Psychiatry'),(24,'Psychopharmacology'),(25,'Pulmonary Medicine'),(26,'Rheumatology'),(27,'Surgery'),(28,'Urology'),(29,'Other – not listed');
/*!40000 ALTER TABLE `clinician_speciality` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `measure`
--

DROP TABLE IF EXISTS `measure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `measure` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `program_id` int(11) NOT NULL,
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `func_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `total` double NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `IDX_800719253EB8070A` (`program_id`),
  CONSTRAINT `FK_800719253EB8070A` FOREIGN KEY (`program_id`) REFERENCES `program` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `measure`
--

LOCK TABLES `measure` WRITE;
/*!40000 ALTER TABLE `measure` DISABLE KEYS */;
INSERT INTO `measure` VALUES (1,1,'Hemoglobin A1C Control (HbA1C)','A1c',20,1),(2,1,'Hemoglobin A1C (HbA1C) Measurement Twice Annually','A1c2x',2.5,1),(3,1,'Lipid Control','LDL',15,1),(4,1,'Blood Pressure Control','BloodPressure',20,1),(5,1,'Blood Pressure Measurement Twice Annually','BloodPressure2x',2.5,1),(6,1,'Tobacco Use and Cessation Advice and Treatment','Tobacco',7.5,1),(7,1,'Podiatry Examination','Podiatry',5,1),(8,1,'Ophthalmologic Examination','Ophthalmologic',2.5,1),(9,1,'Nephropathy Assessment','Nephropathy',5,1),(10,1,'ACEI/ARB Therapy','ACEI',2.5,1),(11,1,'Body Mass Index/Weight/Nutrition Counseling','BMI',5,1),(12,1,'Cardiovascular Risk Assessment','ASCVD',5,1),(13,1,'Aspirin Use if 10-year risk > 10%','AspirinUse',5,1),(14,1,'Depression Screening Annually','DepressionScreening',2.5,1),(15,2,'Blood Pressure Control','BloodPressure',20,1),(16,2,'Blood Pressure Measurement Twice Annually','BloodPressureTwice',2.5,1),(17,2,'High Intensity Statin Therapy','Statin',15,1),(18,2,'Antiplatelet Therapy','AntiPlatelet',10,1),(19,2,'Beta Blocker Therapy','BetaBlocker',10,1),(20,2,'ACEI/ARB Therapy','ACEI',10,1),(21,2,'Documented Tobacco Status','Tobacco',2.5,1),(22,2,'Tobacco Use and Cessation Advice and Treatment - If User','TobaccoCessation',7.5,1),(23,2,'Body Mass Index (BMI)','BMI',2.5,1),(24,2,'Nutrition and Exercise (Lifestyle) Counseling','Nutrition',7.5,1),(25,2,'Depression Screening','DepressionScreening',10,1),(26,2,'Influenza Immunization','InfluenzaImmunization',2.5,1),(27,3,'Blood Pressure (BP) Control in Patients age > 60 ','BloodPressureOverSixty',20,1),(28,3,'Blood Pressure Control in Patients < 60 ','BloodPressureUnderSixty',20,1),(29,3,'Blood Pressure Measurement Twice Annually','BloodPressureTwice',5,1),(30,3,'Blood Pressure Management in Patients with CKD','BloodPressureKidneyDisease',10,1),(31,3,'ACEI/ARB Therapy in Hypertensive Patients with CKD','ACEI',10,1),(32,3,'Blood Pressure Management in Diabetics','BloodPressureDiabetes',5,1),(33,3,'Blood Pressure Management in Patients with Poorly Controlled Hypertension - Pharmacotherapy ','BloodPressurePharmacotherapy',10,1),(34,3,'Documentation of Annual Urine Protein Test','Hypertensive',2.5,1),(35,3,'Documentation of Annual Serum Creatinine Test - Renal Function Tests','Nephropathy',2.5,1),(36,3,'Documentation of Tobacco Use Status','Tobacco',2.5,1),(37,3,'Documentation of Tobacco Cessation counseling if user - and Treatment','TobaccoCessation',5,1),(38,3,'Body Mass Index calculated','BMI',2.5,1),(39,3,'Documentation of Counseling for Diet, Salt Intake and Physical Activity','NutritionDash',5,1),(40,4,'IBD Type, Anatomic Location, Disease Activity','IBDType',15,1),(41,4,'IBD External Manifestations Assessed','ExternalManifestations',10,1),(42,4,'Corticosteroid-Sparing Therapy Prescribed','Corticosteroid',10,1),(43,4,'Bone Loss Assessment for Patients Receiving Corticosteroid Therapy','BoneLoss',7.5,1),(44,4,'Osteoporosis Therapy in Patients with Bone Loss','Osteoporosis',2.5,1),(45,4,'Testing for latent TB before initiating anti-TNF Therapy','AntiTNF',10,1),(46,4,'Assessment of Hepatitis B Virus before initiating anti-TNF Therapy','HepatitisB',10,1),(47,4,'Influenza Immunization','InfluenzaImmunization',7.5,1),(48,4,'Pneumococcal Immunization','PneumococcalImmunization',7.5,1),(49,4,'Documentation of Tobacco Status','Tobacco',5,1),(50,4,'Documentation of Tobacco Cessation Counseling if user - and Treatment','TobaccoCessation',5,1),(51,4,'Appropriate Use of Surveillance Colonoscopy for Prevention of Colorectal Cancer','Colonoscopy',10,1),(52,4,'Depression Screening Annually','DepressionScreening',0,1),(53,4,'Use of Biologics','IBDBiologics',0,1),(54,5,'Beta Blocker Therapy','BetaBlocker',20,1),(55,5,'ACEI/ARB Therapy','Acei',20,1),(56,5,'Blood Pressure Control in Patients < 60','BloodPressureUnderSixty',15,1),(57,5,'Blood Pressure Control in Patients > 60','calculateBloodPressureOverSixty',2.5,1),(58,5,'Clinical Assessment of Fluid Status','FluidStatus',10,1),(59,5,'Documentation of Tobacco Use Status','Tobacco',7.5,1),(60,5,'Documentation of Tobacco Cessation Counseling if user - and Treatment','TobaccoCessation',7.5,1),(61,5,'Body Mass Index Calculated','BMI',5,1),(62,5,'Documentation of Counseling for Diet, Salt Intake and Physical Activity','Nutrition',5,1),(63,5,'Documentation of Type of Heart Failure','TypeHeartfailure',5,1),(64,5,'Documentation of Severity of Heart Failure','SeverityHeartfailure',5,1),(65,6,'COPD Severity Assessed and Recorded, including Spirometry','SeverityAssessed',10,1),(66,6,'Inhaled Bronchodilator Therapy: LAMA/LABA as Initial Therapy','InhaledBronchodilatorTherapyFEV1',10,1),(67,6,'Inhaled Bronchodilator Therapy: ICS only as Combination Therapy','InhaledBronchodilatorTherapy',10,1),(68,6,'Documented Tobacco Status','Tobacco',10,1),(69,6,'Tobacco Use: Cessation Advice and Treatment','TobaccoCessation',15,1),(70,6,'Assessment of Oxygen Saturation','AssessmentOxygenSaturation',10,1),(71,6,'Long Term Oxygen Therapy','LongTermOxygenTherapy',5,1),(72,6,'Influenza Immunization','InfluenzaImmunization',5,1),(73,6,'Pneumonia Immunization (PCV13)','PneumoniaImmunizationPCV13',5,1),(74,6,'Pneumonia Immunization (PCV23)','PneumoniaImmunizationPCV23',5,1),(75,6,'Follow-Up after Hospital Admission with COPD Exacerbation','FollowUpHospitalAdmission',10,1),(76,6,'Lung Cancer Screening with Low Dose CT','LungCancerScreening',2.5,1),(77,6,'Advanced Health Care Directives','AdvanceHealthCareDirectives',2.5,1),(78,6,'Assessment of COPD Exacerbations(Process Measure)','AssessmentExacerbations',0,1),(79,7,'PHQ-2 Screening','PHQ2Screening',20,1),(80,7,'PHQ-9 Screening','PHQ9Screening',10,1),(81,7,'PHQ-Adolescent Screening','PHQAdolescentScreening',10,1),(82,7,'Positive Depression Screening Follow Up','PositiveDepressionScreening',15,1),(83,7,'PHQ-9 Screening - 6 Month Follow Up','PositiveDepressionScreening6month',5,1),(84,7,'Continuous Antidepressant Treatment- if prescribed and effective','Antidepressant',10,1),(85,7,'Substance Use Screening','SubstanceScreening',5,1),(86,7,'Documentation of Substance Use Intervention/Counseling - if user','SubstanceIntervention',5,1),(87,7,'Documentation of Physical Activity Counseling','PhysicalActivity',10,1),(88,7,'Collaborative Care Model Participation','CollaborativeCareModel',10,1),(89,8,'Documentation of Disease Severity (including spirometry)','DiseaseSeverity',10,1),(90,8,'Short Acting Beta Agonist Prescribed to All Patients with Asthma','BetaAgonistPrescribed',10,1),(91,8,'All patients with persistent asthma (any degree) should be prescribed a controller medication (inhaled corticosteroids - ICS - preferred)','AsthmaControllerMedication',15,1),(92,8,'Patients with Asthma should NOT be taking long-acting beta agonists (LABA) alone','AsthmaBetaAgonist',10,1),(93,8,'Patients with moderate/severe asthma exacerbations should be prescribed oral corticosteroids','AsthmaExacerbations',10,1),(94,8,'Patients with moderate/severe asthma exacerbation should have follow-up within 10 days','AsthmaExacerbations10days',10,1),(95,8,'Patients with persistent asthma (any severity) should have 2 annual visits with their care team','PersistentAsthma',10,1),(96,8,'Patients with asthma should have an \"Asthma Action Plan\" or \"Asthma self-management plan\" documented','AsthmaActionPlan',5,1),(97,8,'Documentation of proper inhaler technique should be provided (5-11 years old)','InhalerTechnique',10,1),(98,8,'Tobacco Use / Tobacco Exposure Status should be assessed and documented','Tobacco',5,1),(99,8,'Documentation of Tobacco Cessation counseling - if user and Treatment (12 years and older)','TobaccoCessation',10,1),(100,8,'Influenza vaccine offered annually)','InfluenzaImmunization',5,1);
/*!40000 ALTER TABLE `measure` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration_versions`
--

DROP TABLE IF EXISTS `migration_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration_versions` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration_versions`
--

LOCK TABLES `migration_versions` WRITE;
/*!40000 ALTER TABLE `migration_versions` DISABLE KEYS */;
INSERT INTO `migration_versions` VALUES ('20170525162159'),('20170525164113'),('20170526115207'),('20170620081018'),('20170627075601'),('20171031213303'),('20171202172436');
/*!40000 ALTER TABLE `migration_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `program`
--

DROP TABLE IF EXISTS `program`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `program` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `table_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `program`
--

LOCK TABLES `program` WRITE;
/*!40000 ALTER TABLE `program` DISABLE KEYS */;
INSERT INTO `program` VALUES (1,'BTE Diabetes Care Recognition Program','Diabete'),(2,'BTE CAD Care Program','CADCare'),(3,'BTE Hypertension Care Program','Hypertension'),(4,'BTE IBD Care Program','IBDCare'),(5,'BTE Heart Failure Care Program','HeartFailure'),(6,'BTE COPD Program','COPD'),(7,'BTE Depression Care Program','Depression'),(8,'BTE Asthma Care Program','Asthma');
/*!40000 ALTER TABLE `program` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provider`
--

DROP TABLE IF EXISTS `provider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `degree_id` int(11) DEFAULT NULL,
  `speciality_id` int(11) DEFAULT NULL,
  `clinician_id` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `npi` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dea` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `license` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `firstname` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `middlename` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `datetime_birth` datetime DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `clinicianId_user_idx` (`clinician_id`,`user_id`),
  UNIQUE KEY `npi_user_idx` (`npi`,`user_id`),
  KEY `IDX_92C4739CA76ED395` (`user_id`),
  KEY `IDX_92C4739CB35C5756` (`degree_id`),
  KEY `IDX_92C4739C3B5A08D7` (`speciality_id`),
  CONSTRAINT `FK_92C4739C3B5A08D7` FOREIGN KEY (`speciality_id`) REFERENCES `clinician_speciality` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_92C4739CA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_92C4739CB35C5756` FOREIGN KEY (`degree_id`) REFERENCES `clinician_degree` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provider`
--

LOCK TABLES `provider` WRITE;
/*!40000 ALTER TABLE `provider` DISABLE KEYS */;
/*!40000 ALTER TABLE `provider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provider_submission`
--

DROP TABLE IF EXISTS `provider_submission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider_submission` (
  `provider_id` int(11) NOT NULL,
  `submission_id` int(11) NOT NULL,
  PRIMARY KEY (`provider_id`,`submission_id`),
  KEY `IDX_FEB3164BA53A8AA` (`provider_id`),
  KEY `IDX_FEB3164BE1FD4933` (`submission_id`),
  CONSTRAINT `FK_FEB3164BA53A8AA` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_FEB3164BE1FD4933` FOREIGN KEY (`submission_id`) REFERENCES `submission` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provider_submission`
--

LOCK TABLES `provider_submission` WRITE;
/*!40000 ALTER TABLE `provider_submission` DISABLE KEYS */;
/*!40000 ALTER TABLE `provider_submission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recognition`
--

DROP TABLE IF EXISTS `recognition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recognition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `submission_id` int(11) DEFAULT NULL,
  `provider_id` int(11) NOT NULL,
  `score` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_95DB6FB2A53A8AA` (`provider_id`),
  KEY `fk_user_details_1_idx` (`submission_id`),
  CONSTRAINT `FK_95DB6FB2A53A8AA` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_95DB6FB2E1FD4933` FOREIGN KEY (`submission_id`) REFERENCES `submission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recognition`
--

LOCK TABLES `recognition` WRITE;
/*!40000 ALTER TABLE `recognition` DISABLE KEYS */;
/*!40000 ALTER TABLE `recognition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recognition_detail`
--

DROP TABLE IF EXISTS `recognition_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recognition_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recognition_id` int(11) NOT NULL,
  `measure_id` int(11) NOT NULL,
  `numerator` int(11) NOT NULL,
  `denominator` int(11) NOT NULL,
  `score` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_EDC624C85DA37D00` (`measure_id`),
  KEY `fk_user_details_1_idx` (`recognition_id`),
  CONSTRAINT `FK_EDC624C853FF93EF` FOREIGN KEY (`recognition_id`) REFERENCES `recognition` (`id`),
  CONSTRAINT `FK_EDC624C85DA37D00` FOREIGN KEY (`measure_id`) REFERENCES `measure` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recognition_detail`
--

LOCK TABLES `recognition_detail` WRITE;
/*!40000 ALTER TABLE `recognition_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `recognition_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `submission`
--

DROP TABLE IF EXISTS `submission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `submission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `program_id` int(11) NOT NULL,
  `from_ehr` tinyint(1) DEFAULT NULL,
  `ehr` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ehr_id` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reporting_start` datetime DEFAULT NULL,
  `reporting_end` datetime DEFAULT NULL,
  `reporting_for` tinyint(1) DEFAULT NULL,
  `reporting_method` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_DB055AF3A76ED395` (`user_id`),
  KEY `IDX_DB055AF33EB8070A` (`program_id`),
  CONSTRAINT `FK_DB055AF33EB8070A` FOREIGN KEY (`program_id`) REFERENCES `program` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_DB055AF3A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `submission`
--

LOCK TABLES `submission` WRITE;
/*!40000 ALTER TABLE `submission` DISABLE KEYS */;
/*!40000 ALTER TABLE `submission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `submission_asthma`
--

DROP TABLE IF EXISTS `submission_asthma`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `submission_asthma` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `submission_id` int(11) DEFAULT NULL,
  `responsible_provider_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `npi` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `individual_group` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chart_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_visit_date` date DEFAULT NULL,
  `patient_dob` date DEFAULT NULL,
  `patient_gender` int(11) DEFAULT NULL,
  `patient_race` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medicare_part_b` tinyint(1) DEFAULT NULL,
  `asthma_diagnosis` tinyint(1) DEFAULT NULL,
  `asthma_disease_severity` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `asthma_disease_severity_date` date DEFAULT NULL,
  `spirometry_eval_results` int(11) DEFAULT NULL,
  `spirometry_eval_date` date DEFAULT NULL,
  `sabaprescribed_date` date DEFAULT NULL,
  `inhaled_corticosteroid_dispensed` tinyint(1) DEFAULT NULL,
  `inhaled_corticosteroid_dispensed_date` date DEFAULT NULL,
  `inhaled_corticosteroid_prescribed` tinyint(1) DEFAULT NULL,
  `inhaled_corticosteroid_prescribed_date` date DEFAULT NULL,
  `oral_corticosteroid_prescribed` date DEFAULT NULL,
  `oral_corticosteroid_prescribed_date` date DEFAULT NULL,
  `inhaled_bronchodilator_prescribed_date` date DEFAULT NULL,
  `oral_beta3agonists_dispensed_date` date DEFAULT NULL,
  `oral_beta3agonists_prescribed_date` date DEFAULT NULL,
  `asthma_medication_prescribed_date` date DEFAULT NULL,
  `asthma_exacerbation_episode` tinyint(1) DEFAULT NULL,
  `asthma_exacerbation_severity` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `asthma_exacerbation_date` date DEFAULT NULL,
  `asthma_exacerbation_referral_date` date DEFAULT NULL,
  `face_to_face_visit1` date DEFAULT NULL,
  `face_to_face_visit2` date DEFAULT NULL,
  `asthma_action_plan_date` date DEFAULT NULL,
  `asthma_self_management_plan_date` date DEFAULT NULL,
  `inhaler_education_date` date DEFAULT NULL,
  `tobacco_exposure_status` tinyint(1) DEFAULT NULL,
  `tobacco_exposure_status_date` date DEFAULT NULL,
  `tobacco_status` int(11) DEFAULT NULL,
  `tobacco_status_assessment_date` date DEFAULT NULL,
  `tobacco_cessation_advice_or_treatment_date` date DEFAULT NULL,
  `influenza_immunization` smallint(6) DEFAULT NULL,
  `influenza_immunization_date` date DEFAULT NULL,
  `temp_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_BC62538AE1FD4933` (`submission_id`),
  CONSTRAINT `FK_BC62538AE1FD4933` FOREIGN KEY (`submission_id`) REFERENCES `submission` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `submission_asthma`
--

LOCK TABLES `submission_asthma` WRITE;
/*!40000 ALTER TABLE `submission_asthma` DISABLE KEYS */;
/*!40000 ALTER TABLE `submission_asthma` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `submission_cadcare`
--

DROP TABLE IF EXISTS `submission_cadcare`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `submission_cadcare` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `submission_id` int(11) DEFAULT NULL,
  `responsible_provider_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `npi` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `individual_group` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chart_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_visit_date` date DEFAULT NULL,
  `patient_dob` date DEFAULT NULL,
  `patient_gender` int(11) DEFAULT NULL,
  `medicare_part_b` tinyint(1) DEFAULT NULL,
  `cad_diagnosis` tinyint(1) DEFAULT NULL,
  `blood_pressure_date1` date DEFAULT NULL,
  `statin_status` tinyint(1) DEFAULT NULL,
  `systolic1` int(11) DEFAULT NULL,
  `diastolic1` int(11) DEFAULT NULL,
  `systolic2` int(11) DEFAULT NULL,
  `diastolic2` int(11) DEFAULT NULL,
  `blood_pressure_date2` date DEFAULT NULL,
  `anti_platelets` tinyint(1) DEFAULT NULL,
  `beta_blockers` tinyint(1) DEFAULT NULL,
  `diabetes_diagnosis` tinyint(1) DEFAULT NULL,
  `hypertension_diagnosis` tinyint(1) DEFAULT NULL,
  `chronic_kidney_disease_diagnosis` tinyint(1) DEFAULT NULL,
  `chf_diagnosis` tinyint(1) DEFAULT NULL,
  `acei_arb_therapy` tinyint(1) DEFAULT NULL,
  `esrd_patient` tinyint(1) DEFAULT NULL,
  `dialysis_patient` tinyint(1) DEFAULT NULL,
  `tobacco_status` int(11) DEFAULT NULL,
  `tobacco_status_assessment_date` date DEFAULT NULL,
  `tobacco_cessation_advice_or_treatment` tinyint(1) DEFAULT NULL,
  `tobacco_cessation_advice_or_treatment_date` date DEFAULT NULL,
  `bmi_value` double DEFAULT NULL,
  `bmi_value_date` date DEFAULT NULL,
  `nutrition_counseling` tinyint(1) DEFAULT NULL,
  `nutrition_counseling_date` date DEFAULT NULL,
  `influenza_immunization` smallint(6) DEFAULT NULL,
  `influenza_immunization_date` date DEFAULT NULL,
  `activity_counseling` tinyint(1) DEFAULT NULL,
  `activity_counseling_date` date DEFAULT NULL,
  `phq2screening` tinyint(1) DEFAULT NULL,
  `phq2screening_date` date DEFAULT NULL,
  `phq9screening` tinyint(1) DEFAULT NULL,
  `phq9screening_date` date DEFAULT NULL,
  `temp_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_E49E68FBE1FD4933` (`submission_id`),
  CONSTRAINT `FK_E49E68FBE1FD4933` FOREIGN KEY (`submission_id`) REFERENCES `submission` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `submission_cadcare`
--

LOCK TABLES `submission_cadcare` WRITE;
/*!40000 ALTER TABLE `submission_cadcare` DISABLE KEYS */;
/*!40000 ALTER TABLE `submission_cadcare` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `submission_copd`
--

DROP TABLE IF EXISTS `submission_copd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `submission_copd` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `submission_id` int(11) DEFAULT NULL,
  `responsible_provider_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `npi` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `individual_group` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chart_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_visit_date` date DEFAULT NULL,
  `patient_dob` date DEFAULT NULL,
  `patient_gender` int(11) DEFAULT NULL,
  `patient_race` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medicare_part_b` tinyint(1) DEFAULT NULL,
  `copddiagnosis` tinyint(1) DEFAULT NULL,
  `copdsymptoms` tinyint(1) DEFAULT NULL,
  `spirometry_eval_results` int(11) DEFAULT NULL,
  `spirometry_eval_date` date DEFAULT NULL,
  `inhaled_bronchodilator_prescribed_date` date DEFAULT NULL,
  `inhaled_corticosteroid_dispensed_date` date DEFAULT NULL,
  `inhaled_corticosteroid_prescribed_date` date DEFAULT NULL,
  `tobacco_status` int(11) DEFAULT NULL,
  `tobacco_status_assessment_date` date DEFAULT NULL,
  `tobacco_cessation_advice_or_treatment_date` date DEFAULT NULL,
  `respiratory_failure_diagnosis` tinyint(1) DEFAULT NULL,
  `right_heart_failure_diagnosis` tinyint(1) DEFAULT NULL,
  `oxygen_saturation_level` int(11) DEFAULT NULL,
  `oxygen_saturation_level_date` date DEFAULT NULL,
  `pa_o2level` int(11) DEFAULT NULL,
  `pa_o2level_date` date DEFAULT NULL,
  `long_term_oxygen_therapy_date` date DEFAULT NULL,
  `influenza_immunization` smallint(6) DEFAULT NULL,
  `influenza_immunization_date` date DEFAULT NULL,
  `pneumococcal_vaccine_pcv13` smallint(6) DEFAULT NULL,
  `pneumococcal_vaccine_pcv13date` date DEFAULT NULL,
  `pneumococcal_vaccine_pcv23` smallint(6) DEFAULT NULL,
  `pneumococcal_vaccine_pcv23date` date DEFAULT NULL,
  `copdexacerbation_episode` int(11) DEFAULT NULL,
  `copdexacerbation_episode_date` date DEFAULT NULL,
  `copdexacerbation_episode_state_date` date DEFAULT NULL,
  `face_to_face_visit` date DEFAULT NULL,
  `low_dose_ctdate` date DEFAULT NULL,
  `low_dose_ctreferral` tinyint(1) DEFAULT NULL,
  `low_dose_ctdocumented` tinyint(1) DEFAULT NULL,
  `advanced_directive` smallint(6) DEFAULT NULL,
  `temp_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_BCBABC34E1FD4933` (`submission_id`),
  CONSTRAINT `FK_BCBABC34E1FD4933` FOREIGN KEY (`submission_id`) REFERENCES `submission` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `submission_copd`
--

LOCK TABLES `submission_copd` WRITE;
/*!40000 ALTER TABLE `submission_copd` DISABLE KEYS */;
/*!40000 ALTER TABLE `submission_copd` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `submission_depression`
--

DROP TABLE IF EXISTS `submission_depression`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `submission_depression` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `submission_id` int(11) DEFAULT NULL,
  `responsible_provider_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `npi` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `individual_group` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chart_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_visit_date` date DEFAULT NULL,
  `patient_dob` date DEFAULT NULL,
  `patient_gender` int(11) DEFAULT NULL,
  `patient_race` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medicare_part_b` tinyint(1) DEFAULT NULL,
  `phq2screening_score` int(11) DEFAULT NULL,
  `phq2screening_date` date DEFAULT NULL,
  `phq9screening_score` int(11) DEFAULT NULL,
  `phq9screening_date` date DEFAULT NULL,
  `phq9screening2score` int(11) DEFAULT NULL,
  `phq9screening2date` date DEFAULT NULL,
  `phqadolescent_screening_score` int(11) DEFAULT NULL,
  `phqadolescent_screening_date` date DEFAULT NULL,
  `cognitive_behavior_therapy_date` date DEFAULT NULL,
  `specialist_referral_date` date DEFAULT NULL,
  `psychotherapy_date` date DEFAULT NULL,
  `antidepressant_initial_dispensed_date` date DEFAULT NULL,
  `antidepressant_active_date` date DEFAULT NULL,
  `antidepressant_initial_prescribed_date` date DEFAULT NULL,
  `depression_diagnosis` tinyint(1) DEFAULT NULL,
  `dysthymia_diagnosis` tinyint(1) DEFAULT NULL,
  `substance_use_screening` tinyint(1) DEFAULT NULL,
  `substance_use_screening_date` date DEFAULT NULL,
  `substance_use_counseling_date` date DEFAULT NULL,
  `activity_counseling` tinyint(1) DEFAULT NULL,
  `activity_counseling_date` date DEFAULT NULL,
  `care_coordination` tinyint(1) DEFAULT NULL,
  `monitoringand_treatment` tinyint(1) DEFAULT NULL,
  `case_load_reviews` tinyint(1) DEFAULT NULL,
  `temp_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_A6C15088E1FD4933` (`submission_id`),
  CONSTRAINT `FK_A6C15088E1FD4933` FOREIGN KEY (`submission_id`) REFERENCES `submission` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `submission_depression`
--

LOCK TABLES `submission_depression` WRITE;
/*!40000 ALTER TABLE `submission_depression` DISABLE KEYS */;
/*!40000 ALTER TABLE `submission_depression` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `submission_diabete`
--

DROP TABLE IF EXISTS `submission_diabete`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `submission_diabete` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `submission_id` int(11) DEFAULT NULL,
  `responsible_provider_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `npi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `individual_group` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chart_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_visit_date` date DEFAULT NULL,
  `patient_dob` date DEFAULT NULL,
  `patient_gender` int(11) DEFAULT NULL,
  `patient_race` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medicare_part_b` tinyint(1) DEFAULT NULL,
  `diabetes_diagnosis` tinyint(1) DEFAULT NULL,
  `hb_a1c1` double DEFAULT NULL,
  `hb_a1c_date1` date DEFAULT NULL,
  `hb_a1c2` double DEFAULT NULL,
  `hb_a1c_date2` date DEFAULT NULL,
  `ldllevel` int(11) DEFAULT NULL,
  `ldldate` date DEFAULT NULL,
  `statin_status` tinyint(1) DEFAULT NULL,
  `systolic1` int(11) DEFAULT NULL,
  `diastolic1` int(11) DEFAULT NULL,
  `blood_pressure_date1` date DEFAULT NULL,
  `systolic2` int(11) DEFAULT NULL,
  `diastolic2` int(11) DEFAULT NULL,
  `blood_pressure_date2` date DEFAULT NULL,
  `tobacco_status` int(11) DEFAULT NULL,
  `tobacco_status_assessment_date` date DEFAULT NULL,
  `tobacco_cessation_advice_or_treatment_date` date DEFAULT NULL,
  `foot_exam` tinyint(1) DEFAULT NULL,
  `foot_exam_date` date DEFAULT NULL,
  `bilateral_amputation` tinyint(1) DEFAULT NULL,
  `retinopathy` tinyint(1) DEFAULT NULL,
  `retinal_exam_date` date DEFAULT NULL,
  `blindness` tinyint(1) DEFAULT NULL,
  `nephropathy_diagnosis` tinyint(1) DEFAULT NULL,
  `nephropathy_evidence` tinyint(1) DEFAULT NULL,
  `microalbuminuria_lab` int(11) DEFAULT NULL,
  `microalbuminuria_date` date DEFAULT NULL,
  `macroalbuminuria_lab` int(11) DEFAULT NULL,
  `macroalbuminuria_date` date DEFAULT NULL,
  `hypertension_diagnosis` tinyint(1) DEFAULT NULL,
  `acei_arb_therapy` tinyint(1) DEFAULT NULL,
  `bmi_calculation` double DEFAULT NULL,
  `bmi_date` date DEFAULT NULL,
  `bmi_counseling` tinyint(1) DEFAULT NULL,
  `bmi_counseling_date` date DEFAULT NULL,
  `ascvd_assessment` tinyint(1) DEFAULT NULL,
  `ascvd_date` date DEFAULT NULL,
  `ascvd_results` int(11) DEFAULT NULL,
  `aspirin_therapy` int(11) DEFAULT NULL,
  `aspirin_use_considered` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phq2screening` tinyint(1) DEFAULT NULL,
  `phq2screening_date` date DEFAULT NULL,
  `phq9screening` tinyint(1) DEFAULT NULL,
  `phq9screening_date` date DEFAULT NULL,
  `temp_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_EC653357E1FD4933` (`submission_id`),
  CONSTRAINT `FK_EC653357E1FD4933` FOREIGN KEY (`submission_id`) REFERENCES `submission` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `submission_diabete`
--

LOCK TABLES `submission_diabete` WRITE;
/*!40000 ALTER TABLE `submission_diabete` DISABLE KEYS */;
/*!40000 ALTER TABLE `submission_diabete` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `submission_heartfailure`
--

DROP TABLE IF EXISTS `submission_heartfailure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `submission_heartfailure` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `submission_id` int(11) DEFAULT NULL,
  `responsible_provider_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `npi` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `individual_group` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chart_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_visit_date` date DEFAULT NULL,
  `patient_dob` date DEFAULT NULL,
  `patient_gender` int(11) DEFAULT NULL,
  `patient_race` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medicare_part_b` tinyint(1) DEFAULT NULL,
  `hfdiagnosis` tinyint(1) DEFAULT NULL,
  `lvsddiagnosis` tinyint(1) DEFAULT NULL,
  `lvf_assess_date` date DEFAULT NULL,
  `betablocker_therapy` tinyint(1) DEFAULT NULL,
  `acei_arb_therapy` tinyint(1) DEFAULT NULL,
  `blood_pressure_date1` date DEFAULT NULL,
  `systolic1` int(11) DEFAULT NULL,
  `diastolic1` int(11) DEFAULT NULL,
  `blood_pressure_date2` date DEFAULT NULL,
  `systolic2` int(11) DEFAULT NULL,
  `diastolic2` int(11) DEFAULT NULL,
  `weight_measure_date` date DEFAULT NULL,
  `peripheraledema_assessment` tinyint(1) DEFAULT NULL,
  `orthopnea_assessment` tinyint(1) DEFAULT NULL,
  `jvpassessment` tinyint(1) DEFAULT NULL,
  `tobacco_status` int(11) DEFAULT NULL,
  `tobacco_status_assessment_date` date DEFAULT NULL,
  `tobacco_cessation_advice_or_treatment` tinyint(1) DEFAULT NULL,
  `tobacco_cessation_advice_or_treatment_date` date DEFAULT NULL,
  `bmi_value` double DEFAULT NULL,
  `bmi_value_date` date DEFAULT NULL,
  `nutrition_counseling` double DEFAULT NULL,
  `nutrition_counseling_date` date DEFAULT NULL,
  `activity_status` tinyint(1) DEFAULT NULL,
  `activity_status_date` date DEFAULT NULL,
  `activity_counseling` tinyint(1) DEFAULT NULL,
  `activity_counseling_date` date DEFAULT NULL,
  `heartfailure_type` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `heartfailure_severity` tinyint(1) DEFAULT NULL,
  `esrd_patient` tinyint(1) DEFAULT NULL,
  `dialysis_patient` tinyint(1) DEFAULT NULL,
  `temp_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_FD701C4EE1FD4933` (`submission_id`),
  CONSTRAINT `FK_FD701C4EE1FD4933` FOREIGN KEY (`submission_id`) REFERENCES `submission` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `submission_heartfailure`
--

LOCK TABLES `submission_heartfailure` WRITE;
/*!40000 ALTER TABLE `submission_heartfailure` DISABLE KEYS */;
/*!40000 ALTER TABLE `submission_heartfailure` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `submission_hypertension`
--

DROP TABLE IF EXISTS `submission_hypertension`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `submission_hypertension` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `submission_id` int(11) DEFAULT NULL,
  `responsible_provider_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `npi` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `individual_group` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chart_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_visit_date` date DEFAULT NULL,
  `patient_dob` date DEFAULT NULL,
  `patient_gender` int(11) DEFAULT NULL,
  `medicare_part_b` int(11) DEFAULT NULL,
  `blood_pressure_date1` date DEFAULT NULL,
  `systolic1` int(11) DEFAULT NULL,
  `diastolic1` int(11) DEFAULT NULL,
  `systolic2` int(11) DEFAULT NULL,
  `diastolic2` int(11) DEFAULT NULL,
  `blood_pressure_date2` date DEFAULT NULL,
  `hypertension_diagnosis` int(11) DEFAULT NULL,
  `chronic_kidney_disease_diagnosis` int(11) DEFAULT NULL,
  `diabetes_diagnosis` int(11) DEFAULT NULL,
  `acei_arb_therapy` int(11) DEFAULT NULL,
  `calcium_channel_blocker` int(11) DEFAULT NULL,
  `thiazide` int(11) DEFAULT NULL,
  `nephropathy_diagnosis` int(11) DEFAULT NULL,
  `nephropathy_screening` int(11) DEFAULT NULL,
  `nephropathy_screening_date` date DEFAULT NULL,
  `esrd_patient` int(11) DEFAULT NULL,
  `dialysis_patient` int(11) DEFAULT NULL,
  `creatinine` int(11) DEFAULT NULL,
  `creatinine_date` date DEFAULT NULL,
  `tobacco_status` int(11) DEFAULT NULL,
  `tobacco_status_assessment_date` date DEFAULT NULL,
  `tobacco_cessation_advice_or_treatment_date` date DEFAULT NULL,
  `bmi_value` double DEFAULT NULL,
  `bmi_value_date` date DEFAULT NULL,
  `dashdiet_counseling` int(11) DEFAULT NULL,
  `dashdiet_date` date DEFAULT NULL,
  `dashsodium_counseling` int(11) DEFAULT NULL,
  `dashsodium_date` date DEFAULT NULL,
  `activity_status` int(11) DEFAULT NULL,
  `activity_status_date` date DEFAULT NULL,
  `activity_counseling` int(11) DEFAULT NULL,
  `activity_counseling_date` date DEFAULT NULL,
  `temp_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_33363D03E1FD4933` (`submission_id`),
  CONSTRAINT `FK_33363D03E1FD4933` FOREIGN KEY (`submission_id`) REFERENCES `submission` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `submission_hypertension`
--

LOCK TABLES `submission_hypertension` WRITE;
/*!40000 ALTER TABLE `submission_hypertension` DISABLE KEYS */;
/*!40000 ALTER TABLE `submission_hypertension` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `submission_ibdcare`
--

DROP TABLE IF EXISTS `submission_ibdcare`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `submission_ibdcare` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `submission_id` int(11) DEFAULT NULL,
  `responsible_provider_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `npi` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `individual_group` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chart_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_visit_date` date DEFAULT NULL,
  `patient_dob` date DEFAULT NULL,
  `patient_gender` int(11) DEFAULT NULL,
  `patient_race` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medicare_part_b` tinyint(1) DEFAULT NULL,
  `ibddiagnosis` tinyint(1) DEFAULT NULL,
  `ibdunder_care_provider` tinyint(1) DEFAULT NULL,
  `ibddiagnosis_one_year` tinyint(1) DEFAULT NULL,
  `ibddiagnosis_eight_year` tinyint(1) DEFAULT NULL,
  `ibdtype` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ibdtype_date` date DEFAULT NULL,
  `ibdlocation` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ibdextent` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ibdextent_date` date DEFAULT NULL,
  `ibdactivity` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ibdactivity_date` date DEFAULT NULL,
  `external_manifestations` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `external_mainfestations_date_assessed` date DEFAULT NULL,
  `corticosteroid10mg60days` tinyint(1) DEFAULT NULL,
  `corticosteroid_sparing_therapy` tinyint(1) DEFAULT NULL,
  `bone_loss_risk_assessed` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bone_loss_risk_date_assessed` date DEFAULT NULL,
  `osteoporosis_therapy` tinyint(1) DEFAULT NULL,
  `osteoporosis_therapy_date_assessed` date DEFAULT NULL,
  `first_course_anti_tnftherapy` tinyint(1) DEFAULT NULL,
  `anti_tnfinitiation_date` date DEFAULT NULL,
  `tuberculosis_screening` tinyint(1) DEFAULT NULL,
  `tuberculosis_screening_date` date DEFAULT NULL,
  `positive_tbreated_tb` tinyint(1) DEFAULT NULL,
  `hepatitis_bstatus` tinyint(1) DEFAULT NULL,
  `hepatitis_bstatus_date` date DEFAULT NULL,
  `influenza_immunization` smallint(6) DEFAULT NULL,
  `influenza_immunization_date` date DEFAULT NULL,
  `pneumococcal_vaccine` smallint(6) DEFAULT NULL,
  `pneumococccal_vaccine_date` date DEFAULT NULL,
  `tobacco_status` int(11) DEFAULT NULL,
  `tobacco_status_assessment_date` date DEFAULT NULL,
  `tobacco_cessation_advice_or_treatment_date` date DEFAULT NULL,
  `surveillance_colonoscopy` tinyint(1) DEFAULT NULL,
  `surveillance_colonoscopy_date` date DEFAULT NULL,
  `ulcerative_colitis_diagnosis` tinyint(1) DEFAULT NULL,
  `crohn_colitis_diagnosis` tinyint(1) DEFAULT NULL,
  `phq2screening` tinyint(1) DEFAULT NULL,
  `phq2screening_date` date DEFAULT NULL,
  `phq9screening` tinyint(1) DEFAULT NULL,
  `phq9screening_date` date DEFAULT NULL,
  `ibdbiologics` tinyint(1) DEFAULT NULL,
  `ibdbiologics_date_assessed` date DEFAULT NULL,
  `temp_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_C67A451BE1FD4933` (`submission_id`),
  CONSTRAINT `FK_C67A451BE1FD4933` FOREIGN KEY (`submission_id`) REFERENCES `submission` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `submission_ibdcare`
--

LOCK TABLES `submission_ibdcare` WRITE;
/*!40000 ALTER TABLE `submission_ibdcare` DISABLE KEYS */;
/*!40000 ALTER TABLE `submission_ibdcare` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `datetime_registration` datetime DEFAULT NULL,
  `datetime_last_login` datetime DEFAULT NULL,
  `forgotten_password_token` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `confirm_email_token` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'test@bte.com','iOgyhdY1gJJPj7y7mMN8obgqMQZH2fLDuQuXfqZesC1Iqxo6iHxRuAA9m8E1ZUz76OIiPGTann7uJ3BNhPDoEA==','sdfsdf','sdfsdf','123123',1,NULL,NULL,NULL,NULL,'2017-05-23 02:51:00','2017-05-23 08:51:00');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_practice`
--

DROP TABLE IF EXISTS `user_practice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_practice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip_code` int(11) DEFAULT NULL,
  `phone` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `npi` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `primary_contact_name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `primary_contact_email` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_9F61D04EA76ED395` (`user_id`),
  KEY `fk_user_details_1_idx` (`user_id`),
  CONSTRAINT `FK_9F61D04EA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_practice`
--

LOCK TABLES `user_practice` WRITE;
/*!40000 ALTER TABLE `user_practice` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_practice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_2DE8C6A3A76ED395` (`user_id`),
  CONSTRAINT `FK_2DE8C6A3A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_session`
--

DROP TABLE IF EXISTS `user_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_session` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `datetime_create` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8849CBDE5F37A13B` (`token`),
  KEY `IDX_8849CBDEA76ED395` (`user_id`),
  CONSTRAINT `FK_8849CBDEA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_session`
--

LOCK TABLES `user_session` WRITE;
/*!40000 ALTER TABLE `user_session` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_session` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-13 16:23:58
-- MySQL dump 10.13  Distrib 5.7.20, for Linux (x86_64)
--
-- Host: localhost    Database: bte_example2
-- ------------------------------------------------------
-- Server version	5.7.20-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `clinician_degree`
--

DROP TABLE IF EXISTS `clinician_degree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clinician_degree` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clinician_degree`
--

LOCK TABLES `clinician_degree` WRITE;
/*!40000 ALTER TABLE `clinician_degree` DISABLE KEYS */;
INSERT INTO `clinician_degree` VALUES (1,'M.D.'),(2,'D.O.'),(3,'N.P.'),(4,'P.A.');
/*!40000 ALTER TABLE `clinician_degree` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clinician_speciality`
--

DROP TABLE IF EXISTS `clinician_speciality`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clinician_speciality` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clinician_speciality`
--

LOCK TABLES `clinician_speciality` WRITE;
/*!40000 ALTER TABLE `clinician_speciality` DISABLE KEYS */;
INSERT INTO `clinician_speciality` VALUES (1,'Allergy/Immunology'),(2,'Cardiology'),(3,'Critical Care Services'),(4,'Dermatology'),(5,'Endocrinology'),(6,'Gastroenterology'),(7,'Gen/Fam Practice'),(8,'Geriatric Medicine'),(9,'Hematology'),(10,'Infectious Disease'),(11,'Internal Medicine'),(12,'Nephrology'),(13,'Neurology'),(14,'Neurosurgery'),(15,'Obstetrics/Gynecology'),(16,'Occ. Medicine'),(17,'Oncology'),(18,'Ophthalmology'),(19,'Orthopedics'),(20,'Otolaryngology'),(21,'Pediatrics'),(22,'Phys/Rehab Medicine'),(23,'Psychiatry'),(24,'Psychopharmacology'),(25,'Pulmonary Medicine'),(26,'Rheumatology'),(27,'Surgery'),(28,'Urology'),(29,'Other – not listed');
/*!40000 ALTER TABLE `clinician_speciality` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `measure`
--

DROP TABLE IF EXISTS `measure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `measure` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `program_id` int(11) NOT NULL,
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `func_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `total` double NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `IDX_800719253EB8070A` (`program_id`),
  CONSTRAINT `FK_800719253EB8070A` FOREIGN KEY (`program_id`) REFERENCES `program` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `measure`
--

LOCK TABLES `measure` WRITE;
/*!40000 ALTER TABLE `measure` DISABLE KEYS */;
INSERT INTO `measure` VALUES (1,1,'Hemoglobin A1C Control (HbA1C)','A1c',20,1),(2,1,'Hemoglobin A1C (HbA1C) Measurement Twice Annually','A1c2x',2.5,1),(3,1,'Lipid Control','LDL',15,1),(4,1,'Blood Pressure Control','BloodPressure',20,1),(5,1,'Blood Pressure Measurement Twice Annually','BloodPressure2x',2.5,1),(6,1,'Tobacco Use and Cessation Advice and Treatment','Tobacco',7.5,1),(7,1,'Podiatry Examination','Podiatry',5,1),(8,1,'Ophthalmologic Examination','Ophthalmologic',2.5,1),(9,1,'Nephropathy Assessment','Nephropathy',5,1),(10,1,'ACEI/ARB Therapy','ACEI',2.5,1),(11,1,'Body Mass Index/Weight/Nutrition Counseling','BMI',5,1),(12,1,'Cardiovascular Risk Assessment','ASCVD',5,1),(13,1,'Aspirin Use if 10-year risk > 10%','AspirinUse',5,1),(14,1,'Depression Screening Annually','DepressionScreening',2.5,1),(15,2,'Blood Pressure Control','BloodPressure',20,1),(16,2,'Blood Pressure Measurement Twice Annually','BloodPressureTwice',2.5,1),(17,2,'High Intensity Statin Therapy','Statin',15,1),(18,2,'Antiplatelet Therapy','AntiPlatelet',10,1),(19,2,'Beta Blocker Therapy','BetaBlocker',10,1),(20,2,'ACEI/ARB Therapy','ACEI',10,1),(21,2,'Documented Tobacco Status','Tobacco',2.5,1),(22,2,'Tobacco Use and Cessation Advice and Treatment - If User','TobaccoCessation',7.5,1),(23,2,'Body Mass Index (BMI)','BMI',2.5,1),(24,2,'Nutrition and Exercise (Lifestyle) Counseling','Nutrition',7.5,1),(25,2,'Depression Screening','DepressionScreening',10,1),(26,2,'Influenza Immunization','InfluenzaImmunization',2.5,1),(27,3,'Blood Pressure (BP) Control in Patients age > 60 ','BloodPressureOverSixty',20,1),(28,3,'Blood Pressure Control in Patients < 60 ','BloodPressureUnderSixty',20,1),(29,3,'Blood Pressure Measurement Twice Annually','BloodPressureTwice',5,1),(30,3,'Blood Pressure Management in Patients with CKD','BloodPressureKidneyDisease',10,1),(31,3,'ACEI/ARB Therapy in Hypertensive Patients with CKD','ACEI',10,1),(32,3,'Blood Pressure Management in Diabetics','BloodPressureDiabetes',5,1),(33,3,'Blood Pressure Management in Patients with Poorly Controlled Hypertension - Pharmacotherapy ','BloodPressurePharmacotherapy',10,1),(34,3,'Documentation of Annual Urine Protein Test','Hypertensive',2.5,1),(35,3,'Documentation of Annual Serum Creatinine Test - Renal Function Tests','Nephropathy',2.5,1),(36,3,'Documentation of Tobacco Use Status','Tobacco',2.5,1),(37,3,'Documentation of Tobacco Cessation counseling if user - and Treatment','TobaccoCessation',5,1),(38,3,'Body Mass Index calculated','BMI',2.5,1),(39,3,'Documentation of Counseling for Diet, Salt Intake and Physical Activity','NutritionDash',5,1),(40,4,'IBD Type, Anatomic Location, Disease Activity','IBDType',15,1),(41,4,'IBD External Manifestations Assessed','ExternalManifestations',10,1),(42,4,'Corticosteroid-Sparing Therapy Prescribed','Corticosteroid',10,1),(43,4,'Bone Loss Assessment for Patients Receiving Corticosteroid Therapy','BoneLoss',7.5,1),(44,4,'Osteoporosis Therapy in Patients with Bone Loss','Osteoporosis',2.5,1),(45,4,'Testing for latent TB before initiating anti-TNF Therapy','AntiTNF',10,1),(46,4,'Assessment of Hepatitis B Virus before initiating anti-TNF Therapy','HepatitisB',10,1),(47,4,'Influenza Immunization','InfluenzaImmunization',7.5,1),(48,4,'Pneumococcal Immunization','PneumococcalImmunization',7.5,1),(49,4,'Documentation of Tobacco Status','Tobacco',5,1),(50,4,'Documentation of Tobacco Cessation Counseling if user - and Treatment','TobaccoCessation',5,1),(51,4,'Appropriate Use of Surveillance Colonoscopy for Prevention of Colorectal Cancer','Colonoscopy',10,1),(52,4,'Depression Screening Annually','DepressionScreening',0,1),(53,4,'Use of Biologics','IBDBiologics',0,1),(54,5,'Beta Blocker Therapy','BetaBlocker',20,1),(55,5,'ACEI/ARB Therapy','Acei',20,1),(56,5,'Blood Pressure Control in Patients < 60','BloodPressureUnderSixty',15,1),(57,5,'Blood Pressure Control in Patients > 60','calculateBloodPressureOverSixty',2.5,1),(58,5,'Clinical Assessment of Fluid Status','FluidStatus',10,1),(59,5,'Documentation of Tobacco Use Status','Tobacco',7.5,1),(60,5,'Documentation of Tobacco Cessation Counseling if user - and Treatment','TobaccoCessation',7.5,1),(61,5,'Body Mass Index Calculated','BMI',5,1),(62,5,'Documentation of Counseling for Diet, Salt Intake and Physical Activity','Nutrition',5,1),(63,5,'Documentation of Type of Heart Failure','TypeHeartfailure',5,1),(64,5,'Documentation of Severity of Heart Failure','SeverityHeartfailure',5,1),(65,6,'COPD Severity Assessed and Recorded, including Spirometry','SeverityAssessed',10,1),(66,6,'Inhaled Bronchodilator Therapy: LAMA/LABA as Initial Therapy','InhaledBronchodilatorTherapyFEV1',10,1),(67,6,'Inhaled Bronchodilator Therapy: ICS only as Combination Therapy','InhaledBronchodilatorTherapy',10,1),(68,6,'Documented Tobacco Status','Tobacco',10,1),(69,6,'Tobacco Use: Cessation Advice and Treatment','TobaccoCessation',15,1),(70,6,'Assessment of Oxygen Saturation','AssessmentOxygenSaturation',10,1),(71,6,'Long Term Oxygen Therapy','LongTermOxygenTherapy',5,1),(72,6,'Influenza Immunization','InfluenzaImmunization',5,1),(73,6,'Pneumonia Immunization (PCV13)','PneumoniaImmunizationPCV13',5,1),(74,6,'Pneumonia Immunization (PCV23)','PneumoniaImmunizationPCV23',5,1),(75,6,'Follow-Up after Hospital Admission with COPD Exacerbation ','FollowUpHospitalAdmission',10,1),(76,6,'Lung Cancer Screening with Low Dose CT','LungCancerScreening',2.5,1),(77,6,'Advanced Health Care Directives','AdvanceHealthCareDirectives',2.5,1),(78,6,'Assessment of COPD Exacerbations(Process Measure)','AssessmentExacerbations',0,1),(79,7,'PHQ-2 Screening','PHQ2Screening',20,1),(80,7,'PHQ-9 Screening','PHQ9Screening',10,1),(81,7,'PHQ-Adolescent Screening','PHQAdolescentScreening',10,1),(82,7,'Positive Depression Screening Follow Up','PositiveDepressionScreening',15,1),(83,7,'PHQ-9 Screening - 6 Month Follow Up','PositiveDepressionScreening6month',5,1),(84,7,'Continuous Antidepressant Treatment- if prescribed and effective','Antidepressant',10,1),(85,7,'Substance Use Screening','SubstanceScreening',5,1),(86,7,'Documentation of Substance Use Intervention/Counseling - if user','SubstanceIntervention',5,1),(87,7,'Documentation of Physical Activity Counseling','PhysicalActivity',10,1),(88,7,'Collaborative Care Model Participation','CollaborativeCareModel',10,1),(89,8,'Documentation of Disease Severity (including spirometry)','DiseaseSeverity',10,1),(90,8,'Short Acting Beta Agonist Prescribed to All Patients with Asthma','BetaAgonistPrescribed',10,1),(91,8,'All patients with persistent asthma (any degree) should be prescribed a controller medication (inhaled corticosteroids - ICS - preferred)','AsthmaControllerMedication',15,1),(92,8,'Patients with Asthma should NOT be taking long-acting beta agonists (LABA) alone','AsthmaBetaAgonist',10,1),(93,8,'Patients with moderate/severe asthma exacerbations should be prescribed oral corticosteroids','AsthmaExacerbations',10,1),(94,8,'Patients with moderate/severe asthma exacerbation should have follow-up within 10 days','AsthmaExacerbations10days',10,1),(95,8,'Patients with persistent asthma (any severity) should have 2 annual visits with their care team','PersistentAsthma',10,1),(96,8,'Patients with asthma should have an \"Asthma Action Plan\" or \"Asthma self-management plan\" documented','AsthmaActionPlan',5,1),(97,8,'Documentation of proper inhaler technique should be provided (5-11 years old)','InhalerTechnique',10,1),(98,8,'Tobacco Use / Tobacco Exposure Status should be assessed and documented','Tobacco',5,1),(99,8,'Documentation of Tobacco Cessation counseling - if user and Treatment (12 years and older)','TobaccoCessation',10,1),(100,8,'Influenza vaccine offered annually)','InfluenzaImmunization',5,1);
/*!40000 ALTER TABLE `measure` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration_versions`
--

DROP TABLE IF EXISTS `migration_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration_versions` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration_versions`
--

LOCK TABLES `migration_versions` WRITE;
/*!40000 ALTER TABLE `migration_versions` DISABLE KEYS */;
INSERT INTO `migration_versions` VALUES ('20170525162159'),('20170525164113'),('20170526115207'),('20170620081018'),('20170627075601'),('20171031213303'),('20171202172436');
/*!40000 ALTER TABLE `migration_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `program`
--

DROP TABLE IF EXISTS `program`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `program` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `table_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `program`
--

LOCK TABLES `program` WRITE;
/*!40000 ALTER TABLE `program` DISABLE KEYS */;
INSERT INTO `program` VALUES (1,'BTE Diabetes Care Recognition Program','Diabete'),(2,'BTE CAD Care Program','CADCare'),(3,'BTE Hypertension Care Program','Hypertension'),(4,'BTE IBD Care Program','IBDCare'),(5,'BTE Heart Failure Care Program','HeartFailure'),(6,'BTE COPD Program','COPD'),(7,'BTE Depression Care Program','Depression'),(8,'BTE Asthma Care Program','Asthma');
/*!40000 ALTER TABLE `program` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provider`
--

DROP TABLE IF EXISTS `provider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `degree_id` int(11) DEFAULT NULL,
  `speciality_id` int(11) DEFAULT NULL,
  `clinician_id` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `npi` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dea` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `license` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `firstname` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `middlename` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `datetime_birth` datetime DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `clinicianId_user_idx` (`clinician_id`,`user_id`),
  UNIQUE KEY `npi_user_idx` (`npi`,`user_id`),
  KEY `IDX_92C4739CA76ED395` (`user_id`),
  KEY `IDX_92C4739CB35C5756` (`degree_id`),
  KEY `IDX_92C4739C3B5A08D7` (`speciality_id`),
  CONSTRAINT `FK_92C4739C3B5A08D7` FOREIGN KEY (`speciality_id`) REFERENCES `clinician_speciality` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_92C4739CA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_92C4739CB35C5756` FOREIGN KEY (`degree_id`) REFERENCES `clinician_degree` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provider`
--

LOCK TABLES `provider` WRITE;
/*!40000 ALTER TABLE `provider` DISABLE KEYS */;
/*!40000 ALTER TABLE `provider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provider_submission`
--

DROP TABLE IF EXISTS `provider_submission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider_submission` (
  `provider_id` int(11) NOT NULL,
  `submission_id` int(11) NOT NULL,
  PRIMARY KEY (`provider_id`,`submission_id`),
  KEY `IDX_FEB3164BA53A8AA` (`provider_id`),
  KEY `IDX_FEB3164BE1FD4933` (`submission_id`),
  CONSTRAINT `FK_FEB3164BA53A8AA` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_FEB3164BE1FD4933` FOREIGN KEY (`submission_id`) REFERENCES `submission` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provider_submission`
--

LOCK TABLES `provider_submission` WRITE;
/*!40000 ALTER TABLE `provider_submission` DISABLE KEYS */;
/*!40000 ALTER TABLE `provider_submission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recognition`
--

DROP TABLE IF EXISTS `recognition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recognition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `submission_id` int(11) DEFAULT NULL,
  `provider_id` int(11) NOT NULL,
  `score` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_95DB6FB2A53A8AA` (`provider_id`),
  KEY `fk_user_details_1_idx` (`submission_id`),
  CONSTRAINT `FK_95DB6FB2A53A8AA` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_95DB6FB2E1FD4933` FOREIGN KEY (`submission_id`) REFERENCES `submission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recognition`
--

LOCK TABLES `recognition` WRITE;
/*!40000 ALTER TABLE `recognition` DISABLE KEYS */;
/*!40000 ALTER TABLE `recognition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recognition_detail`
--

DROP TABLE IF EXISTS `recognition_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recognition_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recognition_id` int(11) NOT NULL,
  `measure_id` int(11) NOT NULL,
  `numerator` int(11) NOT NULL,
  `denominator` int(11) NOT NULL,
  `score` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_EDC624C85DA37D00` (`measure_id`),
  KEY `fk_user_details_1_idx` (`recognition_id`),
  CONSTRAINT `FK_EDC624C853FF93EF` FOREIGN KEY (`recognition_id`) REFERENCES `recognition` (`id`),
  CONSTRAINT `FK_EDC624C85DA37D00` FOREIGN KEY (`measure_id`) REFERENCES `measure` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recognition_detail`
--

LOCK TABLES `recognition_detail` WRITE;
/*!40000 ALTER TABLE `recognition_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `recognition_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `submission`
--

DROP TABLE IF EXISTS `submission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `submission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `program_id` int(11) NOT NULL,
  `from_ehr` tinyint(1) DEFAULT NULL,
  `ehr` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ehr_id` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reporting_start` datetime DEFAULT NULL,
  `reporting_end` datetime DEFAULT NULL,
  `reporting_for` tinyint(1) DEFAULT NULL,
  `reporting_method` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_DB055AF3A76ED395` (`user_id`),
  KEY `IDX_DB055AF33EB8070A` (`program_id`),
  CONSTRAINT `FK_DB055AF33EB8070A` FOREIGN KEY (`program_id`) REFERENCES `program` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_DB055AF3A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `submission`
--

LOCK TABLES `submission` WRITE;
/*!40000 ALTER TABLE `submission` DISABLE KEYS */;
/*!40000 ALTER TABLE `submission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `submission_asthma`
--

DROP TABLE IF EXISTS `submission_asthma`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `submission_asthma` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `submission_id` int(11) DEFAULT NULL,
  `responsible_provider_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `npi` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `individual_group` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chart_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_visit_date` date DEFAULT NULL,
  `patient_dob` date DEFAULT NULL,
  `patient_gender` int(11) DEFAULT NULL,
  `patient_race` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medicare_part_b` tinyint(1) DEFAULT NULL,
  `asthma_diagnosis` tinyint(1) DEFAULT NULL,
  `asthma_disease_severity` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `asthma_disease_severity_date` date DEFAULT NULL,
  `spirometry_eval_results` int(11) DEFAULT NULL,
  `spirometry_eval_date` date DEFAULT NULL,
  `sabaprescribed_date` date DEFAULT NULL,
  `inhaled_corticosteroid_dispensed` tinyint(1) DEFAULT NULL,
  `inhaled_corticosteroid_dispensed_date` date DEFAULT NULL,
  `inhaled_corticosteroid_prescribed` tinyint(1) DEFAULT NULL,
  `inhaled_corticosteroid_prescribed_date` date DEFAULT NULL,
  `oral_corticosteroid_prescribed` date DEFAULT NULL,
  `oral_corticosteroid_prescribed_date` date DEFAULT NULL,
  `inhaled_bronchodilator_prescribed_date` date DEFAULT NULL,
  `oral_beta3agonists_dispensed_date` date DEFAULT NULL,
  `oral_beta3agonists_prescribed_date` date DEFAULT NULL,
  `asthma_medication_prescribed_date` date DEFAULT NULL,
  `asthma_exacerbation_episode` tinyint(1) DEFAULT NULL,
  `asthma_exacerbation_severity` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `asthma_exacerbation_date` date DEFAULT NULL,
  `asthma_exacerbation_referral_date` date DEFAULT NULL,
  `face_to_face_visit1` date DEFAULT NULL,
  `face_to_face_visit2` date DEFAULT NULL,
  `asthma_action_plan_date` date DEFAULT NULL,
  `asthma_self_management_plan_date` date DEFAULT NULL,
  `inhaler_education_date` date DEFAULT NULL,
  `tobacco_exposure_status` tinyint(1) DEFAULT NULL,
  `tobacco_exposure_status_date` date DEFAULT NULL,
  `tobacco_status` int(11) DEFAULT NULL,
  `tobacco_status_assessment_date` date DEFAULT NULL,
  `tobacco_cessation_advice_or_treatment_date` date DEFAULT NULL,
  `influenza_immunization` smallint(6) DEFAULT NULL,
  `influenza_immunization_date` date DEFAULT NULL,
  `temp_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_BC62538AE1FD4933` (`submission_id`),
  CONSTRAINT `FK_BC62538AE1FD4933` FOREIGN KEY (`submission_id`) REFERENCES `submission` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `submission_asthma`
--

LOCK TABLES `submission_asthma` WRITE;
/*!40000 ALTER TABLE `submission_asthma` DISABLE KEYS */;
/*!40000 ALTER TABLE `submission_asthma` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `submission_cadcare`
--

DROP TABLE IF EXISTS `submission_cadcare`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `submission_cadcare` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `submission_id` int(11) DEFAULT NULL,
  `responsible_provider_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `npi` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `individual_group` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chart_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_visit_date` date DEFAULT NULL,
  `patient_dob` date DEFAULT NULL,
  `patient_gender` int(11) DEFAULT NULL,
  `medicare_part_b` tinyint(1) DEFAULT NULL,
  `cad_diagnosis` tinyint(1) DEFAULT NULL,
  `blood_pressure_date1` date DEFAULT NULL,
  `statin_status` tinyint(1) DEFAULT NULL,
  `systolic1` int(11) DEFAULT NULL,
  `diastolic1` int(11) DEFAULT NULL,
  `systolic2` int(11) DEFAULT NULL,
  `diastolic2` int(11) DEFAULT NULL,
  `blood_pressure_date2` date DEFAULT NULL,
  `anti_platelets` tinyint(1) DEFAULT NULL,
  `beta_blockers` tinyint(1) DEFAULT NULL,
  `diabetes_diagnosis` tinyint(1) DEFAULT NULL,
  `hypertension_diagnosis` tinyint(1) DEFAULT NULL,
  `chronic_kidney_disease_diagnosis` tinyint(1) DEFAULT NULL,
  `chf_diagnosis` tinyint(1) DEFAULT NULL,
  `acei_arb_therapy` tinyint(1) DEFAULT NULL,
  `esrd_patient` tinyint(1) DEFAULT NULL,
  `dialysis_patient` tinyint(1) DEFAULT NULL,
  `tobacco_status` int(11) DEFAULT NULL,
  `tobacco_status_assessment_date` date DEFAULT NULL,
  `tobacco_cessation_advice_or_treatment` tinyint(1) DEFAULT NULL,
  `tobacco_cessation_advice_or_treatment_date` date DEFAULT NULL,
  `bmi_value` double DEFAULT NULL,
  `bmi_value_date` date DEFAULT NULL,
  `nutrition_counseling` tinyint(1) DEFAULT NULL,
  `nutrition_counseling_date` date DEFAULT NULL,
  `influenza_immunization` smallint(6) DEFAULT NULL,
  `influenza_immunization_date` date DEFAULT NULL,
  `activity_counseling` tinyint(1) DEFAULT NULL,
  `activity_counseling_date` date DEFAULT NULL,
  `phq2screening` tinyint(1) DEFAULT NULL,
  `phq2screening_date` date DEFAULT NULL,
  `phq9screening` tinyint(1) DEFAULT NULL,
  `phq9screening_date` date DEFAULT NULL,
  `temp_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_E49E68FBE1FD4933` (`submission_id`),
  CONSTRAINT `FK_E49E68FBE1FD4933` FOREIGN KEY (`submission_id`) REFERENCES `submission` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `submission_cadcare`
--

LOCK TABLES `submission_cadcare` WRITE;
/*!40000 ALTER TABLE `submission_cadcare` DISABLE KEYS */;
/*!40000 ALTER TABLE `submission_cadcare` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `submission_copd`
--

DROP TABLE IF EXISTS `submission_copd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `submission_copd` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `submission_id` int(11) DEFAULT NULL,
  `responsible_provider_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `npi` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `individual_group` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chart_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_visit_date` date DEFAULT NULL,
  `patient_dob` date DEFAULT NULL,
  `patient_gender` int(11) DEFAULT NULL,
  `patient_race` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medicare_part_b` tinyint(1) DEFAULT NULL,
  `copddiagnosis` tinyint(1) DEFAULT NULL,
  `copdsymptoms` tinyint(1) DEFAULT NULL,
  `spirometry_eval_results` int(11) DEFAULT NULL,
  `spirometry_eval_date` date DEFAULT NULL,
  `inhaled_bronchodilator_prescribed_date` date DEFAULT NULL,
  `inhaled_corticosteroid_dispensed_date` date DEFAULT NULL,
  `inhaled_corticosteroid_prescribed_date` date DEFAULT NULL,
  `tobacco_status` int(11) DEFAULT NULL,
  `tobacco_status_assessment_date` date DEFAULT NULL,
  `tobacco_cessation_advice_or_treatment_date` date DEFAULT NULL,
  `respiratory_failure_diagnosis` tinyint(1) DEFAULT NULL,
  `right_heart_failure_diagnosis` tinyint(1) DEFAULT NULL,
  `oxygen_saturation_level` int(11) DEFAULT NULL,
  `oxygen_saturation_level_date` date DEFAULT NULL,
  `pa_o2level` int(11) DEFAULT NULL,
  `pa_o2level_date` date DEFAULT NULL,
  `long_term_oxygen_therapy_date` date DEFAULT NULL,
  `influenza_immunization` smallint(6) DEFAULT NULL,
  `influenza_immunization_date` date DEFAULT NULL,
  `pneumococcal_vaccine_pcv13` smallint(6) DEFAULT NULL,
  `pneumococcal_vaccine_pcv13date` date DEFAULT NULL,
  `pneumococcal_vaccine_pcv23` smallint(6) DEFAULT NULL,
  `pneumococcal_vaccine_pcv23date` date DEFAULT NULL,
  `copdexacerbation_episode` int(11) DEFAULT NULL,
  `copdexacerbation_episode_date` date DEFAULT NULL,
  `copdexacerbation_episode_state_date` date DEFAULT NULL,
  `face_to_face_visit` date DEFAULT NULL,
  `low_dose_ctdate` date DEFAULT NULL,
  `low_dose_ctreferral` tinyint(1) DEFAULT NULL,
  `low_dose_ctdocumented` tinyint(1) DEFAULT NULL,
  `advanced_directive` smallint(6) DEFAULT NULL,
  `temp_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_BCBABC34E1FD4933` (`submission_id`),
  CONSTRAINT `FK_BCBABC34E1FD4933` FOREIGN KEY (`submission_id`) REFERENCES `submission` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `submission_copd`
--

LOCK TABLES `submission_copd` WRITE;
/*!40000 ALTER TABLE `submission_copd` DISABLE KEYS */;
/*!40000 ALTER TABLE `submission_copd` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `submission_depression`
--

DROP TABLE IF EXISTS `submission_depression`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `submission_depression` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `submission_id` int(11) DEFAULT NULL,
  `responsible_provider_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `npi` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `individual_group` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chart_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_visit_date` date DEFAULT NULL,
  `patient_dob` date DEFAULT NULL,
  `patient_gender` int(11) DEFAULT NULL,
  `patient_race` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medicare_part_b` tinyint(1) DEFAULT NULL,
  `phq2screening_score` int(11) DEFAULT NULL,
  `phq2screening_date` date DEFAULT NULL,
  `phq9screening_score` int(11) DEFAULT NULL,
  `phq9screening_date` date DEFAULT NULL,
  `phq9screening2score` int(11) DEFAULT NULL,
  `phq9screening2date` date DEFAULT NULL,
  `phqadolescent_screening_score` int(11) DEFAULT NULL,
  `phqadolescent_screening_date` date DEFAULT NULL,
  `cognitive_behavior_therapy_date` date DEFAULT NULL,
  `specialist_referral_date` date DEFAULT NULL,
  `psychotherapy_date` date DEFAULT NULL,
  `antidepressant_initial_dispensed_date` date DEFAULT NULL,
  `antidepressant_active_date` date DEFAULT NULL,
  `antidepressant_initial_prescribed_date` date DEFAULT NULL,
  `depression_diagnosis` tinyint(1) DEFAULT NULL,
  `dysthymia_diagnosis` tinyint(1) DEFAULT NULL,
  `substance_use_screening` tinyint(1) DEFAULT NULL,
  `substance_use_screening_date` date DEFAULT NULL,
  `substance_use_counseling_date` date DEFAULT NULL,
  `activity_counseling` tinyint(1) DEFAULT NULL,
  `activity_counseling_date` date DEFAULT NULL,
  `care_coordination` tinyint(1) DEFAULT NULL,
  `monitoringand_treatment` tinyint(1) DEFAULT NULL,
  `case_load_reviews` tinyint(1) DEFAULT NULL,
  `temp_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_A6C15088E1FD4933` (`submission_id`),
  CONSTRAINT `FK_A6C15088E1FD4933` FOREIGN KEY (`submission_id`) REFERENCES `submission` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `submission_depression`
--

LOCK TABLES `submission_depression` WRITE;
/*!40000 ALTER TABLE `submission_depression` DISABLE KEYS */;
/*!40000 ALTER TABLE `submission_depression` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `submission_diabete`
--

DROP TABLE IF EXISTS `submission_diabete`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `submission_diabete` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `submission_id` int(11) DEFAULT NULL,
  `responsible_provider_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `npi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `individual_group` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chart_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_visit_date` date DEFAULT NULL,
  `patient_dob` date DEFAULT NULL,
  `patient_gender` int(11) DEFAULT NULL,
  `patient_race` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medicare_part_b` tinyint(1) DEFAULT NULL,
  `diabetes_diagnosis` tinyint(1) DEFAULT NULL,
  `hb_a1c1` double DEFAULT NULL,
  `hb_a1c_date1` date DEFAULT NULL,
  `hb_a1c2` double DEFAULT NULL,
  `hb_a1c_date2` date DEFAULT NULL,
  `ldllevel` int(11) DEFAULT NULL,
  `ldldate` date DEFAULT NULL,
  `statin_status` tinyint(1) DEFAULT NULL,
  `systolic1` int(11) DEFAULT NULL,
  `diastolic1` int(11) DEFAULT NULL,
  `blood_pressure_date1` date DEFAULT NULL,
  `systolic2` int(11) DEFAULT NULL,
  `diastolic2` int(11) DEFAULT NULL,
  `blood_pressure_date2` date DEFAULT NULL,
  `tobacco_status` int(11) DEFAULT NULL,
  `tobacco_status_assessment_date` date DEFAULT NULL,
  `tobacco_cessation_advice_or_treatment_date` date DEFAULT NULL,
  `foot_exam` tinyint(1) DEFAULT NULL,
  `foot_exam_date` date DEFAULT NULL,
  `bilateral_amputation` tinyint(1) DEFAULT NULL,
  `retinopathy` tinyint(1) DEFAULT NULL,
  `retinal_exam_date` date DEFAULT NULL,
  `blindness` tinyint(1) DEFAULT NULL,
  `nephropathy_diagnosis` tinyint(1) DEFAULT NULL,
  `nephropathy_evidence` tinyint(1) DEFAULT NULL,
  `microalbuminuria_lab` int(11) DEFAULT NULL,
  `microalbuminuria_date` date DEFAULT NULL,
  `macroalbuminuria_lab` int(11) DEFAULT NULL,
  `macroalbuminuria_date` date DEFAULT NULL,
  `hypertension_diagnosis` tinyint(1) DEFAULT NULL,
  `acei_arb_therapy` tinyint(1) DEFAULT NULL,
  `bmi_calculation` double DEFAULT NULL,
  `bmi_date` date DEFAULT NULL,
  `bmi_counseling` tinyint(1) DEFAULT NULL,
  `bmi_counseling_date` date DEFAULT NULL,
  `ascvd_assessment` tinyint(1) DEFAULT NULL,
  `ascvd_date` date DEFAULT NULL,
  `ascvd_results` int(11) DEFAULT NULL,
  `aspirin_therapy` int(11) DEFAULT NULL,
  `aspirin_use_considered` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phq2screening` tinyint(1) DEFAULT NULL,
  `phq2screening_date` date DEFAULT NULL,
  `phq9screening` tinyint(1) DEFAULT NULL,
  `phq9screening_date` date DEFAULT NULL,
  `temp_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_EC653357E1FD4933` (`submission_id`),
  CONSTRAINT `FK_EC653357E1FD4933` FOREIGN KEY (`submission_id`) REFERENCES `submission` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `submission_diabete`
--

LOCK TABLES `submission_diabete` WRITE;
/*!40000 ALTER TABLE `submission_diabete` DISABLE KEYS */;
/*!40000 ALTER TABLE `submission_diabete` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `submission_heartfailure`
--

DROP TABLE IF EXISTS `submission_heartfailure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `submission_heartfailure` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `submission_id` int(11) DEFAULT NULL,
  `responsible_provider_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `npi` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `individual_group` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chart_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_visit_date` date DEFAULT NULL,
  `patient_dob` date DEFAULT NULL,
  `patient_gender` int(11) DEFAULT NULL,
  `patient_race` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medicare_part_b` tinyint(1) DEFAULT NULL,
  `hfdiagnosis` tinyint(1) DEFAULT NULL,
  `lvsddiagnosis` tinyint(1) DEFAULT NULL,
  `lvf_assess_date` date DEFAULT NULL,
  `betablocker_therapy` tinyint(1) DEFAULT NULL,
  `acei_arb_therapy` tinyint(1) DEFAULT NULL,
  `blood_pressure_date1` date DEFAULT NULL,
  `systolic1` int(11) DEFAULT NULL,
  `diastolic1` int(11) DEFAULT NULL,
  `blood_pressure_date2` date DEFAULT NULL,
  `systolic2` int(11) DEFAULT NULL,
  `diastolic2` int(11) DEFAULT NULL,
  `weight_measure_date` date DEFAULT NULL,
  `peripheraledema_assessment` tinyint(1) DEFAULT NULL,
  `orthopnea_assessment` tinyint(1) DEFAULT NULL,
  `jvpassessment` tinyint(1) DEFAULT NULL,
  `tobacco_status` int(11) DEFAULT NULL,
  `tobacco_status_assessment_date` date DEFAULT NULL,
  `tobacco_cessation_advice_or_treatment` tinyint(1) DEFAULT NULL,
  `tobacco_cessation_advice_or_treatment_date` date DEFAULT NULL,
  `bmi_value` double DEFAULT NULL,
  `bmi_value_date` date DEFAULT NULL,
  `nutrition_counseling` double DEFAULT NULL,
  `nutrition_counseling_date` date DEFAULT NULL,
  `activity_status` tinyint(1) DEFAULT NULL,
  `activity_status_date` date DEFAULT NULL,
  `activity_counseling` tinyint(1) DEFAULT NULL,
  `activity_counseling_date` date DEFAULT NULL,
  `heartfailure_type` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `heartfailure_severity` tinyint(1) DEFAULT NULL,
  `esrd_patient` tinyint(1) DEFAULT NULL,
  `dialysis_patient` tinyint(1) DEFAULT NULL,
  `temp_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_FD701C4EE1FD4933` (`submission_id`),
  CONSTRAINT `FK_FD701C4EE1FD4933` FOREIGN KEY (`submission_id`) REFERENCES `submission` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `submission_heartfailure`
--

LOCK TABLES `submission_heartfailure` WRITE;
/*!40000 ALTER TABLE `submission_heartfailure` DISABLE KEYS */;
/*!40000 ALTER TABLE `submission_heartfailure` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `submission_hypertension`
--

DROP TABLE IF EXISTS `submission_hypertension`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `submission_hypertension` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `submission_id` int(11) DEFAULT NULL,
  `responsible_provider_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `npi` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `individual_group` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chart_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_visit_date` date DEFAULT NULL,
  `patient_dob` date DEFAULT NULL,
  `patient_gender` int(11) DEFAULT NULL,
  `medicare_part_b` int(11) DEFAULT NULL,
  `blood_pressure_date1` date DEFAULT NULL,
  `systolic1` int(11) DEFAULT NULL,
  `diastolic1` int(11) DEFAULT NULL,
  `systolic2` int(11) DEFAULT NULL,
  `diastolic2` int(11) DEFAULT NULL,
  `blood_pressure_date2` date DEFAULT NULL,
  `hypertension_diagnosis` int(11) DEFAULT NULL,
  `chronic_kidney_disease_diagnosis` int(11) DEFAULT NULL,
  `diabetes_diagnosis` int(11) DEFAULT NULL,
  `acei_arb_therapy` int(11) DEFAULT NULL,
  `calcium_channel_blocker` int(11) DEFAULT NULL,
  `thiazide` int(11) DEFAULT NULL,
  `nephropathy_diagnosis` int(11) DEFAULT NULL,
  `nephropathy_screening` int(11) DEFAULT NULL,
  `nephropathy_screening_date` date DEFAULT NULL,
  `esrd_patient` int(11) DEFAULT NULL,
  `dialysis_patient` int(11) DEFAULT NULL,
  `creatinine` int(11) DEFAULT NULL,
  `creatinine_date` date DEFAULT NULL,
  `tobacco_status` int(11) DEFAULT NULL,
  `tobacco_status_assessment_date` date DEFAULT NULL,
  `tobacco_cessation_advice_or_treatment_date` date DEFAULT NULL,
  `bmi_value` double DEFAULT NULL,
  `bmi_value_date` date DEFAULT NULL,
  `dashdiet_counseling` int(11) DEFAULT NULL,
  `dashdiet_date` date DEFAULT NULL,
  `dashsodium_counseling` int(11) DEFAULT NULL,
  `dashsodium_date` date DEFAULT NULL,
  `activity_status` int(11) DEFAULT NULL,
  `activity_status_date` date DEFAULT NULL,
  `activity_counseling` int(11) DEFAULT NULL,
  `activity_counseling_date` date DEFAULT NULL,
  `temp_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_33363D03E1FD4933` (`submission_id`),
  CONSTRAINT `FK_33363D03E1FD4933` FOREIGN KEY (`submission_id`) REFERENCES `submission` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `submission_hypertension`
--

LOCK TABLES `submission_hypertension` WRITE;
/*!40000 ALTER TABLE `submission_hypertension` DISABLE KEYS */;
/*!40000 ALTER TABLE `submission_hypertension` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `submission_ibdcare`
--

DROP TABLE IF EXISTS `submission_ibdcare`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `submission_ibdcare` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `submission_id` int(11) DEFAULT NULL,
  `responsible_provider_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `npi` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `group_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `individual_group` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chart_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_visit_date` date DEFAULT NULL,
  `patient_dob` date DEFAULT NULL,
  `patient_gender` int(11) DEFAULT NULL,
  `patient_race` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `medicare_part_b` tinyint(1) DEFAULT NULL,
  `ibddiagnosis` tinyint(1) DEFAULT NULL,
  `ibdunder_care_provider` tinyint(1) DEFAULT NULL,
  `ibddiagnosis_one_year` tinyint(1) DEFAULT NULL,
  `ibddiagnosis_eight_year` tinyint(1) DEFAULT NULL,
  `ibdtype` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ibdtype_date` date DEFAULT NULL,
  `ibdlocation` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ibdextent` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ibdextent_date` date DEFAULT NULL,
  `ibdactivity` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ibdactivity_date` date DEFAULT NULL,
  `external_manifestations` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `external_mainfestations_date_assessed` date DEFAULT NULL,
  `corticosteroid10mg60days` tinyint(1) DEFAULT NULL,
  `corticosteroid_sparing_therapy` tinyint(1) DEFAULT NULL,
  `bone_loss_risk_assessed` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bone_loss_risk_date_assessed` date DEFAULT NULL,
  `osteoporosis_therapy` tinyint(1) DEFAULT NULL,
  `osteoporosis_therapy_date_assessed` date DEFAULT NULL,
  `first_course_anti_tnftherapy` tinyint(1) DEFAULT NULL,
  `anti_tnfinitiation_date` date DEFAULT NULL,
  `tuberculosis_screening` tinyint(1) DEFAULT NULL,
  `tuberculosis_screening_date` date DEFAULT NULL,
  `positive_tbreated_tb` tinyint(1) DEFAULT NULL,
  `hepatitis_bstatus` tinyint(1) DEFAULT NULL,
  `hepatitis_bstatus_date` date DEFAULT NULL,
  `influenza_immunization` smallint(6) DEFAULT NULL,
  `influenza_immunization_date` date DEFAULT NULL,
  `pneumococcal_vaccine` smallint(6) DEFAULT NULL,
  `pneumococccal_vaccine_date` date DEFAULT NULL,
  `tobacco_status` int(11) DEFAULT NULL,
  `tobacco_status_assessment_date` date DEFAULT NULL,
  `tobacco_cessation_advice_or_treatment_date` date DEFAULT NULL,
  `surveillance_colonoscopy` tinyint(1) DEFAULT NULL,
  `surveillance_colonoscopy_date` date DEFAULT NULL,
  `ulcerative_colitis_diagnosis` tinyint(1) DEFAULT NULL,
  `crohn_colitis_diagnosis` tinyint(1) DEFAULT NULL,
  `phq2screening` tinyint(1) DEFAULT NULL,
  `phq2screening_date` date DEFAULT NULL,
  `phq9screening` tinyint(1) DEFAULT NULL,
  `phq9screening_date` date DEFAULT NULL,
  `ibdbiologics` tinyint(1) DEFAULT NULL,
  `ibdbiologics_date_assessed` date DEFAULT NULL,
  `temp_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_C67A451BE1FD4933` (`submission_id`),
  CONSTRAINT `FK_C67A451BE1FD4933` FOREIGN KEY (`submission_id`) REFERENCES `submission` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `submission_ibdcare`
--

LOCK TABLES `submission_ibdcare` WRITE;
/*!40000 ALTER TABLE `submission_ibdcare` DISABLE KEYS */;
/*!40000 ALTER TABLE `submission_ibdcare` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `datetime_registration` datetime DEFAULT NULL,
  `datetime_last_login` datetime DEFAULT NULL,
  `forgotten_password_token` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `confirm_email_token` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'test@bte.com','iOgyhdY1gJJPj7y7mMN8obgqMQZH2fLDuQuXfqZesC1Iqxo6iHxRuAA9m8E1ZUz76OIiPGTann7uJ3BNhPDoEA==','sdfsdf','sdfsdf','123123',1,NULL,NULL,NULL,NULL,'2017-05-23 02:51:00','2017-05-23 08:51:00');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_practice`
--

DROP TABLE IF EXISTS `user_practice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_practice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip_code` int(11) DEFAULT NULL,
  `phone` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `npi` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `primary_contact_name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `primary_contact_email` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_9F61D04EA76ED395` (`user_id`),
  KEY `fk_user_details_1_idx` (`user_id`),
  CONSTRAINT `FK_9F61D04EA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_practice`
--

LOCK TABLES `user_practice` WRITE;
/*!40000 ALTER TABLE `user_practice` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_practice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_2DE8C6A3A76ED395` (`user_id`),
  CONSTRAINT `FK_2DE8C6A3A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_session`
--

DROP TABLE IF EXISTS `user_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_session` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `datetime_create` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8849CBDE5F37A13B` (`token`),
  KEY `IDX_8849CBDEA76ED395` (`user_id`),
  CONSTRAINT `FK_8849CBDEA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_session`
--

LOCK TABLES `user_session` WRITE;
/*!40000 ALTER TABLE `user_session` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_session` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-13 16:24:05
