<?php

namespace App\DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170525164113 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('INSERT INTO clinician_degree (id, name) VALUES (1, \'M.D.\');
                        INSERT INTO clinician_degree(id, name) VALUES (2, \'D.O.\');
                        INSERT INTO clinician_degree(id, name) VALUES (3, \'N.P.\');
                        INSERT INTO clinician_degree(id, name) VALUES (4, \'P.A.\');
                        ');

        $this->addSql('INSERT INTO clinician_speciality(id, name) VALUES (1, \'Allergy/Immunology\');
                        INSERT INTO clinician_speciality(id, name) VALUES (2, \'Cardiology\');
                        INSERT INTO clinician_speciality(id, name) VALUES (3, \'Critical Care Services\');
                        INSERT INTO clinician_speciality(id, name) VALUES (4, \'Dermatology\');
                        INSERT INTO clinician_speciality(id, name) VALUES (5, \'Endocrinology\');
                        INSERT INTO clinician_speciality(id, name) VALUES (6, \'Gastroenterology\');
                        INSERT INTO clinician_speciality(id, name) VALUES (7, \'Gen/Fam Practice\');
                        INSERT INTO clinician_speciality(id, name) VALUES (8, \'Geriatric Medicine\');
                        INSERT INTO clinician_speciality(id, name) VALUES (9, \'Hematology\');
                        INSERT INTO clinician_speciality(id, name) VALUES (10, \'Infectious Disease\');
                        INSERT INTO clinician_speciality(id, name) VALUES (11, \'Internal Medicine\');
                        INSERT INTO clinician_speciality(id, name) VALUES (12, \'Nephrology\');
                        INSERT INTO clinician_speciality(id, name) VALUES (13, \'Neurology\');
                        INSERT INTO clinician_speciality(id, name) VALUES (14, \'Neurosurgery\');
                        INSERT INTO clinician_speciality(id, name) VALUES (15, \'Obstetrics/Gynecology\');
                        INSERT INTO clinician_speciality(id, name) VALUES (16, \'Occ. Medicine\');
                        INSERT INTO clinician_speciality(id, name) VALUES (17, \'Oncology\');
                        INSERT INTO clinician_speciality(id, name) VALUES (18, \'Ophthalmology\');
                        INSERT INTO clinician_speciality(id, name) VALUES (19, \'Orthopedics\');
                        INSERT INTO clinician_speciality(id, name) VALUES (20, \'Otolaryngology\');
                        INSERT INTO clinician_speciality(id, name) VALUES (21, \'Pediatrics\');
                        INSERT INTO clinician_speciality(id, name) VALUES (22, \'Phys/Rehab Medicine\');
                        INSERT INTO clinician_speciality(id, name) VALUES (23, \'Psychiatry\');
                        INSERT INTO clinician_speciality(id, name) VALUES (24, \'Psychopharmacology\');
                        INSERT INTO clinician_speciality(id, name) VALUES (25, \'Pulmonary Medicine\');
                        INSERT INTO clinician_speciality(id, name) VALUES (26, \'Rheumatology\');
                        INSERT INTO clinician_speciality(id, name) VALUES (27, \'Surgery\');
                        INSERT INTO clinician_speciality(id, name) VALUES (28, \'Urology\');
                        INSERT INTO clinician_speciality(id, name) VALUES (29, \'Other – not listed\');
                        ');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
