<?php

namespace App\DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180420120856 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE submission_asthma ADD asthma_disease_severity_temp SMALLINT DEFAULT NULL');

        // "Intermittent", "Persistent Mild", "Persistent Moderate", "Persistent Severe"
        $this->addSql('UPDATE submission_asthma SET asthma_disease_severity_temp  = 1 WHERE asthma_disease_severity = "Intermittent"');
        $this->addSql('UPDATE submission_asthma SET asthma_disease_severity_temp  = 2 WHERE asthma_disease_severity = "Persistent Mild"');
        $this->addSql('UPDATE submission_asthma SET asthma_disease_severity_temp  = 3 WHERE asthma_disease_severity = "Persistent Moderate"');
        $this->addSql('UPDATE submission_asthma SET asthma_disease_severity_temp  = 4 WHERE asthma_disease_severity = "Persistent Severe"');

        $this->addSql('UPDATE submission_asthma SET asthma_disease_severity = NULL');
        $this->addSql('ALTER TABLE submission_asthma CHANGE asthma_disease_severity asthma_disease_severity SMALLINT DEFAULT NULL');

        $this->addSql('UPDATE submission_asthma SET asthma_disease_severity = asthma_disease_severity_temp');
        $this->addSql('ALTER TABLE submission_asthma DROP COLUMN asthma_disease_severity_temp');


        // ------
        $this->addSql('ALTER TABLE submission_asthma ADD asthma_exacerbation_severity_temp SMALLINT DEFAULT NULL');

        // "Mild", "Moderate", "Severe", "Life Threatening"
        $this->addSql('UPDATE submission_asthma SET asthma_exacerbation_severity_temp  = 1 WHERE asthma_exacerbation_severity = "Mild"');
        $this->addSql('UPDATE submission_asthma SET asthma_exacerbation_severity_temp  = 2 WHERE asthma_exacerbation_severity = "Moderate"');
        $this->addSql('UPDATE submission_asthma SET asthma_exacerbation_severity_temp  = 3 WHERE asthma_exacerbation_severity = "Severe"');
        $this->addSql('UPDATE submission_asthma SET asthma_exacerbation_severity_temp  = 4 WHERE asthma_exacerbation_severity = "Life Threatening"');

        $this->addSql('UPDATE submission_asthma SET asthma_exacerbation_severity = NULL');
        $this->addSql('ALTER TABLE submission_asthma CHANGE asthma_exacerbation_severity asthma_exacerbation_severity SMALLINT DEFAULT NULL');

        $this->addSql('UPDATE submission_asthma SET asthma_exacerbation_severity = asthma_exacerbation_severity_temp');
        $this->addSql('ALTER TABLE submission_asthma DROP COLUMN asthma_exacerbation_severity_temp');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE submission_asthma CHANGE asthma_disease_severity asthma_disease_severity VARCHAR(32) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE asthma_exacerbation_severity asthma_exacerbation_severity VARCHAR(32) DEFAULT NULL COLLATE utf8_unicode_ci');
    }
}
