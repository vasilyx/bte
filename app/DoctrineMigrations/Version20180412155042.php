<?php

namespace App\DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180412155042 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE provider ADD responsible_provider_id VARCHAR(256) DEFAULT NULL, ADD practice_address1 VARCHAR(256) DEFAULT NULL, ADD practice_address2 VARCHAR(256) DEFAULT NULL, ADD practice_city VARCHAR(256) DEFAULT NULL, ADD practice_state VARCHAR(256) DEFAULT NULL, ADD practice_zip_code VARCHAR(256) DEFAULT NULL, ADD emailaddress VARCHAR(256) DEFAULT NULL, ADD practice_phone VARCHAR(256) DEFAULT NULL, ADD practice_id VARCHAR(256) DEFAULT NULL, ADD practice_name VARCHAR(256) DEFAULT NULL, ADD individual_group VARCHAR(256) DEFAULT NULL, ADD cchitcertified VARCHAR(256) DEFAULT NULL, ADD full_patient_panel VARCHAR(256) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE provider DROP responsible_provider_id, DROP practice_address1, DROP practice_address2, DROP practice_city, DROP practice_state, DROP practice_zip_code, DROP emailaddress, DROP practice_phone, DROP practice_id, DROP practice_name, DROP individual_group, DROP cchitcertified, DROP full_patient_panel');
    }
}
