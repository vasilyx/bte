<?php

namespace App\DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180420094923 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE submission_ibdcare ADD ibdlocation_temp SMALLINT DEFAULT NULL');

        // "Upper GI", "Small Intestine", "Large Intestine"
        $this->addSql('UPDATE submission_ibdcare SET ibdlocation_temp  = 1 WHERE ibdlocation = "Upper GI"');
        $this->addSql('UPDATE submission_ibdcare SET ibdlocation_temp  = 2 WHERE ibdlocation = "Small Intestine"');
        $this->addSql('UPDATE submission_ibdcare SET ibdlocation_temp  = 3 WHERE ibdlocation = "Large Intestine"');

        $this->addSql('UPDATE submission_ibdcare SET ibdlocation = NULL');
        $this->addSql('ALTER TABLE submission_ibdcare CHANGE ibdlocation ibdlocation SMALLINT DEFAULT NULL');

        $this->addSql('UPDATE submission_ibdcare SET ibdlocation = ibdlocation_temp');
        $this->addSql('ALTER TABLE submission_ibdcare DROP COLUMN ibdlocation_temp');


    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE submission_ibdcare CHANGE ibdlocation ibdlocation VARCHAR(32) DEFAULT NULL COLLATE utf8_unicode_ci');
    }
}
