<?php

namespace App\DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180503161408 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE INDEX npi_idx ON submission_cadcare (npi)');
        $this->addSql('CREATE INDEX npi_idx ON submission_depression (npi)');
        $this->addSql('CREATE INDEX npi_idx ON submission_hypertension (npi)');
        $this->addSql('CREATE INDEX npi_idx ON submission_copd (npi)');
        $this->addSql('CREATE INDEX npi_idx ON submission_heartfailure (npi)');
        $this->addSql('CREATE INDEX npi_idx ON submission_ibdcare (npi)');
        $this->addSql('CREATE INDEX npi_idx ON submission_diabete (npi)');
        $this->addSql('CREATE INDEX npi_idx ON submission_asthma (npi)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX npi_idx ON submission_asthma');
        $this->addSql('DROP INDEX npi_idx ON submission_cadcare');
        $this->addSql('DROP INDEX npi_idx ON submission_copd');
        $this->addSql('DROP INDEX npi_idx ON submission_depression');
        $this->addSql('DROP INDEX npi_idx ON submission_diabete');
        $this->addSql('DROP INDEX npi_idx ON submission_heartfailure');
        $this->addSql('DROP INDEX npi_idx ON submission_hypertension');
        $this->addSql('DROP INDEX npi_idx ON submission_ibdcare');
    }
}
