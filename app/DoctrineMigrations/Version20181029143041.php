<?php

namespace App\DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181029143041 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('INSERT INTO program(id, name, table_name) VALUES (9, \'BTE Maternity Care\', \'Maternity\');');

        $measures = array(

            array('pid' => 9, 'name' => 'Frequency of Prenatal & Postpartum Visits', 'fname' => 'Visits', 'total' => 20),
            array('pid' => 9, 'name' => 'Risk-Appropriate Screenings During Pre-Natal Care Visits', 'fname' => 'RiskVisits', 'total' => 10),
            array('pid' => 9, 'name' => 'Genetic Carrier Screenings', 'fname' => 'GeneticCarrier', 'total' => 10),
            array('pid' => 9, 'name' => 'Pre-Natal Immunizations', 'fname' => 'Immunizations', 'total' => 5),
            array('pid' => 9, 'name' => 'Low-Dose Aspirin for Prevention of Pre-Eclampsia', 'fname' => 'Aspirin', 'total' => 5),
            array('pid' => 9, 'name' => 'Performed ultrasound at 18–22 weeks of pregnancy', 'fname' => 'Ultrasound', 'total' => 5),
            array('pid' => 9, 'name' => 'Antibiotic Prophylaxis if GBS (Group B Streptococcus) Positive', 'fname' => 'Antibiotic', 'total' => 5),
            array('pid' => 9, 'name' => 'Optimal Antenatal Corticosteroid Administration', 'fname' => 'Corticosteroid', 'total' => 5),
            array('pid' => 9, 'name' => 'Vaginal Birth After Cesarean (VBAC) Consent', 'fname' => 'VBAC', 'total' => 5),
            array('pid' => 9, 'name' => 'Primary C-Section Rates (NTSV Rates)', 'fname' => 'NTSV', 'total' => 10),
            array('pid' => 9, 'name' => 'VLBW Babies Managed in NICU level 3 or 4', 'fname' => 'VLBW', 'total' => 20),
            array('pid' => 9, 'name' => 'Postpartum Depression Screening (optional)', 'fname' => 'DepressionScreening', 'total' => 2.5),
            array('pid' => 9, 'name' => 'Drug and Alcohol Screening (optional)', 'fname' => 'AlcoholScreening', 'total' => 2.5),
            array('pid' => 9, 'name' => 'Interpersonal Violence Screening (optional)', 'fname' => 'ViolenceScreening', 'total' => 2.5),

        );

        $id = 100;
        foreach ($measures as $measure) {
            $id++;
            $this->addSql("INSERT INTO measure(id, program_id, name, func_name, total) VALUES ($id, :pid, :name, :fname, :total);", $measure);
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {

    }
}
