<?php

namespace App\DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180420090100 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE submission_ibdcare ADD ibdtype_temp SMALLINT DEFAULT NULL');

        // "Crohn's Disease", "Indeterminate Colitis", "Ulcerative Colitis"
        $this->addSql('UPDATE submission_ibdcare SET ibdtype_temp  = 1 WHERE ibdtype LIKE "%Disease"');
        $this->addSql('UPDATE submission_ibdcare SET ibdtype_temp  = 2 WHERE ibdtype LIKE "Indeterminate%"');
        $this->addSql('UPDATE submission_ibdcare SET ibdtype_temp  = 3 WHERE ibdtype LIKE "Ulcerative%"');

        $this->addSql('UPDATE submission_ibdcare SET ibdtype = NULL');
        $this->addSql('ALTER TABLE submission_ibdcare CHANGE ibdtype ibdtype SMALLINT DEFAULT NULL');
        $this->addSql('UPDATE submission_ibdcare SET ibdtype = ibdtype_temp');
        $this->addSql('ALTER TABLE submission_ibdcare DROP COLUMN ibdtype_temp');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE submission_ibdcare CHANGE ibdtype ibdtype VARCHAR(32) DEFAULT NULL COLLATE utf8_unicode_ci');
    }
}
