<?php

namespace App\DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181104210720 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE submission_maternity (id INT AUTO_INCREMENT NOT NULL, submission_id INT DEFAULT NULL, responsible_provider_id VARCHAR(255) DEFAULT NULL, npi VARCHAR(32) DEFAULT NULL, group_id VARCHAR(32) DEFAULT NULL, individual_group VARCHAR(32) DEFAULT NULL, chart_id VARCHAR(32) DEFAULT NULL, delivery_date DATE DEFAULT NULL, patient_dob DATE DEFAULT NULL, patient_gender INT DEFAULT NULL, patient_race VARCHAR(64) DEFAULT NULL, medicare_part_b SMALLINT DEFAULT NULL, lmp_date DATE DEFAULT NULL, due_date DATE DEFAULT NULL, initial_visit_date DATE DEFAULT NULL, tri2visit_date1 DATE DEFAULT NULL, tri2visit_date2 DATE DEFAULT NULL, tri2visit_date3 DATE DEFAULT NULL, tri2visit_date4 DATE DEFAULT NULL, tri2visit_date5 DATE DEFAULT NULL, tri2visit_date6 DATE DEFAULT NULL, tri3visit_date1 DATE DEFAULT NULL, tri3visit_date2 DATE DEFAULT NULL, tri3visit_date3 DATE DEFAULT NULL, tri3visit_date4 DATE DEFAULT NULL, tri3visit_date5 DATE DEFAULT NULL, tri3visit_date6 DATE DEFAULT NULL, tri3visit_date7 DATE DEFAULT NULL, postpartum_visit_date DATE DEFAULT NULL, abo_rh_screen_date DATE DEFAULT NULL, cbc_date1 DATE DEFAULT NULL, vdrl_screen_date DATE DEFAULT NULL, chlamydia_screen_date DATE DEFAULT NULL, gonorrhea_screen_date DATE DEFAULT NULL, hiv_screen_date DATE DEFAULT NULL, urine_culture_sensitivity_date DATE DEFAULT NULL, hb_a1cdate DATE DEFAULT NULL, rubella_screen_date DATE DEFAULT NULL, fetal_aneuploidy_screen_date DATE DEFAULT NULL, afp_screen_date DATE DEFAULT NULL, quad_screen_date DATE DEFAULT NULL, cbc_date2 DATE DEFAULT NULL, glucose_screen_date DATE DEFAULT NULL, antibodies_screen_date DATE DEFAULT NULL, rho_gamscreen_date DATE DEFAULT NULL, group_bstrep_screen_date DATE DEFAULT NULL, cystic_fibrosis_screening_date DATE DEFAULT NULL, spinal_muscular_atrophy_date DATE DEFAULT NULL, hemoglobinopathies_date DATE DEFAULT NULL, pan_ethic_expanded_carrier_screen_date DATE DEFAULT NULL, gentic_carrier_screen DATE DEFAULT NULL, influenza_date DATE DEFAULT NULL, tetanus_date DATE DEFAULT NULL, diphtheria_date DATE DEFAULT NULL, pertussis_tdap_date DATE DEFAULT NULL, hx_preeclampsia SMALLINT DEFAULT NULL, hx_pretermdelivery INT DEFAULT NULL, low_dose_aspirin_date DATE DEFAULT NULL, ultrasound_date DATE DEFAULT NULL, group_bstrep SMALLINT DEFAULT NULL, antibiotic_prophylaxis_date DATE DEFAULT NULL, cortico_steroid_date DATE DEFAULT NULL, hx_csection SMALLINT DEFAULT NULL, birth_plan_date DATE DEFAULT NULL, decision_making_consult_date DATE DEFAULT NULL, ntsv SMALLINT DEFAULT NULL, electivecsection_date DATE DEFAULT NULL, infant_weight INT DEFAULT NULL, niculevel INT DEFAULT NULL, post_partum_depression_screen_date DATE DEFAULT NULL, drug_alcohol_screen_date DATE DEFAULT NULL, interpersonal_violence_screen_date DATE DEFAULT NULL, temp_id INT DEFAULT NULL, INDEX IDX_DAA20A47E1FD4933 (submission_id), INDEX npi_idx (npi), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE submission_maternity ADD CONSTRAINT FK_DAA20A47E1FD4933 FOREIGN KEY (submission_id) REFERENCES submission (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE submission_maternity');
    }
}
