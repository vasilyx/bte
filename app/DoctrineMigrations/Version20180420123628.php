<?php

namespace App\DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180420123628 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE submission_heartfailure ADD heartfailure_type_temp SMALLINT DEFAULT NULL');

        // "Systolic", "Diastolic", "Persistent", "Preserved EF", "Reduced EF"
        $this->addSql('UPDATE submission_heartfailure SET heartfailure_type_temp  = 1 WHERE heartfailure_type = "Systolic"');
        $this->addSql('UPDATE submission_heartfailure SET heartfailure_type_temp  = 2 WHERE heartfailure_type = "Diastolic"');
        $this->addSql('UPDATE submission_heartfailure SET heartfailure_type_temp  = 3 WHERE heartfailure_type = "Persistent"');
        $this->addSql('UPDATE submission_heartfailure SET heartfailure_type_temp  = 4 WHERE heartfailure_type = "Preserved EF"');
        $this->addSql('UPDATE submission_heartfailure SET heartfailure_type_temp  = 5 WHERE heartfailure_type = "Reduced EF"');

        $this->addSql('UPDATE submission_heartfailure SET heartfailure_type = NULL');
        $this->addSql('ALTER TABLE submission_heartfailure CHANGE heartfailure_type heartfailure_type SMALLINT DEFAULT NULL');

        $this->addSql('UPDATE submission_heartfailure SET heartfailure_type = heartfailure_type_temp');
        $this->addSql('ALTER TABLE submission_heartfailure DROP COLUMN heartfailure_type_temp');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE submission_heartfailure CHANGE heartfailure_type heartfailure_type VARCHAR(32) DEFAULT NULL COLLATE utf8_unicode_ci');
    }
}
