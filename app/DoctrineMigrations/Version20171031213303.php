<?php

namespace App\DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171031213303 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('INSERT INTO program(id, name, table_name) VALUES (6, \'BTE COPD Program\', \'COPD\');');
        $this->addSql('INSERT INTO program(id, name, table_name) VALUES (7, \'BTE Depression Care Program\', \'Depression\');');
        $this->addSql('INSERT INTO program(id, name, table_name) VALUES (8, \'BTE Asthma Care Program\', \'Asthma\');');


    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
