<?php

namespace App\DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171202172436 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $measures = array(

            array('pid' => 1, 'name' => 'Hemoglobin A1C Control (HbA1C)', 'fname' => 'A1c', 'total' => 20),
            array('pid' => 1, 'name' => 'Hemoglobin A1C (HbA1C) Measurement Twice Annually', 'fname' => 'A1c2x', 'total' => 2.5),
            array('pid' => 1, 'name' => 'Lipid Control', 'fname' => 'LDL', 'total' => 15),
            array('pid' => 1, 'name' => 'Blood Pressure Control', 'fname' => 'BloodPressure', 'total' => 20),
            array('pid' => 1, 'name' => 'Blood Pressure Measurement Twice Annually', 'fname' => 'BloodPressure2x', 'total' => 2.5),
            array('pid' => 1, 'name' => 'Tobacco Use and Cessation Advice and Treatment', 'fname' => 'Tobacco', 'total' => 7.5),
            array('pid' => 1, 'name' => 'Podiatry Examination', 'fname' => 'Podiatry', 'total' => 5),
            array('pid' => 1, 'name' => 'Ophthalmologic Examination', 'fname' => 'Ophthalmologic', 'total' => 2.5),
            array('pid' => 1, 'name' => 'Nephropathy Assessment', 'fname' => 'Nephropathy', 'total' => 5),
            array('pid' => 1, 'name' => 'ACEI/ARB Therapy', 'fname' => 'ACEI', 'total' => 2.5),
            array('pid' => 1, 'name' => 'Body Mass Index/Weight/Nutrition Counseling', 'fname' => 'BMI', 'total' => 5),
            array('pid' => 1, 'name' => 'Cardiovascular Risk Assessment', 'fname' => 'ASCVD', 'total' => 5),
            array('pid' => 1, 'name' => 'Aspirin Use if 10-year risk > 10%', 'fname' => 'AspirinUse', 'total' => 5),
            array('pid' => 1, 'name' => 'Depression Screening Annually', 'fname' => 'DepressionScreening', 'total' => 2.5),

            array('pid' => 2, 'name' => 'Blood Pressure Control', 'fname' => 'BloodPressure', 'total' => 20 ),
            array('pid' => 2, 'name' => 'Blood Pressure Measurement Twice Annually', 'fname' => 'BloodPressureTwice', 'total' => 2.5 ),
            array('pid' => 2, 'name' => 'High Intensity Statin Therapy', 'fname' => 'Statin', 'total' => 15 ),
            array('pid' => 2, 'name' => 'Antiplatelet Therapy', 'fname' => 'AntiPlatelet', 'total' => 10 ),
            array('pid' => 2, 'name' => 'Beta Blocker Therapy', 'fname' => 'BetaBlocker', 'total' => 10 ),
            array('pid' => 2, 'name' => 'ACEI/ARB Therapy', 'fname' => 'ACEI', 'total' => 10 ),
            array('pid' => 2, 'name' => 'Documented Tobacco Status', 'fname' => 'Tobacco', 'total' => 2.5 ),
            array('pid' => 2, 'name' => 'Tobacco Use and Cessation Advice and Treatment - If User', 'fname' => 'TobaccoCessation', 'total' => 7.5 ),
            array('pid' => 2, 'name' => 'Body Mass Index (BMI)', 'fname' => 'BMI', 'total' => 2.5 ),
            array('pid' => 2, 'name' => 'Nutrition and Exercise (Lifestyle) Counseling', 'fname' => 'Nutrition', 'total' => 7.5 ),
            array('pid' => 2, 'name' => 'Depression Screening', 'fname' => 'DepressionScreening', 'total' => 10 ),
            array('pid' => 2, 'name' => 'Influenza Immunization', 'fname' => 'InfluenzaImmunization', 'total' => 2.5 ),

            array('pid' => 3, 'name' => 'Blood Pressure (BP) Control in Patients age > 60 ', 'fname' => 'BloodPressureOverSixty', 'total' => 20),
            array('pid' => 3, 'name' => 'Blood Pressure Control in Patients < 60 ', 'fname' => 'BloodPressureUnderSixty', 'total' => 20),
            array('pid' => 3, 'name' => 'Blood Pressure Measurement Twice Annually', 'fname' => 'BloodPressureTwice', 'total' => 5),
            array('pid' => 3, 'name' => 'Blood Pressure Management in Patients with CKD', 'fname' => 'BloodPressureKidneyDisease', 'total' => 10),
            array('pid' => 3, 'name' => 'ACEI/ARB Therapy in Hypertensive Patients with CKD', 'fname' => 'ACEI', 'total' => 10),
            array('pid' => 3, 'name' => 'Blood Pressure Management in Diabetics', 'fname' => 'BloodPressureDiabetes', 'total' => 5),
            array('pid' => 3, 'name' => 'Blood Pressure Management in Patients with Poorly Controlled Hypertension - Pharmacotherapy ', 'fname' => 'BloodPressurePharmacotherapy', 'total' => 10),
            array('pid' => 3, 'name' => 'Documentation of Annual Urine Protein Test', 'fname' => 'Hypertensive', 'total' => 2.5),
            array('pid' => 3, 'name' => 'Documentation of Annual Serum Creatinine Test - Renal Function Tests', 'fname' => 'Nephropathy', 'total' => 2.5),
            array('pid' => 3, 'name' => 'Documentation of Tobacco Use Status', 'fname' => 'Tobacco', 'total' => 2.5),
            array('pid' => 3, 'name' => 'Documentation of Tobacco Cessation counseling if user - and Treatment', 'fname' => 'TobaccoCessation', 'total' => 5),
            array('pid' => 3, 'name' => 'Body Mass Index calculated', 'fname' => 'BMI', 'total' => 2.5),
            array('pid' => 3, 'name' => 'Documentation of Counseling for Diet, Salt Intake and Physical Activity', 'fname' => 'NutritionDash', 'total' => 5),

            array('pid' => 4, 'name' => 'IBD Type, Anatomic Location, Disease Activity', 'fname' => 'IBDType', 'total' => 15),
            array('pid' => 4, 'name' => 'IBD External Manifestations Assessed', 'fname' => 'ExternalManifestations', 'total' => 10),
            array('pid' => 4, 'name' => 'Corticosteroid-Sparing Therapy Prescribed', 'fname' => 'Corticosteroid', 'total' => 10),
            array('pid' => 4, 'name' => 'Bone Loss Assessment for Patients Receiving Corticosteroid Therapy', 'fname' => 'BoneLoss', 'total' => 7.5),
            array('pid' => 4, 'name' => 'Osteoporosis Therapy in Patients with Bone Loss', 'fname' => 'Osteoporosis', 'total' => 2.5),
            array('pid' => 4, 'name' => 'Testing for latent TB before initiating anti-TNF Therapy', 'fname' => 'AntiTNF', 'total' => 10),
            array('pid' => 4, 'name' => 'Assessment of Hepatitis B Virus before initiating anti-TNF Therapy', 'fname' => 'HepatitisB', 'total' => 10),
            array('pid' => 4, 'name' => 'Influenza Immunization', 'fname' => 'InfluenzaImmunization', 'total' => 7.5),
            array('pid' => 4, 'name' => 'Pneumococcal Immunization', 'fname' => 'PneumococcalImmunization', 'total' => 7.5),
            array('pid' => 4, 'name' => 'Documentation of Tobacco Status', 'fname' => 'Tobacco', 'total' => 5),
            array('pid' => 4, 'name' => 'Documentation of Tobacco Cessation Counseling if user - and Treatment', 'fname' => 'TobaccoCessation', 'total' => 5),
            array('pid' => 4, 'name' => 'Appropriate Use of Surveillance Colonoscopy for Prevention of Colorectal Cancer', 'fname' => 'Colonoscopy', 'total' => 10),
            array('pid' => 4, 'name' => 'Depression Screening Annually', 'fname' => 'DepressionScreening', 'total' => 0),
            array('pid' => 4, 'name' => 'Use of Biologics', 'fname' => 'IBDBiologics', 'total' => 0),

            array('pid' => 5, 'name' => 'Beta Blocker Therapy', 'fname' => 'BetaBlocker', 'total' => 20),
            array('pid' => 5, 'name' => 'ACEI/ARB Therapy', 'fname' => 'Acei', 'total' => 20),
            array('pid' => 5, 'name' => 'Blood Pressure Control in Patients < 60', 'fname' => 'BloodPressureUnderSixty', 'total' => 15),
            array('pid' => 5, 'name' => 'Blood Pressure Control in Patients > 60', 'fname' => 'calculateBloodPressureOverSixty', 'total' => 2.5),
            array('pid' => 5, 'name' => 'Clinical Assessment of Fluid Status', 'fname' => 'FluidStatus', 'total' => 10),
            array('pid' => 5, 'name' => 'Documentation of Tobacco Use Status', 'fname' => 'Tobacco', 'total' => 7.5),
            array('pid' => 5, 'name' => 'Documentation of Tobacco Cessation Counseling if user - and Treatment', 'fname' => 'TobaccoCessation', 'total' => 7.5),
            array('pid' => 5, 'name' => 'Body Mass Index Calculated', 'fname' => 'BMI', 'total' => 5),
            array('pid' => 5, 'name' => 'Documentation of Counseling for Diet, Salt Intake and Physical Activity', 'fname' => 'Nutrition', 'total' => 5),
            array('pid' => 5, 'name' => 'Documentation of Type of Heart Failure', 'fname' => 'TypeHeartfailure', 'total' => 5),
            array('pid' => 5, 'name' => 'Documentation of Severity of Heart Failure', 'fname' => 'SeverityHeartfailure', 'total' => 5),

            array('pid' => 6, 'name' => 'COPD Severity Assessed and Recorded, including Spirometry', 'fname' => 'SeverityAssessed', 'total' => 10),
            array('pid' => 6, 'name' => 'Inhaled Bronchodilator Therapy: LAMA/LABA as Initial Therapy', 'fname' => 'InhaledBronchodilatorTherapyFEV1', 'total' => 10),
            array('pid' => 6, 'name' => 'Inhaled Bronchodilator Therapy: ICS only as Combination Therapy', 'fname' => 'InhaledBronchodilatorTherapy', 'total' => 10),
            array('pid' => 6, 'name' => 'Documented Tobacco Status', 'fname' => 'Tobacco', 'total' => 10),
            array('pid' => 6, 'name' => 'Tobacco Use: Cessation Advice and Treatment', 'fname' => 'TobaccoCessation', 'total' => 15),
            array('pid' => 6, 'name' => 'Assessment of Oxygen Saturation', 'fname' => 'AssessmentOxygenSaturation', 'total' => 10),
            array('pid' => 6, 'name' => 'Long Term Oxygen Therapy', 'fname' => 'LongTermOxygenTherapy', 'total' => 5),
            array('pid' => 6, 'name' => 'Influenza Immunization', 'fname' => 'InfluenzaImmunization', 'total' => 5),
            array('pid' => 6, 'name' => 'Pneumonia Immunization (PCV13)', 'fname' => 'PneumoniaImmunizationPCV13', 'total' => 5),
            array('pid' => 6, 'name' => 'Pneumonia Immunization (PCV23)', 'fname' => 'PneumoniaImmunizationPCV23', 'total' => 5),
            array('pid' => 6, 'name' => 'Follow-Up after Hospital Admission with COPD Exacerbation', 'fname' => 'FollowUpHospitalAdmission', 'total' => 10),
            array('pid' => 6, 'name' => 'Lung Cancer Screening with Low Dose CT', 'fname' => 'LungCancerScreening', 'total' => 2.5),
            array('pid' => 6, 'name' => 'Advanced Health Care Directives', 'fname' => 'AdvanceHealthCareDirectives', 'total' => 2.5),
            array('pid' => 6, 'name' => 'Assessment of COPD Exacerbations(Process Measure)', 'fname' => 'AssessmentExacerbations', 'total' => 0),

            array('pid' => 7, 'name' => 'PHQ-2 Screening', 'fname' => 'PHQ2Screening', 'total' => 20),
            array('pid' => 7, 'name' => 'PHQ-9 Screening', 'fname' => 'PHQ9Screening', 'total' => 10),
            array('pid' => 7, 'name' => 'PHQ-Adolescent Screening', 'fname' => 'PHQAdolescentScreening', 'total' => 10),
            array('pid' => 7, 'name' => 'Positive Depression Screening Follow Up', 'fname' => 'PositiveDepressionScreening', 'total' => 15),
            array('pid' => 7, 'name' => 'PHQ-9 Screening - 6 Month Follow Up', 'fname' => 'PositiveDepressionScreening6month', 'total' => 5),
            array('pid' => 7, 'name' => 'Continuous Antidepressant Treatment- if prescribed and effective', 'fname' => 'Antidepressant', 'total' => 10),
            array('pid' => 7, 'name' => 'Substance Use Screening', 'fname' => 'SubstanceScreening', 'total' => 5),
            array('pid' => 7, 'name' => 'Documentation of Substance Use Intervention/Counseling - if user', 'fname' => 'SubstanceIntervention', 'total' => 5),
            array('pid' => 7, 'name' => 'Documentation of Physical Activity Counseling', 'fname' => 'PhysicalActivity', 'total' => 10),
            array('pid' => 7, 'name' => 'Collaborative Care Model Participation', 'fname' => 'CollaborativeCareModel', 'total' => 10),

            array('pid' => 8, 'name' => 'Documentation of Disease Severity (including spirometry)', 'fname' => 'DiseaseSeverity', 'total' => 10 ),
            array('pid' => 8, 'name' => 'Short Acting Beta Agonist Prescribed to All Patients with Asthma', 'fname' => 'BetaAgonistPrescribed', 'total' => 10 ),
            array('pid' => 8, 'name' => 'All patients with persistent asthma (any degree) should be prescribed a controller medication (inhaled corticosteroids - ICS - preferred)', 'fname' => 'AsthmaControllerMedication', 'total' => 15 ),
            array('pid' => 8, 'name' => 'Patients with Asthma should NOT be taking long-acting beta agonists (LABA) alone', 'fname' => 'AsthmaBetaAgonist', 'total' => 10 ),
            array('pid' => 8, 'name' => 'Patients with moderate/severe asthma exacerbations should be prescribed oral corticosteroids', 'fname' => 'AsthmaExacerbations', 'total' => 10 ),
            array('pid' => 8, 'name' => 'Patients with moderate/severe asthma exacerbation should have follow-up within 10 days', 'fname' => 'AsthmaExacerbations10days', 'total' => 10 ),
            array('pid' => 8, 'name' => 'Patients with persistent asthma (any severity) should have 2 annual visits with their care team', 'fname' => 'PersistentAsthma', 'total' => 10 ),
            array('pid' => 8, 'name' => 'Patients with asthma should have an "Asthma Action Plan" or "Asthma self-management plan" documented', 'fname' => 'AsthmaActionPlan', 'total' => 5 ),
            array('pid' => 8, 'name' => 'Documentation of proper inhaler technique should be provided (5-11 years old)', 'fname' => 'InhalerTechnique', 'total' => 10 ),
            array('pid' => 8, 'name' => 'Tobacco Use / Tobacco Exposure Status should be assessed and documented', 'fname' => 'Tobacco', 'total' => 5 ),
            array('pid' => 8, 'name' => 'Documentation of Tobacco Cessation counseling - if user and Treatment (12 years and older)', 'fname' => 'TobaccoCessation', 'total' => 10 ),
            array('pid' => 8, 'name' => 'Influenza vaccine offered annually)', 'fname' => 'InfluenzaImmunization', 'total' => 5 ),
        );

        $id = 0;
        foreach ($measures as $measure) {
            $id++;
            $this->addSql("INSERT INTO measure(id, program_id, name, func_name, total) VALUES ($id, :pid, :name, :fname, :total);", $measure);
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {

    }
}
