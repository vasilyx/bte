<?php

namespace App\DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180402074400 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('UPDATE recognition r SET r.numerator=(SELECT COALESCE(SUM(rd.numerator), 0) FROM recognition_detail rd WHERE r.id = rd.recognition_id)');
        $this->addSql('UPDATE recognition r SET r.denominator=(SELECT COALESCE(SUM(rd.denominator), 0) FROM recognition_detail rd WHERE r.id = rd.recognition_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
