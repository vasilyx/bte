<?php

namespace App\DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170620081018 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('INSERT INTO program(id, name, table_name) VALUES (2, \'BTE CAD Care Program\', \'CADCare\');');
        $this->addSql('INSERT INTO program(id, name, table_name) VALUES (3, \'BTE Hypertension Care Program\', \'Hypertension\');');
        $this->addSql('UPDATE program SET table_name=\'Diabete\' WHERE id=1;');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
