<?php

namespace App\DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180420095550 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE submission_ibdcare ADD ibdextent_temp SMALLINT DEFAULT NULL');
        // "Less than 1/3 of colon", "Greater than 1/3 of colon"
        $this->addSql('UPDATE submission_ibdcare SET ibdextent_temp  = 1 WHERE ibdextent = "Less than 1/3 of colon"');
        $this->addSql('UPDATE submission_ibdcare SET ibdextent_temp  = 2 WHERE ibdextent = "Greater than 1/3 of colon"');

        $this->addSql('UPDATE submission_ibdcare SET ibdextent = NULL');
        $this->addSql('ALTER TABLE submission_ibdcare CHANGE ibdextent ibdextent SMALLINT DEFAULT NULL');

        $this->addSql('UPDATE submission_ibdcare SET ibdextent = ibdextent_temp');
        $this->addSql('ALTER TABLE submission_ibdcare DROP COLUMN ibdextent_temp');



        $this->addSql('ALTER TABLE submission_ibdcare ADD ibdactivity_temp SMALLINT DEFAULT NULL');
        // "Mild", "Moderate", "Quiescent", "Severe"
        $this->addSql('UPDATE submission_ibdcare SET ibdactivity_temp  = 1 WHERE ibdactivity = "Mild"');
        $this->addSql('UPDATE submission_ibdcare SET ibdactivity_temp  = 2 WHERE ibdactivity = "Moderate"');
        $this->addSql('UPDATE submission_ibdcare SET ibdactivity_temp  = 3 WHERE ibdactivity = "Quiescent"');
        $this->addSql('UPDATE submission_ibdcare SET ibdactivity_temp  = 4 WHERE ibdactivity = "Severe"');

        $this->addSql('UPDATE submission_ibdcare SET ibdactivity = NULL');
        $this->addSql('ALTER TABLE submission_ibdcare CHANGE ibdactivity ibdactivity SMALLINT DEFAULT NULL');

        $this->addSql('UPDATE submission_ibdcare SET ibdactivity = ibdactivity_temp');
        $this->addSql('ALTER TABLE submission_ibdcare DROP COLUMN ibdactivity_temp');



        $this->addSql('ALTER TABLE submission_ibdcare ADD external_manifestations_temp SMALLINT DEFAULT NULL');
        // Arthritis", "Biliary", "Dermatologic", "None", "Ocular", "Thromboembolic"
        $this->addSql('UPDATE submission_ibdcare SET external_manifestations_temp = 0 WHERE external_manifestations = "None"');
        $this->addSql('UPDATE submission_ibdcare SET external_manifestations_temp = 1 WHERE external_manifestations = "Arthritis"');
        $this->addSql('UPDATE submission_ibdcare SET external_manifestations_temp = 2 WHERE external_manifestations = "Biliary"');
        $this->addSql('UPDATE submission_ibdcare SET external_manifestations_temp = 3 WHERE external_manifestations = "Dermatologic"');
        $this->addSql('UPDATE submission_ibdcare SET external_manifestations_temp = 4 WHERE external_manifestations = "Ocular"');
        $this->addSql('UPDATE submission_ibdcare SET external_manifestations_temp = 5 WHERE external_manifestations = "Thromboembolic"');

        $this->addSql('UPDATE submission_ibdcare SET external_manifestations = NULL');
        $this->addSql('ALTER TABLE submission_ibdcare CHANGE external_manifestations external_manifestations SMALLINT DEFAULT NULL');

        $this->addSql('UPDATE submission_ibdcare SET external_manifestations = external_manifestations_temp');
        $this->addSql('ALTER TABLE submission_ibdcare DROP COLUMN external_manifestations_temp');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE submission_ibdcare CHANGE ibdextent ibdextent VARCHAR(32) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE ibdactivity ibdactivity VARCHAR(32) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE external_manifestations external_manifestations VARCHAR(32) DEFAULT NULL COLLATE utf8_unicode_ci');
    }
}
