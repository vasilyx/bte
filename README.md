# BTE
### Applications & Requirments

1. Apache 2+ example of configuration
    bte.conf
    
2. MySQL 5.7+
3. PHP 7.1 with modules (
        php7.1-cli       php7.1-json      php7.1-mysql     php7.1-readline  php7.1-mcrypt
        php7.1-common    php7.1-mbstring  php7.1-opcache   php7.1-xml       php7.1-fpm
)

4. wkhtmltox for pdf generation
    cd ~
    wget https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.3/wkhtmltox-0.12.3_linux-generic-amd64.tar.xz
    tar vxf wkhtmltox-0.12.3_linux-generic-amd64.tar.xz 
    cp wkhtmltox/bin/wk* /usr/local/bin/
    
5. install composer (https://getcomposer.org/download/)
    
    
## Installation
1. Create database(file for database parameters and mailer parameters app/config/parameters.yml), example below

        parameters:
            database_host: 127.0.0.1
            database_port: null
            database_name: bte
            database_user: root
            database_password: 123123
            mailer_transport: smtp
            mailer_host: ~
            mailer_user: ~
            mailer_password: ~
            mailer_port: 587
            mailer_auth_mode: login
            mailer_encryption: tls
            secret: ThisTokenIsNotSoSecretChangeIt
    
2. open terminal and go into root project folder

3. composer install

4. Some folders should have rights on write
        var 
        submissions 
        frontend/submission_data
   
5. php bin/console doctrine:migrations:migrate
   
6. Set parameters for generating PDF 
   app/config/config.yml - knp_snappy
   
7. Admin login - bteadmin@altarum.org
    to  create/change password 
    php bin/console app:change-password _password_

 
 