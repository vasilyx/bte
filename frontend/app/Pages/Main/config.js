'use strict';
/**
 * Created by dzmitry.barkouski on 26.01.2017.
 */

config.$inject = [
  '$stateProvider',
];

export default function config($stateProvider) {
  $stateProvider
    .state({
      name: 'main',
      template: require('./layout_template.html'),
      controller: 'mainPageController',
      controllerAs: 'vm',
      resolve: {}
    });
}