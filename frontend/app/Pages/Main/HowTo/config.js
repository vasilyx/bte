'use strict';
/**
 * Created by dzmitry.barkouski on 26.01.2017.
 */

config.$inject = [
    '$stateProvider'
];

export default function config($stateProvider) {
    $stateProvider
        .state({
            name: 'main.howto',
            url: '/howto',
            template: require('./templates/template.html'),
            controller: 'howtoPageController',
            controllerAs: 'vm',
            resolve: {}
        })
        .state({
            name: 'main.howto.prepare-submission',
            url: '/prepare-submission',
            template: require('./templates/prepare-submission.html'),
            controller: 'howtoPageController',
            controllerAs: 'vm',
            resolve: {}
        })
        .state({
            name: 'main.howto.many-recognition',
            url: '/many-recognition',
            template: require('./templates/many-recognition.html'),
            controller: 'howtoPageController',
            controllerAs: 'vm',
            resolve: {}
        });
}