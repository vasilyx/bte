'use strict';
/**
 * Created by dzmitry.barkouski on 26.01.2017.
 */
import angular from 'angular';
import config from './config';
import controller from './controller';
import './styles.scss';


import editProvider from '../MyPractice/EditProvider';

let dependencies = [
  editProvider
];

export default angular.module('howtoPageModule', dependencies)
  .controller('howtoPageController', controller)
  .config(config)
  .name;