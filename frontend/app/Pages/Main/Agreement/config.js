'use strict';
/**
 * Created by dzmitry.barkouski on 26.01.2017.
 */

config.$inject = [
  '$stateProvider',
];

export default function config($stateProvider) {
  $stateProvider
    .state({
      name: 'main.agreement',
      url: '/agreement',
      template: require('./template.html'),
      controller: 'agreementPageController',
      controllerAs: 'vm',
      resolve: {}
    });
}