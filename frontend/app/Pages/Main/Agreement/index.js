'use strict';
/**
 * Created by dzmitry.barkouski on 26.01.2017.
 */
import './styles.scss';

import angular from 'angular';
import config from './config';
import controller from './controller';
// import service from './service';

// import derectives from './Directives';

let dependencies = [
  // derectives
];

export default angular.module('agreementPageModule', dependencies)
  .controller('agreementPageController', controller)
  // .service('providerService', service)
  .config(config)
  .name;