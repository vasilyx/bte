'use strict';
/*
 * controller.js
*/

let dependencies = [
  '$state'
];
export default class AgreementPageController {
  constructor($state) {
    this.state = $state;
  }
  
  onPdfClick(){
    this.state.go('main.dashboard');
  }
}
AgreementPageController.$inject = dependencies;