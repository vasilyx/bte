'use strict';
import _eq from "lodash/eq";

let dependencies = [
  '$scope',
  'authService',
  '$state',
  'practiceService',
  'recognitionsService',
  '$localStorage',
  '$sce'
];
export default class RecognitionPageController {
  constructor($scope, authService, $state, practiceService, recognitionsService, $localStorage, $sce) {
    this.state = $state;
    this.service = recognitionsService;
    this.pageNumber = 1;
    this.pageSize = 10;
    this.lst = $localStorage.ltk;
    this.$sce = $sce;
    
    let stateChange = () => {
      if (this.state.current.name === 'main.recognitions') {
        this.requestRecognitions(this.pageNumber, this.pageSize);
        // practiceService.requestPractice(true).then(()=>{
        // }, (redirect)=>{
        //   if (redirect) {
        //     $state.go('main.dashboardWelcome');
        //   }
        // });
      }
    };
    
    // $scope.$on('$stateChangeSuccess', stateChange);
    stateChange();
  }
  
  requestRecognitions(pageNumber, pageSize) {
    this.service.getRecognitions(pageNumber, pageSize).then((data) => {
      this.recognitions = data.recognitions;
      this.totalCount = data.totalCount;
    }, () => {
      delete this.totalCount;
      delete this.recognitions;
    });
  }
  
  changeListPage() {
    console.log('PAGE NUM = ', this.pageNumber); //TODO: Delete this before checkIN
    this.requestRecognitions(this.pageNumber, this.pageSize);
  }
  
  showRecognitionInfo(submission) {
    this.recognitions.forEach((e) => {
      if (!_eq(e, submission)) {
        delete e.isActivated;
      }
    });
    submission.isActivated = !submission.isActivated;
    submission.recognition.forEach((recognition) => {
      if (!recognition.recognitionDetailsTbody) {
        recognition.recognitionDetailsTbody = this.$sce.trustAsHtml(this.generateTbody(recognition));
      }
    });
    
  }
  
  generateTbody(recognition) {
    let string = '';
    recognition.recognition_detail.forEach((recognition_detail) => {
      string += '<tr>';
      string += '<td class="text-left">' + recognition_detail.measure.name + '</td>';
      string += '<td>' + recognition_detail.measure.total + '</td>';
      string += '<td>' + recognition_detail.denominator + '</td>';
      string += '<td>' + recognition_detail.numerator + '</td>';
      string += '<td>' + recognition_detail.score + '</td>';
      string += '</tr>';
    });
    return string;
  }
}
RecognitionPageController.$inject = dependencies;