"use strict";
/**
 * Created by dzmitry.barkouski on 27.04.2017.
 **/
import angular from 'angular';
import _isArray from 'lodash/isArray';
import _cloneDeep from 'lodash/cloneDeep';

export default class RecognitionsService {
  constructor($q, $http, $state, $localStorage) {
    this.$q = $q;
    this.$http = $http;
    this.$state = $state;
    this.ls = $localStorage;
    this.pageSize = 10;
    this.pageNumber = 1;
  }
  
  getRecognitions(pageNumber, pageSize) {
    let def = this.$q.defer(),
      prom = def.promise;
    if (this.abort) {
      this.abort.resolve();
    }
    this.abort = this.$q.defer();
    this.$http
      .get('/recognitions', {
        params: {
          page: pageNumber || this.pageNumber,
          perPage: pageSize || this.pageSize
        },
        timeout: this.abort
      })
      .then((success) => {
        let data = this.reworkRecognitions(success.data);
        if (_isArray(data)) {
          if (data.length) {
            def.resolve({
              recognitions: data,
              totalCount: success.headers('X-Total-Count')
            });
          } else {
            def.reject();
          }
        }
      }, () => {
        def.reject();
      });
    
    return prom;
  }
  
  reworkRecognitions(data) {
    let reworkedData = [];
    if (_isArray(data)) {
      data.forEach((element) => {
        if (_isArray(element.recognition)) {
          element.expiration_at = new Date(element.modified_at);
          element.expiration_at = element.expiration_at.setYear(element.expiration_at.getFullYear() + 2);
          
          element.modified_at = new Date(element.modified_at);
          
          let starRating = 0;
          element.recognition.forEach((element) => {
            starRating +=
              (element.score >= 50 && element.score < 65) ? 3 : (element.score >= 65 && element.score < 85) ? 4 : (element.score >= 85) ? 5 : 0;
          });
          element.starRating =
            starRating = (element.recognition.length === 1) ? starRating : Math.round(starRating / element.recognition.length);
          
          if (starRating >= 3) {
            for (let i = 0; i < starRating; i++) {
              if (element.stars) {
                element.stars.push({})
              } else {
                element.stars = [{}];
              }
            }
          }
          if (angular.isArray(element.recognition) && element.recognition.length) {
            if (element.reporting_for === 2) {
              let tempRecArray = [], groupId = '', resultedArray = [];
      
              element.recognition.forEach((recognition, i, array) => {
                if (recognition.group_i_d) {
                  if ((recognition.group_i_d !== groupId) || i === (array.length - 1)) {
                    groupId = recognition.group_i_d;
                    resultedArray = [recognition];
                  } else {
                    resultedArray.push(recognition);
                  }
          
                  if (i === (array.length - 1) || array[i + 1].group_i_d !== groupId) {
                    if (tempRecArray.length) {
                      tempRecArray = [
                        ...tempRecArray,
                        this.getTotalRecognitionTable(resultedArray, groupId),
                        ...resultedArray
                      ];
                    } else {
                      tempRecArray = [
                        this.getTotalRecognitionTable(resultedArray, groupId),
                        ...resultedArray
                      ];
                    }
                  }
                } else {
                  tempRecArray.push(recognition);
                }
              });
              element.recognition = tempRecArray;
            } else {
              element.recognition.unshift(this.getTotalRecognitionTable(element.recognition));
            }
          }
        }
      });
    }
    return data;
  }
  
  getTotalRecognitionTable(recognitions, groupID) {
    if (!recognitions.length) return [];
    let totalRecognition = {
      provider: {
        lastname: 'Group'
      },
      // recognition_detail: ,
      score: 0
    };
    if (groupID) totalRecognition.totalgroup_i_d = groupID;
    
    recognitions.forEach((recognition) => {
      let curDetails = recognition.recognition_detail;
      if (totalRecognition.recognition_detail) {
        totalRecognition.recognition_detail.forEach((details, idx) => {
          if (details.measure.name === curDetails[idx].measure.name) {
            details.denominator += curDetails[idx].denominator;
            details.numerator += curDetails[idx].numerator;
            // details.score += curDetails[idx].score;
          }
        });
      } else {
        totalRecognition.recognition_detail = _cloneDeep(curDetails);
      }
      // totalRecognition.score += recognition.score;
    });
    totalRecognition.score = 0;
    totalRecognition.recognition_detail.forEach((details) => {
      details.denominator = parseFloat(details.denominator.toFixed(2));
      details.numerator = parseFloat(details.numerator.toFixed(2));
      details.score = (details.denominator && details.numerator) ? parseFloat((details.numerator / details.denominator * details.measure.total).toFixed(2)) : 0;
      totalRecognition.score += details.score;
    });
    totalRecognition.score = parseFloat((totalRecognition.score).toFixed(2));
    
    return totalRecognition;
  }
  
  cacheControl(headers) {
    if (!headers || !headers['cache-control']) return;
    const cacheControl = headers['cache-control'].replace(' ', '').split(',');
    let cacheControlIterator = (cc) => {
      if (!(cc && angular.isString(cc) && cc.length)) return;
      switch (cc) {
        case 'no-cache':
          this.practiceData._request = true;
          break;
        default:
          break;
      }
    };
    if (cacheControl.length > 1) {
      cacheControl.forEach(cacheControlIterator);
    } else {
      cacheControlIterator(cacheControl[0]);
    }
  }
  
  savePractice(data) {
    delete data._request;
    let req = this.$http
      .post('/user/practice', data);
    req.then(() => {
      this.practiceData._request = true;
    });
    return req;
  }
  
};

RecognitionsService.$inject = [
  '$q',
  '$http',
  '$state',
  '$localStorage'
];