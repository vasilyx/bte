'use strict';
/**
 * Created by dzmitry.barkouski on 26.06.2017.
 **/

import angular from 'angular';
import _cloneDeep from 'lodash/cloneDeep';

export default class SubmissionService {
  constructor($q, $http, $state, modalService) {
    this.$q = $q;
    this.$http = $http;
    this.$state = $state;
    this.modal = modalService;
    this.submissionData = {};
    this.pageSize = 10;
    this.pageNumber = 1;
  }
  
  requestSubmissions(pageNumber, pageSize) {
    let def = this.$q.defer(),
      prom = def.promise;
    if (this.abort) {
      this.abort.resolve();
    }
    this.abort = this.$q.defer();
    this.$http
      .get('/submissions', {
        params: {
          page: pageNumber || this.pageNumber,
          perPage: pageSize || this.pageSize
        },
        timeout: this.abort
      })
      .then((success) => {
        let data = success.data;
        if (angular.isArray(data)) {
          if (data.length) {
            this.submissions = data /*this.reworkSubmissions(data)*/;
            def.resolve({
              submissions: this.submissions,
              totalCount: success.headers('X-Total-Count')
            });
          } else {
            delete this.submissions;
            def.reject();
          }
        }
      }, () => {
        def.reject();
      });
    return prom;
  }
  
  reworkSubmissions(data) {
    data.forEach((element) => {
      if (angular.isArray(element.recognition) && element.recognition.length && element.reporting_for === 2) {
        let tempRecArray = [], groupId = '', resultedArray = [];
        
        element.recognition.forEach((recognition, i, array) => {
          if (recognition.group_i_d) {
            if ((recognition.group_i_d !== groupId) || i === (array.length - 1)) {
              groupId = recognition.group_i_d;
              resultedArray = [recognition];
            } else {
              resultedArray.push(recognition);
            }
  
            if (i === (array.length - 1) || array[i + 1].group_i_d !== groupId) {
              if (tempRecArray.length) {
                tempRecArray = [
                  ...tempRecArray,
                  this.getTotalRecognitionTable(resultedArray, groupId),
                  ...resultedArray
                ];
              } else {
                tempRecArray = [
                  this.getTotalRecognitionTable(resultedArray, groupId),
                  ...resultedArray
                ];
              }
            }
          } else {
            tempRecArray.push(recognition);
          }
        });
        element.recognition = tempRecArray;
        
      }
    });
    return data;
  }
  
  getTotalRecognitionTable(recognitions, groupID) {
    if (!recognitions.length) return [];
    let totalRecognition = {
      provider: {
        lastname: 'Group'
      },
      // recognition_detail: ,
      score: 0
    };
    if (groupID) {
      totalRecognition.group_i_d = groupID;
    }
    recognitions.forEach((recognition) => {
      let curDetails = recognition.recognition_detail;
      if (totalRecognition.recognition_detail) {
        totalRecognition.recognition_detail.forEach((details, idx) => {
          if (details.measure.name === curDetails[idx].measure.name) {
            details.denominator += curDetails[idx].denominator;
            details.numerator += curDetails[idx].numerator;
            details.score += curDetails[idx].score;
          }
        });
      } else {
        totalRecognition.recognition_detail = _cloneDeep(curDetails);
      }
      totalRecognition.score += recognition.score;
    });
    totalRecognition.recognition_detail.forEach((details) => {
      details.denominator = parseFloat((details.denominator / recognitions.length).toFixed(2));
      details.numerator = parseFloat((details.numerator / recognitions.length).toFixed(2));
      details.score = parseFloat((details.score / recognitions.length).toFixed(2));
    });
    totalRecognition.score = parseFloat((totalRecognition.score / recognitions.length).toFixed(2));
    
    return totalRecognition;
  }
  
  convertNumToString(data) {
    for (let i in data) {
      if (data.hasOwnProperty(i)) {
        if (angular.isObject(data[i])) {
          data[i] = this.convertNumToString(data[i]);
        } else if (angular.isNumber(data[i])) {
          data[i] += '';
        }
      }
    }
    return data;
  }
  
  convertBool(data) {
    for (let i in data) {
      if (data.hasOwnProperty(i)) {
        if (angular.isObject(data[i])) {
          data[i] = this.convertBool(data[i]);
        } else if (angular.isString(data[i]) && (/true/ig.test(data[i]) || /false/ig.test(data[i]))) {
          data[i] = (/true/ig.test(data[i]));
        }
      }
    }
    return data;
  }
};

SubmissionService.$inject = [
  '$q',
  '$http',
  '$state',
  'modalService'
];