'use strict';
/**
 * Created by dzmitry.barkouski on 26.01.2017.
 */

config.$inject = [
  '$stateProvider'
];

export default function config($stateProvider) {
  $stateProvider
    .state({
      name: 'main.submissions',
      url: '/submissions',
      template: require('./template.html'),
      controller: 'submissionsPageController',
      controllerAs: 'vm',
      resolve: {
        practiceResolver: practiceResolver
      }
    });
}

function practiceResolver(practiceService) {
  return practiceService.checkPractice({redirectState:'main.dashboardWelcome', reload:true});
}
practiceResolver.$inject = [
  'practiceService'
];