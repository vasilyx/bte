'use strict';
/**
 * Created by dzmitry.barkouski on 26.01.2017.
 */
import angular from 'angular';
import config from './config';
import controller from './controller';
import service from './service';


import editProvider from '../MyPractice/EditProvider';
import addSubmission from './AddSubmission';

let dependencies = [
  editProvider,
  addSubmission
];

export default angular.module('submissionsPageModule', dependencies)
  .controller('submissionsPageController', controller)
  .service('submissionsListService', service)
  .config(config)
  .name;