'use strict';

import _eq from "lodash/eq";

let dependencies = [
  '$scope',
  '$localStorage',
  '$state',
  'providerService',
  'submissionsListService',
  'submissionService',
  'modalService',
  'userInfoService',
  'practiceService',
  '$sce'
];
export default class SubmissionsPageController {
  constructor($scope, $localStorage, $state, providerService, submissionsListService, submissionService, modalService, userInfoService, practiceService, $sce) {
    this.state = $state;
    this.service = submissionsListService;
    this.modalService = modalService;
    this.$sce = $sce;
    this.lst = $localStorage.ltk;
    this.pageNumber = 1;
    this.pageSize = 10;
    this.has_no_agreement = false;
    
    let stateChange = () => {
      if (this.state.current.name === 'main.submissions') {
        $localStorage.cleaSubmData = true;
        let throwProvidersError = () => {
          practiceService.requestPractice().then((data) => {
            if (!data.data_aggregator) {
              this.throwError('Submissions', 'There are no active providers, please activate any provider to start submission.');
            }
          }, () => {
            this.throwError('Submissions', 'There are no active providers, please activate any provider to start submission.');
          });
        };
        providerService.getProviders().then((providers) => {
          providers = submissionService.reworkProviders(providers);
          if (!providers || !providers.length) {
            throwProvidersError();
          }
        }, () => {
          throwProvidersError();
        });
        this.requestSubmissions(this.pageNumber, this.pageSize);
        
      }
    };
    
    userInfoService.getUserData().then((data) => {
      this.has_no_agreement = !data.has_agreement;
    }, () => {
      this.has_no_agreement = true;
    });
    
    // stateChange();
    $scope.$on('$stateChangeSuccess', stateChange);
  }
  
  requestSubmissions(pageNumber, pageSize) {
    this.service.requestSubmissions(pageNumber, pageSize).then((data) => {
      this.submissions = data.submissions;
      this.totalCount = data.totalCount;
    }, () => {
      delete this.submissions;
      delete this.totalCount;
    });
  }
  
  changeListPage() {
    console.log('PAGE NUM = ', this.pageNumber); //TODO: Delete this before checkIN
    this.requestSubmissions(this.pageNumber, this.pageSize);
  }
  
  throwError(title, error) {
    const data = {
      title: title,
      error: error
    };
    
    let modal = this.modalService.alert(data);
    modal.closed.then(() => {
      this.state.go('main.practice', {}, {reload: true});
    });
  }
  
  showSubmissionInfo(submission) {
    this.submissions.forEach((e) => {
      if (!_eq(e, submission)) {
        delete e.isActivated;
      }
    });
    submission.isActivated = !submission.isActivated;
    submission.recognition.forEach((recognition) => {
      if (!recognition.recognitionDetailsTbody) {
        recognition.recognitionDetailsTbody = this.$sce.trustAsHtml(this.generateTbody(recognition));
      }
    });
    
  }
  
  generateTbody(recognition) {
    let string = '';
    recognition.recognition_detail.forEach((recognition_detail) => {
      string += '<tr>';
      string += '<td class="text-left">' + recognition_detail.measure.name + '</td>';
      string += '<td>' + recognition_detail.measure.total + '</td>';
      string += '<td>' + recognition_detail.denominator + '</td>';
      string += '<td>' + recognition_detail.numerator + '</td>';
      string += '<td>' + recognition_detail.score + '</td>';
      string += '</tr>';
    });
    return string;
  }
}
SubmissionsPageController.$inject = dependencies;