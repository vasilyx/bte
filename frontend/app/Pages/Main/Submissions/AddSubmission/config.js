'use strict';
/**
 * Created by dzmitry.barkouski on 26.01.2017.
 */

config.$inject = [
  '$stateProvider'
];

export default function config($stateProvider) {
  $stateProvider
    .state({
      name: 'main.submissions.add',
      url: '/add',
      template: require('./template.html'),
      controller: 'submissionRouterController',
      controllerAs: 'vm',
      resolve: {
        userAgreement: userAgreementResolver
      }
    });
}


function userAgreementResolver(userInfoService, $q, $state) {
  let def = $q.defer(),
    prom = def.promise;
  
  userInfoService.getUserData().then((data) => {
    if (data.has_agreement) {
      def.resolve();
    } else {
      $state.go('main.agreement');
      def.reject();
    }
  }, () => {
    $state.go('main.agreement');
    def.reject();
  });
  return prom;
}

userAgreementResolver.$inject = [
  'userInfoService',
  '$q',
  '$state'
];