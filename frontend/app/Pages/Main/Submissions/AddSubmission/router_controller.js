'use strict';

let dependencies = [
  '$localStorage',
  '$state',
  'practiceService'
];
export default class SubmissionRouterController {
  constructor($localStorage, $state, practiceService) {
    this.state = $state;
    this.tabIndex = 1;
    
    let stateChange = ()=>{
      if (this.state.current.name === 'main.submissions') {
        $localStorage.cleaSubmData = true;
        practiceService.requestPractice(true).then(()=>{},
          (redirect)=>{
            if (redirect) {
              $state.go('main.dashboardWelcome');
            }
          });
      }
    };
  
    stateChange();
  }
  
  goToTab(tabIndex){
    this.tabIndex = tabIndex;
  }
  getCurrentTab(){
    return this.tabIndex;
  }
}
SubmissionRouterController.$inject = dependencies;