'use strict';

import _find from 'lodash/find';
import _cloneDeep from 'lodash/cloneDeep';
import _isArray from 'lodash/isArray';
import _keys from 'lodash/keys';
import moment from 'moment';

let dependencies = [
  '$scope',
  '$state',
  '$document',
  '$uibModal',
  '$localStorage',
  'modalService',
  'practiceService',
  'submissionService',
  'providerService',
];
let providersState = {},
  controller;
export default class SubmissionController {
  constructor($scope, $state, $document, $uibModal, $localStorage, modalService, practiceService, submissionService, providerService) {
    if (controller && controller.pages) {
      this.pages = controller.pages;
    }
    controller = this;
    
    this.allowedFileTypes = [
      'text/plain',
      'text/x-csv',
      'application/vnd.ms-excel',
      'application/csv',
      'application/x-csv',
      'text/csv',
      'text/comma-separated-values',
      'text/x-comma-separated-values',
      'text/tab-separated-values'
    ];
    
    this._$scope = $scope;
    this.state = $state;
    this.$uibModal = $uibModal;
    this.$localStorage = $localStorage;
    this.modalService = modalService;
    this.service = submissionService;
    this.practiceService = practiceService;
    this.providerService = providerService;
    this.tabIndex = 1;
    this.document = $document;
    this._parentScope = $scope.$parent;
    if ($localStorage.cleaSubmData) {
      delete $localStorage.cleaSubmData;
      this.service.clearStoredSubmissionData();
      if (this.pages) {
        this.pages[0].available = this.pages[1].available = false;
      }
    }
    this.submission = this.service.getStoredSubmissionData() || {
      providers: [],
      from_ehr: 'false',
      reporting_for: '0'
    };
    if (this.submission.submission_file_data) {
      this.submission_file_data = this.submission.submission_file_data;
      delete this.submission.submission_file_data;
    }
    this.submission.addData = this.submission.addData || {
      file: 'true',
      multiselect_options: {
        displayProp: 'displayName',
        idProperty: 'id',
        showCheckAll: false,
        showUncheckAll: false,
        scrollableHeight: '180px',
        scrollable: true
      }
    };
    
    this.formData = {};
    this.formData.providersState = {};
    let todayStartDate = (moment().year(moment().year() - 1).hour(0).minutes(0).seconds(0).milliseconds(0))['_d'];
    this.startDatepickerOptions = {
      maxDate: todayStartDate,
      initDate: todayStartDate
    };
    this.popup1 = {
      opened: false
    };
    this.popup2 = {opened: false};
    
    if (this._parentScope.vm.getCurrentTab() === 1) {
      this.submission.from_ehr += '';
      this.startForm();
    }
    if (this.submission.reportingStart) {
      this.submission.reportingStart = new Date(this.submission.reportingStart);
    }
    if (this.submission.reportingEnd) {
      this.submission.reportingEnd = new Date(this.submission.reportingEnd);
    }
    
    this.$onDestroy = function () {
      if (this._reporting_forWatcher) {
        this._reporting_forWatcher();
        delete this._reporting_forWatcher;
      }
      if (this._programmWatcher) {
        this._programmWatcher();
        delete this._programmWatcher;
      }
    };
    
  }
  
  startForm() {
    this.service.getPrograms().then((programs) => {
      this.formData.programs = programs;
    }, () => {
      delete this.formData.programs;
    });
    
    this.practiceService.requestPractice(true).then((practice) => {
      this.formData.clinicName = practice.name;
      this.formData.practice = practice;
    }, () => {
      delete this.formData.clinicName;
    });
    this.updateProviders(true);
    // this.submission.addData.file = 'true';
    
  }
  
  updateProviders(subscribe) {
    let subscribeChanges = () => {
      if (subscribe) {
        this._reporting_forWatcher = this._$scope.$watch('vm.submission.reporting_for', (newVal, lastVal) => {
          if (newVal === '2' && lastVal !== '2') {
            this.clearProviders();
          }
        });
        this._programmWatcher = this._$scope.$watch('vm.submission.program_id', (newVal, lastVal) => {
          if (newVal === lastVal) return;
          delete this.submission.provider;
          this.clearProviders();
          this.freezePactice = true;
          this.updateProviders();
        });
        
      }
    };
    this.providerService.getProviders({program_id: this.submission.program_id}).then((providers) => {
      delete this.formData.providers;
      let reworkedProviders = this.service.reworkProviders(providers);
      if (_isArray(reworkedProviders) && reworkedProviders.length && !this.submission.provider) {
        let firstEnabled = _find(reworkedProviders, function (e) {
          return !e.disabled;
        });
        if (firstEnabled) {
          this.submission.provider = firstEnabled;
        }
        this.formData.disabledProviders = reworkedProviders.some(el => el.disabled);
      }
      this.formData.providers = reworkedProviders;
      console.log('providers - ', this.formData.providers); //TODO: Delete this before checkIN
      if (!this.freezePactice) {
        this.practiceService.requestPractice(true).then((practice) => {
          if (!(providers && providers.length) && practice) {
            this.submission.reporting_for = '2';
          }
        }, () => {
        });
      }
      subscribeChanges();
      delete this.freezePactice;
    }, () => {
      subscribeChanges();
      delete this.formData.providers
      ;
    });
  }
  
  submitState1() {
    let pagesAvailability = false;
    let form = this._$scope.submissionForm;
    
    if (this.formData.providersState._isInited) {
      delete this.formData.providersState._isInited;
    }
    
    if (form.$error && _keys(form.$error).length) {
      form.$setSubmitted();
      this.throwError('Submission failed', 'Form validation failed.');
      // return;
    } else if (this.submission.reporting_for !== '0' && (!this.submission.providers || this.submission.providers.length === 0 || this.formData.uploadedFileErrors)) {
      this.throwError('Submission failed', 'Form validation failed.');
    } else {
      pagesAvailability = true;
      this.goToTab(2);
    }
    
    if (this.pages) {
      this.pages[0].available = this.pages[1].available = pagesAvailability;
    }
  }
  
  changeProvidersInitEvent() {
    controller.formData.providersState._isInited = true;
    delete controller.formData.providersState._isChanged;
  }
  
  changeProvidersState() {
    delete controller.formData.providersState._isInited;
  }
  
  clearProviders() {
    delete this.submission.providersListData;
    delete this.providersFileData;
    if (this.submission.providersListData) {
      delete this.submission.providersListData.errorData;
    }
    this.submission.providers = [];
    this.submission.from_ehr = 'false';
    this.submission.ehr = '';
    this.submission.ehr_id = '';
    controller.formData.providersState._isInited = true;
  }
  
  goToTab(tabIndex, options) {
    if (options && options.check1state) {
      this.submitState1();
      return;
    }
    
    
    if (this._reporting_forWatcher) {
      this._reporting_forWatcher();
      delete this._reporting_forWatcher;
    }
    if (this._programmWatcher) {
      this._programmWatcher();
      delete this._programmWatcher;
    }
    
    if (tabIndex > 2 && this.pages) {
      this.pages[0].available = this.pages[1].available = false;
    }
    if (this._parentScope && this._parentScope.vm && angular.isFunction(this._parentScope.vm.goToTab)) {
      let data = {};
      switch (tabIndex - 1) {
        case 1:
          data = this.collect_addData_tab1();
          break;
        default:
          data = this.submission;
          break;
      }
      this.service.storeSubmission(data, (tabIndex === 1));
      if (tabIndex === 2) {
        tabIndex = 21;
      }
      this._parentScope.vm.goToTab(tabIndex);
    }
  }
  
  gotoProviderCreate() {
    this.$localStorage.returnToState = this.state.current.name;
    this.state.go('main.provider.create');
  }
  
  gotoPracticeEdit() {
    this.$localStorage.returnToState = this.state.current.name;
    this.state.go('main.practice.edit');
  }
  
  showDatePicker1() {
    this.popup1.opened = true;
  }
  
  dateChanged() {
    let date = new Date(this.submission.reportingStart);
    this.submission.reportingEnd = new Date(date.setFullYear(date.getFullYear() + 1));
  }
  
  showDatePicker2() {
    this.popup2.opened = true;
  }
  
  changeFilesList(filesArray) {
    let self = this;
    this.formData.files = filesArray;
    if (this.uploadPopup) {
      this.uploadPopup.close();
    }
    this.uploadPopup = this
      .$uibModal
      .open({
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        template: require('./Directives/State2_1_Popup.html'),
        controllerAs: 'vm_p',
        size: 'm',
        // backdrop: 'static',
        controller: function () {
          this.controller = self;
        }
      });
    
    // this.uploadPopup.closed.then(() => {
    //   // delete this.uploadPopup;
    // });
  }
  
  uploadProvidersFileToServer(files) {
    this.providersListFileUploading = true;
    this.uploadDisabled = true;
    let file = files[0];
    
    // if (!this.isFileAllowed(file)) return;
    this.clearProviders();
    controller.formData.providersState._isInited = true;
    // delete this.submission.providersListData.errorData;
    this.service.uploadProvidersFile(file, this.submission.program_id).then((data) => {
      if (data && data.file && data.data) {
        this.submission.providersListData = {
          fileName: data.file.name,
          status: 'Complete',
          count: data.data.data.length,
          errors: data.data.errors,
          warnings: data.data.warnings,
          errorData: data.data.data.filter((e) => {
            return e.disabled == 1;
          })
        };
        
        this.providersFileData = data;
        
        this.submission.providers = this.service.reworkProviders(data.data.data.filter((e) => {
          return e.disabled != 1;
        }));
        
        this.formData.uploadedFileErrors = !!data.data.errors;
        delete this.providersListFileUploading;
        
        this.updateProviders();
      }
    }, (errors) => {
      delete this.providersListFileUploading;
      this.clearProviders();
      this.throwError(errors.data.message);
    });
  }
  
  uploadFileToServer(files) {
    if (this.submission.temp_id) {
      this.delFileFromServer(this.submission.temp_id, true);
    }
    
    this.loading = true;
    this.uploadDisabled = true;
    let file = files[0],
      addData = {
        submissionReportingEnd: this.submission.reportingEnd,
        reporting_for: this.submission.reporting_for
      };
    
    if (!this.isFileAllowed(file)) return;
    if (this.submission.reporting_for === '0') {
      addData.provider = this.submission.provider.id;
    } else {
      addData.providers = this.submission.providers;
    }
    this.service.uploadFile(file, this.submission.program_id, addData)
      .then((fileData) => {
        this.submission.temp_id = fileData.id;
        this.submission.submission_file_data = fileData.data;
        this.submission.addData.count = fileData.count;
        this.submission.addData.fileName = file.name;
        // this.submission.addData.status = 'Complete';
        
        // let errors = this.service.getDataErrors(fileData.data);
        this.submission.addData.errors = fileData.errors;
        this.submission.addData.disableSubmit =
          (this.submission.addData.errors > 0 || fileData.count === 0) ? true : false;
        this.submission.addData.warnings = fileData.warnings;
        this.submission.addData.status = 'Complete';
        if (this.uploadPopup) {
          this.uploadPopup.close();
        }
        delete this.uploadDisabled;
        delete this.loading;
        
        this.pages[0].available = this.pages[1].available = false;
      }, (errors) => {
        this.clearFileData(errors.data.message);
        delete this.loading;
      });
  }
  
  isFileAllowed(file) {
    let allow = false,
      fileType = file.type;
    if (fileType && fileType.length) {
      this.allowedFileTypes.forEach((type) => {
        if (type === fileType) allow = true;
      });
    }
    if (!allow) {
      this.clearFileData('Wrong file type. Only CSV files allowed');
    }
    return allow;
  }
  
  clearFileData(titleText) {
    delete this.submission.temp_id;
    delete this.submission.submission_file_data;
    delete this.submission.addData.fileName;
    delete this.submission.addData.status;
    delete this.submission.addData.warnings;
    delete this.submission.addData.errors;
    delete this.submission.addData.count;
    
    // if (this.uploadPopup) { //todo: enable if there is needed to close upload popup after errors
    //   this.uploadPopup.close();
    // }
    
    this.modalService.alert({
      title: 'Upload file error.',
      error: titleText
    });
    delete this.uploadDisabled;
  }
  
  sendManualData(data) {
    this.service.sendData(data, 2, this.submission.program_id).then((fileData) => {
      fileData = fileData.data;
      this.submission.temp_id = fileData.id;
      this.submission.addData.count = fileData.count;
      this.submission.addData.fileName = '';
      this.submission.addData.status = 'Complete';
      
      this.goToTab(4);
    }, (errors) => {
      delete this.submission.temp_id;
      delete this.submission.addData.fileName;
      delete this.submission.addData.status;
      delete this.submission.addData.count;
      this.modalService.alert({
        title: 'Upload file error.',
        error: errors.data.message
      });
    });
  }
  
  delFileFromServer(filesId, dropAsk) {
    let deleteFile = () => {
      this.service.deleteFile(filesId, this.submission.program_id).then(() => {
        delete this.submission.temp_id;
        delete this.submission.addData.fileName;
        delete this.submission.addData.status;
        delete this.submission.addData.count;
        this.pages[0].available = this.pages[1].available = true;
        this.goToTab(2);
      });
    };
    
    if (!dropAsk) {
      this.modalService.ask_2btns({
        ok_btn_title: 'Delete',
        alert: true,
        title: 'Delete file confirmation.',
        content_text: 'Delete this file?'
      }).then(
        () => {
          deleteFile();
        }
      );
    } else {
      deleteFile();
    }
  }
  
  getSubmissionFileData() {
    return _cloneDeep(this.submission_file_data);
  }
  
  submitSubmission() {
    this.providersListFileUploading = true;
    this.service.submitSubmission()
      .then(() => {
        delete this.providersListFileUploading;
        this.goToTab(5);
      }, () => {
        delete this.providersListFileUploading;
      });
  }
  
  clearData() {
    this.service.clearStoredSubmissionData();
    this.state.go('main.submissions');
  }
  
  collect_addData_tab1() {
    let submission = this.submission;
    
    if (this.formData.programs && this.formData.programs.length && this.submission.program_id) {
      let currentProgram = _find(this.formData.programs, [
        'id',
        this.submission.program_id
      ]);
      submission.addData.programName = (currentProgram) ? currentProgram.name : '';
    }
    return submission;
  }
  
  getProvidersFileData() {
    return {
      data: this.providersFileData.data.data,
      head: this.providersFileData.data.head
    };
  }
  
  
  throwError(title, error) {
    const data = {
      title: title,
      error: error
    };
    
    this.modalService.alert(data);
    console.warn('some error - ', error); //TODO: Delete this before checkIN
  }
}
SubmissionController.$inject = dependencies;