"use strict";
/**
 * Created by dzmitry.barkouski on 27.04.2017.
 **/
import angular from 'angular';
import _merge from 'lodash/merge';
import _keys from 'lodash/keys';
import _cloneDeep from 'lodash/cloneDeep';
import _isArray from 'lodash/isArray';
import _isString from 'lodash/isString';
import moment from 'moment';

export default class SubmissionService {
  constructor($q, $http, $state, modalService, dateFilter) {
    this.$q = $q;
    this.$http = $http;
    this.$state = $state;
    this.modal = modalService;
    this.dateFilter = dateFilter;
    this.submissionData = {};
  }
  
  getPrograms() {
    let def = this.$q.defer(),
      prom = def.promise;
    
    this
      .$http
      .get('/programs')
      .then((success) => {
          if (angular.isArray(success.data) && success.data.length) {
            def.resolve(this.convertNumToString(success.data));
          } else {
            def.reject();
          }
        },
        () => {
          def.reject();
        });
    
    return prom;
  }
  
  sendData(program_id, type, data, addData, url) {
    url = url || '/submission/data';
    return this.$http.post(url, _merge({}, {
      program_id: program_id,
      type: type,
      data: data
    }, addData || {}));
  }
  
  uploadProvidersFile(file, program_id){
    let freader = new FileReader(),
      def = this.$q.defer(),
      prom = def.promise;
  
    freader.onload = (event) => {
      // console.log(event.target.result); //TODO: Delete this before checkIN
      // this.$http.post('/submission/data', _merge({}, {
      //   program_id: program_id,
      //   type: type,
      //   data: data
      // }, addData || {}));
      this.sendData(program_id, file.type, event.target.result.split(',')[1], {name: file.name}, '/provider/data')
        .then((response) => {
          if (response && response.data) {
            if (response.data.data && _isArray(response.data.data) && response.data.data.length) {
              let allData = response.data;
              allData.head = allData.data[0];
              allData.data = allData.data.slice(1);
              allData.data = allData.data.map((el)=>{
                let entity = _merge(el[el.length - 1], el[el.length - 1].entity);
                delete entity.entity;
                entity.fileData = el.slice(0, el.length - 1);
                
                // el['0'].disabled = el.disabled;
                // el['0'].last_submission_date = el.last_submission_date;
                return entity;
              });
              def.resolve({data: allData, file: file});
            } else {
              def.reject();
            }
          } else {
            def.reject();
          }
        }, (error) => {
          def.reject(error);
        });
    };
    freader.onerror = function (event) {
      def.reject();
    };
    freader.readAsDataURL(file);
    // freader.readAsText(file);
    
    return prom;
  }
  
  uploadFile(file, program_id, addData) {
    let freader = new FileReader(),
      def = this.$q.defer(),
      prom = def.promise;
  
    if (_isArray(addData.providers) && addData.providers.length) {
      let _providers = _cloneDeep(addData.providers);
      addData.providers = [];
      _providers.forEach((e) => {
        addData.providers.push(e.id);
      });
      delete addData.provider;
    } else {
      addData.providers = [addData.provider];
      delete addData.provider;
    }
    
    addData.providers = addData.providers.join(',');
    
    freader.onload = (event) => {
      this.sendData(program_id, 1, event.target.result.split(',')[1], addData)
        .then((response) => {
          if (response.data && response.data && response.data.id) {
            def.resolve(response.data);
          } else {
            def.reject();
          }
        }, (error) => {
          def.reject(error);
        });
    };
    freader.onerror = function (event) {
      def.reject();
    };
    freader.readAsDataURL(file);
    
    return prom;
  }
  
  getDataErrors(data) {
    let errors = 0,
      warnings = 0;
  
    if (_isArray(data)) {
      for (let i = 1, l = data.length; i < l; i++) {
        if (_isArray(data[i])) {
          data[i].forEach((cell)=>{
            if (cell.error) {
              ++errors;
            }
            if (cell.warning) {
              ++warnings;
            }
          });
        }
      }
    }
    return {
      errors: errors,
      warnings: warnings
    }
  }
  
  deleteFile(fileId, program_id) {
    let def = this.$q.defer(),
      prom = def.promise;
  
    this
      .$http
      .delete('/submission/data', {
        params: {
          program_id: program_id,
          fileId: fileId
        }
      })
      .then(() => {
        def.resolve();
      }, () => {
        def.reject();
      });
    
    return prom;
  }
  
  storeSubmission(data, reset) {
    if (reset) {
      this.submissionData = {
        addData: {}
      };
    }
    this.submissionData = _merge(this.submissionData, data);
    this.submissionData = /*this.convertBool(this.submissionData)*/
      this.convertNumToString(this.convertBool(this.submissionData));
  }
  
  clearStoredSubmissionData() {
    this.submissionData = {};
  }
  
  getStoredSubmissionData() {
    return (_keys(this.submissionData).length) ? this.submissionData : null;
  }
  
  reworkProviders(providers) {
    let retProviders = [];
    if (providers && _isArray(providers)) {
      providers.forEach((e, idx) => {
        if (e.active) {
          e.id += '';
          e.disabled = (e.disabled == 1 /*|| (idx % 2)*/) ? true : false;
          if (e.displayName && e.disabled && _isString(e.last_submission_date) && e.last_submission_date.length) {
            // e.displayName += ' (submitted ' + moment(e.last_submission_date).fromNow() + ')';
  
            let diff = moment(moment(e.last_submission_date).add(90, 'days').format()).diff(moment(), 'd'); // 88
            diff = moment.langData().relativeTime(diff, false, diff == 1 ? 'd' : 'dd');  //  "88 days"
            
            e.displayName += ' (' + diff + ' before next submission)';
          }
          retProviders.push(e);
        }
      });
    }
    return retProviders;
  }
  
  submitSubmission() {
    let data = _cloneDeep(this.submissionData);
    if (data.providers && data.providers.length) {
      let _providers = _cloneDeep(data.providers);
      data.providers = [];
      _providers.forEach((e) => {
        data.providers.push(e.id);
      });
      delete data.provider;
    } else {
      if (data.provider) {
        data.providers = [data.provider.id];
      }
      delete data.provider;
    }
    data.reportingStart_date = this.dateFilter(data.reportingStart, 'MM/dd/yy');
    data.reportingEnd_date = this.dateFilter(data.reportingEnd, 'MM/dd/yy');
    delete data.addData;
    data.providers = data.providers.join(',');
    // data.program_id = parseInt(data.program_id);
    // data.provider = parseInt(data.provider);
    return this.$http.post('/submission', data/*, {params: data}*/);
  }
  
  convertNumToString(data) {
    for (let i in data) {
      if (data.hasOwnProperty(i)) {
        if (angular.isObject(data[i])) {
          data[i] = this.convertNumToString(data[i]);
        } else if (angular.isNumber(data[i])) {
          data[i] += '';
        }
      }
    }
    return data;
  }
  
  convertBool(data) {
    for (let i in data) {
      if (data.hasOwnProperty(i)) {
        if (angular.isObject(data[i])) {
          data[i] = this.convertBool(data[i]);
        } else if (angular.isString(data[i]) && (/true/ig.test(data[i]) || /false/ig.test(data[i]))) {
          data[i] = (/true/ig.test(data[i]));
        }
      }
    }
    return data;
  }
};

SubmissionService.$inject = [
  '$q',
  '$http',
  '$state',
  'modalService',
  'dateFilter'
];