'use strict';
/**
 * Created by dzmitry.barkouski on 26.01.2017.
 */
import angular from 'angular';
import 'angularjs-dropdown-multiselect/dist/src/angularjs-dropdown-multiselect';
import submissionViewer from '../../../../Modules/CustomUIDirectives_and_Services/SubmissionViewer';
import providersFileViewer from './Directives/ProvidersFileViewer';
import service from './service';
import config from './config';
import controller from './controller';
import submissionRouterController from './router_controller';
import directives from './Directives';
import './styles.scss';


let dependencies = [
  directives,
  submissionViewer,
  providersFileViewer,
  'angularjs-dropdown-multiselect'
];

export default angular.module('submissionModule', dependencies)
  .controller('submissionRouterController', submissionRouterController)
  .controller('submissionController', controller)
  .service('submissionService', service)
  .config(config)
  .name;