"use strict";
/**
 * Created by dzmitry.barkouski on 07.05.2017.
 **/
import angular from 'angular';

export default angular.module('addSubmissionDirectives', [])
  .directive('state1', state1)
  .directive('state21', state21)
  .directive('state22', state22)
  .directive('state3', state3)
  .directive('state4', state4)
  .directive('state5', state5)
  .directive('statePagination', pagination)
  .directive('inputProvider', inputProvider)
  // .directive('ngDropdownRequired', function () {
  //   return {
  //     restrict: 'A',
  //     require: '?ngModel',
  //     link: function ($scope, elem, attrs, ngModel) {
  //
  //       $scope.updateForm = function (model, mode) {
  //         model.$setValidity("requiredSelect", mode);
  //       };
  //
  //       $scope.checkModel = function (model) {
  //         return !((angular.isObject(model) && angular.equals(model, {})) || (angular.isArray(model) && model.length == 0));
  //       };
  //
  //       $scope.$watch(attrs.selectedModel, function (val) {
  //         $scope.updateForm(ngModel, $scope.checkModel(val));
  //       }, true);
  //     }
  //   };
  // })
  .name;

function state1() {
  return {
    restrict: 'EA',
    template: require('./State1.html'),
    controller: 'submissionController',
    controllerAs: 'vm',
    scope: true
  };
}

function state21() {
  return {
    restrict: 'EA',
    template: require('./State2_1.html'),
    controller: 'submissionController',
    controllerAs: 'vm',
    scope: true,
    link: state21Link
  };
}

function state22() {
  return {
    restrict: 'EA',
    template: require('./State2_2.html'),
    controller: 'submissionController',
    controllerAs: 'vm',
    scope: true,
    link: state22Link
  };
}

function state3() {
  return {
    restrict: 'EA',
    template: require('./State3.html'),
    controller: 'submissionController',
    controllerAs: 'vm',
    scope: true
  };
}

function state4() {
  return {
    restrict: 'EA',
    template: require('./State4.html'),
    controller: 'submissionController',
    controllerAs: 'vm',
    scope: true
  };
}

function state5() {
  return {
    restrict: 'EA',
    template: require('./State5.html'),
    controller: 'submissionController',
    controllerAs: 'vm',
    scope: true
  };
}

function pagination() {
  return {
    restrict: 'EA',
    template: require('./StatesPagination.html'),
    link: function (scope, element, attributes, controller) {
      scope.vm.pages = scope.vm.pages || [
        {number: 1},
        {number: 2},
        {number: 3},
        {number: 4},
        {number: 5}
      ];
      scope.vm.pages.forEach((page) => {
        delete page.active
      });
      if (attributes.page) {
        const page = parseInt(attributes.page, 10);
        if (!isNaN(page) && scope.vm.pages[page - 1]) {
          scope.vm.pages[page - 1].active = true;
        }
      }
    }
  };
}

function inputProvider() {
  return {
    restrict: 'EA',
    template: require('./StateInputProviderFiles.html'),
    scope: false,
    link: state1Link
  };
}

function state1Link(scope, element, attributes) {
  let self = this,
    controller = scope.vm,
    doc = controller.document[0];
  let input = angular.element(doc.querySelectorAll('#providersFileUploader')[0]);
  
  function addFile(event, drop) {
    let files = event.target.files,
      filesArray = self.filesArray = [];
    
    for (let i = files.length; i--;) {
      filesArray[i] = files[i];
    }
    if (filesArray && filesArray.length) {
      controller.uploadProvidersFileToServer(filesArray);
    }
    if (!drop) {
      input.val('');
    }
    event.preventDefault();
    event.stopImmediatePropagation();
  }
  
  input
    .off('change', addFile)
    .on('change', addFile);
}

function state21Link(scope, element, attributes, controller, transcludeFn) {
  let self = this,
    doc = controller.document[0];
  doc.body.removeEventListener('drop', globalDropHandler);
  doc.body.addEventListener('drop', globalDropHandler, false);
  doc.body.removeEventListener('dragover', globalDropHandler);
  doc.body.addEventListener('dragover', globalDropHandler, false);
  let input = angular.element(doc.querySelectorAll('#submissionFileUploader')[0]),
    label = angular.element(doc.querySelectorAll('label[for="submissionFileUploader"]')[0]);
  label
    .off('dragenter')
    .on('dragenter', (event) => {
      label.addClass('bg-info');
      event.preventDefault();
      event.stopImmediatePropagation();
    })
    .off('dragleave')
    .on('dragleave', (event) => {
      label.removeClass('bg-info');
      event.preventDefault();
      event.stopImmediatePropagation();
    })
    .off('drop')
    .on('drop', (e) => {
      addFile(e, true);
    });
  
  input
    .off('change', addFile)
    .on('change', addFile);
  
  controller.uploadNew = () => {
    let event = new MouseEvent('click', {
      'view': window,
      'bubbles': true,
      'cancelable': true
    });
    input[0].dispatchEvent(event);
  };
  
  function globalDropHandler(e) {
    e.preventDefault();
  }
  
  function addFile(event, drop) {
    label.removeClass('bg-info');
    let files = event.target.files,
      filesArray = self.filesArray = [];
    
    if (drop) {
      if (event.originalEvent && event.originalEvent.dataTransfer) {
        files = event.originalEvent.dataTransfer.files;
      } else if (event.dataTransfer) {
        files = event.dataTransfer.files;
      }
    }
    for (let i = files.length; i--;) {
      filesArray[i] = files[i];
    }
    if (filesArray && filesArray.length) {
      /*if (options && options.maxItems && filesArray.length > options.maxItems) {
       filesArray = filesArray.slice(0, options.maxItems);
       }*/
      // debugger;
      controller.uploadFileToServer(filesArray);
    }
    if (!drop) {
      input.val('');
    }
    event.preventDefault();
    event.stopImmediatePropagation();
  }
}

function state22Link(scope, element, attributes, controller, transcludeFn) {
  let table_body = element.find('tbody'),
    table_row = require('./State2_2_tr_template.html');
  scope.vm.link = {
    addRow: () => {
      table_body.append(table_row);
    },
    collectData: () => {
      let fullData = [];
      let tr = table_body.find('tr'),
        tr_length = tr.length;
      let ii, ik;
      for (ii = 0; ii < tr_length; ii++) {
        let tr_inputs = angular.element(tr[ii]).find('input'),
          tr_inputs_length = tr_inputs.length,
          rowData = [];
        for (ik = 0; ik < tr_inputs_length; ik++) {
          let input = angular.element(tr_inputs[ik]);
          rowData.push(input.val());
        }
        
        fullData.push(rowData.join(','));
      }
      
      return fullData.join('\n');
    },
    sendData: () => {
      scope.vm.sendManualData(scope.vm.link.collectData());
      
    }
  };
  scope.vm.link.addRow();
}