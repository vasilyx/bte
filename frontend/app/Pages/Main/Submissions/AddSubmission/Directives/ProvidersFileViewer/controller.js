'use strict';
import _isArray from 'lodash/isArray';
import _cloneDeep from 'lodash/cloneDeep';

let dependencies = [
  '$scope',
  '$sce',
  '$uibModal',
  'modalService'
];
export default class ProvidersListViewerController {
  constructor($scope, $sce, $uibModal, modalService) {
    this.scope = $scope;
    this.$sce = $sce;
    this.$uibModal = $uibModal;
    this.modal = modalService;
    this.table_row_template = require('./table_row_template.hbs');
    this.pageSizes = [
      {
        val: 25,
        title: '25'
      },
      {
        val: 50,
        title: '50'
      },
      {
        val: 100,
        title: '100'
      }
    ];
    this.pageSize;
  }
  
  shopPopup() {
    
    let alldata = this.scope.get_providers_file_data();
    this.body_data = this.getHTMLData(this.reworkBodyData(alldata.data));
    if (!_isArray(this.body_data)) {
      return;
    }
    
    
    let self = this;
    this.modal = this
      .$uibModal
      .open({
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        size: 'lg',
        windowClass: 'modal-submissionData',
        ok_btn_title: 'Close',
        controller: function () {
          
          self.modalController = this;
          this.modal_title = self.scope.modal_title;
          this.pageSizes = [];
          this.btn_print = self.scope.btn_print;
          this.modal = self.modal;
  
          this.head = self.reworkHeadData(alldata.head);
          
          this.setPage = (pageData) => {
            this.data_onepage = pageData;
          };
          
          this.setModalControllerData = () => {
            self.setModalControllerData();
          };
          
          this.closePopup = () => {
            delete self.modalController;
            this.modal.close();
          };
          
          this.changePage = () => {
            if (self.providers_file_data.length) {
              this.setPage(self.providers_file_data[this.pageNumber - 1]);
            }
          };
          
        },
        controllerAs: 'vm',
        template: require('./modal_template.html')
      });
    
    this.modal.rendered.then(() => {
      this.setModalControllerData();
    });
    
  }
  
  reworkHeadData(data) {
    return this.getHTMLData([data])[0];
  }
  
  reworkBodyData(data) {
    return data.map((e) => {
      return e.fileData
    });
  }
  
  getHTMLData(data) {
    // debugger;
    let htmlArray = [];
    data.forEach((el) => {
      let tempArray = [];
      for (let i = 0; i < el.length; i++) {
        let element = el[i],
          tempEl = {
            value: element[0]
          };
        if (element[1] === 1) {
          tempEl.warning = true;
        } else if (element[1] === 2) {
          tempEl.error = true;
        }
        
        tempArray.push(tempEl);
      }
      htmlArray.push(this.$sce.trustAsHtml(this.table_row_template({data: tempArray})));
    });
    return htmlArray;
  }
  
  setModalControllerData() {
    this.providers_file_data = [];
    this.modalController.pageNumber = 1;
    
    this.modalController.totalCount = this.body_data.length;
    this.resetPageSizes(this.body_data);
    if (this.modalController.pageSizes[0] && this.modalController.pageSizes[0].val && !this.modalController.pageSize) {
      this.modalController.pageSize = this.modalController.pageSizes[0].val;
    }
    this.modalController.setPage(this.setModalControllerBodyData(this.body_data)[0]);
  }
  
  setModalControllerBodyData(dataWithHtml) {
    let clone_data = _cloneDeep(dataWithHtml),
      pageSize = this.modalController.pageSize,
      totalPageCount = Math.ceil(clone_data.length / pageSize),
      length = clone_data.length;
    //todo:todo:todo
    if (clone_data.length > pageSize) {
      for (let i = 1, l = totalPageCount; i <= l; i++) {
        let finish = pageSize * i - 1;
        if ((finish > length)) {
          finish = length;
        }
        this.providers_file_data.push(clone_data.slice(pageSize * (i - 1), finish));
      }
    } else {
      this.providers_file_data = [clone_data];
    }
    return this.providers_file_data;
  }
  
  resetPageSizes() {
    let allExist = false;
    this.modalController.pageSizes = [];
    this.pageSizes.forEach((el) => {
      let element = {
        val: el.val,
        title: el.title
      };
      if (element.val === this.body_data.length) {
        allExist = true;
        element.title = 'All(' + element.val + ')';
      }
      if (element.val <= this.body_data.length) {
        this.modalController.pageSizes.push(element);
      }
    });
    return this.modalController.pageSizes;
  }
  
  throwError(title, error) {
    const data = {
      title: title,
      error: error
    };
    
    this.modal.alert(data);
  }
}
ProvidersListViewerController.$inject = dependencies;