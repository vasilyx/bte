'use strict';
/**
 * Created by dzmitry.barkouski on 26.01.2017.
 */
import angular from 'angular';
import directive from './directive';
import './styles.scss';

let dependencies = [
];

export default angular.module('ProvidersFileViewerDirective', dependencies)
  .directive('providersViewer', directive)
  .name;