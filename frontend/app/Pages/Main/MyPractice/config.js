'use strict';
/**
 * Created by dzmitry.barkouski on 26.01.2017.
 */

config.$inject = [
  '$stateProvider'
];

export default function config($stateProvider) {
  $stateProvider
    .state({
      name: 'main.practice',
      url: '/practice',
      template: require('./template.html'),
      controller: 'practicePageController',
      controllerAs: 'vm',
      resolve: {
        practiceResolver: practiceResolver
      }
    });
}

function practiceResolver(practiceService, $state, $q) {
  let def = $q.defer();
  if ($state && $state._nextState && $state._nextState.state && $state._nextState.state.name === "main.practice") {
    def = practiceService.checkPractice({
      redirectState: 'main.dashboardWelcome',
      reload: true
    });
  } else {
    def.resolve();
  }
  return def.promise || def;
}
practiceResolver.$inject = [
  'practiceService',
  '$state',
  '$q'
];