'use strict';

let dependencies = [
  '$scope',
  'authService',
  '$state',
  'practiceService',
  'providerService'
];
export default class PracticePageController {
  constructor($scope, authService, $state, practiceService, providerService) {
    // this.authService = authService;
    this.state = $state;
    this.service = practiceService;
    // alert('home PAGE');
    let stateChange = ()=>{
      if (this.state.current.name === 'main.practice') {
        practiceService.requestPractice(true).then((practice)=>{
          this.practice = practice;
        }, (redirect)=>{
          if (redirect) {
            // $state.go('main.dashboardWelcome');
          }
        });
        providerService.getProviders().then((providers)=>{
          if (providers) {
            this.providers = providers;
          } else{
            delete this.providers;
          }
        }, ()=>{
          delete this.providers;
        });
      }
    };
  
    $scope.$on('$stateChangeSuccess', stateChange);
    // stateChange();
  }
  
  activateProvider(provider){
    this.providers.forEach((e)=>{
      delete e._active;
    });
    provider._active = true;
    if (provider && provider.id) {
      this.editProviderId = provider.id;
    } else {
      delete this.editProviderId;
    }
  }
}
PracticePageController.$inject = dependencies;