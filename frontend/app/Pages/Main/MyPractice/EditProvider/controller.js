'use strict';
/*
 * controller.js
*/
import _cloneDeep from 'lodash/cloneDeep';
import _isDate from 'lodash/isDate';
import _isFunction from 'lodash/isFunction';
import _keys from 'lodash/keys';

let dependencies = [
  '$scope',
  '$state',
  '$stateParams',
  '$localStorage',
  'providerService',
  'modalService',
  'userInfoService'
];
export default class EditProviderPageController {
  constructor($scope, $state, $stateParams, $localStorage, providerService, modalService, userInfoService) {
    this.state = $state;
    this._$scope = $scope;
    this.$stateParams = $stateParams;
    this.service = providerService;
    this.modalService = modalService;
    this.provider = {};
    this.$localStorage = $localStorage;
    this.newUser = $localStorage.newUser;
    this.userInfoService = userInfoService;
    
    
    if ($localStorage.returnToState) {
      this.returnToState = $localStorage.returnToState;
      delete $localStorage.returnToState;
    }
    
    if (this.$stateParams && this.$stateParams.id) {
      this.service.getProviderData().then((provider) => {
        this.provider = provider;
      }, () => {
        this.provider = {};
      });
    } else {
      this.provider = {};
    }
    this.popup = {
      opened: false
    };
    
    if (this.providerForm) {
      if (_isFunction(this.providerForm.$setPristine)) this.providerForm.$setPristine();
      if (_isFunction(this.providerForm.$setUntouched)) this.providerForm.$setUntouched();
    }
    
    this.formData = {};
    this.service.getProviderClinician_degree().then((degree) => {
      this.formData.clinician_degree = degree;
    }, () => {
      delete this.formData.clinician_degree;
    });
    this.service.getProviderClinician_speciality().then((speciality) => {
      this.formData.clinician_speciality = speciality;
    }, () => {
      delete this.formData.clinician_speciality;
    });
  }
  
  
  showDatePicker() {
    this.popup.opened = true;
  }
  
  saveProvider(formdata) {
    let form = this._$scope.providerForm;
    
    if ((form.$error && _keys(form.$error).length)) {
      form.$setSubmitted();
      this.throwError('Provider save failed', 'Form validation failed.');
      return;
    }
    if (!formdata.date_birth || !_isDate(formdata.date_birth)) {
      form.$setSubmitted();
      this.throwError('Provider save failed', 'Wrong date.');
      return;
    }
    this.service.checkActivity(formdata).then((data) => {
      this.service.saveProvider(_cloneDeep(data))
        .then(() => {
          if (this.returnToState) {
            this.state.go(this.returnToState);
            return;
          }
          if (this.$stateParams && this.$stateParams.id) {
            this.state.go('main.practice');
          } else {
            this.modalService.ask_2btns({
              ok_btn_title: 'Yes',
              cancel_btn_title: 'No',
              content_text: 'Would you like to add an additional provider?'
            }).then(() => {
              delete this.newUser;
              this.provider = {};
              this.state.reload('main.provider.create');
            }, () => {
              this.userInfoService.getUserData().then((data) => {
                if (data.has_agreement) {
                  if (!this.$localStorage.newUser) {
                    this.state.go('main.practice');
                  } else {
                    delete this.$localStorage.newUser;
                    this.state.go('main.dashboard');
                  }
                } else {
                  this.state.go('main.agreement');
                }
              }, () => {
                this.state.go('main.agreement');
              });
              
            });
          }
        }, (error) => {
          let message = (this.$stateParams && this.$stateParams.id) ? 'Unknown provider update error.' : 'Unknown provider save error.';
          let modal = this.modalService.alert({
            title: 'Practice edit.',
            error: (error && error.data && error.data.message) ? error.data.message : message
          });
        });
    }, () => {
    
    });
  }
  
  cancelSave() {
    if (this.returnToState) {
      this.state.go(this.returnToState);
    } else {
      this.state.go('main.practice');
    }
  }
  
  throwError(title, error) {
    const data = {
      title: title,
      error: error
    };
    
    this.modalService.alert(data);
    console.warn('some error - ', error); //TODO: Delete this before checkIN
  }
}
EditProviderPageController.$inject = dependencies;