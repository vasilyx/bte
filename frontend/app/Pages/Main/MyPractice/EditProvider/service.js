"use strict";
/**
 * Created by dzmitry.barkouski on 27.04.2017.
 **/
import angular from 'angular';
import _cloneDeep from 'lodash/cloneDeep';
import _keys from 'lodash/keys';
import _merge from 'lodash/merge';

export default class ProviderService {
  constructor($q, $http, $state, $stateParams, modalService) {
    this.$q = $q;
    this.$http = $http;
    this.$state = $state;
    this.$stateParams = $stateParams;
    this.modalService = modalService;
    
    // console.log(this.$stateParams); //TODO: Delete this before checkIN
  }
  
  getProviders(addData) {
    let def = this.$q.defer(),
      prom = def.promise;
    this.$http
      .get('/providers', {params: addData})
      .then((success) => {
        // this.cacheControl(success.headers());
        let data = success.data;
        if (angular.isArray(data)) {
          if (data.length) {
            
            data.forEach((e)=>{
              if (e[0] && typeof e[0] === 'object') {
                let tempObj = e[0];
                delete e[0];
                e = _merge(e, tempObj);
              }
              e.displayName = e.lastname + ' ' + e.firstname;
              if (e.degree && e.degree.name) {
                e.displayName += ', ' + e.degree.name
              }
              e.displayName += '  ' + e.npi;
            });
            
            this.providers = data;
            def.resolve(this.providers);
          } else {
            def.resolve();
          }
        }
      }, () => {
        def.reject();
      });
    return prom;
  }
  
  getProviderData() {
    let def = this.$q.defer(),
      prom = def.promise;
    delete this.providerData;
    if (this.$stateParams && this.$stateParams.id) {
      this.$http
        .get('/provider', {
          params: {
            id: this.$stateParams.id
          }
        })
        .then((success) => {
          let data = success.data;
          if (angular.isArray(data) && angular.isObject(data[0])) {
            this.providerData = this.convertNumToString(data[0]);
            this.providerDataClone = _cloneDeep(this.providerData);
            if (this.$stateParams && this.$stateParams.id) {
              this.providerData.id = this.$stateParams.id;
            }
            this.providerData._active = this.providerData.active;
            if (this.providerData.date_birth) {
              this.providerData.date_birth = new Date(this.providerData.date_birth);
            }
            def.resolve(this.providerData);
          } else {
            def.reject();
          }
        }, () => {
          def.reject();
        });
    } else {
      def.reject();
    }
    
    return prom;
  }
  
  getProviderClinician_degree() {
    let def = this.$q.defer();
    this.$http
      .get('/provider/clinician_degree')
      .then((success) => {
        def.resolve(success.data);
      }, (error) => {
        def.reject(error);
      });
    return def.promise;
  }
  
  getProviderClinician_speciality() {
    let def = this.$q.defer();
    this.$http
      .get('/provider/clinician_speciality')
      .then((success) => {
        def.resolve(success.data);
      }, (error) => {
        def.reject(error);
      });
    return def.promise;
  }
  
  cacheControl(headers) {
    if (!headers || !headers['cache-control']) return;
    const cacheControl = headers['cache-control'].replace(' ', '').split(',');
    let cacheControlIterator = (cc) => {
      if (!(cc && angular.isString(cc) && cc.length)) return;
      switch (cc) {
        case 'no-cache':
          this.practiceData._request = true;
          break;
        default:
          break;
      }
    };
    if (cacheControl.length > 1) {
      cacheControl.forEach(cacheControlIterator);
    } else {
      cacheControlIterator(cacheControl[0]);
    }
  }
  
  convertNumToString(data){
    for (let i in data) {
      if (data.hasOwnProperty(i)) {
        if (angular.isObject(data[i])) {
          data[i] = this.convertNumToString(data[i]);
        } else if(angular.isNumber(data[i])) {
          data[i] += '';
        }
      }
    }
    return data;
  }
  
  getDifference(obj) {
    let diff = {}, i;
    for (i in obj) {
      if (obj.hasOwnProperty(i)) {
        if (!angular.isObject(obj[i]) && !angular.isObject(this.providerDataClone[i]) && this.providerDataClone[i] !== obj[i]) {
          diff[i] = obj[i];
        }
      }
    }
    return diff;
  }
  
  checkActivity(formData) {
    let def = this.$q.defer();
    if (formData.active) {
      if (formData.cbState) {
        this.modalService.ask_2btns({
          content_text: 'Are you sure you would <br> inactivate this provider? ',
          ok_btn_title: 'Yes',
          cancel_btn_title: 'No'
        }).then(() => {
          formData.active = false;
          def.resolve(formData);
        }, () => {
          formData.cbState = false;
          def.reject(formData);
        });
      } else {
        def.resolve(formData);
      }
    } else {
      if (formData.cbState) {
        this.modalService.ask_2btns({
          content_text: 'Are you sure you would <br> activate this provider? ',
          ok_btn_title: 'Yes',
          cancel_btn_title: 'No'
        }).then(() => {
          formData.active = true;
          def.resolve(formData);
        }, () => {
          formData.cbState = false;
          def.reject(formData);
        });
      } else {
        def.resolve(formData);
      }
    }
    return def.promise;
  }
  
  saveProvider(data) {
    let request,
      date_birth = data.date_birth.toISOString(),
      sendData = _cloneDeep(data);
    delete sendData._active;
    sendData.date_birth = date_birth.split('T')[0];
    if (this.$stateParams && this.$stateParams.id) {
      sendData = this.getDifference(sendData);
      if (_keys(sendData).length) {
        sendData.id = this.$stateParams.id;
        request = this.$http
          .put('/provider', sendData);
      } else {
        let def = this.$q.defer();
        def.resolve();
        request = def.promise;
      }
    } else {
      request = this.$http
        .post('/provider', sendData);
    }
    request.then(() => {
    }, (error) => {
      console.log(error); //TODO: Delete this before checkIN
    });
    // this.practiceData._request = true;
    return request;
  }
};

ProviderService.$inject = [
  '$q',
  '$http',
  '$state',
  '$stateParams',
  'modalService'
];