'use strict';
/**
 * Created by dzmitry.barkouski on 26.01.2017.
 */

config.$inject = [
  '$stateProvider',
];

export default function config($stateProvider) {
  $stateProvider
    .state({
      name: 'main.provider',
      url: '/provider',
      template: '<ui-view></ui-view>',
      resolve: {}
    })
    .state({
      name: 'main.provider.edit',
      url: '/edit?id',
      template: require('./template.html'),
      controller: 'providerEditPageController',
      params: {
        id: null
      },
      controllerAs: 'vm',
      resolve: {}
    })
    .state({
      name: 'main.provider.create',
      url: '/create',
      template: require('./template.html'),
      controller: 'providerEditPageController',
      params: {
        id: null
      },
      controllerAs: 'vm',
      resolve: {}
    });
}