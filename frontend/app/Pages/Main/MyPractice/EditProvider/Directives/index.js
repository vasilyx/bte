"use strict";
/**
 * Created by Dzmitry.Barkouski on 29.05.2017.
 **/

import angular from 'angular';
import checkClinicianID from './checkClinicianID';
import checkClinicianNPI from './checkClinicianNPI';

export default angular.module('editProviderValidationDirectives', [])
  .directive('checkClinicianId', checkClinicianID)
  .directive('checkClinicianNpi', checkClinicianNPI)
  .name;