"use strict";
/**
 * Created by Dzmitry.Barkouski on 29.05.2017.
 **/
CheckClinicianNPI.$inject = [
  '$q',
  '$http',
  '$stateParams'
];
export default function CheckClinicianNPI($q, $http, $stateParams) {
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function (scope, element, attributes, controller) {
      console.log('checkClinicianNPI', scope, element, attributes, controller);
      controller.$asyncValidators.asyncValidator = function (value) {
        if (controller.validationTimer) {
          clearTimeout(controller.validationTimer);
          delete controller.validationTimer;
        }
        controller.validationTimer = setTimeout(request, 500);
        let def = $q.defer(),
          data = {
            value: value
          };
        if ($stateParams.id) {
          data.id = $stateParams.id;
        }
        function request() {
          let form = scope.providerForm,
            errors = angular.extend({},
              form.providerNPI.$error);
          if (Object.keys(errors).length) {
            def.resolve();
            return;
          }
          
          $http
            .post('/provider/check/npi', data)
            .then(() => {
              def.resolve();
            }, () => {
              def.reject();
            });
        }
        console.log(value); //TODO: Delete this before checkIN
        // should always return a promise
        //return asyncDateValidator(value);
        return def.promise;
      }
    }
  }
}