'use strict';

import angular from 'angular';
import config from './config';
import controller from './controller';

import editPractice from './EditPractice';
import editProvider from './EditProvider';

import './styles.scss';


let dependencies = [
  editPractice,
  editProvider
];

export default angular.module('practicePageModule', dependencies)
  .controller('practicePageController', controller)
  .config(config)
  .name;