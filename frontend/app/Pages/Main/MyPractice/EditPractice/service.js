"use strict";
/**
 * Created by dzmitry.barkouski on 27.04.2017.
 **/
import angular from 'angular';
import _merge from 'lodash/merge';
import _isObject from 'lodash/isObject';
export default class PracticeService{
  constructor($q, $http, $state, $localStorage){
    this.$q = $q;
    this.$http = $http;
    this.$state = $state;
    this.$localStorage = $localStorage;
    this.clearPracticeData();
  }
  clearPracticeData(){
    this.practiceData = {
      name: '',
      address: '',
      city: '',
      state: '',
      zipCode: '',
      phone: '',
      npi: '',
      primaryContactName: '',
      primaryContactEmail: '',
      _request: true
    };
  }
  requestPractice(needRedirect, forceRequest){
    let def = this.$q.defer(),
    prom = def.promise;
    if (this.practiceData._request || forceRequest) {
      this.clearPracticeData();
      this.$http
        .get('/user/practice')
        .then((success)=>{
          this.practiceData._request = false;
          this.cacheControl(success.headers());
          console.log('practice | ', success.headers()); //TODO: Delete this before checkIN
          let data = success.data;
          if (angular.isArray(data)) {
            if (data.length && data[0]) {
              this.practiceData = _merge(this.practiceData, data[0]);
              def.resolve(this.practiceData);
            } else if (!needRedirect) {
              def.resolve(this.practiceData);
            } else {
              def.reject(needRedirect);
            }
          }
        }, ()=>{
          def.reject();
        });
    } else {
      def.resolve(this.practiceData);
    }
    
    return prom;
  }
  
  checkPractice(redirectStateObj){
    let def = this.$q.defer(),
      prom = def.promise;
    
    this.requestPractice(true).then(()=>{
      def.resolve();
    },()=>{
      def.reject((redirectStateObj && _isObject(redirectStateObj))? redirectStateObj : null);
    });
    
    return prom;
  }
  
  cacheControl(headers){
    if (!headers || !headers['cache-control']) return;
    const cacheControl = headers['cache-control'].replace(' ', '').split(',');
    let cacheControlIterator = (cc)=>{
      if (!(cc && angular.isString(cc) && cc.length)) return;
      switch (cc) {
        case 'no-cache':
          this.practiceData._request = true;
          break;
        default:
          break;
      }
    };
    if (cacheControl.length > 1) {
      cacheControl.forEach(cacheControlIterator);
    } else {
      cacheControlIterator(cacheControl[0]);
    }
  }
  
  savePractice(data){
    delete data._request;
    let req = this.$http
      .post('/user/practice', data);
    req.then(()=>{
      this.practiceData._request = true;
      // this.requestPractice();
    });
    return req;
  }
};

PracticeService.$inject = [
  '$q',
  '$http',
  '$state',
  '$localStorage'
];