'use strict';
/**
 * Created by dzmitry.barkouski on 26.01.2017.
 */

config.$inject = [
  '$stateProvider',
];

export default function config($stateProvider) {
  $stateProvider
    .state({
      name: 'main.practice.create',
      url: '/create',
      template: require('./template.html'),
      controller: 'dashboardPracticePageController',
      controllerAs: 'vm',
      resolve: {}
    })
    .state({
      name: 'main.practice.edit',
      url: '/edit',
      template: require('./template.html'),
      controller: 'dashboardPracticePageController',
      controllerAs: 'vm',
      params: {
        check: true
      },
      resolve: {}
    });
}