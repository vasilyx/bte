'use strict';
/**
 * Created by dzmitry.barkouski on 26.01.2017.
 */
import './styles.scss';

import angular from 'angular';
import config from './config';
import controller from './controller';
import service from './service';

let dependencies = [
];

export default angular.module('dashboardPracticePageModule', dependencies)
  .controller('dashboardPracticePageController', controller)
  .service('practiceService', service)
  .config(config)
  .name;