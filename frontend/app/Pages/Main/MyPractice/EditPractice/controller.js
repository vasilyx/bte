'use strict';
/*
 * controller.js
*/
import _keys from 'lodash/keys';

let dependencies = [
  '$scope',
  'authService',
  '$state',
  '$localStorage',
  'practiceService',
  'modalService'
];
export default class DashboardPracticePageController {
  constructor($scope, authService, $state, $localStorage, practiceService, modalService) {
    this.authService = authService;
    this.state = $state;
    this._$scope = $scope;
    this.service = practiceService;
    this.modalService = modalService;
    this.$localStorage = $localStorage;
    // alert('home PAGE');
    if ($localStorage.returnToState) {
      this.returnToState = $localStorage.returnToState;
      delete $localStorage.returnToState;
    }
    
    this.service.requestPractice(false, true).then((practice) => {
      this.practice = practice;
    });
  }
  
  savePractice(data) {
    let form = this._$scope.practiceForm;
    
    if (form.$error && _keys(form.$error).length) {
      form.$setSubmitted();
      this.throwError('Practice save failed', 'Form validation failed.');
      return;
    }
    this.service.savePractice(data)
      .then(() => {
        
        if (this.returnToState) {
          this.state.go(this.returnToState);
          return;
        }
        
        if (this.state.current.name === 'main.practice.create') {
          if (data.data_aggregator) {
            this.state.go('main.agreement');
          } else {
            this.state.go('main.provider.create');
          }
        } else {
          this.state.go('main.practice');
        }
      }, (error) => {
        let modal = this.modalService.alert({
          title: 'Practice edit.',
          error: (error && error.data && error.data.message && typeof error.data.message === 'string') ? error.data.message : 'Unknown practice update error.'
        });
        
        // modal.closed.then(()=>{
        //   this.state.go('main.practice');
        // });
      });
  }
  
  
  cancelSave() {
    if (this.returnToState) {
      this.state.go(this.returnToState);
    } else {
      this.state.go('main.practice', null, {reload: true});
    }
  }
  
  throwError(title, error) {
    const data = {
      title: title,
      error: error
    };
    
    this.modalService.alert(data);
    console.warn('some error - ', error); //TODO: Delete this before checkIN
  }
}
DashboardPracticePageController.$inject = dependencies;