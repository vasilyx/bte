'use strict';
/*
 * controller.js
*/
let dependencies = [
  'authService',
  'userInfoService',
  '$state'
];
export default class MainPageController {
  constructor(authService, userInfoService, $state) {
    this.authService = authService;
    this.state = $state;
    userInfoService.clearUserData();
    // alert('home PAGE');
  }
  
  logout () {
    this.authService.logout().then(()=>{
      this.state.go('login.page');
    });
  }
  
}
MainPageController.$inject = dependencies;