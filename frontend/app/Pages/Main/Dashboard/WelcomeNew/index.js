'use strict';
/**
 * Created by dzmitry.barkouski on 26.01.2017.
 */
import './styles.scss';

import angular from 'angular';
import config from './config';
import controller from './controller';

let dependencies = [];

export default angular.module('dashboardWelcomePageModule', dependencies)
  .controller('dashboardWelcomePageController', controller)
  .config(config)
  .name;