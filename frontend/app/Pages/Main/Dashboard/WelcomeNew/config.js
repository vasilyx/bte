'use strict';
/**
 * Created by dzmitry.barkouski on 26.01.2017.
 */

config.$inject = [
  '$stateProvider',
];

export default function config($stateProvider) {
  $stateProvider
    .state({
      name: 'main.dashboardWelcome',
      url: '/welcome',
      template: require('./template.html'),
      controller: 'dashboardWelcomePageController',
      controllerAs: 'vm',
      resolve: {}
    });
}