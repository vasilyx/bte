'use strict';
/*
 * controller.js
*/
let dependencies = [
  'authService',
  '$state',
  '$localStorage',
  'userInfoService'
];
export default class DashboardWelcomePageController {
  constructor(authService, $state, $localStorage, userInfoService) {
    this.authService = authService;
    this.state = $state;
    $localStorage.newUser = true;
    userInfoService.getUserData().then((data)=>{
      this.user = data;
    }, ()=>{ delete this.user });
    // alert('home PAGE');
  }
  
}
DashboardWelcomePageController.$inject = dependencies;