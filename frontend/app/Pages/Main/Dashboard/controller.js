'use strict';

let dependencies = [
  '$scope',
  '$sce',
  '$localStorage',
  '$state',
  'practiceService',
  'submissionsListService',
  'recognitionsService',
  'userInfoService',
  '$window'
];
export default class DashboardPageController {
  constructor($scope, $sce, $localStorage, $state, practiceService, submissionService, recognitionsService, userInfoService, $window) {
    // this.authService = authService;
    this.state = $state;
    this.$sce = $sce;
    this.$scope = $scope;
    this.$window = $window;
    this.service = practiceService;
    this.submissions_service = submissionService;
    this.recognitions_service = recognitionsService;
    // alert('home PAGE');
    this.tasks = [];
    
    let stateChange = () => {
      if (this.state.current.name === 'main.dashboard') {
        this.service.requestPractice(true).then((practice) => {
          this.practice = practice;
          delete $localStorage.newUser;
          this.submissions_service.requestSubmissions(1, 3).then((data) => {
            let submissions = data.submissions;
            if (submissions && submissions.length > 2) {
              this.submissionsLink = 'main.submissions';
              submissions = submissions.slice(0, 2);
            } else {
              delete this.submissionsLink;
            }
            this.submissions = submissions;
            
          }, () => {
            delete this.submissions;
            delete this.submissionsLink;
          });
          this.recognitions_service.getRecognitions(1, 3).then((data) => {
            let recognitions = data.recognitions;
            if (recognitions && recognitions.length > 2) {
              this.recognitionsLink = 'main.recognitions';
              recognitions = recognitions.slice(0, 2);
            } else {
              delete this.recognitionsLink;
            }
            this.recognitions = recognitions;
          }, () => {
            delete this.recognitions;
            delete this.recognitionsLink;
          });
        }, (redirect) => {
          // if (redirect) {
          //  
          //   $state.go('main.dashboardWelcome', null, {reload: true});
          // }
        });
        
        
        userInfoService.getUserData().then((data) => {
          this.user = data;
          if (!this.user.has_agreement) {
            this.tasks.push({
              text: this.$sce.trustAsHtml('Data Submitter Agreement pending. Please note that this task must be completed in order to submit data. Once you have completed this step please return the from via email to <a href="mailto:bte@altarum.org">bte@altarum.org</a> for processing. <a class="text-danger" \n' +
                '        target="_self"\n' +
                '        href="/app/Resources/Data_Submitter_Agreement_Final_2017.pdf"\n' +
                '        title="Data Submitter Agreement Final 2017.pdf"\n' +
                '        download="Data Submitter Agreement Final 2017.pdf"">Download Data Submitter Agreement</a>')
            });
          }
        }, () => {
          delete this.user
        });
      }
    };
    // $scope.$on('$stateChangeSuccess', stateChange);
    stateChange();
    
  }
  
  /*reloadMasonry(){
    return;
    if (this.timeR) {
      clearTimeout(this.timeR);
      delete this.timeR;
    }
    this.timeR = setTimeout(()=>{
      if (this.reloadMasonry) {
      
      }
    },500);
  }*/
  
}
DashboardPageController.$inject = dependencies;