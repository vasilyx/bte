'use strict';
/**
 * Created by dzmitry.barkouski on 26.01.2017.
 */
import angular from 'angular';
// import directive from './directive';
import config from './config';
import controller from './controller';

import welcome from './WelcomeNew';
import practice from '../MyPractice/EditPractice';
import './styles.scss';


let dependencies = [
  welcome,
  practice
];

export default angular.module('dashboardPageModule', dependencies)
  .controller('dashboardPageController', controller)
  // .directive('dashboardMasonry', directive)
  .config(config)
  .name;