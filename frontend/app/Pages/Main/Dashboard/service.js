"use strict";
/**
 * Created by dzmitry.barkouski on 27.04.2017.
 **/
import angular from 'angular';
export default class DashboardService{
  constructor($q, $http, $state){
    this.$q = $q;
    this.$http = $http;
    this.$state = $state;
  }
  
  requestPractice(){
    let def = this.$q.defer(),
    prom = def.promise;
    
    this.$http
      .get('/user/practice')
      .then((success)=>{
        console.log('practice | ', success); //TODO: Delete this before checkIN
        let data = success.data;
        if (angular.isArray(data)) {
          if (data.length && data[0]) {
            this.practice = data[0];
            def.resolve(this.practice);
          } else {
            const redirect = true;
            def.reject(redirect);
          }
        }
      }, ()=>{
        def.reject();
      });
    return prom;
  }
};

DashboardService.$inject = [
  '$q',
  '$http',
  '$state'
];