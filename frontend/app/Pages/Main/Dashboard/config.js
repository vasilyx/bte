'use strict';
/**
 * Created by dzmitry.barkouski on 26.01.2017.
 */

config.$inject = [
  '$stateProvider'
];

export default function config($stateProvider) {
  $stateProvider
    .state({
      name: 'main.dashboard',
      url: '/dashboard',
      template: require('./template.html'),
      controller: 'dashboardPageController',
      controllerAs: 'vm',
      resolve: {
        practiceResolver: practiceResolver
      }
    });
}

function practiceResolver(practiceService) {
  return practiceService.checkPractice({redirectState:'main.dashboardWelcome', reload:true});
}
practiceResolver.$inject = [
  'practiceService'
];