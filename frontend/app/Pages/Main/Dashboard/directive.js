"use strict";
/**
 * Created by dzmitry.barkouski on 23.05.2017.
 **/
import masonryLayout from 'masonry-layout';
import angular from 'angular';
let dependencies = [
  '$window',
  '$timeout'
];

export default function MasonryDirective($window, $timeout) {
  $window.Masonry = masonryLayout;
  return function(scope, element, attrs, controller) {
    $timeout(function () {
      let masonry = new Masonry(element[0], {
        itemSelector: 'li',
        isAnimated: true,
        animationOptions: {
          duration: 500,
          easing: 'linear',
          queue: false
        },
        transitionDuration : "0.4s",
        isResizable: false
      });
  
      if (scope && scope.vm) {
        scope.vm.reloadMasonry = () => {
          if (masonry && angular.isFunction(masonry.layout)) {
            if (scope.vm.timeR) {
              clearTimeout(scope.vm.timeR);
              delete scope.vm.timeR;
            }
            scope.vm.timeR = setTimeout(()=>{
              masonry.layout();
            }, 50);
          }
        };
      }
    });
  };
}
MasonryDirective.$inject = dependencies;