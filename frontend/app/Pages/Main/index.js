'use strict';
/**
 * Created by dzmitry.barkouski on 26.01.2017.
 */
import angular from 'angular';
import config from './config';
import controller from './controller';
import './styles.scss';

import dashboard from './Dashboard';
import practice from './MyPractice';
import submissions from './Submissions';
import recognitions from './Recognitions';
import howto from './HowTo';

import agreement from './Agreement'


import userInfo from './../../Modules/UserInfo';

let dependencies = [
  dashboard,
  practice,
  submissions,
  recognitions,
  howto,
  userInfo,
  agreement
];

export default angular.module('homePageModule', dependencies)
  .controller('mainPageController', controller)
  .config(config)
  .name;