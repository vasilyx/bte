'use strict';
/**
 * Created by dzmitry.barkouski on 10.01.2017.
 */

import angular from 'angular';
let dependencies = [
  'authService',
  '$state',
  '$stateParams',
  '$scope',
  'modalService'
];
export default class ResetPasswordController{
  constructor(authService, $state, $stateParams, $scope, modalService) {
    this.recModel = {};
    this.authService = authService;
    this.$state = $state;
    this._$scope = $scope;
    this.modal = modalService;
    this.resetModel = {};
    // authService.checkMailVerification($stateParams).then(()=>{this.doneRedirect()}, (error)=>{this.throwError(error)});
  }
  
  setNewPass(data) {
    let form = this._$scope.resetForm,
      errors = angular.extend({}, form.resetPassword.$error, form.resetConfirmPassword.$error);
    if (Object.keys(errors).length || !(data && data.password)) {
      // this.throwError({data: {message: 'Validation failed.'}}, true);
    } else {
      this.authService
        .setNewPass(data)
        .then(() => {
          let modal = this.modal.info({
            title: 'Reset password',
            error: 'New password is set.' + '<br> You will be redirected after closing the window'
          });
          modal.closed.then(() => {
            this.doneRedirect();
          });
        }, (error) => {
          this.throwError(error);
        });
    }
  }
  
  throwError(error, no_redirect) {
    let data = {
      title: 'Reset password',
      error: (error && error.data && error.data.message) ? error.data.message : 'Unknown reset password error.'
    };
    if (!no_redirect) {
      data.error += '<br> You will be redirected after closing the window';
  
      let modal = this.modal.alert(data);
  
      modal.closed.then(() => {
        this.doneRedirect();
      });
    }
  }
  
  doneRedirect() {
    this.$state.go('main.dashboard');
  }
}
ResetPasswordController.$inject = dependencies;