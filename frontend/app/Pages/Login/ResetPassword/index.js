'use strict';
/**
 * Created by dzmitry.barkouski on 10.01.2017.
 */

import angular from 'angular';
import config from './config';
import controller from './controller';
import './styles.scss';

export default angular.module('resetRecoveryModule', [])
  .controller('resetRecoveryModule', controller)
  .config(config)
  .name;