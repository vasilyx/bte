'use strict';
/**
 * Created by dzmitry.barkouski on 10.01.2017.
 */
config.$inject = [
  '$stateProvider',
];

export default function config($stateProvider) {
  $stateProvider.state({
    name: 'login.passreset',
    url: '/password_reset?username&token',
    template: require('./template.html'),
    controller: 'resetRecoveryModule',
    controllerAs: 'vm',
    unauthorized: true,
    resolve: {}
  });
}
