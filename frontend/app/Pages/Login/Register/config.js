'use strict';
/**
 * Created by dzmitry.barkouski on 10.01.2017.
 */
config.$inject = [
  '$stateProvider',
];

export default function config($stateProvider) {
  $stateProvider.state({
    name: 'login.register',
    url: '/register',
    template: require('./template.html'),
    controller: 'loginRegisterController',
    controllerAs: 'vm',
    unauthorized: true,
    resolve: {}
  });
}
