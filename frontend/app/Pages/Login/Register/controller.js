'use strict';
/*
 * controller.js
 */
import angular from 'angular';
import _keys from 'lodash/keys';
let dependencies = [
  'authService',
  '$state',
  '$scope',
  'modalService'
];
export default class LoginRegisterController {
  constructor(authService, $state, $scope, modalService) {
    let vm = this;
    vm.regModel = {};
    vm.authService = authService;
    vm.$state = $state;
    vm._$scope = $scope;
    vm.modal = modalService;
  }
  
  register(credential) {
    let form = this._$scope.registerForm;
    if ((form.$error && _keys(form.$error).length) || !(credential && credential.username && credential.password)) {
      form.$setSubmitted();
      this.throwError('Registration failed', 'Form validation failed.');
    } else {
      this.authService.signIn(credential).then(
        (user_data, ...etc)=>{
          // console.info('user created - ', user_data);
          // console.log('etc - ', etc); //TODO: Delete this before checkIN
          const data = {
            title: 'Register is done.',
            error: 'Verification email is sent to your email.'
          };
          let modal = this.modal.info(data);
          modal.closed.then(()=>{
            this.doneRedirect();
          });
        },
        (error)=>{
          this.throwError('Registration failed', (error && error.data && error.data.message) ? error.data.message : 'Unknown register error.');
        });
    }
  }
  
  throwError(title, error) {
    const data = {
      title: title,
      error: error
    };
    this.modal.alert(data);
  }
  
  doneRedirect() {
    if (this.$state.params && this.$state.params.returnUrl) {
      this.$location.url(this.$state.params.returnUrl);
    } else {
      this.$state.go('main.dashboard');
    }
  }
  
}
LoginRegisterController.$inject = dependencies;