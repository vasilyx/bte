'use strict';

import angular from 'angular';
import config from './config';
import controller from './controller';

import resetPassPage from './ResetPassword';
import recoveryPage from './Recovery';
import registerPage from './Register';
import confirm_emailPage from './Confirm_email';

import './styles.scss';

let dependencies = [
  resetPassPage,
  recoveryPage,
  registerPage,
  confirm_emailPage
];

export default angular.module('loginPageModule', dependencies)
  .controller('loginPageController', controller)
  .config(config)
  .name;