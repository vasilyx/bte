'use strict';
/**
 * Created by dzmitry.barkouski on 10.01.2017.
 */
config.$inject = [
  '$stateProvider',
];

export default function config($stateProvider) {
  $stateProvider.state({
    name: 'login.confirm_email',
    url: '/confirm_email?username&token',
    template: require('./template.html'),
    controller: 'confirmEmailController',
    controllerAs: 'vm',
    unauthorized: true,
    resolve: {}
  });
}
