'use strict';
/**
 * Created by dzmitry.barkouski on 10.01.2017.
 */

import angular from 'angular';
import _merge from 'lodash/merge';
import _keys from "lodash/keys";

let dependencies = [
  'authService',
  '$state',
  '$stateParams',
  '$scope',
  'modalService'
];
export default class ConfirmEmailController {
  constructor(authService, $state, $stateParams, $scope, modalService) {
    
    this.recModel = {};
    this.authService = authService;
    this.$state = $state;
    this._$scope = $scope;
    this.modal = modalService;
    this.$stateParams = $stateParams;
    
    // authService.checkMailVerification($stateParams).then(()=>{this.confirmed = true;}, (error)=>{this.throwError(error)});
    this.twoFactorAuthShow($stateParams);
  }
  
  twoFactorAuthShow(stateParams) {
    this.authService.getGoogleQr(stateParams).then(
      (success) => {
        this.two_factor_auth_qr_code = success.data.two_factor_auth_qr_code;
        this.two_factor_auth_key = success.data.two_factor_auth_key;
      },
      (error) => {
        this.throwError(error, true);
      }
    );
  }
  
  confirmEmail(code) {
    if (!code) {
      return;
    }
    
    this.authService
      .checkMailVerification(_merge({}, this.$stateParams, {code: code}))
      .then(
        () => {
          this.throwError({data: {message: 'Your email is confirmed.'}}, true);
        },
        (error) => {
          this.throwError(error)
        }
      );
  }
  
  throwError(error, redirect) {
    let data = {
      title: 'Confirm email',
      error: (error && error.data && error.data.message) ? error.data.message : 'Unknown confirm email error.'
    };
    
    if (redirect) {
      data.error += '<br> You will be redirected after closing the window';
    }
      let modal = this.modal.alert(data);
    if (redirect) {
      modal.closed.then(() => {
        this.doneRedirect();
      });
    }
  }
  
  doneRedirect() {
    this.$state.go('main.dashboard');
  }
}
ConfirmEmailController.$inject = dependencies;