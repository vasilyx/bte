'use strict';
/*
 * config.js
*/

config.$inject = [
  '$stateProvider',
];

export default function config($stateProvider) {
  $stateProvider
    .state({
      name: 'login',
      template: require('./layout_template.html')
    })
    .state({
      name: 'login.page',
      url: '/login?returnUrl',
      template: require('./template.html'),
      controller: 'loginPageController',
      controllerAs: 'vm',
      unauthorized: true,
      params: {
        returnUrl: null
      },
      resolve: {
        redirectResolver: ['$q', '$state','$location', 'authService',
          function ($q, $state, $location, authService) {
            let redirectPromise = $q.defer();
            authService.requireSignIn(true).then(()=>{
              if ($state.params && $state.params.returnUrl) {
                $location.url($state.params.returnUrl);
                redirectPromise.reject();
              } else {
                $state.go('main.dashboard');
              }
            }, ()=>{
              redirectPromise.resolve();
            });
            return redirectPromise.promise;
          }]
      }
    });
}