'use strict';
/*
 * controller.js
*/
import _keys from 'lodash/keys';
import _merge from "lodash/merge";

let dependencies = [
  'authService',
  '$state',
  '$location',
  '$scope',
  '$uibModal',
  '$sce',
  'modalService'
];
export default class LoginPageController {
  constructor(authService, $state, $location, $scope, $uibModal, $sce, modalService) {
    let vm = this;
    this.$sce = $sce;
    vm.loginModel = {};
    vm.regModel = {};
    vm.authService = authService;
    vm.$state = $state;
    vm.$location = $location;
    vm._$scope = $scope;
    vm.modal = modalService;
    vm.$uibModal = $uibModal;
  }
  
  login(credentials) {
    this._$scope.loginForm.$setSubmitted();
    if (this._$scope.loginForm.$error && _keys(this._$scope.loginForm.$error).length) {
      this.throwError('Login failed', 'Form validation failed.');
      return;
    }
    this.authService.login(credentials).then(
      () => {
        this.doneRedirect();
      },
      (error, ...etc) => {
        if (error.status !== 401) {
          this.throwError('Login failed', (error && error.data && error.data.message) ? error.data.message : 'Unknown login error.');
        } else {
          this.showAuthCodePopup((error && error.data && error.data.message) ? error.data.message : null);
        }
      });
  }
  
  showAuthCodePopup(message) {
    if (message) {
      message = this.$sce.trustAsHtml(message);
    }
    if (this.authCodeModal && this.authCodeModal.close) {
      this.authCodeModal.close();
      delete this.authCodeModal;
    }
    let parentController = this,
      modal = this.authCodeModal = this
        .$uibModal
        .open({
          ariaLabelledBy: 'modal-title',
          ariaDescribedBy: 'modal-body',
          template: require('./autCode_template.html'),
          controllerAs: 'vm',
          controller: function () {
            this.modal = modal;
            this.message = message;
            this.loginModel = parentController.loginModel;
            this.login = function (credentials) {
              parentController.login(credentials);
              modal.close();
            };
            this.close = () => {
              delete this.loginModel.code;
              modal.close();
            };
          }
        });
  }
  
  throwError(title, error) {
    const data = {
      title: title,
      error: error
    };
    
    this.modal.alert(data);
    // console.warn('some error - ', error); //TODO: Delete this before checkIN
  }
  
  doneRedirect() {
    if (this.$state.params && this.$state.params.returnUrl) {
      this.$location.url(this.$state.params.returnUrl);
    } else {
      this.$state.go('main.dashboard');
    }
  }
}
LoginPageController.$inject = dependencies;