'use strict';
/**
 * Created by dzmitry.barkouski on 10.01.2017.
 */

import angular from 'angular';
import _keys from 'lodash/keys';
let dependencies = [
  'authService',
  '$state',
  // '$location',
  '$scope',
  'modalService'
];
export default class ResetPasswordController{
  constructor(authService, $state, $scope, modalService) {
    let vm = this;
    vm.recModel = {};
    vm.authService = authService;
    vm.$state = $state;
    vm._$scope = $scope;
    vm.modal = modalService;
  }
  
  recoveryPassword(mail) {
    let form = this._$scope.recoveryPassForm;
    
    if ((form.$error && _keys(form.$error).length)  || !(mail && mail.length)) {
      form.$setSubmitted();
      this.throwError('Recovery password', 'Form validation failed.');
    } else {
      this.authService.recoveryPass(mail).then(
        ()=>{
          let modal = this.modal.info({
            title: 'Password reset',
            error: 'Email with instruction is sent to your email.'
          });
          modal.closed.then(()=>{
            this.doneRedirect();
          });
        },
        (error)=>{
          this.throwError('Recovery password', (error && error.data && error.data.message) ? error.data.message : 'Unknown recovery error.');
        });
    }
  }
  
  throwError(title, message) {
    const data = {
      title: title,
      error: message
    };
    this.modal.alert(data);
  }
  
  doneRedirect() {
    if (this.$state.params && this.$state.params.returnUrl) {
      this.$location.url(this.$state.params.returnUrl);
    } else {
      this.$state.go('main.dashboard');
    }
  }
}
ResetPasswordController.$inject = dependencies;