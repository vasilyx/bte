'use strict';
config.$inject = [
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',
  '$httpProvider',
  '$localStorageProvider',
  '$qProvider',
  '$provide'
];

export default function config($stateProvider,
  $urlRouterProvider,
  $locationProvider,
  $httpProvider,
  $localStorageProvider,
  $qProvider,
  $provide) {
  
  $qProvider.errorOnUnhandledRejections(false);
  
  $locationProvider.html5Mode({
    enabled: true,
    requireBase: false
  });
  
  $urlRouterProvider.otherwise('/login');
  
  $httpProvider.interceptors.push([
    '$q',
    '$localStorage',
    '$window',
    '$rootScope',
    ($q, $localStorage, $window, $rootScope) => {
      return {
        request: function (request) {
          //add token in headers
          const ignoreList = [
            '/auth/login',
            '/auth/reset_password',
            '/auth/forgotten',
            '/auth/registration'
          ];
          
          let noIgnore = true;
          
          ignoreList.forEach((item) => {
            let regexp = new RegExp(item, "ig");
            if (request && angular.isString(request.url) && request.url.search(regexp) !== -1 || request.url.search(/^uib/ig) !== -1) {
              noIgnore = false;
            }
          });
          if (noIgnore && request.headers && $localStorage && $localStorage.ltk) {
            request.headers.token = $localStorage.ltk;
  
            if (request.method === 'POST' || request.method === 'PUT') {
              $rootScope.$emit('root_pause_session');
            }
          }
          
          //ignore templates requests
          if (request.url.indexOf('templates') === -1 && request.url.indexOf('.html') === -1 && request.url.search(/^uib/ig) === -1) {
            const basePath = $window.basePath || "/api/app.php";
            request.url = basePath + request.url;
          }
          return request || $q.when(request);
        },
        response: function (response) {
          if (response.config.method === 'POST' || response.config.method === 'PUT') {
            $rootScope.$emit('root_continue_session');
          }
          return response || $q.when(response);
        }
      }
    }
  ]);
  $localStorageProvider.setKeyPrefix('_bteStore');
  
  $provide.decorator('$state', ['$delegate', '$rootScope', function($delegate, $rootScope) {
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams) {
      $delegate._nextState = {
        state: toState,
        params: toParams
      };
    });
    return $delegate;
  }]);
}