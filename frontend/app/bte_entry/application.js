'use strict';
/**
 * Created by dzmitry.barkouski
 **/
//import custom base styles
import '../Layout/layout.scss';
//import and create local var angular
import angular from 'angular';

//import angular modules
import uiRouter from 'angular-ui-router';
import ngMessages from 'angular-messages';
import ngStorage from 'ngstorage'; // TODO was ngStorage - be careful

//import bootstrap ui for angular
import 'angular-animate';

// import 'bootstrap-css-only';

import uiBootstrap from 'angular-ui-bootstrap';

//import application parts
import config from './application_config';
import customUI from '../Modules/CustomUIDirectives_and_Services';

//application dependencies
import loginPageModule from './../Pages/Login';
import mainPageModule from '../Pages/Main';
import authServiceModule from './../Modules/AuthService';

//utils
import _isObject from 'lodash/isObject';

//start application
(function () {
  
  let dependencies = [
    uiRouter,
    ngMessages,
    ngStorage.name,
    uiBootstrap,
    loginPageModule,
    mainPageModule,
    authServiceModule,
    customUI
  ];
  
  angular.module('bteApp', dependencies);
  
  angular.module('bteApp')
    .config(config)
    .run([
      '$q',
      '$rootScope',
      '$location',
      '$state',
      'authService',
      '$window',
      function ($q, $rootScope, $location, $state, authService, $window) {
        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
          toState.resolve = toState.resolve || {};
          if (!toState.unauthorized) {
            toState.resolve.authServiceResolver = toState.resolve.authServiceResolver || [ 'authService', function (authService) {
              return authService.requireSignIn();
            }];
          } else if (toParams && toParams.returnUrl && toParams.returnUrl.length && fromState.name && fromState.name.length) {
            toState.resolve.redirectResolver = toState.resolve.redirectResolver || function () {
              let redirectPromise = $q.defer();
              authService.requireSignIn.then(()=>{
                redirectPromise.reject();
                $location.url( toParams.returnUrl);
              }, ()=>{
                redirectPromise.resolve();
              });
              return redirectPromise.promise;
            };
          }
        });
        
        $rootScope.$on('$stateChangeError', function (evt, toState, toParams, fromState, fromParams, error) {
          // We can catch the error thrown when the $requireSignIn promise is rejected
          // and redirect the user back to the home page
          if (error === 'AUTH_REQUIRED') {
            $state.go('login.page', {
              returnUrl: (fromParams && fromParams.returnUrl)? fromParams.returnUrl : (!fromState.unauthorized)? $location.url() : null
            });
          } else if (_isObject(error)) {
            if (error.redirectState) {
              $state.go(error.redirectState, error.params, {reload: error.reload});
            }
          }
        });
  
  
        (function (_window) {
          try {
            new MouseEvent('test');
            return false; // No need to polyfill
          } catch (e) {
            // Need to polyfill - fall through
          }
    
          // Polyfills DOM4 MouseEvent
    
          let MouseEvent = function (eventType, params) {
            params = params || { bubbles: false, cancelable: false };
            let mouseEvent = document.createEvent('MouseEvent');
            mouseEvent.initMouseEvent(eventType, params.bubbles, params.cancelable, _window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
      
            return mouseEvent;
          };
    
          MouseEvent.prototype = Event.prototype;
    
          _window.MouseEvent = MouseEvent;
        })($window);
      }
    ]);
})();
