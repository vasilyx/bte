'use strict';

export default class UserInfoService {
  constructor($q, $http, $localStorage, $stateParams, modalService) {
    this.$q = $q;
    this.$http = $http;
    this.ls = $localStorage;
    this.$stateParams = $stateParams;
    this.modal = modalService;
  }
  
  clearUserData(){
    delete this.user;
  }
  
  getUserData(){
    let def = this.$q.defer();
    if (this.user) {
      def.resolve(this.user);
    } else {
      this.$http.get('/user').then((success) => {
        if (success && success.data) {
          this.user = success.data;
        }
        def.resolve(this.user);
      }, (error) => {
        def.reject(error);
      });
    }
    
    return def.promise;
  }
}

UserInfoService.$inject = [
  '$q',
  '$http',
  '$localStorage',
  '$stateParams',
  'modalService'
];
