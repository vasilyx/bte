'use strict';
/**
 * Created by dzmitry.barkouski on 11.01.2017.
 */


import angular from 'angular';

import service from './service';

let dependencies = [
];

export default angular.module('UserInfoModule', dependencies)
  .service('userInfoService', service)
  .name;