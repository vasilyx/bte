'use strict';
import _merge from 'lodash/merge';

export default class AuthService {
  constructor($q, $http, $localStorage, $rootScope, $stateParams, $state, modalService) {
    this.auth = '';
    this.$q = $q;
    this.$http = $http;
    this.ls = $localStorage;
    this.$rootScope = $rootScope;
    this.$stateParams = $stateParams;
    this.$state = $state;
    this.modal = modalService;
    this.sessionDuration = 15.2 * 60 * 1000;
  
    this.onPause_session = $rootScope.$on('root_pause_session', () => {
      this.pause_session();
    });
  
  }
  
  login(credential) {
    let request = this.$http({
      method: 'POST',
      url: '/auth/login',
      data: credential
    });
    request.then((successData) => {
      if (angular.isObject(successData) && successData.data && successData.data.token) {
        this.ls.ltk = successData.data.token;//login token key
      }
      this.startHandlingSessionPause();
    });
    
    return request;
  }
  
  logout() {
    let req = this.$http({
      method: 'POST',
      url: '/auth/logout'
    });
    req.then(() => {
      if (this.ls && this.ls.ltk) {
        delete this.ls.ltk;
      }
    });
    
    return req;
  }
  
  signIn(credential) {
    return this.$http.post('/user/registration', credential);
  }
  
  requireSignIn(ignoreSession) {
    // console.warn('CHECK TOKEN, duration - ', this.sessionDuration / 1000 / 60, 'min'); //TODO: Delete this before checkIN
    let def = this.$q.defer(),
      prom = def.promise;
    
    this.$http(
      {
        method: 'GET',
        url: '/auth/check'
      }
    )
      .then(() => {
        if (!ignoreSession) {
          this.restartSession();
        } else if (this.sessionTimer) {
          clearTimeout(this.sessionTimer);
          delete this.sessionTimer;
        }
        def.resolve();
      }, (errorResponse) => {
        if (this.sessionTimer) {
          clearTimeout(this.sessionTimer);
          delete this.sessionTimer;
        }
        def.reject('AUTH_REQUIRED');
      });
    return prom;
  }
  
  recoveryPass(mail) {
    return this.$http({
      method: 'POST',
      url: '/auth/forgotten',
      data: {
        username: mail
      }
    });
  }
  
  getGoogleQr(stateParams) {
    return this.$http({
      method: 'POST',
      url: '/auth/twofactorauth',
      data: stateParams
    });
  }
  
  setNewPass(data) {
    data = _merge(data, {
      token: this.$stateParams.token,
      username: this.$stateParams.username
    });
    return this.$http
      .post('/auth/reset_password', data);
  }
  
  checkMailVerification(data) {
    return this.$http
      .post('/auth/email_confirmation', data);
  }
  
  startHandlingSessionPause() {
    // this.onPause_session = $rootScope.$on('root_pause_session', this.pause_session);
  }
  
  restartSession(duration) {
    duration = duration || this.sessionDuration;
    if (this.sessionTimer) {
      clearTimeout(this.sessionTimer);
      delete this.sessionTimer;
    }
    
    let restartSession = () => {
      this
        .requireSignIn()
        .then(
          () => {},
          () => {
            let params = {
              returnUrl: (this.$state.params && this.$state.params.returnUrl) ? this.$state.params.returnUrl : this.$state.current.url
            };
            this.logout().then(() => {
              this.$state.go('login.page', params);
            });
          }
        );
    };
    this.sessionEnd = new Date((new Date()).getTime() + duration);
    this.sessionTimer = setTimeout(() => {
      restartSession();
    }, duration);
  }
  
  pause_session(){
    if (this.sessionTimer) {
      clearTimeout(this.sessionTimer);
      delete this.sessionTimer;
      
      this.onContinue_session = this.$rootScope.$on('root_continue_session', () => {
        this.continue_session();
      });
  
      console.warn('pause session'); //TODO: Delete this before checkIN
      this.sessionRestTime = this.sessionEnd.getTime() - (new Date()).getTime();
    }
  }
  
  continue_session(){
    console.warn('continue session, rest time is - ', this.sessionRestTime / 1000 / 60, 'min'); //TODO: Delete this before checkIN
    this.restartSession(this.sessionRestTime || null);
    if (this.onContinue_session()) {
      this.onContinue_session();
    }
  }
  
  $onDestroy(){
    if (this.onPause_session) {
      this.onPause_session();
    }
    if (this.onContinue_session()) {
      this.onContinue_session();
    }
  }
}

AuthService.$inject = [
  '$q',
  '$http',
  '$localStorage',
  '$rootScope',
  '$stateParams',
  '$state',
  'modalService'
];
