"use strict";

import moment from 'moment';

export default function CheckDateDiffFromToday() {
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function (scope, element, attributes, ngModel) {
      function validate(value) {
        let selectedDate = moment(value),
        currentDate = moment(moment().year(moment().year() - 1).format('MM/DD/YY'));
        ngModel.$setValidity('wrongDate', selectedDate.diff(currentDate) <= 0);
        
      }
  
      scope.$watch(function () {
        return ngModel.$viewValue;
      }, validate);
    }
  }
}

CheckDateDiffFromToday.$inject = [

];
