"use strict";

export default function CheckDateFormat() {
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function (scope, element, attributes, ngModel) {
      console.log('checkDateFormat', scope, element, attributes, ngModel);
      const re = /^([01][0-9]\/[0-3][0-9]\/\d{4})$/g;
      let preValidateTimer;
  
      function preValidate(value) {
        ngModel.$setValidity('badDate', true);
        if (preValidateTimer) {
          clearTimeout(preValidateTimer);
          preValidateTimer = null;
        }
        preValidateTimer = setTimeout(()=>{validate(value)},400);
      }
      
      function validate(value) {
        if (ngModel.$isEmpty(value)) {
          // consider empty models to be valid
          ngModel.$setValidity('badDate', true);
          return;
        }
        if (value && value.search(re) !== -1) {
          // it is valid
          ngModel.$setValidity('badDate', true);
          return;
        }
  
        // it is invalid
        ngModel.$setValidity('badDate', false);
        /*
        if (value !== undefined && value != null) {
          if (value instanceof Date) {
            let d = Date.parse(value);
            // it is a date
            if (isNaN(d)) {
            
            }
          } else {
            if (value != '' && !re.test(value)) {
              controller.$setValidity('badDate', false);
            }
          }
        } else {
        
        }*/
      }
  
      scope.$watch(function () {
        return ngModel.$viewValue;
      }, validate);
    }
  }
}

CheckDateFormat.$inject = [

];
