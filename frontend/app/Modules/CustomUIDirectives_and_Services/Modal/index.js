'use strict';

import angular from 'angular';
import modal_service from './service';

export default angular.module('customUIDirectives.modal', [])
  .service('modalService', modal_service)
  .name;
