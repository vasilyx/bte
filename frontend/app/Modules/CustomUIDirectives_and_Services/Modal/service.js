'use strict';

import $ from 'jquery';
import _merge from 'lodash/merge'

export default class ModalService { //modalService
  constructor($q, $uibModal, $sce) {
    this.$q = $q;
    this.$uibModal = $uibModal;
    this.$sce = $sce;
    
    this.smDefaults = {
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      template: require('./default_template.html'),
      controllerAs: 'vm',
      size: 'sm'
    };
  }
  
  alert(data) {
    
    if (data.error) {
      data.error = this.$sce.trustAsHtml(data.error);
    }
    let modal = this
      .$uibModal
      .open($.extend(true, {}, this.smDefaults, {
        controller: function () {
          this.data = data;
          this.modal = modal;
          this.alert = true;
        }
      }));
    return modal;
  }
  
  info(data) {
    
    if (data.error) {
      data.error = this.$sce.trustAsHtml(data.error);
    }
    let modal = this
      .$uibModal
      .open($.extend(true, {}, this.smDefaults, {
        controller: function () {
          this.data = data;
          this.modal = modal;
          this.info = true;
        }
      }));
    return modal;
  }
  
  ask_2btns(data) {
    let def = this.$q.defer(),
      prom = def.promise;
    data = _merge({
      ok_btn_title: 'Ok',
      cancel_btn_title: 'Cancel'
    }, data);
    
    if (data.content_text) {
      data.content_text = this.$sce.trustAsHtml(data.content_text);
    }
    let modal = this
      .$uibModal
      .open(_merge({}, this.smDefaults, {
        template: require('./ask_2btns_template.html')
      }, {
        controller: function () {
          this.data = data;
          this.close = (reject) => {
            if (!reject) {
              def.resolve();
            } else {
              def.reject();
            }
            modal.close();
          };
        }
      }));
    return prom;
  }
}

ModalService.$inject = [
  '$q',
  '$uibModal',
  '$sce'
];