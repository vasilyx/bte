"use strict";
/**
 * Created by dzmitry.barkouski on 27.04.2017.
 **/
import _isArray from 'lodash/isArray';
import _cloneDeep from 'lodash/cloneDeep';
import _isObject from 'lodash/isObject';
import _isString from 'lodash/isString';

export default class SubmissionViewerService {
  
  constructor($q, $http, $sce, $uibModal, modalService) {
    this.$q = $q;
    this.$http = $http;
    this.$sce = $sce;
    this.$uibModal = $uibModal;
    this.pageNumber = 1;
    this.table_row_template = require('./table_row_template.hbs');
    this.pageSizes = [
      {
        val: 25,
        title: '25'
      },
      {
        val: 50,
        title: '50'
      },
      {
        val: 100,
        title: '100'
      },
    ];
    this.pageSize = this.pageSizes[0].val;
    this.modal = modalService;
  }
  
  setSubmissionData(data, modalTitle, id, btn_print) {
    if (_isObject(data)) {
      this.submission_data = data;
      this.submission_data = this.preworkData(this.submission_data);
      this.shopPopup();
    } else if (_isString(data)) {
      this.getSubmissionData(data).then((submission_data) => {
        this.submission_data = submission_data;
        this.submission_data = this.preworkData(this.submission_data);
        this.shopPopup();
      }, () => {
        if (this.modal && this.modal.close) {
          this.modal.close();
        }
        delete this.submission_data
      });
    }
    this.modalTitle = modalTitle;
    this.submissionId = id;
    this.btnPrint = btn_print;
  }
  
  getSubmissionData(id) {
    let def = this.$q.defer(),
      prom = def.promise;
    if (this.abort) {
      this.abort.resolve();
    }
    if (!id) def.eject();
    this.abort = this.$q.defer();
    this.$http
      .get('/submission/data', {
        params: {
          id: id
        },
        timeout: this.abort
      })
      .then((success) => {
        let data = success.data;
        if (_isArray(data)) {
          if (data.length) {
            def.resolve(data);
          } else {
            def.reject();
          }
        }
      }, (error, ...etc)=>{
        this.throwError('Load submission data failed', (error && error.data && error.data.message) ? error.data.message : 'Unknown submission data error.');
      });
    
    return prom;
  }
  
  preworkData(data){
    if (_isArray(data)) {
      let temptData = [...data];
      for (let i = 0, l = temptData.length; i < l; i++) {
        if (temptData[i]) {
          for (let j = 0; j < temptData[i].length; j++) {
            if (_isArray(temptData[i][j])) {
              let element = temptData[i][j],
                tempEl = {
                  value: element[0]
                };
              if (element[1] == 1) {
                tempEl.warning = true;
              } else if (element[1] == 2) {
                tempEl.error = true;
              }
              temptData[i][j] = tempEl;
            } else {
              temptData[i][j] = {value: temptData[i][j]};
            }
          }
          temptData[i] = this.$sce.trustAsHtml(this.table_row_template({cells: temptData[i]}));
        }
      }
      return temptData.filter(temptData => temptData);
    }
    return data;
  }
  reworkData(data, pageSize) {
    console.time('reworkData');
    if (_isArray(data)) {
      let clone_data = _cloneDeep(data),
        head = clone_data[0];
      clone_data.splice(0, 1);
      let totalCount = Math.ceil(clone_data.length / pageSize),
        length = clone_data.length;
      if (clone_data.length > pageSize) {
        let tempData = [];
        
        for (let i = 1, l = totalCount; i <= l; i++) {
          let finish = pageSize * i - 1;
          if ((finish > length)) {
            finish = length;
          }
          tempData.push(clone_data.slice(pageSize * (i - 1), finish));
        }
        clone_data = tempData;
      } else {
        clone_data = [clone_data];
      }
      console.timeEnd('reworkData');
      return {
        totalCount: length,
        submission_data: {
          head: head,
          body: clone_data
        }
      };
    }
    return {};
  }
  
  shopPopup() {
    let self = this;
    this.modal = this
      .$uibModal
      .open({
        ariaLabelledBy: 'modal-title',
        ariaDescribedBy: 'modal-body',
        size: 'lg',
        windowClass: 'modal-submissionData',
        ok_btn_title: 'Close',
        controller: function () {

          self.modalController = this;
          this.modal_title = self.modalTitle;
          this.submission_id = self.submissionId;
          this.btn_print = self.btnPrint; // TODO rewrite it
          this.modal = self.modal;
          
          this.setModalControllerData = () => {
            self.setModalControllerData(this);
          };
          
          this.closePopup = () => {
            delete self.modalController;
            this.modal.close();
          };
          
          this.changePage = () => {
            this.submission_data_onepage = this.submission_data.body[this.pageNumber - 1];
          };

          // TODO rebuild
         this.downloadFile = (url, title) => {

            function download(data, strFileName, strMimeType) {
              var self = window,
                  defaultMime = 'application/octet-stream',
                  mimeType = strMimeType || defaultMime,
                  payload = data,
                  url = !strFileName && !strMimeType && payload,
                  anchor = document.createElement('a'),
                  toString = function(a){
                    return String(a);
                  },
                  MyBlob = (self.Blob || self.MozBlob || self.WebKitBlob || toString),
                  fileName = strFileName || 'download',
                  blob,
                  reader;

              MyBlob = MyBlob.call ? MyBlob.bind(self) : Blob ;

              if(url && url.length < 2048){
                fileName = url.split('/').pop().split('?')[0];
                anchor.href = url;
                if(anchor.href.indexOf(url) !== -1) {
                  var ajax = new XMLHttpRequest();
                  ajax.open('GET', url, true);
                  ajax.responseType = 'blob';
                  ajax.onload = function(e){
                    download(e.target.response, fileName, defaultMime);
                  };
                  setTimeout(function(){
                    ajax.send();
                  }, 0);

                  return ajax;
                }
              }

              if(/^data\:[\w+\-]+\/[\w+\-]+[,;]/.test(payload)){

                if(payload.length > (1024 * 1024 * 1.999) && MyBlob !== toString ){
                  payload = dataUrlToBlob(payload);
                  mimeType = payload.type || defaultMime;
                }else{
                  return navigator.msSaveBlob ?
                      navigator.msSaveBlob(dataUrlToBlob(payload), fileName) :
                      saver(payload);
                }

              }

              blob = payload instanceof MyBlob ?
                  payload :
                  new MyBlob([payload], {type: mimeType}) ;


              function dataUrlToBlob(strUrl) {
                var parts= strUrl.split(/[:;,]/);
                var type= parts[1];
                var decoder = (parts[2] === 'base64' ? atob : decodeURIComponent);
                var binData= decoder( parts.pop() );
                var mx= binData.length;
                var uiArr= new Uint8Array(mx);

                for(var i = 0 ; i < mx; ++i) {
                  uiArr[i]= binData.charCodeAt(i);
                }

                return new MyBlob([uiArr], {type: type});
              }

              function saver(url, winMode){

                if ('download' in anchor) {
                  anchor.href = url;
                  anchor.setAttribute('download', fileName);
                  anchor.className = 'download-js-link';
                  anchor.innerHTML = 'downloading...';
                  anchor.style.display = 'none';
                  document.body.appendChild(anchor);
                  setTimeout(function() {
                    anchor.click();
                    document.body.removeChild(anchor);
                    if(winMode === true){
                      setTimeout(function(){
                        self.URL.revokeObjectURL(anchor.href);
                      }, 250 );
                    }
                  }, 66);
                  return true;
                }

                if(/(Version)\/(\d+)\.(\d+)(?:\.(\d+))?.*Safari\//.test(navigator.userAgent)) {
                  url=url.replace(/^data:([\w\/\-\+]+)/, defaultMime);
                  if(!window.open(url)){
                    if(confirm('Displaying New Document\n\nUse Save As... to download, then click back to return to this page.')){
                      location.href = url;
                    }
                  }

                  return true;
                }

                var f = document.createElement('iframe');
                document.body.appendChild(f);

                if(!winMode){
                  url = 'data:' + url.replace(/^data:([\w\/\-\+]+)/, defaultMime);
                }
                f.src=url;
                setTimeout(function(){
                  document.body.removeChild(f);
                }, 333);

              }

              if (navigator.msSaveBlob) {
                return navigator.msSaveBlob(blob, fileName);
              }

              if(self.URL){
                saver(self.URL.createObjectURL(blob), true);
              }else{
                if(typeof blob === 'string' || blob.constructor===toString ){
                  try{
                    return saver( 'data:' +  mimeType   + ';base64,'  +  self.btoa(blob)  );
                  }catch(y){
                    return saver( 'data:' +  mimeType   + ',' + encodeURIComponent(blob)  );
                  }
                }

                reader = new FileReader();
                reader.onload = function(e){
                  saver(this.result);
                };

                reader.readAsDataURL(blob);
              }

              return true;
            }

            var request = new XMLHttpRequest();
            request.open('get', url, true);
            request.responseType = 'blob';
            request.onload = function(e){
/*              var description = request.getResponseHeader('Content-Disposition');
              var matches = description.match(/filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/);
              if (matches != null && matches[1] && !title) {
                title = matches[1].replace(/['"]/g, '');
              }*/
              download(request.response, title);
            };
            request.send();
          }
        },
        controllerAs: 'vm',
        template: require('./modal_template.html')
      });
    
    this.modal.rendered.then(() => {
      this.setModalControllerData(this.modalController);
    });
  }
  
  setModalControllerData(modalController) {
    
    modalController.pageSize = modalController.pageSize || this.pageSize;
    
    let tempData = this.reworkData(this.submission_data, modalController.pageSize);
    let allExist = false;
    modalController.submission_data = tempData.submission_data;
    modalController.totalCount = tempData.totalCount || 0;
    
    modalController.pageSizes = [];
    
    this.pageSizes.forEach((el) => {
      let element = {
        val: el.val,
        title: el.title
      };
      if (element.val === modalController.totalCount) {
        allExist = true;
        element.title = 'All(' + element.val + ')';
      }
      if (element.val <= modalController.totalCount) {
        modalController.pageSizes.push(element);
      }
    });
    
    modalController.pageNumber = this.pageNumber;
    
    modalController.submission_data_onepage = modalController.submission_data.body[modalController.pageNumber - 1];
    
    
    if (!allExist) {
      modalController.pageSizes[modalController.pageSizes.length] = {
        val: modalController.totalCount,
        title: 'All(' + modalController.totalCount + ')'
      };
    }
  }
  throwError(title, error) {
    const data = {
      title: title,
      error: error
    };
    
    this.modal.alert(data);
    // console.warn('some error - ', error); //TODO: Delete this before checkIN
  }
};

SubmissionViewerService.$inject = [
  '$q',
  '$http',
  '$sce',
  '$uibModal',
  'modalService'
];