"use strict";
/**
 * Created by dzmitry.barkouski on 30.06.2017.
 **/
import controller from './controller'
export default function SubmissionViewerDirective() {
  return {
    restrict: 'EA',
    template: require('./btn_template.html'),
    controller: controller,
    controllerAs: 'vm',
    transclude: false,
    scope: {
      title: '@btnTitle',
      modal_title: '@modalTitle',
      submission_id: '@submissionId',
      btn_print: '@btnPrint',
      submission_file_data: '&submissionFileData'
    },
    link: function (scope, element, attributes, controller, transcludeFn) {
      // console.log(scope.submission_file_data()); //TODO: Delete this before checkIN
      // console.info('scope - ', scope); //TODO: Delete this before checkIN
      // console.info('element - ', element); //TODO: Delete this before checkIN
      // console.info('attributes - ', attributes); //TODO: Delete this before checkIN
      // console.info('controller - ', controller); //TODO: Delete this before checkIN
      // console.info('transcludeFn - ', transcludeFn); //TODO: Delete this before checkIN
    }
  }
}