'use strict';
/**
 * Created by dzmitry.barkouski on 26.01.2017.
 */
import angular from 'angular';
import service from './service';
import directive from './directive';
import './styles.scss';

let dependencies = [
];

export default angular.module('SubmissionViewerModule', dependencies)
  .directive('submissionViewer', directive)
  .service('submissionViewerService', service)
  .name;