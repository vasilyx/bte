'use strict';
import _isFunction from 'lodash/isFunction';
let dependencies = [
  '$scope',
  'submissionViewerService'
];
export default class SubmissionViewerController {
  constructor($scope, service) {
    this.scope = $scope;
    this.service = service;
  }
  
  shopPopup(){
    let submission_data = this.scope.submission_id;
    if (!submission_data && this.scope.submission_file_data) {
      if (_isFunction(this.scope.submission_file_data)) {
        submission_data = this.scope.submission_file_data();
      }
    }
    this.service.setSubmissionData(submission_data, this.scope.modal_title, this.scope.submission_id, this.scope.btn_print);
  }
}
SubmissionViewerController.$inject = dependencies;