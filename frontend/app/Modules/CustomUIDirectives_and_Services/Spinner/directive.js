"use strict";
/**
 * Created by dzmitry.barkouski on 30.06.2017.
 **/
export default function SubmissionViewerDirective() {
  return {
    restrict: 'EA',
    template: require('./template.html')
  }
}