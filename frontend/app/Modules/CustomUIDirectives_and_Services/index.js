'use strict';

import angular from 'angular';
import modal from './Modal';
import checkDateFormat from './checkDateFormat';
import checkDateDiffFromToday from './checkDateDiffFromToday';

import spinner from './Spinner/directive';
import './Spinner/styles.scss';

let dependencies = [
  modal
];
export default angular.module('customUIDirectives_and_services', dependencies)
  .directive('checkDateFormat', checkDateFormat)
  .directive('checkDateDiffFromToday', checkDateDiffFromToday)
  .directive('spinner', spinner)
  .name;